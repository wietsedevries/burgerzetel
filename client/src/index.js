import React from 'react';
import ReactDOM from 'react-dom';
import Relay from 'react-relay';
import {createHistory} from 'history';
import {Router, applyRouterMiddleware, useRouterHistory} from 'react-router';
import useRelay from 'react-router-relay';
// import useScroll from 'react-router-scroll';

import routes from './routes';

import './stylesheets/Normalize.css';
import './stylesheets/Grid.css';
import './stylesheets/App.css';
import './stylesheets/Theme.css';
import './stylesheets/Mobile.css';
import 'font-awesome/css/font-awesome.css';

var headers = {};
if (localStorage.authToken)
	headers = {authorization: 'Bearer ' + localStorage.authToken};
var networkLayer = new Relay.DefaultNetworkLayer('/graphql', {headers});

Relay.Store.injectNetworkLayer(networkLayer);

var history = useRouterHistory(createHistory)({queryKey: false});

ReactDOM.render(
	<Router
		routes={routes}
		history={history}
		render={applyRouterMiddleware(useRelay)}
		environment={Relay.Store}
	/>,
	document.getElementById('root')
);
