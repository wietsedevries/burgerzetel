import React, { Component } from 'react';
// import Relay from 'react-relay';
import '../../stylesheets/home.css';

class CityDropdownList extends Component {

  render() {
		// let {viewer} = this.props;
		// let Cities = viewer.Cities;
		// if(!Cities){
		// 	console.log("Geen Cities gevonden");
		// 	return null;
		// }
		// let options = Cities.edges;

		return (
      <div>
				<select>
					<option>Selecteer uw woonplaats</option>
				</select>
				<div className="caret"></div>
      </div>

    );
  }
}

export default CityDropdownList;
// export default Relay.createContainer(CityDropdownList, {
//     fragments: {
//         viewer: () => Relay.QL `
//         query {
//             fragment on viewer {
// 							Cities(first:10){
// 					      edges{
// 					        node{
// 					          name,
// 					        }
// 					      }
// 					    }
//             }
//           }
//         `
//     }
// });
