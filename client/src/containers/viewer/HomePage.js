import React, { Component } from 'react';
import Relay from 'react-relay';
import {debounce} from 'lodash';
import {Link} from 'react-router';
import DocumentMeta from 'react-document-meta';

import Footer from '../../components/sidebar/Footer';
import Trending from '../../components/sidebar/Trending';
import TrendingTags from '../../components/sidebar/TrendingTags';
import Header from '../../components/Header/Header';
import Menu from '../../components/Menu/Menu';
import UnansweredItem from '../../components/Feed/UnansweredItem';
import AnsweredItem from '../../components/Feed/AnsweredItem';
import ProfileProgress from '../../components/User/ProfileProgress';

import '../../stylesheets/home.css';

let newLimitFeed = 12;
let showMoreFeed =  false;

class HomePage extends Component {
	constructor(props) {
			super(props);
			this.setVariables = debounce(this.props.relay.setVariables, 300);
	}
	setLimitFeed = (limit) => {
			newLimitFeed = limit + 12;
			this.setVariables({ limitFeed: Number(newLimitFeed) });
			showMoreFeed =  true;
	};

	render() {

		const meta = {
			title: 'Burgerzetel | Laat je stem horen!',
			description: 'Politiek vraag & antwoord platform. Stel een vraag, krijg antwoord en deel kennis.',
			canonical: 'http://burgerzetel.nl/',
			meta: {
				charset: 'utf-8',
				property: {
					'og:title': "Burgerzetel | Laat je stem horen!",
					'og:description': 'Politiek vraag & antwoord platform. Stel een vraag, krijg antwoord en deel kennis.',
					'og:url': 'http://burgerzetel.nl/',
				}
			}
		};

		const {viewer} = this.props;
		var Questions;
		if(viewer.AuthUser){
			var currentUser = viewer.AuthUser;
			Questions = viewer.AuthUser.Feed;
		}else{
			Questions = viewer.AllQuestions;
		}
		let questions = Questions.edges;
		let loadQuestions = Questions.pageInfo.hasNextPage;
		let showSuggestion = false;
		if(questions.length < 12){
			showSuggestion = true;
		}

		return (
			<div>
				<DocumentMeta {...meta} />
				<Header notifications={viewer} />
				<div className="container">
					<div className="row">
						<Menu page="home" userData={viewer}/>
						{/*Main panel*/}
						<div className="col-xs-12 col-sm-8 no-padding padding-sm center-panel hidden-md hidden-lg">
							{currentUser ? (
								<ProfileProgress viewer={viewer}/>
							):(
								<div></div>
							)}
						</div>
						<div className="col-xs-12 col-sm-8 col-md-6 no-padding padding-sm center-panel">
							<div className="list">
								{Object.keys(questions).map(function(key) {
									let data = questions[key].node;
									if (data.Answers.edges.length >  0) {
										return (
											<AnsweredItem key={key} data={data} viewer={viewer} />
										)
									}else{
										let User = questions[key].node.User;
										return (
											<UnansweredItem key={key} data={data} user={User} viewer={viewer}/>
										)
									}
								})}
								{showSuggestion ? (
									<div className="panel padding no-feed">
										<i className="fa fa-newspaper-o"></i>
										<div className="no-results color-grey">Om uw feed te personaliseren kunt u meer
											<strong> <Link className="dark-blue-link" to="/onderwerpen/overzicht">onderwerpen</Link> </strong>
											volgen</div>
									</div>
								):(
									""
								)}
								{loadQuestions ? (
										<div className="load-more">
											<div onClick={this.setLimitFeed.bind(null,newLimitFeed)} className="btn secondary-btn">Meer laden</div>
										</div>
									) : (
										showMoreFeed ? (
											<div className="load-more">
												<div className="color-grey">Einde van alle vragen</div>
											</div>
										) : (
											<div className="load-more">
											</div>
										)
								)}
							</div>
						</div>
						{/*Side panel*/}
						<div className="col-md-3 hidden-xs hidden-sm">
							{currentUser ? (

								<ProfileProgress viewer={viewer}/>
							):(
								<div></div>
							)}

							<Trending questions={viewer}/>
							<TrendingTags tags={viewer}/>
							<Footer/>
						</div>
					</div>
				</div>
			</div>

		);
	}
}
export default Relay.createContainer(HomePage, {
	initialVariables: {
		limitFeed: 12,
	},
	fragments: {
			viewer: () => Relay.QL `
				fragment on viewer {
					${Trending.getFragment('questions')},
					${Header.getFragment('notifications')},
					${Menu.getFragment('userData')},
					${TrendingTags.getFragment('tags')},
					${ProfileProgress.getFragment('viewer')},
					${AnsweredItem.getFragment('viewer')},
					${UnansweredItem.getFragment('viewer')},
					AuthUser{
						firstName,
						lastName,
						avatar,
						function,
						bio,
						gender,
						Feed(first:$limitFeed){

							pageInfo {
								hasNextPage,

							},
							edges{
								node{
									qid,
									title,
									description,
									downvoted,
									upvoted,
									dateCreated,
									slug,
									Favorites(first:1) {
										edges{
											node{
												dateCreated,
											},
										},
									},
									User{
										firstName,
										lastName,
										userName,
										avatar
									},
									Answers(first:1){
										edges{
											node {
												answer,
												User{
													firstName,
													lastName,
													userName,
													function,
													avatar
												}
											}
										}
									},
								}
							}
						}
					},
					AllQuestions(first:$limitFeed) {
						pageInfo {
							hasNextPage,
						},
						edges{
							node{
								qid,
								title,
								description,
								downvoted,
								upvoted,
								dateCreated,
								slug,
								Favorites(first:1) {
									edges{
										node{
											dateCreated,
										},
									},
								},
								User{
									firstName,
									lastName,
									userName,
									avatar
								},
								Answers(first:1){
									edges{
										node {
											answer,
											User{
												firstName,
												lastName,
												userName,
												function,
												avatar
											}
										}
									}
								},
							}
						}
					},
				}
			`
		}
});
