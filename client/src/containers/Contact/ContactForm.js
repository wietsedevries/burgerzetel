import M from '../../main.js';
import React, { Component } from 'react';
import Relay from 'react-relay';
import $ from 'jquery';
import validator from 'email-validator';

import Session from '../../services/Session';
import Footer from '../../components/sidebar/Footer';
import InfoPages from '../../components/sidebar/InfoPages';
import Header from '../../components/Header/Header';
import Menu from '../../components/Menu/Menu';

import '../../stylesheets/contact.css';

class ContactForm extends Component {

	reset(e) {
		$(e.target).removeClass('invalid');
		$("#formError").fadeOut(200);
	}
	resetAll(e) {
		this.reset(e);
		$("#emailError").fadeOut(300);
	}
	send (){
		var form = $("#mail");
		var name = form.find('#name');
		var email = form.find('#email');
		var msg = form.find('#msg');
		var type = form.find('#type');

		if(form.find('#agree').val() === ""){
			if(name.val() !== "" && email.val() !== "" && msg.val() !== ""){
				validator.validate_async(email.val(), function(err, isValidEmail) {
					if(isValidEmail){
						var data = {
							recipient: "wietse.l.devries@gmail.com",
							subject: "Contact formulier",
							tpl: "contact",
							params: {
								SENDER: name.val(),
								EMAIL: email.val(),
								MSG: msg.val(),
								TYPE: type.val(),
							}
						}
						$.post('/sendMail',data,function(response) {
							if(response === "OK"){
								form[0].reset();
								M.toast("Formulier verzonden", "Bedankt voor uw bericht, burgerzetel.nl zal er alles aan doen uw email zo spoedig mogelijk te beantwoorden.");
							}else{
								M.toast("Niet verzonden", "Het formulier kon niet verzonden worden, probeer het a.u.b. opnieuw.", "error");
							}
						});
					}else{
						$("#emailError").fadeIn(100);
						email.addClass('invalid');
					}
				});

			}else{
				if(name.val() === ""){
					name.addClass('invalid');
				}
				if(email.val() === ""){
					email.addClass('invalid');
				}
				if(msg.val() === ""){
					msg.addClass('invalid');
				}
				$("#formError").html("Vul aub alle velden in.");
				$("#formError").fadeIn(200);
			}

		}else{
			$("#formError").html("U bent duidelijk een kneus die bijdehand wil doen ;)");
			$("#formError").fadeIn(200);
		}

	}


	render() {

		var feedback = false;
		if(this.props.location.pathname === "/contact/feedback"){
			feedback = true;
		}
		var user = Session.User();
		if(user){
			var name = user.firstName+" "+user.lastName;
			var email = user.email;
		}
		let {viewer} = this.props;

		return (
			<div>
				<Header notifications={viewer}/>
				<div className="container">
					<div className="row">
						<Menu userData={viewer}/>
						{/*Main panel*/}
						<div className="col-xs-12 col-sm-8 col-md-6 no-padding padding-sm center-panel">
							<div className="panel">
								<div className="contact">
									<form id="mail">
										{feedback ? (
											<span>
												<h3 className="title color">
													Feedback
												</h3>
												<div className="dotted extra-side-margin"></div>
												<p className="color-grey">
													Heeft u feedback, commentaar, suggesties of een klacht over Burgerzetel.nl?<br/>
													Laat het ons weten. Wij gebruiken uw input graag om onze website en
	service te verbeteren.
												</p>
												<input type="hidden" id="type" defaultValue="Feedback"/>
											</span>
										):(
											<span>
												<h3 className="title color">
													Contact
												</h3>
												<div className="dotted extra-side-margin"></div>
												<p className="color-grey">
													Neem contact op met de beheerders van Burgerzetel.nl
												</p>
												<input type="hidden" id="type" defaultValue="Contact"/>
											</span>
										)}
										<div className="row">
											<div className="col-xs-12 col-sm-8 col-md-7">
												<label className="color-grey">Naam</label>
												<input defaultValue={name} type="text" id="name" onFocus={this.reset.bind(this)}/>
											</div>
											<div className="col-xs-12 col-sm-8 col-md-7">
												<label className="color-grey">Email <span className="error-msg" id="emailError"><i className="fa fa-times-circle"></i> Ongeldig e-mail adres</span></label>
												<input defaultValue={email} type="text" id="email" onFocus={this.resetAll.bind(this)}/>
											</div>
											<div className="col-xs-12">
												<label className="color-grey">Bericht</label>
												<textarea id="msg" onFocus={this.reset.bind(this)}></textarea>
												<input type="text" className="flip-flop" id="agree" />
												<div className="error-msg left" id="formError"></div>
												<div className="btn primary-btn right" onClick={this.send.bind(this)}>Verzenden</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
						{/*Side panel*/}
						<div className="col-md-3 hidden-xs hidden-sm">
							<InfoPages/>
							<Footer/>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
ContactForm.contextTypes = {
	router: React.PropTypes.object
};
export default Relay.createContainer(ContactForm, {
	fragments: {
		viewer: () => Relay.QL `
		fragment on viewer {
			${Menu.getFragment('userData')},
			${Header.getFragment('notifications')},
		}
		`
	}
});
