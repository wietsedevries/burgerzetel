import React, { Component } from 'react';
import Relay from 'react-relay';
import Moment from 'moment';

import Avatar from '../../components/User/Avatar';
import '../../stylesheets/user.css';
import M from '../../main.js';

import SubscriptionMutation from '../../mutations/SubscriptionMutation';

class Page extends Component {
	constructor(props) {
			super(props);
			var subscribed = this.checksubscription(this.props.tagData);
			this.state = {
				subscribed: subscribed,
			};
	}

	checksubscription = (Tag) => {
		let subscribedTo = [];
		if(this.props.viewer.AuthUser){
			subscribedTo = this.props.viewer.AuthUser.Interests.edges;
		}
		let subscribedToArray = [];
		for(var i = 0; i < subscribedTo.length; i++){
			subscribedToArray.push(subscribedTo[i].node.Tag.key);
		}
		var subscribed = false;
		if(subscribedToArray.length > 0){
			if(subscribedToArray.indexOf(Tag.key) > -1){
				subscribed = true;
			}
		}
		return subscribed;
	}

	componentWillReceiveProps(nextProps){
		if(JSON.stringify(this.props.tagData) !== JSON.stringify(nextProps.tagData)) {
			var subscribed = this.checksubscription(nextProps.tagData);
			this.setState({subscribed: subscribed});
		}
	}

	follow = (state) => {
		this.setState({subscribed: state});
		let tagValueId = this.props.tagData.tagValueId;
		this.props.relay.commitUpdate(new SubscriptionMutation({
			viewer: this.props.viewer,
			tagValueId,
			state,

		}), {
			onSuccess: (response) => {
				console.log("following:",response);
			},
			onFailure: (t) => {
				console.log('SubscriptionMutation FAILED', t.getError());
			},
		});
	}

	render() {
		let user = this.props.viewer.AuthUser;
		let Tag = this.props.tagData;
		let showText = this.props.data;
		let img = "";
		let formattedDT = "";
		if(showText){
			Moment.locale('nl');
			formattedDT = Moment(Tag.Page.dateUpdated).format('LL');
			img = Tag.Page.img;
		}

		return (

				<div className="panel">
					<div className="profile tag-block">
						<Avatar hasURL="false" imagePath={img} type="page" tag={Tag.value}/>
						<div className="name color-accent">{Tag.value}</div>
						{showText ? (
							<span>
								<div className="bio color-grey">
									{Tag.Page.content}
								</div>
								<div className="meta-data">
									Bron: {Tag.Page.data}, Laatst gewijzigd op {formattedDT}
								</div>
							</span>
						):(
							<div className="bio color-grey">
								Er is nog geen informatie beschikbaar over dit onderwerp.
							</div>
						)}
						{user ? (
							this.state.subscribed ? (
								<div onClick={this.follow.bind(this,false)} className="btn outline-btn centered active" style={{display:"block",width: 120+"px"}}>
									<i className="fa fa-share"></i> Ontvolgen
								</div>
							):(
								<div onClick={this.follow.bind(this,true)} className="btn outline-btn centered" style={{display:"block",width: 120+"px"}}>
									<i className="fa fa-share"></i> Volgen
								</div>
							)
						):(
							<div onClick={M.login} className="btn outline-btn centered" style={{display:"block",width: 120+"px"}}>
								<i className="fa fa-share"></i> Volgen
							</div>
						)}
					</div>
				</div>
		);
	}
}
export default Relay.createContainer(Page, {
		fragments: {
			viewer: () => Relay.QL `
				fragment on viewer {
					${SubscriptionMutation.getFragment('viewer')},
					AuthUser{
						Interests(first: 200000){
							edges{
								node{
									id
									Tag{
										key
									}
								}
							}
						}
					},
				}
				`
			}
		});
