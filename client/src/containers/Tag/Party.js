import React, { Component } from 'react';
import '../../stylesheets/user.css';

class Party extends Component {

  render() {
		let Tag = this.props.props;
		let Party = Tag.Page.Party;
		console.log(Party);
		function formatNumber(x) {
	    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
		}
		return (

				<div className="panel padding">
					<div className="party">
						<div className="panel-title">
							<i className="fa fa-university"></i>
							<span>Partij informatie</span>
						</div>
						<div className="dotted"></div>
						<div className="title color">{Tag.value}</div>
						<div className="label color-grey">
							<span>Partijleider:</span>
							<span className="label color">{Party.leader}</span>
						</div>
						<div className="label color-grey">
							<span>Aantal leden:</span>
							<span className="label color">{formatNumber(Party.members)}</span>
						</div>
						<div className="label color-grey">
							<span>Opgericht:</span>
							<span className="label color">{Party.established}</span>
						</div>
						<div className="dotted"></div>
						<br/>
						<div className="label color">
							Eerste kamer &nbsp;
							<span className="label color-grey">({Party.seatsFirst}/75)</span>
						</div>
						<div className="result-progress"><div style={{width: ((Party.seatsFirst/75)*100)+'%'}}></div></div>
						<div className="label color">
							Tweede kamer &nbsp;
							<span className="label color-grey">({Party.seatsSecond}/150)</span>
						</div>
						<div className="result-progress"><div style={{width: ((Party.seatsSecond/150)*100)+'%'}}></div></div>
						<div className="label color">
							Europees parlement &nbsp;
							<span className="label color-grey">({Party.seatsEU}/26)</span>
						</div>
						<div className="result-progress"><div style={{width: ((Party.seatsEU/26)*100)+'%'}}></div></div>
						<br/>
					</div>
				</div>

    );
  }
}
export default Party;
