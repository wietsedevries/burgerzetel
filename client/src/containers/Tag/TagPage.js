import React, { Component } from 'react';
import Relay from 'react-relay';
import DocumentMeta from 'react-document-meta';

import Footer from '../../components/sidebar/Footer';
import UnansweredItem from '../../components/Feed/UnansweredItem';
import AnsweredItem from '../../components/Feed/AnsweredItem';
import Header from '../../components/Header/Header';
import Menu from '../../components/Menu/Menu';
import Page from './Page';
import Party from './Party';

import '../../stylesheets/user.css';

class TagPage extends Component {
	redirect (path){
		this.context.router.push(path);
	}
	render() {
		let pageComponent;
		let partyComponent;

		let {viewer} = this.props;
		var Tag = viewer.TagValueByKey;

		const meta = {
			title: Tag.value+" | Burgerzetel",
			description: "Overzicht van alle vragen die gesteld zijn over '"+Tag.value+"'",
			canonical: 'http://burgerzetel.nl/onderwerp/'+Tag.key,
			meta: {
				charset: 'utf-8',
			}
		};

		if(!Tag){
			this.redirect('/error/404');
			return null;
		}

		if(Tag.pageId > 0){
			pageComponent = <Page viewer={viewer} tagData={Tag} data={true}/>;
			if(Tag.Page.partyId > 0){
				partyComponent = <Party props={Tag}/>;
			}
		}else{
			pageComponent = <Page viewer={viewer} tagData={Tag} data={false}/>;
		}

		let tags = Tag.Tags.edges;
		let showResults = false;
		if(tags.length > 0){
			showResults =  true;
		}

		return (
			<div>
				<DocumentMeta {...meta} />
				<Header notifications={viewer}/>
				<div className="container">
					<div className="row">
						<Menu userData={viewer}/>
						{/*Main panel*/}
						<div className="col-xs-12 col-sm-8 col-md-6 no-padding padding-sm center-panel">
							<div className="panel padding">
								<div className="color panel-title-lg bold">
									<i className="fa fa-hashtag"></i>
									<span>{Tag.value}</span>
								</div>
							</div>
							{showResults ? (
								<div className="list">
									{Object.keys(tags).map((key) => {
										let data = tags[key].node;
										if(data.Question){
											if (data.Question.Answers.edges.length > 0) {
												return (
													<AnsweredItem key={key} data={data.Question} viewer={viewer}/>
												)
											}else{
												let User = tags[key].node.Question.User;
												return (
													<UnansweredItem key={key} data={data.Question} user={User} viewer={viewer}/>
												)
											}
										}
										return true;

									})}
								</div>
							):(
								<div className="panel padding">
									<div className="no-results color-grey">Er zijn nog geen vragen gesteld over dit onderwerp</div>
								</div>
							)}

						</div>
						{/*Side panel*/}
						<div className="col-md-3 col-sm-8 col-xs-12 padding-md">
							{pageComponent}
							{partyComponent}
							<Footer/>
						</div>
					</div>
				</div>
			</div>

		);
	}
}
TagPage.contextTypes = {
	router: React.PropTypes.object
};
export default Relay.createContainer(TagPage, {
		initialVariables: {
			tag: null
		},
		fragments: {
			viewer: () => Relay.QL `
				fragment on viewer {
					${Menu.getFragment('userData')},
					${Header.getFragment('notifications')},
					${Page.getFragment('viewer')},
					${AnsweredItem.getFragment('viewer')},
					${UnansweredItem.getFragment('viewer')},
					TagValueByKey(key:$tag){
						tagValueId,
						value,
						key,
						pageId,
						Tags(first:20){
							edges{
								node{
									id,
									Question{
										qid,
										title,
										description,
										downvoted,
										upvoted,
										dateCreated,
										slug,
										Favorites(first:1) {
											edges{
												node{
													dateCreated,
												},
											},
										},
										Answers(first:1){
											edges{
												node {
													answer,
													User{
														firstName,
														lastName,
														userName,
														function,
														avatar
													}
												}
											}
										},
										User{
											avatar,
											firstName,
											lastName,
											userName,
										}
									}
								}
							}
						},
						Page{
							id,
							partyId,
							content,
							data,
							img,
							dateUpdated,
							Party{
								leader,
								leaderUrl,
								established,
								members,
								seatsFirst,
								seatsSecond,
								seatsEU,
							}
						}
					}
				}
				`
			}
		});
