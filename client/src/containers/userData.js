import Relay from 'react-relay';

export default Relay.createContainer(UserData, {
    fragments: {
        viewer: () => Relay.QL `
	        fragment on viewer {
						Interests(first:20){
			        edges{
			          node{
			            Tag{
			              value,

			            }
			          }
			        }
			      },
	        }
        `
    }
});
