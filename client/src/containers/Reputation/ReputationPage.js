import React, { Component } from 'react';
import Relay from 'react-relay';
import Moment from 'moment';

import Footer from '../../components/sidebar/Footer';
import Header from '../../components/Header/Header';
import Menu from '../../components/Menu/Menu';
import LeaderBoard from './LeaderBoard';

import '../../stylesheets/reputation.css';

class ReputationPage extends Component {

	render() {
		const {viewer} = this.props;
		const User = viewer.UserByName;
		if(!User){
			return (<div>Geen user gevonden met deze username</div>);
		}
		let badge;
		if(User.verified === 1) {
			badge = (
				<i className="fa fa-check-circle"></i>
			);
		}
		Moment.locale('nl');
		const formattedDT = Moment(User.dateRegistered).format('LL');

		//@TODO: add totalCount to query instead of doing this here
		const totalQuestions = User.Questions.edges.length;
		const totalAnswers = User.Answers.edges.length;
		const totalReactions = User.Reactions.edges.length;
		const totalReplies = User.Replies.edges.length;
		// const userMilestones = User.Milestones.edges;
		/* Array with milestoneTypes (from the database) */
		// var MilestoneTypes = viewer.MilestoneTypes.edges;

		/* Array with unlockable content by reputation */
		// const unlocks = {
		// 	0:{value:"Toevoegen van een poll aan een vraag", points:50},
		// 	1:{value:"Downvoten van content", points:150},
		// 	2:{value:"Een overzicht van alle gerapporteerde content inzien", points:450},
		// 	3:{value:"Bepaalde websitegegevens inzien (geen gebruikersinformatie, alleen ongevoeligde data)", points:1250},
		// 	4:{value:"Het rapporteren van dubbele vragen", points:2250},
		// 	5:{value:"Een lapel pin, thuisgestuurd in de post", points:10000},
		// }
		/* Array with actions rewarded every time they're performed */
		const rewards = {
			4:{value:"U beantwoordt een vraag", points:15},
			2:{value:"U reageert op een antwoord", points:5},
			3:{value:"Uw antwoord wordt gewaardeerd", points:10},
			1:{value:"Uw vraag wordt gewaardeerd", points:5},
			0:{value:"Uw reactie wordt gewaardeerd", points:2},
		}
		return (
			<div>
				<Header notifications={viewer}/>
				<div className="container">
					<div className="row">
						<Menu userData={viewer}/>
						{/*Main panel*/}
						<div className="col-xs-12 col-sm-8 col-md-6 no-padding padding-sm center-panel">
							<div className="panel">
								<div className="reputation-page">
									<div className="name color-accent">{User.firstName} {User.lastName} {badge}</div>
									<div className="dotted extra-side-margin"></div>
									<div className="reputation-label color" >
										<i className="fa fa-trophy"></i>
										{User.reputation}
									</div>
									<table className="stats color">
										<tbody>
											<tr>
												<td>{totalQuestions+" vragen gesteld"}</td>
											</tr>
											<tr>
												<td>{totalAnswers+" antwoorden gegeven"}</td>
											</tr>
											<tr>
												<td>{totalReactions+" reacties geplaatst"}</td>
											</tr>
											<tr>
												<td>{totalReplies+"x gereageerd op een reactie"}</td>
											</tr>
											<tr>
												<td>{User.loginCount+"x ingelogd sinds "+formattedDT}</td>
											</tr>
										</tbody>
									</table>
									<div className="dotted extra-side-margin"></div>
									<div className="details">
										<div className="info color-grey">
												Burgerzetel maakt gebruik van een reputatiesysteem. Reputatie kan behaald worden
												door verschillende acties op het platform uit te voeren. De reputatie bepaald wat
												een gebruiker mag doen op het platform.
										</div>

										{/* <div className="title color">Vrij te spelen onderdelen</div>
										<table className="unlockable">
											<thead>
												<tr className="color-grey">
													<th></th>
													<th>Benodigd</th>
												</tr>
											</thead>
											<tbody>
												{Object.keys(unlocks).map(function(key) {
													const unlock = unlocks[key];
													return (
														<tr className={(User.reputation >= unlock.points) ? 'completed' : ''} key={key}>
															<td>{unlock.value}</td>
															<td style={{width:50+"px"}}>{unlock.points}</td>
														</tr>
													)
												})}
											</tbody>
										</table>
										<div className="dotted extra-side-margin"></div>
										<div className="title color">Mijlpalen</div>
										<table className="unlockable">
											<thead>
												<tr className="color-grey">
													<th></th>
													<th>Beloning</th>
												</tr>
											</thead>
											<tbody>
												{Object.keys(MilestoneTypes).map(function(key) {
													const milestoneType = MilestoneTypes[key].node;
													if (!milestoneType.hidden) {

														var milestoneCompleted = false;
														Object.keys(userMilestones).map(function(n) {
															const userMilestone = userMilestones[n].node;
															if (milestoneType.key === userMilestone.Details.key) {
																if (userMilestone.total >= milestoneType.number){
																	milestoneCompleted = true;
																}
															}
															return null;
														})
														return (
																<tr className={(milestoneCompleted) ? 'completed' : ''} key={key}>
																	<td>{milestoneType.value}</td>
																	<td style={{width:50+"px"}}>{milestoneType.reward}</td>
																</tr>
															)
														}
														return null;
												})}
											</tbody>
										</table>
										<div className="dotted extra-side-margin"></div>*/}
										<div className="title color">Acties die beloond worden</div>
										<table>
											<thead>
												<tr className="color-grey">
													<th></th>
													<th className="text-right">Beloning</th>
												</tr>
											</thead>
											<tbody className="colored">
												{Object.keys(rewards).map(function(key) {
													const reward = rewards[key];
													return (
														<tr key={key}>
															<td>{reward.value}</td>
															<td className="text-right">{reward.points} Punten{(reward.info) ? reward.info : ''}</td>
														</tr>
													)
												})}
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						{/*Side panel*/}
						<div className="col-md-3 col-sm-8 col-xs-12 padding-md">
							<LeaderBoard viewer={viewer}/>
							<Footer/>
						</div>
					</div>
				</div>
			</div>

		);
	}
}
export default Relay.createContainer(ReputationPage, {
		initialVariables: {
			username: null
		},
		fragments: {
				viewer: () => Relay.QL `
						fragment on viewer {
							${Menu.getFragment('userData')},
							${Header.getFragment('notifications')},
							${LeaderBoard.getFragment('viewer')}

							UserByName(username:$username){
									firstName,
									lastName,
									bio,
									reputation,
									function,
									verified,
									avatar,
									lastLogin,
									dateRegistered,
									loginCount,
									verified,
									Milestones(first:1000){
										edges{
											node{
												total,
												Details{
													key,
													number,
												},
											}
										}
									},
									Questions(first:100000) {
										edges {
											node {
												id,
											}
										}
									},
									Answers(first:10000) {
										edges {
											node {
												id,
											}
										}
									},

									Reactions(first:10000) {
										edges {
											node {
												id,
											}
										}
									},
									Replies(first:10000) {
										edges {
											node {
												id,
											}
										}
									}
							},
							MilestoneTypes(first:100){
								edges{
									node{
										key,
										value,
										number,
										reward,
										hidden,
									}
								}
							},
						}
				`
		}
});
