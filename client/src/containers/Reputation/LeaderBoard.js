import React, { Component } from 'react';
import Relay from 'react-relay';
import {Link} from 'react-router';

import Avatar from '../../components/User/Avatar';

import '../../stylesheets/user.css';

class LeaderBoard extends Component {

	render() {
		let {viewer} = this.props;
		let users = viewer.GetTopTenUsers.edges;
		let rank = 0;

		return (
			<div className="panel padding share-block leaderboard">
				<div className="panel-title bold">
					<i className="fa fa-trophy"></i>
					<span>Top 10 gebruikers</span>
				</div>
				<div className="dotted"></div>
				{Object.keys(users).map((key) => {
					const user = users[key].node;
					rank++;
					return (
						<Link to={"/"+user.userName} key={key} className="top-user">
							<Avatar imagePath={user.avatar} userName={user.userName} userFirstName={user.firstName} userLastName={user.lastName} hasURL="false"/>
							<div className="color">#{rank}. {user.firstName+" "+user.lastName}</div>
							<div className="color-grey"><i className="fa fa-trophy"></i> {user.reputation}</div>
						</Link>
					)
				})}
			</div>
		);
	}
};
export default Relay.createContainer(LeaderBoard, {
		fragments: {
			viewer: () => Relay.QL `
				fragment on viewer {
					GetTopTenUsers(first:10){
						edges{
							node{
								firstName
								lastName
								userName
								avatar
								reputation
							}
						}

					}
				},
			`
		},
});
