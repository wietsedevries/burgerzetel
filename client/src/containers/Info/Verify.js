import React, { Component } from 'react';
import Relay from 'react-relay';

import Footer from '../../components/sidebar/Footer';
import InfoPages from '../../components/sidebar/InfoPages';
import Header from '../../components/Header/Header';
import Menu from '../../components/Menu/Menu';

import '../../stylesheets/content.css';

import VerifyMutation from '../../mutations/VerifyMutation';

class Verify extends Component {
	componentDidMount() {
		if(this.props.viewer.AuthUser){
			this.props.relay.commitUpdate(new VerifyMutation({
				viewer: this.props.viewer,
				username: this.props.viewer.AuthUser.userName,
			}), {
				onSuccess: (response) => {},
				onFailure: (t) => {
					console.log('ViewMutation FAILED', t.getError());
				},
			});
		}
	}
	render() {
		let {viewer} = this.props;
		let title;
		let text;
		if(viewer.AuthUser.verified === 0){
			title = "Account verifiëren";
			text = "Uw verzoek is ingediend. U kunt binnen 48 uur een antwoord verwachten.";
		}else{
			title = "Account al verifiëerd";
			text = "Uw account is al verifiëerd. Het is niet nodig om uw account nogmaals te verifiëren";
		}
		return (
			<div>
				<Header notifications={viewer}/>
				<div className="container">
					<div className="row">
						<Menu userData={viewer}/>
						{/*Main panel*/}
						<div className="col-xs-12 col-sm-8 col-md-6 no-padding padding-sm center-panel">
							<div className="panel padding">
								<h1 className="color">{title}</h1>
								<div className="dotted"></div>
								<div className="color-grey">{text}</div>
							</div>
						</div>
						{/*Side panel*/}
						<div className="col-md-3 hidden-xs hidden-sm">
							<InfoPages/>
							<Footer/>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default Relay.createContainer(Verify, {
	fragments: {
		viewer: () => Relay.QL `
		fragment on viewer {

			${Menu.getFragment('userData')}
			${Header.getFragment('notifications')}
			${VerifyMutation.getFragment('viewer')}

			AuthUser{
				userName
				verified
			},
		}
		`
	}
});
