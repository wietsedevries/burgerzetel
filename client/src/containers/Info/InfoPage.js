import React, { Component } from 'react';
import Relay from 'react-relay';
import Moment from 'moment';

import Footer from '../../components/sidebar/Footer';
import InfoPages from '../../components/sidebar/InfoPages';
import Header from '../../components/Header/Header';
import Menu from '../../components/Menu/Menu';

import '../../stylesheets/content.css';

class InfoPage extends Component {

	render() {
		let {viewer} = this.props;
		let Content = viewer.Content;
		Moment.locale('nl');
		const formattedDT = Moment(Content.dateUpdated).format('LL');
		return (
			<div>
				<Header notifications={viewer}/>
				<div className="container">
					<div className="row">
						<Menu userData={viewer}/>
						{/*Main panel*/}
						<div className="col-xs-12 col-sm-8 col-md-6 no-padding padding-sm center-panel">
							<div className="panel padding">
								<h1 className="color">{Content.title}</h1>
								<div className="updated color-grey">Laatst gewijzigd op: {formattedDT}</div>
								<div className="dotted"></div>
								<span className="fr-view content" dangerouslySetInnerHTML={{__html: Content.content}}/>
							</div>
						</div>
						{/*Side panel*/}
						<div className="col-md-3 hidden-xs hidden-sm">
							<InfoPages/>
							<Footer/>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default Relay.createContainer(InfoPage, {
	initialVariables: {
		slug: null,
		limitQuestions: 1,
	},
	fragments: {
		viewer: () => Relay.QL `
		fragment on viewer {
			${Menu.getFragment('userData')},
			${Header.getFragment('notifications')},
			Content(slug:$slug){
				title,
				content,
				dateUpdated,
			},
		}
		`
	}
});
