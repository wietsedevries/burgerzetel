import React, { Component } from 'react';
import Relay from 'react-relay';
import {debounce} from 'lodash';
import {Link} from 'react-router';
import DocumentMeta from 'react-document-meta';

import Footer from '../../components/sidebar/Footer';
import Trending from '../../components/sidebar/Trending';
import TrendingTags from '../../components/sidebar/TrendingTags';
import Header from '../../components/Header/Header';
import Menu from '../../components/Menu/Menu';
import Avatar from '../../components/User/Avatar';

import '../../stylesheets/user.css';

let newLimitTags = 12;
let showMoreTags =  false;

class TopicPage extends Component {
	constructor(props) {
			super(props);
			this.setVariables = debounce(this.props.relay.setVariables, 300);
			this.state = {
				search: false,
			};
	}
	setLimitTags = (limit) => {
		newLimitTags = limit + 12;
		this.setVariables({ limitTags: Number(newLimitTags) });
		showMoreTags =  true;
	};
	search(event) {
		event.preventDefault();
		var input = document.getElementById('searchbox');
		this.props.relay.setVariables({ query: input.value});
		if(input.value.length > 0){
			this.setState({search: true	});
		}else{
			this.setState({search: false	});
		}

	}
	render() {

		const meta = {
			title: "Onderwerpen overzicht | Burgerzetel",
			description: "Overzicht van alle besproken politieke onderwerpen van Nederland",
			canonical: 'http://burgerzetel.nl/onderwerpen/overzicht',
			meta: {
				charset: 'utf-8',
			}
		};

		let {viewer} = this.props;
		let tags;
		let loadTags;
		if(this.state.search){
			tags = viewer.TagValueByQuery.edges;
			loadTags = viewer.TagValueByQuery.pageInfo.hasNextPage;
		}else{
			tags = viewer.GetAllTags.edges;
			loadTags = viewer.GetAllTags.pageInfo.hasNextPage;
		}

		let anyTags = false;
		if(tags.length > 0){
			anyTags = true;
		}

		return (
			<div>
				<DocumentMeta {...meta} />
				<Header notifications={viewer}/>
				<div className="container">
					<div className="row">
						<Menu userData={viewer}/>
						{/*Main panel*/}
						<div className="col-xs-12 col-sm-8 col-md-6 no-padding padding-sm center-panel">
							<div className="panel padding">
								<div className="topic">
									<div className="title color">
										Zoek een onderwerp
									</div>
									<div className="row">
										<div className="col-xs-12 col-md-6">
											<form onSubmit={this.search.bind(this)}>
												<input autoFocus="on" autoComplete="off" type="text" placeholder="Zoeken" id="searchbox"/>
												<button type="submit" className="grey-link">
													<i className="fa fa-search"></i>
												</button>
											</form>
										</div>
									</div>
									<div className="dotted"></div>
									{anyTags ? (
										Object.keys(tags).map(function(key) {
											let tag = tags[key].node;
											let previewImg = "";
											if(tag.Page > 0){
												previewImg = tag.Page.img;
											}
											return (
												<Link to={"/onderwerp/"+tag.key} className="preview-item" key={key}>
													<Avatar imagePath={previewImg} type="page" tag={tag.value} hasURL="false"/>
													<div className="color">{tag.value}</div>
													<span className="color-grey">{tag.used}x Gebruikt</span>
												</Link>
											)
										})
									):(
										<div className="preview-item no-result">
											<div className="color">Geen resultaten</div>
											<span className="color-grey">Probeer een ander trefwoord</span>
										</div>
									)}

								</div>
							</div>
							{loadTags ? (
									<div className="load-more">
										<div onClick={this.setLimitTags.bind(null,newLimitTags)} className="btn secondary-btn">Meer laden</div>
									</div>
								) : (
									showMoreTags ? (
										<div className="load-more">
											<div className="color-grey">Einde van zoekresultaten</div>
										</div>
									) : (
										<div className="load-more">
										</div>
									)
							)}

						</div>
						{/*Side panel*/}
						<div className="col-md-3 hidden-xs hidden-sm">
							<TrendingTags tags={viewer}/>
							<Trending questions={viewer}/>
							<Footer/>
						</div>
					</div>
				</div>
			</div>

		);
	}
}
export default Relay.createContainer(TopicPage, {
		initialVariables: {
			tag: null,
			limitTags: 12,
			query: "",
		},
		fragments: {
				viewer: () => Relay.QL `
					fragment on viewer {
						${Trending.getFragment('questions')},
						${Menu.getFragment('userData')},
						${Header.getFragment('notifications')},
						${TrendingTags.getFragment('tags')},
						TagValueByQuery(query:$query,first:$limitTags) {
							pageInfo{
								hasNextPage,
							},
							edges{
								node{
									tagValueId,
									value,
									key,
									used,
									Page{
										img,
									},
								},
							},
						},
						GetAllTags(first: $limitTags){
							pageInfo{
								hasNextPage,
							},
							edges {
								node{
									value,
									used,
									key,
									Page{
										id,
										partyId,
										content,
										data,
										img,
										dateUpdated,
									},
								},
							},
						},
					}
					`
				}
		});
