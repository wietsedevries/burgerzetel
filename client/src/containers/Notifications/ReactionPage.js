import React, { Component } from 'react';
import Relay from 'react-relay';

import Footer from '../../components/sidebar/Footer';
import InfoPages from '../../components/sidebar/InfoPages';
import Header from '../../components/Header/Header';
import Menu from '../../components/Menu/Menu';
import NoNotification from '../../components/Notifications/Blocks/NoNotification';
import ReactionBlock from '../../components/Notifications/Blocks/ReactionBlock';

import '../../stylesheets/notifications.css';

class Notifications extends Component {

	redirect (path){
		this.context.router.push(path);
	}
	render() {


		let {viewer} = this.props;
		var notificationBlock;
		let reaction = viewer.Reaction;
		if(reaction){
			if(reaction.User.userName === viewer.AuthUser.userName){
				notificationBlock = <ReactionBlock reaction={reaction} viewer={viewer}/>;
			}else{
				this.redirect('/error/403');
				return null;
			}
		}else{
			notificationBlock = <NoNotification/>;
		}

		return (
			<div>
				<Header notifications={viewer}/>
				<div className="container">
					<div className="row">
						<Menu userData={viewer}/>
						{/*Main panel*/}
						<div className="col-xs-12 col-sm-8 col-md-6 no-padding padding-sm center-panel">
							{notificationBlock}
						</div>
						{/*Side panel*/}
						<div className="col-md-3 hidden-xs hidden-sm">
							<InfoPages/>
							<Footer/>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
Notifications.contextTypes = {
	router: React.PropTypes.object
};
export default Relay.createContainer(Notifications, {
	initialVariables: {
		id: null,
	},
	fragments: {
		viewer: () => Relay.QL `
		fragment on viewer {
			${Menu.getFragment('userData')},
			${Header.getFragment('notifications')},
			${ReactionBlock.getFragment('viewer')},
			AuthUser{
				userName,
			}
			Reaction(id:$id) {
				Answer{
					Question{
						title,
						slug,
					}
				},
				rid,
				reaction,
				upvoted,
				downvoted,
				dateCreated,
				User{
					firstName,
					lastName,
					userName,
					avatar,
				},
				Replies(first:200){
					edges{
						node{
							id,
							replyId,
							
							reply,
							upvoted,
							downvoted,
							dateCreated,
							User{
								firstName,
								lastName,
								userName,
								avatar,
							}
						}
					}
				},
			}
		}
		`
	}
});
