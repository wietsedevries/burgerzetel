import React, { Component } from 'react';
import Relay from 'react-relay';
import Footer from '../../components/sidebar/Footer';
import InfoPages from '../../components/sidebar/InfoPages';
import Header from '../../components/Header/Header';
import Menu from '../../components/Menu/Menu';
import NotificationPreview from '../../components/Notifications/Blocks/NotificationPreview';
import NoNotification from '../../components/Notifications/Blocks/NoNotification';

import '../../stylesheets/notifications.css';

class FlaggedPage extends Component {
	redirect (path){
		this.context.router.push(path);
	}

	render() {
		let {viewer} = this.props;

		var notificationBlock;
		let notification = viewer.Notification;
		if(notification){
			if(notification.User.userName === viewer.AuthUser.userName){
				notificationBlock = <NotificationPreview data={notification}/>;
			}else{
				this.redirect('/error/403');
				return null;
			}
		}else{
			notificationBlock = <NoNotification/>;
		}

		return (
			<div>
				<Header notifications={viewer}/>
				<div className="container">
					<div className="row">
						<Menu userData={viewer}/>
						{/*Main panel*/}
						<div className="col-xs-12 col-sm-8 col-md-6 no-padding padding-sm center-panel">
							{notificationBlock}
						</div>
						{/*Side panel*/}
						<div className="col-md-3 hidden-xs hidden-sm">
							<InfoPages/>
							<Footer/>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
FlaggedPage.contextTypes = {
	router: React.PropTypes.object
};
export default Relay.createContainer(FlaggedPage, {
	initialVariables: {
		id: null,
	},
	fragments: {
		viewer: () => Relay.QL `
		fragment on viewer {
			${Menu.getFragment('userData')},
			${Header.getFragment('notifications')},
			AuthUser{
				userName,
			}
			Notification(id:$id) {
				dateCreated,
				NotificationType{
					value,
					key,
				},
				User{
					firstName,
					lastName,
					avatar,
					userName,
				},
				AskedBy{
					firstName,
					lastName,
					avatar,
					userName,
				},
				Reply{
					reply,
					User{
						avatar,
						firstName,
						lastName,
						userName,
					},
				},
				Reaction{
					reaction,
					User{
						avatar,
						firstName,
						lastName,
						userName,
					},
				}
				Answer{
					answer
					User{
						avatar,
						firstName,
						lastName,
						userName,
					},
				},
				Question{
					title,
					slug,
				}
			},
		}
		`
	}
});
