import React, { Component } from 'react';
import Relay from 'react-relay';
import Footer from '../../components/sidebar/Footer';
import InfoPages from '../../components/sidebar/InfoPages';
import Header from '../../components/Header/Header';
import Menu from '../../components/Menu/Menu';
import NoNotification from '../../components/Notifications/Blocks/NoNotification';
import AnswerBlock from '../../components/Notifications/Blocks/AnswerBlock';

import '../../stylesheets/notifications.css';

class AnswerPage extends Component {

	redirect (path){
		this.context.router.push(path);
	}

	render() {
		let {viewer} = this.props;

		var notificationBlock;
		let answer = viewer.Answer;
		if(answer){
			if(answer.User.userName === viewer.AuthUser.userName){
				notificationBlock = <AnswerBlock answer={answer} reactions={answer.Reactions.edges} viewer={viewer}/>;
			}else{
				this.redirect('/error/403');
				return null;
			}
		}else{
			notificationBlock = <NoNotification/>;
		}

		return (
			<div>
				<Header notifications={viewer}/>
				<div className="container">
					<div className="row">
						<Menu userData={viewer}/>
						{/*Main panel*/}
						<div className="col-xs-12 col-sm-8 col-md-6 no-padding padding-sm center-panel">
							{notificationBlock}
						</div>
						{/*Side panel*/}
						<div className="col-md-3 hidden-xs hidden-sm">
							<InfoPages/>
							<Footer/>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
AnswerPage.contextTypes = {
	router: React.PropTypes.object
};
export default Relay.createContainer(AnswerPage, {
	initialVariables: {
		id: null,
	},
	fragments: {
		viewer: () => Relay.QL `
		fragment on viewer {
			${Menu.getFragment('userData')},
			${Header.getFragment('notifications')},
			${AnswerBlock.getFragment('viewer')},
			AuthUser{
				userName,
			},
			Answer(id:$id)  {
				id,
				aid,
				answer,
				upvoted,
				downvoted,
				shared,
				correct,
				dateCreated,
				User{
					firstName,
					lastName,
					userName,
					function,
					avatar,
				},
				Question{
					title,
					slug,
				},
				Reactions(first:200){
					edges{
						node{
							id,
							rid,
							reaction,
							upvoted,
							downvoted,
							dateCreated,
							User{
								firstName,
								lastName,
								userName,
								avatar,
							},
							Replies(first:200){
								edges{
									node{
										id,
										replyId,
										reply,
										upvoted,
										downvoted,
										dateCreated,
										User{
											firstName,
											lastName,
											userName,
											avatar,
										}
									}
								}
							},
						}
					}
				}
			},
		}
		`
	}
});
