import React, { Component } from 'react';
import Relay from 'react-relay';
import {debounce} from 'lodash';
import $ from 'jquery';
import Footer from '../../components/sidebar/Footer';
import InfoPages from '../../components/sidebar/InfoPages';
import Header from '../../components/Header/Header';
import Menu from '../../components/Menu/Menu';
import NotificationItem from '../../components/Notifications/NotificationItem';

import '../../stylesheets/notifications.css';

var allNotifications;
let newLimitNotifications;
let showMoreNotifications =  false;

class Notifications extends Component {
	constructor(props) {
			super(props);
			newLimitNotifications = 12;
			this.setVariables = debounce(this.props.relay.setVariables, 300);
	}
	redirect (path){
		this.context.router.push(path);
	}
	load() {
		$("#loader").css({'top':'0','width':'0px'});
		$("#loader").animate({
				width: "100%"
			}, 1400, function() {
				$("#loader").animate({
				top: "-3px"
			}, 200, function() {
				$("#loader").css('top','-3px');
			});
		});
	}
	setLimitNotifications = (limit) => {
			newLimitNotifications = limit + 12;
			this.setVariables({ limitNotifications: Number(newLimitNotifications) });
			showMoreNotifications =  true;
			this.load();
	};
	render() {


		let {viewer} = this.props;

		allNotifications = viewer.AuthUser.Notifications.edges;
		let loadNotifications = viewer.AuthUser.Notifications.pageInfo.hasNextPage;


		return (
			<div>
				<Header notifications={viewer}/>
				<div className="container">
					<div className="row">
						<Menu userData={viewer}/>
						{/*Main panel*/}
						<div className="col-xs-12 col-sm-8 col-md-6 no-padding padding-sm center-panel">
							<div className="naked-header color-grey">Notificaties</div>
							<span className="notification-box">
								{allNotifications.length > 0  ? (
									Object.keys(allNotifications).map(function(key) {
										var data = allNotifications[key].node;
										return (
											<div key={key} className="panel notification-panel">
												<NotificationItem  data={data} rows={6}/>
											</div>
										)
									})
								):(
									<div className="panel padding">
										<div className="no-results color-grey">U heeft nog geen notificaties</div>
									</div>
								)}

							</span>
							{loadNotifications ? (
									<div className="load-more">
										<div onClick={this.setLimitNotifications.bind(null,newLimitNotifications)} className="btn secondary-btn">Meer laden</div>
									</div>
								) : (
									showMoreNotifications ? (
										<div className="load-more">
											<div className="color-grey">Einde van Notificaties</div>
										</div>
									) : (
										<div className="load-more">
										</div>
									)
							)}
						</div>
						{/*Side panel*/}
						<div className="col-md-3 hidden-xs hidden-sm">
							<InfoPages/>
							<Footer/>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
Notifications.contextTypes = {
	router: React.PropTypes.object
};
export default Relay.createContainer(Notifications, {
	initialVariables: {
		limitNotifications: 12,
		username: null,
		id: null,
	},
	fragments: {
		viewer: () => Relay.QL `
		fragment on viewer {
			${Menu.getFragment('userData')},
			${Header.getFragment('notifications')},
			AuthUser {
				Notifications(first:$limitNotifications){
					pageInfo {
						hasNextPage,
					},
					edges{
						node{
							nid,
							msg,
							dateCreated,
							NotificationType{
								value,
								key,
							},
							User{
								firstName,
								lastName,
								avatar,
								userName,
							},
							AskedBy{
								firstName,
								lastName,
								avatar,
								userName,
							},
							Reply{
								reply,
								User{
									avatar,
									firstName,
									lastName,
									userName,
								},
							},
							Reaction{
								rid,
								reaction,
								upvoted,
								downvoted,
								dateCreated,
								User{
									firstName,
									lastName,
									userName,
									avatar,
								},
								Replies(first:200){
									edges{
										node{
											id,
											reply,
											upvoted,
											downvoted,
											dateCreated,
											User{
												firstName,
												lastName,
												userName,
												avatar,
											}
										}
									}
								},
							}
							Answer{
								aid,
								answer,
								User{
									avatar,
									firstName,
									lastName,
									userName,
								},
							},
							Question{
								title,
								slug,
							},
						}
					}
				}
			},
		}
		`
	}
});
