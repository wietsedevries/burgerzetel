import React, { Component } from 'react';
import Relay from 'react-relay';
import Footer from '../../components/sidebar/Footer';
import Trending from '../../components/sidebar/Trending';
import TrendingTags from '../../components/sidebar/TrendingTags';
import Header from '../../components/Header/Header';
import Menu from '../../components/Menu/Menu';
import {browserHistory} from 'react-router';

import '../../stylesheets/error.css';

class Error404 extends Component {

	return (){
		browserHistory.go(-2)
	}

	render() {
		const {viewer} = this.props;
		return (
			<div>
				<Header notifications={viewer}/>
				<div className="container">
					<div className="row">
						<Menu page="" userData={viewer}/>
						{/*Main panel*/}
						<div className="col-xs-12 col-sm-8 col-md-6 no-padding padding-sm center-panel">
							<div className="panel error">
								<h1 className="color">Error <span className="color-accent">404</span></h1>
								<h2 className="color-grey">Pagina niet gevonden</h2>
								<p className="color-grey">
									De pagina die u zoekt bestaat niet of is verwijderd. Ga terug
									of gebruik de zoekbalk om verder te zoeken.
								</p>
								<div onClick={this.return} className="btn outline-btn centered" >
										<i className="fa fa-reply"></i> Terug naar vorige pagina
								</div>
							</div>
						</div>
						{/*Side panel*/}
						<div className="col-md-3 hidden-xs hidden-sm">
							<Trending questions={viewer}/>
							<TrendingTags tags={viewer}/>
							<Footer/>
						</div>
					</div>
				</div>
			</div>

		);
	}
}

export default Relay.createContainer(Error404, {
		fragments: {
				viewer: () => Relay.QL `
					fragment on viewer {
						${Trending.getFragment('questions')},
						${Header.getFragment('notifications')},
						${Menu.getFragment('userData')},
						${TrendingTags.getFragment('tags')},
					}
				`
		}
});
