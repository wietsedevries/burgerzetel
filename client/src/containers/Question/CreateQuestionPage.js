import React, { Component } from 'react';
import Relay from 'react-relay';

import PollItem from '../../components/Poll/PollItem';
import Header from '../../components/Header/Header';
import Menu from '../../components/Menu/Menu';
import Footer from '../../components/sidebar/Footer';
import Tips from '../../components/Question/Tips';
import Rules from '../../components/Question/Rules';
import RichEditor from '../../components/Question/RichEditor';
import AddUser from '../../components/Question/AddUser';
import AddTag from '../../components/Tag/AddTag';
import Confirm from '../../components/Actions/Confirm';
import PollPrev from '../../components/Poll/Poll';

import M from '../../main.js';
import '../../stylesheets/ask.css';

import QuestionMutation from '../../mutations/QuestionMutation';
import EditQuestionMutation from '../../mutations/EditQuestionMutation';

let edit = false;
var count = 0;
let tags = [];
let pollItems = [];
let addpollItems = [];
let invited = [];
let Poll = false;
let Polls = [];
let pollTitleVal = "";
let pollOptions;
let slug;
let qid;
let pollEdited = true;

class CreateQuestionPage extends Component {
	constructor(props) {
		super(props);

		tags = [];
		let pageLabel = "Stel een vraag";
		let advancedOptions = "none";
		let advancedLabel = "Toon geadvanceerde opties";
		let setDescription = "";
		if(this.props.route.path === "/vraag/nieuw/:username"){
			advancedOptions = "block";
			advancedLabel = "Verberg geadvanceerde opties";
		}
		if(this.props.route.path === "/:slug/wijzig"){
			edit = true;
			pageLabel = " Wijzig uw vraag";
			advancedOptions = "block";
			advancedLabel = "Verberg geadvanceerde opties";
			let Q = this.props.viewer.Question;
			slug = Q.slug;
			qid = Q.qid;
			setDescription = Q.description;
			if(Q.Poll){
				pollEdited = false;
				Polls = Q.Poll.edges;
				if(Q.Poll.edges.length > 0){
					pollOptions = Q.Poll.edges[0].node.Options.edges;
					Poll = Q.Poll.edges[0].node;
					pollTitleVal = Poll.title

					Object.keys(pollOptions).map((key) => {
						let value = pollOptions[key].node.value;
						pollItems.push(<PollItem key={key} value={value} onRemove={this.removePoll} onAdd={this.pushPoll}/>);
						addpollItems.push(value);
						return true;
					});
				}

			}

		}
		if(this.props.viewer.UserByName){
			this.pushUsers(this.props.viewer.UserByName.uid);
		}

		this.state = {
			edit: edit,
			advancedOptions: advancedOptions,
			advancedLabel: advancedLabel,
			pageLabel: pageLabel,
			showConfirm: false,
			tags: tags,
			addpollItems: addpollItems,
			invited: invited,
			pollItems: pollItems,
			poll: Poll,
			pollTitleVal: pollTitleVal,
			slug: slug,
			qid: qid,
			pollEdited: pollEdited,
			setDescription: setDescription,
			disabled: "",
		};
	}
	redirect (path,toast){
		this.context.router.push({
			pathname: path,
			state: {toast: toast}
		});
	}
	openAdvancedOptions() {
		if(this.state.advancedOptions === "none"){
			this.setState({advancedOptions: "block",advancedLabel: "Verberg geadvanceerde opties"});
		}else{
			this.setState({advancedOptions: "none",advancedLabel: "Toon geadvanceerde opties"});
		}
	}
	pushTags = (value) => {
		tags.push(value);
	}
	removeTags = (value) => {
		for(var i = tags.length - 1; i >= 0; i--) {
			// eslint-disable-next-line
			if(tags[i] == value) {
				tags.splice(i, 1);
			}
		}
	}
	pushPoll = (value) => {
		addpollItems.push(value);
	}
	removePoll = (value) => {
		for(var i = addpollItems.length - 1; i >= 0; i--) {
			// eslint-disable-next-line
			if(addpollItems[i] == value) {
				addpollItems.splice(i, 1);
			}
		}
	}
	addPollOption(event) {
		const options = document.getElementsByClassName("add-option");
		if (options.length < 15) {
			pollItems.push(<PollItem onRemove={this.removePoll} onAdd={this.pushPoll}/>);
		}
		this.setState({pollItems: pollItems});
	}
	pushUsers = (value) => {
		invited.push(value);
	}
	removeUsers = (value) => {
		for(var i = invited.length - 1; i >= 0; i--) {
			// eslint-disable-next-line
			if(invited[i] == value) {
				invited.splice(i, 1);
			}
		}
	}
	confirm() {
		count++;
		this.setState({showConfirm: count});
	}
	confirmSucces = () => {
		this.post();
	}

	post = (e) => {
		e.preventDefault();
		this.setState({disabled: "disabled"});
		// Form values
		let {question,pollTitle} = this.refs;
		let description;
		if(this.refs.description.refs.description.refs.el.value === ""){
			description = this.state.setDescription;
		}else{
			description = this.refs.description.refs.description.refs.el.value;
		}
		let questionTitle = question.value;
		questionTitle = M.censor(questionTitle);
		description = M.censor(description);

		let PollItems = [];
		for(var i = 0; i < addpollItems.length; i++){
			PollItems.push(addpollItems[i].replace(",", "&#x002C"));
		}

		if(this.state.pollEdited){
			if(pollTitle.value !== "" && this.state.addpollItems.length < 2){
				M.toast("Poll aanmaken mislukt", "Voeg minstens twee opties toe aan uw poll","error");
				return false;
			}
		}else{
			addpollItems = [];
		}

		if(question.value === ""){
			M.toast("Vraag aanmaken mislukt", "U heeft geen vraag gesteld","error");
			return false;
		}
		if(description === ""){
			M.toast("Vraag aanmaken mislukt", "Voeg additionele informatie toe aan uw vraag","error");
			return false;
		}
		if(tags.length < 1){
			M.toast("Vraag aanmaken mislukt", "Voeg minstens één onderwerp toe aan uw vraag","error");
			tags = [];
			return false;
		}

		this.setState({addpollItems: addpollItems});
		this.setState({tags: tags});
		this.setState({invited: invited});

		if(this.state.edit){
			this.props.relay.commitUpdate(new EditQuestionMutation({
				viewer: this.props.viewer,
				question: questionTitle,
				description,
				tags,
				invited,
				pollTitle: pollTitle.value,
				pollItems: PollItems,
				slug: this.state.slug,
				questionId: this.state.qid,
			}), {
				onSuccess: (response) => {
					const toast = {title: "Vraag bijgewerkt", message: "Uw vraag is successvol bijgewerkt!"}
					this.redirect('/vraag/'+this.state.slug, toast);
				},
				onFailure: (t) => {
					M.toast("Vraag aanmaken mislukt", "Probeer opnieuw","error");
					console.log('SaveMutation FAILED', t.getError());
					this.setState({disabled: ""});
				},
			});
		}else{
			this.props.relay.commitUpdate(new QuestionMutation({
				viewer: this.props.viewer,
				question: questionTitle,
				description,
				tags,
				invited,
				pollTitle: pollTitle.value,
				pollItems: PollItems,
			}), {
				onSuccess: (response) => {
					const toast = {title: "Vraag aangemaakt", message: "Uw vraag is successvol aangemaakt!"}
					this.redirect('/vraag/'+response.QuestionMutation.slug, toast);
				},
				onFailure: (t) => {
					M.toast("Vraag aanmaken mislukt", "Probeer opnieuw","error");
					console.log('SaveMutation FAILED', t.getError());
					this.setState({disabled: ""});
				},
			});
		}

	}

	componentWillMount() {
		if(this.props.viewer.Question){
			this.setState({pageLabel: "Wijzig uw vraag"});
		}
	}

	removeQuestionPoll(e){
		this.setState({
			poll: false,
			addpollItems: [],
			pollItems: [],
			pollTitleVal: "",
			pollEdited: true,
		});
	}

	render() {
		// this.pushUsers(this.props.params.username);
		const {viewer} = this.props;
		var Question = viewer.Question;

		//fetching data, if editing question
		if (Question) {
			var Tags = Question.Tags.edges;
			var InvitedUsers = Question.Invites.edges;
		}
		// var Media = Question.Media.edges;

		let advancedOptions = this.state.advancedOptions;
		let advancedLabel = this.state.advancedLabel;
		let pageLabel = this.state.pageLabel;
		return (
			<div>
				<Header notifications={viewer}/>
				<div className="container">
					<div className="row">
						<Menu userData={viewer}/>
						{/*Main panel*/}
						<div className="col-xs-12 col-sm-8 col-md-6 no-padding padding-sm center-panel">
							<div className="panel create">
								<div className="ask question">
									<div className="title color">{pageLabel}</div>
									<div className="dotted extra-side-margin"></div>
									<form className="form" id="create" onSubmit={this.post.bind(this)}>
										<div className="spacer"></div>
										<input ref="question" type="text" placeholder="Uw vraag" defaultValue={Question ? Question.title : ''}/>
										<div className="spacer"></div>
										<RichEditor ref="description" value={Question ? this.state.setDescription : ''}/>
										<div className="spacer"></div>
										<div className="tag-label color-grey">Voeg minimaal èèn gerelateerd onderwerp toe. (Max. 5)</div>
										<AddTag onRemove={this.removeTags} onAdd={this.pushTags} ref="tags" viewer={viewer} limit={5} addedTags={Tags} edit={this.state.edit}/>
										<span style={{display: advancedOptions}}>
											<br/>
											<div className="dotted extra-side-margin"></div>
											<div className="advanced">
												<div className="title color">
													Geavanceerde opties
												</div>
												<div className="spacer"></div>
												<div className="tag-label color-grey">Stel uw vraag aan een of meerdere gebruikers (max. 5)</div>
												<AddUser onRemove={this.removeUsers} onAdd={this.pushUsers} directedQuestion={viewer.UserByName} viewer={viewer} addedUsers={InvitedUsers} limit={5}/>
												<div className="dotted no-side-margin"></div>
												<div className="spacer"></div>
												<div className="add-poll color">

													{this.state.poll ? (
														<span>
															<div className="tag-label color-grey">Toegevoegde poll</div>
															<br/>
															<span className="edit-poll">
																{Object.keys(Polls).map((key) => {
																	return <PollPrev edit={1} currentUser={viewer.AuthUser}  options={Polls[key].node.Options} title={Polls[key].node.title} key={key} />;
																})}
															</span>
															<div className="delete-poll" onClick={this.removeQuestionPoll.bind(this)}><i className="fa fa-trash"></i> Poll verwijderen</div>
															<input ref="pollTitle" type="hidden" defaultValue=""/>
														</span>

													):(
														<span>
															<div className="tag-label color-grey">Voeg een poll toe aan de vraag (Min. 2 opties)</div>
															<br/>
															<input ref="pollTitle" name="poll-title" type="text" placeholder="Titel - Bij voorkeur een gesloten vraag" defaultValue={this.state.pollTitleVal}/>
															{this.state.pollItems}
															<div className="add-extra blue-link" onClick={this.addPollOption.bind(this)}>
																<i className="fa fa-plus-circle"></i>
																<span>Optie toevoegen aan poll</span>
															</div>
														</span>
													)}

												</div>

												<div className="dotted no-side-margin"></div>

											</div>
										</span>
										<div className="actions">
											<div className="toggle">
												<label className="side-link-lg underline pointer" onClick={this.openAdvancedOptions.bind(this)}>
													<i className="fa fa-sliders" aria-hidden="true"></i> {advancedLabel}
												</label>
											</div>
											<button type="submit" className={"btn primary-btn "+this.state.disabled}>
												<span>Opslaan</span>
												<i className="fa fa-circle-o-notch fa-spin"></i>
											</button>

										</div>
									</form>
								</div>
							</div>
							<Confirm show={this.state.showConfirm} onClick={this.confirmSucces.bind(this)} title="Uw eerste vraag!" msg="Voldoet uw vraag aan alle punten op de checklist? Zo ja, klik dan op doorgaan om uw vraag te stellen."/>
						</div>
						{/*Side panel*/}
						<div className="col-md-3 hidden-xs hidden-sm">
							<Tips/>
							<Rules/>
							<Footer/>
						</div>
					</div>
				</div>
			</div>

		);
	}
}
CreateQuestionPage.contextTypes = {
	router: React.PropTypes.object
};
export default Relay.createContainer(CreateQuestionPage, {
	initialVariables: {
		slug: "1",
		username: "1",
	},
	fragments: {
		viewer: () => Relay.QL `
			fragment on viewer {
				${QuestionMutation.getFragment('viewer')},
				${EditQuestionMutation.getFragment('viewer')},

				${Menu.getFragment('userData')},
				${Header.getFragment('notifications')},
				${AddTag.getFragment('viewer')},
				${AddUser.getFragment('viewer')},
				UserByName(username:$username){
					uid,
					firstName,
					lastName,
					userName,
					avatar,
					function
				}
				AuthUser{
					uid,
					firstName,
					lastName
					userName
					avatar
				}
				Question(slug:$slug){
					slug,
					qid,
					title,
					description,
					User {
						id,
						firstName,
						lastName,
						userName,
						avatar,
					},
					Poll(first:1){
						edges{
							node{
								title,
								Options(first:50){
									edges{
										node{
											value,
											Votes(first:1000000){
												edges{
													node{
														PollOption{
															value
														}
														User{
															userName
														}
													}
												}
											}
										}
									}
								},
							}
						}
					},
					Media(first:20){
						edges{
							node{
								title,
								url,
								Type{
									key,
								}
							}
						}
					},
					Tags(first:20){
						edges{
							node{
								TagValue{
									tagValueId,
									key,
									value,
								}
							}
						}
					},
					Invites(first:50) {
						edges {
							node {
								To {
									avatar,
									firstName,
									lastName,
									userName,
									function,
								},
							},
						},
					},
				},
			},
		`
	},
});
