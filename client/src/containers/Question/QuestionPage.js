import M from '../../main.js';
import React, { Component } from 'react';
import Moment from 'moment';
import Relay from 'react-relay';
import {Link} from 'react-router';
import DocumentMeta from 'react-document-meta';

import Header from '../../components/Header/Header';
import Menu from '../../components/Menu/Menu';
import Footer from '../../components/sidebar/Footer';
import Trending from '../../components/sidebar/Trending';
import TrendingTags from '../../components/sidebar/TrendingTags';
import ShareBlock from '../../components/sidebar/Share';
import Poll from '../../components/Poll/Poll';
import AttachmentPanel from '../../components/Attachment/AttachmentPanel';
import Attachment from '../../components/Attachment/Attachment';
import Tag from '../../components/Tag/Tag';
import Avatar from '../../components/User/Avatar';
import Upvote from '../../components/Actions/Upvote';
import Share from '../../components/Actions/Share';
import SaveQuestion from '../../components/Actions/SaveQuestion';
import Report from '../../components/Actions/Report';
import DummyReport from '../../components/Actions/DummyReport';
import GiveAnswer from '../../components/Answer/GiveAnswer';
import Answer from '../../components/Answer/Answer';
import Edit from '../../components/Actions/Edit';

import '../../stylesheets/question.css';

import SaveMutation from '../../mutations/SaveMutation';
import ViewMutation from '../../mutations/ViewMutation';

class QuestionPage extends Component {
	constructor(props) {
		super(props);

		let flagged = false;
		let upvoted = false;
		if(this.props.viewer.Question){
			if(this.props.viewer.AuthUser){
				// Check if question has been reported by user
				let flaggedQuestions = this.props.viewer.AuthUser.FlaggedQuestions.edges;
				Object.keys(flaggedQuestions).map((key) => {
					if(this.props.viewer.Question.qid === flaggedQuestions[key].node.itemId){
						return flagged = true;
					}else{
						return false;
					}
				})
				// Check if question has been upvoted by user
				let upvotedQuestions = this.props.viewer.AuthUser.UpvotedQuestions.edges;
				Object.keys(upvotedQuestions).map((key) => {
					if(this.props.viewer.Question.qid === upvotedQuestions[key].node.itemId){
						return upvoted = true;
					}else{
						return false;
					}
				})
			}

			var Favorites = props.viewer.Question.Favorites.edges.length;
			let status = "";
			if(Favorites > 0){
				status = "completed";
			}

			this.state = {
				actionState: status,
				flagged: flagged,
				upvoted: upvoted,
			};
		}

	}
	componentDidMount() {
		if(this.props.viewer.AuthUser){
			let tags = [];
			for(var i = 0; i < this.props.viewer.Question.Tags.edges.length;i++){
				tags.push(this.props.viewer.Question.Tags.edges[i].node.TagValue.tagValueId);
			}
			this.props.relay.commitUpdate(new ViewMutation({
				viewer: this.props.viewer,
				questionId: this.props.viewer.Question.qid,
				tags: tags,
			}), {
				onSuccess: (response) => {},
				onFailure: (t) => {
					console.log('ViewMutation FAILED', t.getError());
				},
			});
		}

		if (this.props.location.state) {
			const toast = this.props.location.state.toast;
			M.toast(toast.title, toast.message);
		}
	}
	redirect (path){
		this.context.router.push(path);
	}
	toggleQuestion (questionId) {
		this.props.relay.commitUpdate(new SaveMutation({
			viewer: this.props.viewer,
			questionId,
		}), {
			onSuccess: (response) => {
				if(this.state.actionState === "completed"){
					this.setState({actionState: ""});
				}else{
					this.setState({actionState: "completed"});
				}
			},
			onFailure: (t) => {
				console.log('SaveMutation FAILED', t.getError());
			},
		});
	}
	render() {
		let {viewer} = this.props;
		var Question = viewer.Question;
		if(!Question){
			this.redirect('/error/404');
			return null;
		}
		const meta = {
			title: Question.title+" | Burgerzetel",
			description: Question.description,
			canonical: 'http://burgerzetel.nl/vraag/'+Question.slug,

			meta: {
				charset: 'utf-8',
				property: {
					'og:title': Question.title+" | Burgerzetel",
					'og:description': Question.description,
					'og:url': 'http://burgerzetel.nl/vraag/'+Question.slug,
				}
			}
		};
		let showAnswerOption = true;
		let now = Moment(Date()).format('LLL');
		let endDate =  Moment(Question.closingDate).format('LLL');
		const formattedDT = Moment(Question.dateCreated).format('LLL');
		if(Question.closingDate !== null && endDate < now){
			showAnswerOption =  false;
		}

		var User = Question.User;
		var Polls = Question.Poll.edges;
		var Media = Question.Media.edges;
		var Tags = Question.Tags.edges;
		var Answers = Question.Answers.edges;
		var hasAnswers = false;
		if(Answers.length > 0){
			hasAnswers = true;
		}
		const CurrentUser = viewer.AuthUser;
		let CurrentUserId;
		if(CurrentUser){
			CurrentUserId = CurrentUser.uid;
		}

		const questionPoints = Question.upvoted-Question.downvoted;
		const questionLabel = (questionPoints > 1 || questionPoints < -1 || questionPoints === 0) ? 'Punten' : 'Punt';
		let desc = Question.description.replace(/â€‹/g, "");

		let badge = "";
		if(User.verified > 0) {
			badge = (<i className='fa fa-check-circle'></i>);
		}
		return (
			<div>
				<DocumentMeta {...meta} />
				<Header title={Question.title} description={Question.description} notifications={viewer}/>
				<div className="container">
					<div className="row">
						<Menu userData={viewer}/>
						{/*Main panel*/}
						<div className="col-xs-12 col-sm-8 col-md-6 no-padding padding-sm center-panel">
							<div className="panel">
								<div className="question">
									<Avatar imagePath={User.avatar}  userName={User.userName} userFirstName={User.firstName} userLastName={User.lastName}/>
									<Link to={"/"+User.userName} className="name color-accent">{User.firstName} {User.lastName} {badge}</Link>
									<span className="postdate color-grey">&bull; {formattedDT}</span>
									<div className="title color">{Question.title}</div>
									<div className="description black">
										<span className="fr-view" dangerouslySetInnerHTML={{__html: desc}}/>
									</div>
									{Media.length > 0 &&
										<div className="sources">
											{ Object.keys(Media).map(function(key) {
												return <Attachment title={Media[key].node.title} url={Media[key].node.url} mediaKey={Media[key].node.Type.key} key={key} />;
											})}
										</div>
									}
									{Tags.length > 0 &&
										<div className="tags">
											{Object.keys(Tags).map(function(key) {
												return <Tag value={Tags[key].node.TagValue.value} tagKey={Tags[key].node.TagValue.key} key={key} />;
											})}
										</div>
									}
									<div className="actions color-grey sm bordered">
										<Upvote upvoted={this.state.upvoted} boxed="true" points={questionPoints} label={" "+ questionLabel} viewer={viewer} type="question" id={Question.qid}/>
										<SaveQuestion actionState={this.state.actionState} boxed="true" viewer={viewer} questionId={Question.qid} onClick={this.toggleQuestion.bind(this)}/>
										<Share  boxed="true"/>
										{CurrentUserId === User.uid ? (
											<Edit slug={Question.slug}/>
										):(
											this.state.flagged ? (
												<DummyReport boxed="true" label="true"/>
											):(
												<Report boxed="true" label="true" viewer={viewer} loggedIn={viewer.AuthUser} type="question" id={Question.qid}/>
											)
										)}
									</div>
								</div>
								<div className="dotted no-side-margin"></div>
								{showAnswerOption ? (
									<GiveAnswer user={CurrentUser} viewer={viewer} questionId={Question.qid} userId={Question.User.uid}/>
								):(
									<div className="closed-question color-grey"><i className="fa fa-lock"></i> Deze vraag is gesloten.</div>
								)}

							</div>
							<div>

								{hasAnswers ? (
									<span>
									<div className="naked-header color-grey">Antwoorden ({Answers.length})</div>
									{Object.keys(Answers).map((key) => {
										const answer = Answers[key].node;
										return (
											<div className="panel" key={key}>
												<div className="question answers">
													<div className="given-answer">
														<Answer viewer={viewer} answer={answer} reactions={answer.Reactions.edges} currentUser={CurrentUser}/>
													</div>
												</div>
											</div>
										)
									})}
									</span>
								):(
									<span></span>
								)}
							</div>
						</div>
						{/*Side panel*/}
						<div className="col-md-3 hidden-xs hidden-sm">
							{Media.length > 0 &&
								<AttachmentPanel media={Media} />
							}
							{Object.keys(Polls).map(function(key) {
								return <Poll viewer={viewer} currentUser={CurrentUser.userName} options={Polls[key].node.Options} title={Polls[key].node.title} key={key} />;
							})}
							<Trending questions={viewer}/>
							<TrendingTags tags={viewer}/>
							<ShareBlock title={Question.title} description={Question.description}/>
							<Footer/>
						</div>
					</div>
				</div>
			</div>

		);
	}
}

QuestionPage.contextTypes = {
	router: React.PropTypes.object
};
export default Relay.createContainer(QuestionPage, {
	initialVariables: {
		slug: null
	},
	fragments: {
		viewer: () => Relay.QL `
			fragment on viewer {
				${Trending.getFragment('questions')}
				${Header.getFragment('notifications')}
				${Report.getFragment('viewer')}
				${Menu.getFragment('userData')}
				${SaveMutation.getFragment('viewer')}
				${SaveQuestion.getFragment('viewer')}
				${ViewMutation.getFragment('viewer')}
				${Upvote.getFragment('viewer')}
				${TrendingTags.getFragment('tags')}
				${GiveAnswer.getFragment('viewer')}
				${Answer.getFragment('viewer')}
				${Poll.getFragment('viewer')}
				AuthUser{
					id
					uid
					firstName
					lastName
					userName
					avatar
					reputation
					verified
					FlaggedQuestions(first:100000){
						edges{
							node{
								itemId

							}
						}
					}
					UpvotedQuestions(first:100000){
						edges{
							node{
								itemId
							}
						}
					}
				}
				Question(slug:$slug){
					Favorites(first:1) {
						edges{
							node{
								dateCreated
							}
						}
					}
					title
					description
					upvoted
					downvoted
					qid
					slug
					dateCreated
					closingDate
					User {
						id
						uid
						firstName
						lastName
						userName
						avatar
						verified
					},
					Poll(first:1){
						edges{
							node{
								title
								Options(first:50){
									edges{
										node{
											optionId
											value
											Votes(first:1000000){
												edges{
													node{
														PollOption{
															optionId,
															value
														}
														User{
															userName
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
					Media(first:20){
						edges{
							node{
								title
								url
								Type{
									key
								}
							}
						}
					}
					Tags(first:20){
						edges{
							node{

								TagValue{
									key
									value
									tagValueId
								}
							}
						}
					}
					Answers(first:1000){
						edges{
							node{
								id
								aid
								answer
								upvoted
								downvoted
								shared
								correct
								dateCreated
								User{
									firstName
									verified
									lastName
									userName
									function
									avatar
								}
								Reactions(first:1000){
									edges{
										node{
											id
											rid
											reaction
											upvoted
											downvoted
											dateCreated

											User{
												firstName
												lastName
												userName
												avatar
												verified
											}
											Replies(first:1000){
												edges{
													node{
														id,
														replyId,
														reply,
														upvoted,
														downvoted,
														dateCreated,
														User{
															verified
															firstName
															lastName
															userName
															avatar
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		`
	}
});
