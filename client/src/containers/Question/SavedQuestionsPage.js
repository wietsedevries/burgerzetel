import React, { Component } from 'react';
import Relay from 'react-relay';
// import {Link} from 'react-router';
import Header from '../../components/Header/Header';
import Menu from '../../components/Menu/Menu';
import Footer from '../../components/sidebar/Footer';
import Trending from '../../components/sidebar/Trending';
import UnansweredItem from '../../components/Feed/UnansweredItem';
import AnsweredItem from '../../components/Feed/AnsweredItem';
import '../../stylesheets/question.css';

class SavedQuestions extends Component {
	redirect (path){
		this.context.router.push(path);
	}
	render() {
		let {viewer} = this.props;
		var User = viewer.AuthUser;
		var Favorites = User.Favorites.edges;
		if (!User) {
			this.redirect('/error/403');
			return null;
		}
		let feed;
		if (Favorites.length > 0) {
			feed = (
				<div className="list">
						{Object.keys(Favorites).map((key) => {
							let data = Favorites[key].node.Question;
							if (data.Answers.edges.length >  0) {
								return (
									<AnsweredItem key={key} data={data} viewer={viewer}/>
								)
							}else{
								let User = Favorites[key].node.Question.User;
								return (
									<UnansweredItem key={key} data={data} user={User} viewer={viewer}/>
								)
							}

						})}
				</div>
			)
		} else {
			feed = (
				<div className="panel padding">
					<div className="no-results color-grey">U heeft nog geen vragen opgeslagen</div>
				</div>
			)
		}
		return (
      <div>
				<Header notifications={viewer}/>
				<div className="container">
					<div className="row">
						<Menu page="favorites" userData={viewer}/>
						{/*Main panel*/}
						<div className="col-xs-12 col-sm-8 col-md-6 no-padding padding-sm center-panel">
							<div className="saved-questions">
								{feed}
							</div>
            </div>
						{/*Side panel*/}
						<div className="col-md-3 hidden-xs hidden-sm">
							<Trending questions={viewer}/>
							<Footer/>
						</div>
          </div>
				</div>
      </div>

    );
  }
}
SavedQuestions.contextTypes = {
	router: React.PropTypes.object
};
export default Relay.createContainer(SavedQuestions, {
  initialVariables: {
    username:null,
  },
  fragments: {
      viewer: () => Relay.QL `
        fragment on viewer {
					${Trending.getFragment('questions')},
					${Menu.getFragment('userData')},
					${Header.getFragment('notifications')},
					${AnsweredItem.getFragment('viewer')},
					${UnansweredItem.getFragment('viewer')},
					AuthUser{
						Favorites(first:20) {
							edges {
								node {
									Question {
										qid,
										title,
										slug,
										description,
										User {
											firstName,
											lastName,
											userName,
											function,
											avatar
										},
										Favorites(first:1) {
											edges{
												node{
													dateCreated,
												},
											},
										},
										Answers(first:1) {
											edges {
												node {
													answer,
													User{
														firstName,
														lastName,
														userName,
														function,
														avatar
													}
												}
											}
										}
									}
								}
							}
						}
					},
        }
      `
  }
});
