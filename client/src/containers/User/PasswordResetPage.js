import M from '../../main.js';
import React, { Component } from 'react';
import Relay from 'react-relay';
import Footer from '../../components/sidebar/Footer';
import Header from '../../components/Header/Header';
import Menu from '../../components/Menu/Menu';
import InfoPages from '../../components/sidebar/InfoPages';
import '../../stylesheets/user.css';

import ResetPasswordLinkMutation from '../../mutations/ResetPasswordLinkMutation';
import ResetPasswordMutation from '../../mutations/ResetPasswordMutation';

class PasswordResetPage extends Component {

	constructor(props) {
		super(props);
		this.state = {
			email: "",
			password: "",
			passwordConfirm: "",
			error: "",
		};
		this._handleEmailChange = this._handleEmailChange.bind(this);
		this._handlePasswordChange = this._handlePasswordChange.bind(this);
		this._handlePasswordConfirmChange = this._handlePasswordConfirmChange.bind(this);
		this.requestLink = this.requestLink.bind(this);
	}

	_handleEmailChange(e){
		let error = "";
		// eslint-disable-next-line
		var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
		if(!re.test(e.target.value)){
			error = "Het door u ingevulde e-mail adres is niet geldig.";
		}
		this.setState({error : error, email : e.target.value});
	}

	_handlePasswordChange(e){
		let error = "";
		this.setState({error : error, password : e.target.value});
	}

	_handlePasswordConfirmChange(e){
		let error = "";
		this.setState({error : error, passwordConfirm : e.target.value});
	}

	requestLink() {
		var field = document.getElementById('emailField');
		var {error, email} = this.state;
		if (!email) {
			error = "Het veld mag niet leeg zijn."
		}
		if (error) {
			M.toast("Fout", error, "error");
			return null;
		}
		this.props.relay.commitUpdate(new ResetPasswordLinkMutation({
			viewer: this.props.viewer,
			email,
		}), {
			onSuccess: (response) => {
				const {message} = response.ResetPasswordLinkMutation;
				if (message) {
					M.toast("Mislukt", message, "error");
				} else {
					M.toast("Gelukt!", "Er is een mail verstuurd met een link om uw wachtwoord te herstellen");
					field.value = "";
				}
			},
			onFailure: (t) => {
				console.log('Failed ResetPasswordLinkMutation', t.getError());
			},
		});
	}

	resetPassword(userId, hash) {
		const pwResetEl = document.getElementById("passwordResetFields");
		var {error, password, passwordConfirm} = this.state;
		if (!password && !passwordConfirm) {
			error = "De velden mogen niet leeg zijn."
		}
		if (password !== passwordConfirm) {
			error = "De ingevoerde wachtwoorden komen niet overeen."
		}
		if (password && !M.validatePassword(password)) {
			error = "Wachtwoord moet aan de volgende eisen voldoen: acht karakters lang, tenminste één cijfer en tenminste één letter";
		}
		if (error) {
			M.toast("Fout", error, "error");
			return null;
		}
		this.props.relay.commitUpdate(new ResetPasswordMutation({
			viewer: this.props.viewer,
			password,
			userId,
			hash,
		}), {
			onSuccess: (response) => {
				pwResetEl.innerHTML = "<p class='color'>U wachtwoord is aangepast, u kunt meteen inloggen met uw nieuwe wachtwoord.</p>";
				M.toast("Gelukt!", "U hebt uw wachtwoord aangepast");
				M.login();
			},
			onFailure: (t) => {
				console.log('Failed ResetPasswordMutation', t.getError());
			},
		});
	}

	render() {
		const {viewer} = this.props;
		let reset = viewer.ResetListByHash;
		let formEl;
		if (reset) {
			formEl = (
				<div className="password-reset">
					<div className="title color">
						Wachtwoord opnieuw instellen
					</div>
					<div className="dotted extra-side-margin"></div>
					<div className="color-grey">Gebruik het onderstaande veld om een nieuw wachtwoord in te stellen.</div>
					<span id="passwordResetFields">
						<input type="password" placeholder="Wachtwoord" onChange={this._handlePasswordChange}/>
						<input type="password" placeholder="Wachtwoord bevestigen" onChange={this._handlePasswordConfirmChange}/>
						<div className="btns">
							<div onClick={this.resetPassword.bind(this,reset.userId,reset.hash)} className="btn primary-btn right no-side-margin">Verzenden</div>
						</div>
					</span>
				</div>
			)
		} else {
			formEl = (
				<div className="password-reset">
					<div className="title color">
						Wachtwoord herstellen
					</div>
					<div className="dotted extra-side-margin"></div>
					<div className="color-grey">Vul uw e-mail adres in en klik op 'wachtwoord herstellen'.
						U ontvangt vervolgens een email met instructies om een nieuw wachtwoord aan te maken.
					</div>
					<input id="emailField" type="email" placeholder="E-mail adres" onChange={this._handleEmailChange}/>
					<div className="btns">
						<div onClick={this.requestLink} className="btn primary-btn right no-side-margin">Wachtwoord herstellen</div>
					</div>
				</div>
			)
		}
		return (
			<div>
				<Header notifications={viewer}/>
				<div className="container">
					<div className="row">
						<Menu userData={viewer}/>
						{/*Main panel*/}
						<div className="col-xs-12 col-sm-8 col-md-6 no-padding padding-sm center-panel">
							<div className="panel">
								{formEl}
							</div>
						</div>
						{/*Side panel*/}
						<div className="col-md-3 hidden-xs hidden-sm">
							<InfoPages/>
							<Footer/>
						</div>
					</div>
				</div>
			</div>

		);
	}
}
PasswordResetPage.contextTypes = {
	router: React.PropTypes.object
};
export default Relay.createContainer(PasswordResetPage, {
		initialVariables: {
			hash: "1",
		},
		fragments: {
				viewer: () => Relay.QL `
						fragment on viewer {
							${Menu.getFragment('userData')},
							${Header.getFragment('notifications')},

							${ResetPasswordLinkMutation.getFragment('viewer')},

							${ResetPasswordMutation.getFragment('viewer')},
							ResetListByHash(hash:$hash){
								hash,
								userId,

							}
						}
				`
		}
});
