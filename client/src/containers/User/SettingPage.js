import React, { Component } from 'react';
import Relay from 'react-relay';
import Footer from '../../components/sidebar/Footer';
import InfoPages from '../../components/sidebar/InfoPages';
import Header from '../../components/Header/Header';
import Menu from '../../components/Menu/Menu';

import M from '../../main.js';
import '../../stylesheets/user.css';

import SettingMutation from '../../mutations/SettingMutation';

class SettingPage extends Component {
	redirect (path){
		this.context.router.push(path);
	}
	setSetting(set,key) {
		this.props.relay.commitUpdate(new SettingMutation({
			viewer: this.props.viewer,
			key: key,
			set: set,
		}), {
			onSuccess: (response) => {
				M.toast("Opgeslagen", "Uw instelling is opgeslagen");
			},
			onFailure: (t) => {
				M.toast("Instellen mislukt", "Probeer opnieuw","error");
				console.log('SaveMutation FAILED', t.getError());
			},
		});
	}
	render() {
		const {viewer} = this.props;
		const User = viewer.AuthUser;
		if (!User) {
			this.redirect('/error/403');
			return null;
		}
		const Settings = viewer.SettingTypes.edges;
		const UserSettings = viewer.AuthUser.Settings.edges;
		//collect keys of settings the user has checked
		var activeSettingKeys = [];
		for (let i = 0; i < UserSettings.length; i++) {
			if (UserSettings[i].node.value === "1") {
				activeSettingKeys.push(UserSettings[i].node.SettingType.key);
			}
		}
		return (
			<div>
				<Header notifications={viewer}/>
				<div className="container">
					<div className="row">
						<Menu page="settings" userData={viewer}/>
						{/*Main panel*/}
						<div className="col-xs-12 col-sm-8 col-md-6 no-padding padding-sm center-panel">
							<div className="panel">
								<div className="settings">
									<div className="title color">
										Instellingen
									</div>
									<div className="dotted extra-side-margin"></div>
									{/* <div className="sub-header color">Notificaties</div>
									{Object.keys(Settings).map((key) => {
										const setting = Settings[key].node;
										if (setting.hidden === 0 && setting.key !== 'digest'){
											//check if this setting is active
											let checked = "";
											let set = false;
											if (activeSettingKeys.indexOf(setting.key) !== -1) {
												checked = "active";
												set = true;
											}
											return (
												<div className="setting color-grey" key={key}>
													<label className={"toggle-check "+checked} onClick={this.setSetting.bind(this,set,setting.settingTypeId)}><span></span></label>
													<label onClick={this.setSetting.bind(this,set,setting.settingTypeId)}>{setting.value}</label>
												</div>
											)
										}
										return null;
									})} */}
									<div className="sub-header color">E-mails</div>
									{Object.keys(Settings).map((key) => {
										const setting = Settings[key].node;
										if (setting.hidden === 0 && setting.key === 'digest'){
											//check if this setting is active
											let checked = "";
											let set = false;
											if (activeSettingKeys.indexOf(setting.key) !== -1) {
												checked = "active";
												set = true;
											}
											return (
												<div className="setting color-grey" key={key}>
													<label className={"toggle-check "+checked} onClick={this.setSetting.bind(this,set,setting.settingTypeId)}><span></span></label>
													<label onClick={this.setSetting.bind(this,set,setting.settingTypeId)}>{setting.value}</label>
												</div>
											)
										}
										return null;
									})}

								</div>
							</div>
						</div>
						{/*Side panel*/}
						<div className="col-md-3 col-sm-8 col-xs-12 padding-md">
							<InfoPages/>
							<Footer/>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
SettingPage.contextTypes = {
	router: React.PropTypes.object
};

export default Relay.createContainer(SettingPage, {
	fragments: {
		viewer: () => Relay.QL `
				fragment on viewer {
					${Menu.getFragment('userData')}
					${Header.getFragment('notifications')}
					${SettingMutation.getFragment('viewer')}
					AuthUser{
						userName
						Settings(first:20){
							edges{
								node{
									value
									SettingType{
										key,
										settingTypeId
									}
								}
							}
						}
					}
					SettingTypes(first:5){
						edges{
							node{
								settingTypeId
								key
								value
								hidden
							}
						}
					}
				}
		`
	}
});
