import React, { Component } from 'react';
import Relay from 'react-relay';
import {debounce} from 'lodash';
import {Link} from 'react-router';
import DocumentMeta from 'react-document-meta';

import Footer from '../../components/sidebar/Footer';
import Trending from '../../components/sidebar/Trending';
import Avatar from '../../components/User/Avatar';
import Header from '../../components/Header/Header';
import Menu from '../../components/Menu/Menu';
import UnansweredItem from '../../components/Feed/UnansweredItem';
import AnsweredItem from '../../components/Feed/AnsweredItem';
import ShareBlock from '../../components/sidebar/Share';

import '../../stylesheets/user.css';
import M from '../../main.js';


let newLimitQuestions = 12;
let showMoreQuestions =  false;

let newLimitAnswers = 12;
let showMoreAnswers =  false;

class ProfilePage extends Component {
	constructor(props) {
			super(props);
			this.setVariables = debounce(this.props.relay.setVariables, 300);
	}
	redirect (path){
		this.context.router.push(path);
	}
	setLimitQuestions = (limit) => {
			newLimitQuestions = limit + 12;
			this.setVariables({ limitQuestions: Number(newLimitQuestions) });
			showMoreQuestions =  true;
	};
	setLimitAnswers = (limit) => {
			newLimitAnswers = limit + 12;
			this.setVariables({ limitAnswers: Number(newLimitAnswers) });
			showMoreAnswers =  true;
	};
	showBlock = (block) => {
		document.getElementById("MyAnswers").style.display = "none";
		document.getElementById("MyQuestions").style.display = "none";
		document.getElementById("MyAnswersTab").className = "tab";
		document.getElementById("MyQuestionsTab").className = "tab";
		const tab = document.getElementById(block+'Tab');
		// const box = document.getElementById(block);
		if (tab.className.match(/\bactive\b/)) {
			// tab.className = "tab";
		}else{
			tab.className = "tab active";
			document.getElementById(block).style.display = "block";
		}
	};

	componentDidMount() {
		if (this.props.location.state) {
			const toast = this.props.location.state.toast;
			M.toast(toast.title, toast.message);
		}
	}

	render() {
		let {viewer} = this.props;
		let User = viewer.UserByName;
		if(!User){
			this.redirect('/error/404');
			return null;
		}
		const meta = {
			title: User.firstName+" "+User.lastName+" | Burgerzetel",
			description: User.bio,
			canonical: 'http://burgerzetel.nl/'+User.userName,
			meta: {
				charset: 'utf-8',
				property: {
					'og:title': User.firstName+" "+User.lastName+" | Burgerzetel",
					'og:description': User.bio,
					'og:url': 'http://burgerzetel.nl/'+User.userName,
					'og:image': 'http://burgerzetel.nl/images/avatars/'+User.avatar,
				}
			}
		};



		let loadQuestions = User.Questions.pageInfo.hasNextPage;
		let loadAnswers = User.Answers.pageInfo.hasNextPage;
		let badge;
		if(User.verified === 1) {
			badge = (
				<i className="fa fa-check-circle "></i>
			);
		}
		// Show asked Questions
		let questions = User.Questions.edges;
		let hasQuestions = false;
		if(questions.length > 0){
			hasQuestions = true;
		}

		// Show asked Questions
		let answers = User.Answers.edges;
		let hasAnswers = false;
		if(answers.length > 0){
			hasAnswers = true;
		}

		//Check if current profile is logged in user's profile
		let user = viewer.AuthUser;
		let own = false;
		let menu;
		if(user){
			if(User.userName === user.userName){
				menu = "profile";
				own = true;
			}
		}

		return (
			<div>
				<DocumentMeta {...meta} />
				<Header notifications={viewer}/>
				<div className="container">
					<div className="row">
						<Menu page={menu} userData={viewer}/>
						{/*Main panel*/}
						<div className="col-xs-12 col-sm-8 col-md-6 no-padding padding-sm center-panel">
							<div className="panel">
								<div className="profile">
									<Avatar imagePath={User.avatar} userFirstName={User.firstName} userLastName={User.lastName} hasURL="false"/>
									<div className="name color-accent">{User.firstName} {User.lastName} {badge}</div>
									<div className="headline color-grey">{User.function}</div>
									<Link to={"/"+User.userName+"/reputatie"} className="reputation color">
										<i className='fa fa-trophy'></i>
										{User.reputation}
									</Link>
									<div className="bio color-grey">
										{User.bio}
									</div>
									{own ? (
										<Link to={"/"+User.userName+"/wijzigen"} className="btn outline-btn centered" style={{display:"block",width: 120+"px"}}>
												<i className="fa fa-pencil"></i> Aanpassen
										</Link>
									):(
										user ? (
											<Link to={"/vraag/nieuw/"+User.userName} className="btn outline-btn centered" style={{display:"block",width:200+"px"}}>
													Stel {User.firstName} een vraag
											</Link>
										):(
											<div onClick={M.login} className="btn outline-btn centered" style={{display:"block",width:200+"px"}}>
													Stel {User.firstName} een vraag
											</div>
										)

									)}
								</div>
								<div className="dotted no-side-margin"></div>
								<div className="list">
									<div className="list-header">
										<div id="MyQuestionsTab" className="tab active" onClick={this.showBlock.bind(this,"MyQuestions")}>Gestelde vragen</div>
										<div id="MyAnswersTab" className="tab" onClick={this.showBlock.bind(this,"MyAnswers")}>Gegeven antwoorden</div>
									</div>
								</div>
							</div>

							<div className="list" id="MyQuestions">
								{hasQuestions ? (
									Object.keys(questions).map((key) => {
										let data = questions[key].node;
										if (data.Answers.edges.length >  0) {
											return (
												<AnsweredItem key={key} data={data} viewer={viewer}/>
											)
										}else{
											return (
												<UnansweredItem key={key} data={data} user={User} viewer={viewer}/>
											)
										}

									})
								):(
									<div className="panel padding">
										<div className="no-results color-grey">{User.firstName} heeft nog geen vragen gesteld</div>
									</div>
								)}
								{loadQuestions ? (
										<div className="load-more">
											<div onClick={this.setLimitQuestions.bind(null,newLimitQuestions)} className="btn secondary-btn">Meer laden</div>
										</div>
									) : (
										showMoreQuestions ? (
											<div className="load-more">
												<div className="color-grey">Einde van gestelde vragen</div>
											</div>
										) : (
											<div className="load-more">
											</div>
										)
								)}
							</div>
							<div className="list" id="MyAnswers">
								{hasAnswers ? (
									Object.keys(answers).map((key) => {

										let data = answers[key].node;
										return (
											<AnsweredItem key={key} data={data} user={User} own={true} viewer={viewer}/>
										)
									})
								):(
									<div className="panel padding">
										<div className="no-results color-grey">{User.firstName} heeft nog geen vragen beantwoord</div>
									</div>
								)}
								{loadAnswers ? (
										<div className="load-more">
											<div onClick={this.setLimitAnswers.bind(null,newLimitAnswers)} className="btn secondary-btn">Meer laden</div>
										</div>
									) : (
										showMoreAnswers ? (
											<div className="load-more">
												<div className="color-grey">Einde van gegeven antwoorden</div>
											</div>
										) : (
											<div className="load-more">
											</div>
										)
								)}
							</div>
						</div>
						{/*Side panel*/}
						<div className="col-md-3 hidden-xs hidden-sm">
							<Trending questions={viewer}/>
							<ShareBlock title={User.firstName+" "+User.lastName} description={User.bio}/>
							<Footer/>
						</div>
					</div>
				</div>
			</div>

		);
	}
}
ProfilePage.contextTypes = {
	router: React.PropTypes.object
};
export default Relay.createContainer(ProfilePage, {
		initialVariables: {
			username: null,
			limitQuestions: 12,
			limitAnswers: 12,
		},
		fragments: {
				viewer: () => Relay.QL `
						fragment on viewer {
							${Trending.getFragment('questions')},
							${Header.getFragment('notifications')},
							${Menu.getFragment('userData')},
							${AnsweredItem.getFragment('viewer')},
							${UnansweredItem.getFragment('viewer')},
							AuthUser{
								userName
								verified
							},
							UserByName(username:$username){
								uid,
								firstName,
								lastName,
								userName,
								bio,
								reputation,
								function,
								verified,
								avatar,
								Answers(first:$limitAnswers){
									pageInfo {
										hasNextPage,
									},
									edges {
										node {
											answer,
											User{
												firstName,
												lastName,
												userName,
												function,
												avatar
											},
											Question {
												qid,
												title,
												description,
												downvoted,
												upvoted,
												dateCreated,
												slug,
												Answers(first:1){
													edges{
														node {
															answer,
															User{
																firstName,
																lastName,
																userName,
																function,
																avatar
															}
														}
													}
												},
												Favorites(first:1) {
													edges{
														node{
															dateCreated,
														},
													},
												},
												User {
													userName,
													verified
												},
												title,
												slug,
											}
										}
									}
								},
								Questions(first:$limitQuestions) {
									pageInfo {
										hasNextPage,
									},
									edges{
										node{
											qid,
											title,
											description,
											downvoted,
											upvoted,
											dateCreated,
											slug,
											Favorites(first:1) {
												edges{
													node{
														dateCreated,
													},
												},
											},
											User {
												userName,
												verified
											}
											Answers(first:1){
												edges{
													node {
														answer,
														User{
															firstName,
															lastName,
															userName,
															function,
															avatar,
															verified
														}
													}
												}
											}
										}
									}
								},
							}
						}
				`
		}
});
