import M from '../../main.js';
import React, { Component } from 'react';
import Relay from 'react-relay';
import $ from 'jquery';
import {Link} from 'react-router';

import Footer from '../../components/sidebar/Footer';
import Trending from '../../components/sidebar/Trending';
import Avatar from '../../components/User/Avatar';
import Header from '../../components/Header/Header';
import Menu from '../../components/Menu/Menu';
import AddTag from '../../components/Tag/AddTag';
import Confirm from '../../components/Actions/Confirm';
import '../../stylesheets/user.css';

import ProfileMutation from '../../mutations/ProfileMutation';
import ProfilePicMutation from '../../mutations/ProfilePicMutation';

let count = 0;
let tags = [];

class EditProfilePage extends Component {
	constructor(props) {
			super(props);
			tags = [];
			this.state = {
				showConfirm: false,
				tags: tags,
			};
	}
	redirect (path, toast){
		this.context.router.push({
			pathname: path,
			state: {toast: toast}
		});
	}
	pushTags = (value) => {
		tags.push(value);
	}
	removeTags = (value) => {
		for(var i = tags.length - 1; i >= 0; i--) {
			// eslint-disable-next-line
			if(tags[i] == value) {
				tags.splice(i, 1);
			}
		}
	}

	post = (e) => {
		e.preventDefault();

		let {firstName,lastName,functio,bio,city,gender,education,maritalStatus,children,age,income,curPassword,newPassword,conPassword} = this.refs;
		curPassword = curPassword.value;
		newPassword = newPassword.value;
		conPassword = conPassword.value;
		var errorMessage;
		if(lastName.value === "") {
			errorMessage = "Vul aub uw achternaam in.";
		}
		if(firstName.value === "") {
			errorMessage = "Vul aub uw voornaam in.";
		}
		if (curPassword && (!newPassword && !conPassword)) {
			//if user provided current password, but not a new one, ignore it
			curPassword = "";
		}
		if (!curPassword && (newPassword || conPassword)) {
			//no current password, but new or confirm password
			errorMessage = "U dient uw huidige wachtwoord op te geven";
		}
		if (curPassword && (newPassword || conPassword)) {
			if (newPassword !== conPassword) {
				//new and confirmed password aren't same
				errorMessage = "Nieuwe wachtwoord komt niet overeen met bevestiging";
			}
			else if (newPassword === curPassword) {
				//new and current password are the same
				errorMessage = "Huidige wachtwoord en nieuwe wachtwoord mogen niet hetzelfde zijn";
			}
			else if (newPassword && !M.validatePassword(newPassword)) {
				errorMessage = "Wachtwoord moet aan de volgende eisen voldoen: acht karakters lang, tenminste één cijfer en tenminste één letter";
			}
		}
		if (errorMessage) {
			M.toast("Wijzigen mislukt", errorMessage, "error");
			return null;
		}

		this.setState({tags: tags});

		this.props.relay.commitUpdate(new ProfileMutation({
			viewer: this.props.viewer,
			firstName: firstName.value,
			lastName: lastName.value,
			functio: functio.value,
			bio: bio.value,
			city: parseInt(city.value, 10),
			education: education.value,
			age: parseInt(age.value, 10),
			maritalStatus: parseInt(maritalStatus.value, 10),
			children: children.value,
			income: parseInt(income.value, 10),
			gender: gender.value,
			tags: tags,
			password: curPassword,
			newPassword: newPassword,
		}), {
			onSuccess: (response) => {
				let {error} = response.ProfileMutation;
				if (error) {
					M.toast("Fout", error, "error");
					return null;
				}
				const toast = {title: "Profiel aangepast", message: "Uw profiel is successvol bijgewerkt!"}
				this.redirect('/'+this.props.username, toast);
			},
			onFailure: (t) => {
				console.log(firstName.value);
				console.log('EditMutation FAILED', t.getError());
			},
		});
	}

	viewPreview (event){
		var input = event.target;
		var files = input.files;

		if (files.length > 0){
			var formData = new FormData();
			for (var i = 0; i < files.length; i++) {
				var file = files[i];
				formData.append('uploads[]', file, file.name);
			}
			let filename = "default.png";
			$.ajax({
				url: '/uploadAvatar',
				type: 'POST',
				data: formData,
				processData: false,
				contentType: false,
				context: this,
				success: function(data,that){
					filename = data;
					this.props.relay.commitUpdate(new ProfilePicMutation({
						viewer: this.props.viewer,
						avatar: filename,
					}), {
						onSuccess: (response) => {
							console.log("done",response);
							console.log(this.props);
						},
						onFailure: (t) => {
							console.log('EditPicMutation FAILED', t.getError());
						},
					});
				},
				error: function(jqXHR, textStatus, errorThrown){
						console.log('ERRORS: ' + errorThrown);
				}
			});

		}else{
			console.log("oops..");
		}
	}
	confirm = () => {
		count++;
		this.setState({showConfirm: count});

	}
	confirmSucces = () => {
		alert("Gedeactiveerd!");
	}

	render() {
		let {viewer} = this.props;
		let User = viewer.AuthUser;

		let addedTags= [];
		Object.keys(User.Interests.edges).map((key) => {
			let tag = User.Interests.edges[key].node.Tag;
			addedTags.push(tag);
			return true;
		});
		if(!User){
			this.redirect('/error/404');
			return null;
		}
		let Cities = viewer.Cities.edges;
		let Incomes = viewer.Incomes.edges;
		let Educations = viewer.Educations.edges;
		let MaritalStatuses = viewer.MaritalStatuses.edges;

		// Generate birthyearlist
		var years = [];
		for(var i = 2010; i > 1910; i--){
			years.push(i);
		}

		return (
			<div>
				<Header notifications={viewer}/>
				<div className="container">
					<div className="row">
						<Menu page="profile" userData={viewer}/>
						{/*Main panel*/}
						<div className="col-xs-12 col-sm-8 col-md-6 no-padding padding-sm center-panel">
							<div className="panel">
								<div className="edit-profile">
									<div className="avatar-upload">
										<Avatar imagePath={User.avatar} hasURL="false" userFirstName={User.firstName} userLastName={User.lastName}/>
										<label htmlFor="pic">
											<div className="upload">
												<i className="fa fa-cloud-upload" aria-hidden="true"></i>
												<br/>
												Profielfoto wijzigen
											</div>
										</label>
										<input type="file" accept="image/*" id="pic" className="hide" name="avatar" onChange={this.viewPreview.bind(this)}/>
									</div>
									<br/>
									<div className="dotted extra-side-margin"></div>
									<div className="title color">Profiel wijzigen</div>
									<form onSubmit={this.post.bind(this)}>
										<div className="row">
											<div className="col-xs-12 col-sm-6">
												<input ref="firstName" type="text" defaultValue={User.firstName} placeholder="Voornaam"/>
											</div>
											<div className="col-xs-12 col-sm-6">
												<input ref="lastName" type="text" defaultValue={User.lastName} placeholder="Achternaam"/>
											</div>
											<div className="col-xs-12">
												<input ref="functio" type="text" defaultValue={User.function} placeholder="Functie (bijv. Docent)"/>
											</div>
											<div className="col-xs-12">
												<textarea ref="bio" maxLength="200" defaultValue={User.bio} placeholder="Bio &bull; Een korte omschrijving van uzelf"></textarea>
											</div>
										</div>
										<div className="dotted extra-side-margin"></div>
										<div className="title color">Uw interesses</div>
										<AddTag onRemove={this.removeTags} onAdd={this.pushTags} ref="tags" viewer={viewer} limit={100} addedTags={addedTags}/>
										<div className="dotted extra-side-margin"></div>
										<div>
											<div className="title color">Persoonlijke gegevens
												<span className="space-left color-grey">
													Niet zichtbaar voor anderen
													{/* <i className="fa fa-question-circle space-left"></i> */}
												</span>
											</div>
											<div className="row">
												<div className="col-xs-12">
													<select ref="city" defaultValue={(User.City.cityId) ? User.City.cityId : "Selecteer Woonplaats"}>
														<option value="0" disabled>Selecteer Woonplaats</option>
														{Object.keys(Cities).map(function(key) {
															const city = Cities[key].node;
															return (
																<option key={key} value={city.cityId}>{city.name}</option>
															)
														})}
													</select>
													<div className="caret"></div>
												</div>
												<div className="col-xs-12 col-sm-6">
													<select ref="age" defaultValue={(User.age) ? User.age : "Selecteer geboortejaar"}>
														<option value="0">Selecteer geboortejaar</option>
															{Object.keys(years).map(function(key) {
																const year = years[key];
																return (
																	<option value={year} key={key}>{year}</option>
																)
															})}
													</select>
													<div className="caret"></div>
												</div>
												<div className="col-xs-12 col-sm-6">
													<select ref="gender" defaultValue={(User.gender) ? User.gender : 0}>
														<option value="0" disabled>Selecteer Geslacht</option>
														<option value="f">Vrouw</option>
														<option value="m">Man</option>
													</select>
													<div className="caret"></div>
												</div>
												<div className="col-xs-12 col-sm-6">
													<select ref="education" defaultValue={(User.Education) ? User.Education.educationId : 0}>
														<option value="0" disabled>Selecteer opleidingsniveau</option>
														{Object.keys(Educations).map(function(key) {
															const education = Educations[key].node;
															return (
																<option key={key} value={education.educationId}>{education.value}</option>
															)
														})}
													</select>
													<div className="caret"></div>
												</div>
												<div className="col-xs-12 col-sm-6">
													<select ref="income" defaultValue={(User.Income) ? User.Income.incomeId : 0}>
														<option value="0" disabled>Selecteer inkomen</option>
														{Object.keys(Incomes).map(function(key) {
															const income = Incomes[key].node;
															return (
																<option key={key} value={income.incomeId}>{income.value}</option>
															)
														})}
													</select>
													<div className="caret"></div>
												</div>
												<div className="col-xs-12 col-sm-6">
													<select ref="maritalStatus" defaultValue={(User.MaritalStatus) ? User.MaritalStatus.value : 0}>
														<option value="0" disabled>Selecteer burgerlijke staat</option>
														{Object.keys(MaritalStatuses).map(function(key) {
															const maritalStatus = MaritalStatuses[key].node;
															return (
																<option key={key} value={maritalStatus.maritalStatusId}>{maritalStatus.value}</option>
															)
														})}
													</select>
													<div className="caret"></div>
												</div>
												<div className="col-xs-12 col-sm-6">
													<select ref="children" defaultValue={(User.children) ? User.children : 0}>
														<option value="0" disabled>Selecteer kinderen</option>
														<option value="n">Geen kinderen</option>
														<option value="y">Wel kinderen</option>
													</select>
													<div className="caret"></div>
												</div>
											</div>
										</div>
										<div className="dotted extra-side-margin"></div>
										<div>
											<div className="title color">Wachtwoord aanpassen
												<span className="space-left color-grey">
													Laat de velden leeg als u uw wachtwoord niet wenst aan te passen
													{/* <i className="fa fa-question-circle space-left"></i> */}
												</span>
											</div>
											<div className="row">
												<div className="col-xs-12">
													<input ref="curPassword" type="password" placeholder="Huidige wachtwoord"/>
												</div>
												<div className="col-xs-12 col-sm-6">
													<input ref="newPassword" type="password" placeholder="Nieuw wachtwoord"/>
												</div>
												<div className="col-xs-12 col-sm-6">
													<input ref="conPassword" type="password" placeholder="Nieuw wachtwoord bevestigen"/>
												</div>
											</div>
										</div>
										<div className="btn-holder">
											{User.verified === 1 ?
												<span className="color-accent"><i className="fa fa-check-circle" aria-hidden="true"></i><span className="space-left">U bent geverifeerd!</span></span>
												:
												<Link to="/info/verifieren" className="color-accent">
													<i className="fa fa-check-circle" aria-hidden="true"></i>
													<span className="space-left">Profiel laten verifiëren?</span>
												</Link>
											}
											<button type="submit" className="btn primary-btn right">Opslaan</button>
										</div>
									</form>
								</div>
							</div>
							<div className="delete-acount blue-link" onClick={this.confirm.bind(this)}>Account deactiveren</div>
						</div>
						{/*Side panel*/}
						<div className="col-md-3 hidden-xs hidden-sm">
							<Trending questions={viewer}/>
							<Footer/>
						</div>
					</div>
				</div>
				<Confirm show={this.state.showConfirm} onClick={this.confirmSucces.bind(this)} title="Weet u het zeker?" msg="U staat op het punt uw account te deactiveren, wilt u doorgaan?"/>
			</div>
		);
	}
};
EditProfilePage.contextTypes = {
	router: React.PropTypes.object
};
export default Relay.createContainer(EditProfilePage, {
		initialVariables: {
			username: null,
		},
		fragments: {
			viewer: () => Relay.QL `
				fragment on viewer {

					${ProfilePicMutation.getFragment('viewer')}
					${ProfileMutation.getFragment('viewer')},
					${Trending.getFragment('questions')},
					${Menu.getFragment('userData')},
					${Header.getFragment('notifications')},
					${AddTag.getFragment('viewer')},
					AuthUser{
						Interests(first:100){
							edges{
								node{
									Tag{
										value
										key
										tagValueId
									}
								}
							}
						}
						email,
						userName,
						firstName,
						lastName,
						avatar,
						function,
						bio,
						gender,
						children,
						verified,
						age,
						Education {
							value,
							educationId,
						},
						Income {
							value,
							incomeId,
						},
						MaritalStatus {
							value,
							maritalStatusId,
						},
						City {
							name,
							cityId,
						},
					},
					Cities(first:5000){
						edges{
							node{
								name,
								cityId,
							},
						},
					},
					Incomes(first:50){
						edges{
							node{
								value,
								incomeId,
							},
						},
					},
					MaritalStatuses(first:50){
						edges{
							node{
								value,
								maritalStatusId,
							},
						},
					},
					Educations(first:50){
						edges{
							node{
								value,
								educationId,
							},
						},
					},

				},
			`
		},
});
