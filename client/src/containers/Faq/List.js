import React, { Component } from 'react';
import Relay from 'react-relay';
import {Link} from 'react-router';
import DocumentMeta from 'react-document-meta';

import Footer from '../../components/sidebar/Footer';
import InfoPages from '../../components/sidebar/InfoPages';
import Header from '../../components/Header/Header';
import Menu from '../../components/Menu/Menu';

class List extends Component {

	render() {
		const meta = {
			title: "Veel gestelde vragen | Burgerzetel",
			description: "Overzicht van de meest gestelde vragen op Burgerzetel.nl",
			canonical: 'http://burgerzetel.nl/faq/overzicht',
			meta: {
				charset: 'utf-8',
			}
		};

		let {viewer} = this.props;
		let algemeen = {};
		let huisregels = {};
		let burgerzetel = {};
		if(viewer.algemeen){
			algemeen = viewer.algemeen.edges;
		}
		if(viewer.huisregels){
			huisregels = viewer.huisregels.edges;
		}
		if(viewer.burgerzetel){
			burgerzetel = viewer.burgerzetel.edges;
		}
		return (
			<div>
				<DocumentMeta {...meta} />
				<Header notifications={viewer}/>
				<div className="container">
					<div className="row">
						<Menu userData={viewer}/>
						{/*Main panel*/}
						<div className="col-xs-12 col-sm-8 col-md-6 no-padding padding-sm center-panel">
							<div className="panel padding">
								<h1 className="color">Veel gestelde vragen</h1>
								<div className="dotted"></div>
								<h3 className="color-accent">Algemeen</h3>
								{Object.keys(algemeen).map((key) => {
									let data = algemeen[key].node.Question;
									return (
										<Link to={"/vraag/"+data.slug} className="blue-link" key={key}>{data.title}</Link>
									)
								})}
								<h3 className="color-accent">Huisregels</h3>
								{Object.keys(huisregels).map((key) => {
									let data = huisregels[key].node.Question;
									return (
										<Link to={"/vraag/"+data.slug} className="blue-link" key={key}>{data.title}</Link>
									)
								})}
								<h3 className="color-accent">Burgerzetel</h3>
								{Object.keys(burgerzetel).map((key) => {
									let data = burgerzetel[key].node.Question;
									return (
										<Link to={"/vraag/"+data.slug} className="blue-link" key={key}>{data.title}</Link>
									)
								})}
							</div>
						</div>
						{/*Side panel*/}
						<div className="col-md-3 hidden-xs hidden-sm">
							<InfoPages/>
							<Footer/>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default Relay.createContainer(List, {
	fragments: {
		viewer: () => Relay.QL `
		fragment on viewer {
			${Menu.getFragment('userData')},
			${Header.getFragment('notifications')},
			algemeen: Faqs(category:"1",first: 100){
				edges{
					node{
						Question{
							title,
							slug,
						},
					},
				},
			},
			huisregels: Faqs(category:"2",first: 100){
				edges{
					node{
						Question{
							title,
							slug,
						},
					},
				},
			},
			burgerzetel: Faqs (category:"3",first: 100){
				edges{
					node{
						Question{
							title,
							slug,
						},
					},
				},
			},
		},
		`
	}

});
