let $ = require('jquery');

module.exports = {
  get(url,accessToken) {
    return new Promise(function(success, error) {
      $.ajax({
        url: url,
        dataType: "json",
        success,
        error
      });
    });
  },
  del(url, accessToken) {
    return new Promise(function(success, error) {
      $.ajax({
        url: url,
        type: 'DELETE',
        success,
        error
      })
    })
  },
  post(url, data, accessToken) {
    return new Promise(function(success, error) {
      $.ajax({
        url,
        type: 'POST',
        data,
        success,
        error
      })
    })
  },
  put(url, data, accessToken) {
    return new Promise(function(success, error) {
      $.ajax({
        url,
        type: 'PUT',
        data,
        success,
        error
      })
    })
  }
}
