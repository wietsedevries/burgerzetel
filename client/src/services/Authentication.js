/* eslint-disable */
let $ = require('jquery');
let {get, post, del, put} = require("./RestHelper.js");
let base = "/";
module.exports = {
	login(data, cb) {
			cb = arguments[arguments.length - 1];
			let post_path;
			if (data.social) {
				post_path = "/auth/socialSignin";
			} else {
				post_path = "/auth/signin";
			}
			post(base+post_path, data)
				.then((data) => {
					console.log(data);
					if(data.status === 200){
						localStorage.token = data.token;
						if(cb){
							cb(true);
						}
						location.reload();
					}else{
						if(cb){
							cb(false, data.errorMsg);
						}
					}
				});
		},

		getToken() {
			return (typeof window !== "undefined") ? localStorage.token : undefined;
		},

		logout(cb) {
			delete localStorage.token;
			delete localStorage.userName;
			delete localStorage.firstName;
			delete localStorage.lastName;
			delete localStorage.avatar;
			delete localStorage.reputation;
		},

		loggedIn() {
			return !!((typeof window !== "undefined") ? localStorage.token : undefined)
		},

		onChange() {},

		randomString(length) {
			var text = "";
			var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
			for(var i = 0; i < length; i++) {
					text += possible.charAt(Math.floor(Math.random() * possible.length));
			}
			return text;
		},
	
}
