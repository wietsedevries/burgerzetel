import React from 'react';
import {Router, Route} from 'react-router';
import Relay from 'react-relay';
import $ from 'jquery';

import HomePage from './containers/viewer/HomePage';
import ProfilePage from './containers/User/ProfilePage';
import QuestionPage from './containers/Question/QuestionPage';
import TagPage from './containers/Tag/TagPage';
import ReputationPage from './containers/Reputation/ReputationPage';
import EditProfilePage from './containers/User/EditProfilePage';
import SettingPage from './containers/User/SettingPage';
import InfoPage from './containers/Info/InfoPage';
import Verify from './containers/Info/Verify';
import CreateQuestionPage from './containers/Question/CreateQuestionPage';
import SavedQuestionsPage from './containers/Question/SavedQuestionsPage';
import NotificationsPage from './containers/Notifications/NotificationsPage';
import AnswerPage from './containers/Notifications/AnswerPage';
import FlaggedPage from './containers/Notifications/FlaggedPage';
import ReactionPage from './containers/Notifications/ReactionPage';
import TopicPage from './containers/TopicPage/TopicPage';
import Error404 from './containers/error/Error404';
import Error403 from './containers/error/Error403';
import ContactForm from './containers/Contact/ContactForm';
import Faq from './containers/Faq/List';
import PasswordResetPage from './containers/User/PasswordResetPage';

var ViewerQueries = {
	viewer: () => Relay.QL`
		query { viewer }
	`,
};
var actions = {
	load() {
		window.scrollTo(0, 0)
		$("#loader").css({'top':'0','width':'0px'});
		$("#loader").animate({
				width: "100%"
			}, 1400, function() {
				$("#loader").animate({
				top: "-3px"
			}, 200, function() {
				$("#loader").css('top','-3px');
			});
		});
		actions.setTestEnv();
	},
	setTestEnv() {
		if(!localStorage.enabled){
			let scene = Math.random() >= 0.5;
			localStorage.enabled = scene;
		}
	},
}

function loggedIn() {
	if(localStorage.authToken){
		return true;
	}else{
		return false;
	}
}

function requireAuth(nextState, replace) {
	if (!loggedIn()) {
		replace({
			pathname: '/error/403'
		})
	}
}

export default (
	<Router onChange={actions.load} >
		<Route path="/" component={HomePage} queries={ViewerQueries} loggedInUser={loggedIn} onEnter={actions.setTestEnv}/>
		<Route path="/vraag/nieuw" component={CreateQuestionPage} queries={ViewerQueries} onEnter={requireAuth} loggedInUser={loggedIn}/>
		<Route path="/vraag/nieuw/:username" component={CreateQuestionPage} queries={ViewerQueries} onEnter={requireAuth} loggedInUser={loggedIn}/>
		<Route path="/vraag/:slug" component={QuestionPage} queries={ViewerQueries} loggedInUser={loggedIn}/>
		<Route path="/contact" component={ContactForm} queries={ViewerQueries} loggedInUser={loggedIn}/>
		<Route path="/contact/feedback" component={ContactForm} queries={ViewerQueries} loggedInUser={loggedIn}/>
		<Route path="/wachtwoord/herstellen" component={PasswordResetPage} queries={ViewerQueries} loggedInUser={loggedIn}/>
		<Route path="/wachtwoord/:hash" component={PasswordResetPage} queries={ViewerQueries} loggedInUser={loggedIn}/>
		<Route path="/:username" component={ProfilePage} queries={ViewerQueries} loggedInUser={loggedIn}/>
		<Route path="/onderwerp/:tag" component={TagPage} queries={ViewerQueries} loggedInUser={loggedIn}/>
		<Route path="/onderwerpen/overzicht" component={TopicPage} queries={ViewerQueries} loggedInUser={loggedIn}/>
		<Route path="/:username/reputatie" component={ReputationPage} queries={ViewerQueries} loggedInUser={loggedIn}/>
		<Route path="/:username/wijzigen" component={EditProfilePage} queries={ViewerQueries} onEnter={requireAuth} loggedInUser={loggedIn}/>
		<Route path="/:username/instellingen" component={SettingPage} queries={ViewerQueries} onEnter={requireAuth} loggedInUser={loggedIn}/>
		<Route path="/info/:slug" component={InfoPage} queries={ViewerQueries} loggedInUser={loggedIn}/>
		<Route path="/account/verifieren" component={Verify} queries={ViewerQueries} loggedInUser={loggedIn} onEnter={requireAuth}/>
		<Route path="/:slug/wijzig" component={CreateQuestionPage} queries={ViewerQueries} onEnter={requireAuth} loggedInUser={loggedIn}/>
		<Route path="/:username/opgeslagen" component={SavedQuestionsPage} queries={ViewerQueries} onEnter={requireAuth} loggedInUser={loggedIn}/>
		<Route path="/notificaties/overzicht" component={NotificationsPage} queries={ViewerQueries} onEnter={requireAuth} loggedInUser={loggedIn}/>
		<Route path="/notificaties/gerapporteerd/:id" component={FlaggedPage} queries={ViewerQueries} onEnter={requireAuth} loggedInUser={loggedIn}/>
		<Route path="/notificaties/antwoord/:id" component={AnswerPage} queries={ViewerQueries} onEnter={requireAuth} loggedInUser={loggedIn}/>
		<Route path="/notificaties/reactie/:id" component={ReactionPage} queries={ViewerQueries} onEnter={requireAuth} loggedInUser={loggedIn}/>
		<Route path="/faq/overzicht" component={Faq} queries={ViewerQueries} loggedInUser={loggedIn}/>
		<Route path="/error/404" component={Error404} queries={ViewerQueries} loggedInUser={loggedIn}/>
		<Route path="/error/403" component={Error403} queries={ViewerQueries} loggedInUser={loggedIn}/>
		<Route path="*" component={Error404} queries={ViewerQueries} loggedInUser={loggedIn}/>
	</Router>
);
