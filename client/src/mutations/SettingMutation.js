import Relay from 'react-relay';

export default class SettingMutation extends Relay.Mutation {

	static fragments = {
		viewer: () => Relay.QL`
			fragment on viewer {
				id
				AuthUser
			}
		`,
	};

	getMutation() {
		return Relay.QL`mutation {SettingMutation}`;
	}

	getFatQuery() {
		return Relay.QL`
			fragment on SettingPayload {
				viewer{
					AuthUser

				}
			}
		`;
	}

	getConfigs() {
		return [
			{
				type: 'FIELDS_CHANGE',
				fieldIDs: {
					viewer: this.props.viewer.id,
				},
			},
		];
	}

	getVariables() {
		return {
			key: this.props.key,
			set: this.props.set,
		};
	}
}
