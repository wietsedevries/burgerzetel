import Relay from 'react-relay';

export default class AnswerMutation extends Relay.Mutation {

	static fragments = {
		viewer: () => Relay.QL`
			fragment on viewer {
				id,
				AuthUser,
			}
		`,
	};

	getMutation() {
		return Relay.QL`mutation {AnswerMutation}`;
	}

	getFatQuery() {
		return Relay.QL`
			fragment on AnswerPayload {
				viewer
			}
		`;
	}

	getConfigs() {
		return [
			{
				type: 'FIELDS_CHANGE',
				fieldIDs: {
					viewer: this.props.viewer.id,
				},
			},
		];
	}

	getVariables() {
		return {
			questionId: this.props.questionId,
			description: this.props.description,
			slug: this.props.slug,
		};
	}
}
