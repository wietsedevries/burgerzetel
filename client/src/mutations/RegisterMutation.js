import Relay from 'react-relay';

export default class RegisterMutation extends Relay.Mutation {

	static fragments = {
		viewer: () => Relay.QL`
			fragment on viewer {
				id
				AuthUser
			}
		`,
	};

	getMutation() {
		return Relay.QL`mutation { RegisterMutation }`;
	}

	getFatQuery() {
		return Relay.QL`
			fragment on RegisterPayload {
				viewer {
					id
					AuthUser
				}
			}
		`;
	}

	getConfigs() {
		return [
			{
				type: 'FIELDS_CHANGE',
				fieldIDs: {
					viewer: this.props.viewer.id
				},
			},
			{
				type: 'REQUIRED_CHILDREN',
				children: [
					Relay.QL`
						fragment on RegisterPayload {
							email,
							message,
							authToken,
						}
					`,
				],
			},
		];
	}

	getVariables() {
		return {
			email: this.props.email,
			userName: this.props.userName,
			code: this.props.code,
			firstName: this.props.firstName,
			lastName: this.props.lastName,
			cityId: this.props.cityId,
			password: this.props.password,
			invites: this.props.invites,
		};
	}
}
