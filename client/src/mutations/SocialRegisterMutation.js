import Relay from 'react-relay';

export default class SocialRegisterMutation extends Relay.Mutation {

	static fragments = {
		viewer: () => Relay.QL`
			fragment on viewer {
				id
			}
		`,
	};

	getMutation() {
		return Relay.QL`mutation { SocialRegisterMutation }`;
	}

	getFatQuery() {
		return Relay.QL`
			fragment on SocialRegisterPayload {
				viewer {
					id
				}
			}
		`;
	}

	getConfigs() {
		return [
			{
				type: 'FIELDS_CHANGE',
				fieldIDs: {
					viewer: this.props.viewer.id
				},
			},
			{
				type: 'REQUIRED_CHILDREN',
				children: [
					Relay.QL`

						fragment on SocialRegisterPayload {
							email
							message

						}
					`,
				],
			},
		];
	}

	getVariables() {
		return {
			email: this.props.email,
			userName: this.props.userName,
			firstName: this.props.firstName,
			lastName: this.props.lastName,
			cityId: this.props.cityId,
			password: this.props.password,
			facebookId: this.props.facebookId,
			linkedinId: this.props.linkedinId,
			googleId: this.props.googleId,
		};
	}
}
