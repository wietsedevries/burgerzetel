import Relay from 'react-relay';

export default class ProfileMutation extends Relay.Mutation {

	static fragments = {
		viewer: () => Relay.QL`
			fragment on viewer {
				id,
				AuthUser,
			},
		`,
	};

	getMutation() {
		return Relay.QL`mutation { ProfileMutation }`;
	}

	getFatQuery() {
		return Relay.QL`
			fragment on ProfilePayload {
				viewer
			}
		`;
	}

	getConfigs() {
		return [
			{
				type: 'FIELDS_CHANGE',
				fieldIDs: {
					viewer: this.props.viewer.id,
				},
			},
			{
				type: 'REQUIRED_CHILDREN',
				children: [
					Relay.QL`
					
						fragment on ProfilePayload {
							error
						}
					`,
				],
			},
		];
	}

	getVariables() {
		return {
			firstName: this.props.firstName,
			lastName: this.props.lastName,
			functio: this.props.functio,
			bio: this.props.bio,
			city: this.props.city,
			education: this.props.education,
			age: this.props.age,
			maritalStatus: this.props.maritalStatus,
			children: this.props.children,
			income: this.props.income,
			gender: this.props.gender,
			tags: this.props.tags,
			password: this.props.password,
			newPassword: this.props.newPassword,
		};
	}
}
