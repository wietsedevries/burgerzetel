import Relay from 'react-relay';

export default class ReactionMutation extends Relay.Mutation {

	static fragments = {
			viewer: () => Relay.QL`
				fragment on viewer {
					id
					AuthUser
				}
			`,
	};

	getMutation() {
		return Relay.QL`mutation {ReactionMutation}`;
	}

	getFatQuery() {
		return Relay.QL`
			fragment on ReactionPayload {
				viewer 
			}
		`;
	}

	getConfigs() {
		return [
			{
				type: 'FIELDS_CHANGE',
				fieldIDs: {
					viewer: this.props.viewer.id,
				},
			},
		];
	}

	getVariables() {
		return {
			answerId: this.props.answerId,
			description: this.props.description,
		};
	}
}
