import Relay from 'react-relay';

export default class VerifyMutation extends Relay.Mutation {

	static fragments = {
		viewer: () => Relay.QL`
			fragment on viewer {
				id
			}
		`,
	};

	getMutation() {
		return Relay.QL`mutation {VerifyMutation}`;
	}

	getFatQuery() {
		return Relay.QL`
			fragment on VerifyPayload {
				viewer
			}
		`;
	}

	getConfigs() {
		return [
			{
				type: 'FIELDS_CHANGE',
				fieldIDs: {
					viewer: this.props.viewer.id,
				},
			},
		];
	}

	getVariables() {
		return {
			username: this.props.username,

		};
	}
}
