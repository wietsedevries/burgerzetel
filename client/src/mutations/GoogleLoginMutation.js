import Relay from 'react-relay';

export default class GoogleLoginMutation extends Relay.Mutation {

	static fragments = {
		viewer: () => Relay.QL`
			fragment on viewer {
				id
				AuthUser
			}
		`,
	};

	getMutation() {
		return Relay.QL`mutation { GoogleLoginMutation }`;
	}

	getFatQuery() {
		return Relay.QL`
			fragment on GoogleLoginPayload {
				viewer {
					AuthUser

				}
			}
		`;
	}

	getConfigs() {
		return [
			{
				type: 'FIELDS_CHANGE',
				fieldIDs: {
					viewer: this.props.viewer.id,
				},
			},
			{
				type: 'REQUIRED_CHILDREN',
				children: [
					Relay.QL`
						fragment on GoogleLoginPayload {
							authToken,
							msg
						}
					`,
				],
			},
		];
	}

	getVariables() {
		return {
			googleAuthToken: this.props.googleAuthToken,
		};
	}
}
