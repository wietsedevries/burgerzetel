import Relay from 'react-relay';

export default class ReplyMutation extends Relay.Mutation {

	static fragments = {
			viewer: () => Relay.QL`
				fragment on viewer {
					id
					AuthUser
				}
			`,
	};

	getMutation() {
		return Relay.QL`mutation {ReplyMutation}`;
	}

	getFatQuery() {
		return Relay.QL`
			fragment on ReplyPayload {
				viewer
			}
		`;
	}

	getConfigs() {
		return [
			{
				type: 'FIELDS_CHANGE',
				fieldIDs: {
					viewer: this.props.viewer.id,
				},
			},
		];
	}

	getVariables() {
		return {
			reactionId: this.props.reactionId,
			description: this.props.description,
		};
	}
}
