import Relay from 'react-relay';

export default class ResetPasswordLinkMutation extends Relay.Mutation {

	static fragments = {
		viewer: () => Relay.QL`
			fragment on viewer {
				id
			}
		`,
	};

	getMutation() {
		return Relay.QL`mutation { ResetPasswordLinkMutation }`;
	}

	getFatQuery() {
		return Relay.QL`
			fragment on ResetPasswordLinkPayload {
				viewer 
			}
		`;
	}

	getConfigs() {
		return [
			{
				type: 'FIELDS_CHANGE',
				fieldIDs: {
					viewer: this.props.viewer.id
				},
			},
			{
				type: 'REQUIRED_CHILDREN',
				children: [
					Relay.QL`
						fragment on ResetPasswordLinkPayload {
							message
						}
					`,
				],
			},
		];
	}

	getVariables() {
		return {
			email: this.props.email,
		};
	}
}
