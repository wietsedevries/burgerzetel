import Relay from 'react-relay';

export default class QuestionMutation extends Relay.Mutation {

	static fragments = {
		viewer: () => Relay.QL`
			fragment on viewer {
				id,
				AuthUser,
			}
		`,
	};

	getMutation() {
		return Relay.QL`mutation { QuestionMutation }`;
	}

	getFatQuery() {
		return Relay.QL`
			fragment on QuestionPayload {
				viewer
			}
		`;
	}

	getConfigs() {
		return [
			{
				type: 'FIELDS_CHANGE',
				fieldIDs: {
					viewer: this.props.viewer.id,
				},
			},
			{
				type: 'REQUIRED_CHILDREN',
				children: [
					Relay.QL`
						fragment on QuestionPayload {
							slug,
							
						}
					`,
				],
			},
		];
	}

	getVariables() {
		return {
			question: this.props.question,
			description: this.props.description,
			tags: this.props.tags,
			invited: this.props.invited,
			pollTitle: this.props.pollTitle,
			pollItems: this.props.pollItems,
		};
	}
}
