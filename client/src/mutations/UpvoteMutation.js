import Relay from 'react-relay';

export default class UpvoteMutation extends Relay.Mutation {

	static fragments = {
		viewer: () => Relay.QL`
			fragment on viewer {
				id
				AuthUser
				
			}
		`,
	};

	getMutation() {
		return Relay.QL`mutation { UpvoteMutation }`;
	}

	getFatQuery() {
		return Relay.QL`
			fragment on UpvotePayload {
				viewer {
					AuthUser
				}
			}
		`;
	}

	getConfigs() {
		return [
			{
				type: 'FIELDS_CHANGE',
				fieldIDs: {
					viewer: this.props.viewer.id,
				},
			}
		];
	}

	getVariables() {
		return {
			id: this.props.id,
			type: this.props.type,
		};
	}
}
