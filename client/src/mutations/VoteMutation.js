import Relay from 'react-relay';

export default class VoteMutation extends Relay.Mutation {

	static fragments = {
		viewer: () => Relay.QL`
			fragment on viewer {
				id
				AuthUser
			}
		`,
	};

	getMutation() {
		return Relay.QL`mutation { VoteMutation }`;
		
	}

	getFatQuery() {
		return Relay.QL`
			fragment on VotePayload {
				viewer

			}
		`;
	}

	getConfigs() {
		return [
			{
				type: 'FIELDS_CHANGE',
				fieldIDs: {
					viewer: this.props.viewer.id,
				},
			},
		];
	}

	getVariables() {
		return {
			optionId: this.props.optionId,
		};
	}
}
