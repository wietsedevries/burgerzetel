import Relay from 'react-relay';

export default class ProfilePicMutation extends Relay.Mutation {

	static fragments = {
		viewer: () => Relay.QL`
			fragment on viewer {
				id,
				AuthUser,

			},
		`,
	};

	getMutation() {
		return Relay.QL`mutation { ProfilePicMutation }`;
	}

	getFatQuery() {
		return Relay.QL`
			fragment on ProfilePicPayload {
				viewer {
					AuthUser,
				},
			}
		`;
	}

	getConfigs() {
		return [
			{
				type: 'FIELDS_CHANGE',
				fieldIDs: {
					viewer: this.props.viewer.id,
				},
			},
		];
	}

	getVariables() {
		return {
			avatar: this.props.avatar,
		};
	}
}
