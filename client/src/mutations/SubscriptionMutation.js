import Relay from 'react-relay';

export default class SubscriptionMutation extends Relay.Mutation {

	static fragments = {
		viewer: () => Relay.QL`
			fragment on viewer {
				id,
				AuthUser
			}
		`,
	};

	getMutation() {
		return Relay.QL`mutation {SubscriptionMutation}`;
	}

	getFatQuery() {
		return Relay.QL`
			fragment on SubscriptionPayload {
				viewer {
					AuthUser
				}
			}
		`;
	}

	getConfigs() {
		return [
			{
				type: 'FIELDS_CHANGE',
				fieldIDs: {
					viewer: this.props.viewer.id,
				},
			},
		];
	}

	getVariables() {
		return {
			tagValueId: this.props.tagValueId,
			state: this.props.state,
		};
	}
}
