import Relay from 'react-relay';

export default class LinkedinLoginMutation extends Relay.Mutation {

	static fragments = {
		viewer: () => Relay.QL`
			fragment on viewer {
				id
				AuthUser
			}
		`,
	};

	getMutation() {
		return Relay.QL`mutation { LinkedinLoginMutation }`;
	}

	getFatQuery() {
		return Relay.QL`
			fragment on LinkedinLoginPayload {
				viewer {
					AuthUser

				}
			}
		`;
	}

	getConfigs() {
		return [
			{
				type: 'FIELDS_CHANGE',
				fieldIDs: {
					viewer: this.props.viewer.id,
				},
			},
			{
				type: 'REQUIRED_CHILDREN',
				children: [
					Relay.QL`
						fragment on LinkedinLoginPayload {
							authToken,
							msg
						}
					`,
				],
			},
		];
	}

	getVariables() {
		return {
			linkedinAuthToken: this.props.linkedinAuthToken,
			linkedinState: this.props.linkedinState,
			redirectUri: this.props.redirectUri,
		};
	}
}
