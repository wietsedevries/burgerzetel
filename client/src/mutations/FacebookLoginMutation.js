import Relay from 'react-relay';

export default class FacebookLoginMutation extends Relay.Mutation {

	static fragments = {
		viewer: () => Relay.QL`
			fragment on viewer {
				id
				AuthUser
				
			}
		`,
	};

	getMutation() {
		return Relay.QL`mutation { FacebookLoginMutation }`;
	}

	getFatQuery() {
		return Relay.QL`
			fragment on FacebookLoginPayload {
				viewer {
					AuthUser

				}
			}
		`;
	}

	getConfigs() {
		return [
			{
				type: 'FIELDS_CHANGE',
				fieldIDs: {
					viewer: this.props.viewer.id,
				},
			},
			{
				type: 'REQUIRED_CHILDREN',
				children: [
					Relay.QL`
						fragment on FacebookLoginPayload {
							authToken,
							msg,

						}
					`,
				],
			},
		];
	}

	getVariables() {
		return {
			facebookAuthToken: this.props.facebookAuthToken,
		};
	}
}
