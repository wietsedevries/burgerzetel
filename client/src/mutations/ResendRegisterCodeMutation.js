import Relay from 'react-relay';

export default class ResendRegisterCodeMutation extends Relay.Mutation {

	static fragments = {
		viewer: () => Relay.QL`
			fragment on viewer {
				id,
			}
		`,
	};

	getMutation() {
		return Relay.QL`mutation { ResendRegisterCodeMutation }`;
	}

	getFatQuery() {
		return Relay.QL`
			fragment on ResendRegisterCodePayload {
				viewer {
					id,
				}
			}
		`;
	}

	getConfigs() {
		return [
			{
				type: 'FIELDS_CHANGE',
				fieldIDs: {
					viewer: this.props.viewer.id,
				},
			},
		];
	}

	getVariables() {
		return {
			email: this.props.email,
		};
	}
}
