import Relay from 'react-relay';

export default class ReportMutation extends Relay.Mutation {

	static fragments = {
		viewer: () => Relay.QL`
			fragment on viewer {
				id
				AuthUser
				
			}
		`,
	};

	getMutation() {
		return Relay.QL`mutation { ReportMutation }`;
	}

	getFatQuery() {
		return Relay.QL`
			fragment on ReportPayload {
				viewer {
					AuthUser{
						FlaggedReactions(first:100000){
							edges{
								node{
									itemId
								}
							}
						}
						FlaggedReplies(first:100000){
							edges{
								node{
									itemId
								}
							}
						}
						FlaggedQuestions(first:100000){
							edges{
								node{
									itemId
								}
							}
						}
						FlaggedAnswers(first:100000){
							edges{
								node{
									itemId
								}
							}
						}
						NotificationCount,
						Notifications(first:12){
							edges{
								node{
									nid,
									dateCreated,
									NotificationType{
										value,
										key,
									},
									User{
										firstName,
										lastName,
										avatar,
										userName,
									},
									AskedBy{
										firstName,
										lastName,
										avatar,
										userName,
									},
									Reply{
										reply,
										User{
											avatar,
											firstName,
											lastName,
											userName,
										},
									},
									Reaction{
										rid,
										reaction,
										User{
											avatar,
											firstName,
											lastName,
											userName,
										},
									},
									Answer{
										aid,
										answer,
										User{
											avatar,
											firstName,
											lastName,
											userName,
										},
									},
									Question{
										title,
										slug,
									},
								},
							},
						},
					}
				}
			}
		`;
	}

	getConfigs() {
		return [
			{
				type: 'FIELDS_CHANGE',
				fieldIDs: {
					viewer: this.props.viewer.id,
				},
			}
		];
	}

	getVariables() {
		return {
			type: this.props.type,
			id: this.props.id,
			reputation: this.props.reputation,
		};
	}
}
