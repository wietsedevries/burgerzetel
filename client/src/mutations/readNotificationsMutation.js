import Relay from 'react-relay';

export default class readNotificationsMutation extends Relay.Mutation {

	static fragments = {
		viewer: () => Relay.QL`
			fragment on viewer {
				id
				AuthUser

			}
		`,
	};

	getMutation() {
		return Relay.QL`mutation {readNotificationsMutation}`;
	}

	getFatQuery() {
		return Relay.QL`
			fragment on readNotificationsPayload {
				viewer {
					AuthUser

				}
			}
		`;
	}

	getConfigs() {
		return [
			{
				type: 'FIELDS_CHANGE',
				fieldIDs: {
					viewer: this.props.viewer.id

				},
			},
		];
	}

	getVariables() {
		return {
			id: this.props.id
			
		};
	}
}
