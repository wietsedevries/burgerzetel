import Relay from 'react-relay';

export default class LoginMutation extends Relay.Mutation {

	static fragments = {
		viewer: () => Relay.QL`
			fragment on viewer {
				id
				AuthUser
			}
		`,
	};

	getMutation() {
		return Relay.QL`mutation { LoginMutation }`;
	}

	getFatQuery() {
		return Relay.QL`
			fragment on LoginPayload {
				viewer {
					AuthUser
				}
			}
		`;
	}

	getConfigs() {
		return [
			{
				type: 'FIELDS_CHANGE',
				fieldIDs: {
					viewer: this.props.viewer.id,
				},
			},
			{
				type: 'REQUIRED_CHILDREN',
				children: [
					Relay.QL`
						fragment on LoginPayload {
							authToken
						}
					`,
				],
			},
		];
	}

	getVariables() {
		return {
			email: this.props.email,
			password: this.props.password,
		};
	}
}
