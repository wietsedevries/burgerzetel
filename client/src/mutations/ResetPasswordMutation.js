import Relay from 'react-relay';

export default class ResetPasswordMutation extends Relay.Mutation {

	static fragments = {
		viewer: () => Relay.QL`
			fragment on viewer {
				id

			}
		`,
	};

	getMutation() {
		return Relay.QL`mutation { ResetPasswordMutation }`;
	}

	getFatQuery() {
		return Relay.QL`
			fragment on ResetPasswordPayload {
				viewer {
					id

				}
			}
		`;
	}

	getConfigs() {
		return [
			{
				type: 'FIELDS_CHANGE',
				fieldIDs: {
					viewer: this.props.viewer.id
				},
				
			}
		];
	}

	getVariables() {
		return {
			userId: this.props.userId,
			password: this.props.password,
			hash: this.props.hash,
		};
	}
}
