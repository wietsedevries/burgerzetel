import Relay from 'react-relay';

export default class EditQuestionMutation extends Relay.Mutation {

	static fragments = {
		viewer: () => Relay.QL`
			fragment on viewer {

				id
				AuthUser

			}
		`,
	};

	getMutation() {
		return Relay.QL`mutation { EditQuestionMutation }`;
	}

	getFatQuery() {
		return Relay.QL`
			fragment on EditQuestionPayload {
				viewer
			}
		`;
	}

	getConfigs() {
		return [
			{
				type: 'FIELDS_CHANGE',
				fieldIDs: {
					viewer: this.props.viewer.id,
				},
			},
		];
	}

	getVariables() {
		return {
			question: this.props.question,
			description: this.props.description,
			tags: this.props.tags,
			invited: this.props.invited,
			pollTitle: this.props.pollTitle,
			pollItems: this.props.pollItems,
			slug: this.props.slug,
			questionId: this.props.questionId,
		};
	}
}
