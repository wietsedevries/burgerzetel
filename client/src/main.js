import $ from 'jquery';
import passwordValidator from 'password-validator';

module.exports = {
	login: function () {
		document.getElementById('loginBox').className = 'popup open';
	},
	register: function () {
		document.getElementById('loginBox').className = 'popup ';
		document.getElementById('registerBox').className = 'popup open';
	},
	toast: function (title, message, type = "success") {
		const toaster = $('#toaster');
		let icon;
		switch (type) {
			case "warning":
			  icon = "fa-info-circle";
			  break;
			case "error":
			  icon = "fa-times-circle";
			  break;
			case "success":
			  icon = "fa-check-circle";
			  break;
			case "rep":
			  icon = "fa-trophy";
			  break;
			default:
			  icon = "fa-check-circle";
			  break;
			}
		const sandwichFilling = "<div class='image'><i class='fa "+icon+"'></i></div><div class='title bold'>"+title+"</div><div class='message'>"+message+"</div>";
		const toast = $("<div class='toast "+type+"'>"+sandwichFilling+"</div>");
		toast.appendTo(toaster);
		setTimeout(function() {
			const fadeTime = 500;
			toast.fadeOut(fadeTime);
			setTimeout(function() {
				toast.remove();
			},fadeTime)
		},3000)
	},
	validatePassword: function (pass) {
		var schema = new passwordValidator();
		schema.isMin(8).digits().letters();
		if (!schema.validate(pass)) {
			return false;
		}
		return true;
	},
	censor: function (text) {
		let nonos = [
			" fuck ",
			" kut ",
			" hoer ",
			" eikel ",
			" klote ",
			" slet ",
			" klootzak ",
			" lul ",
			" bitch ",
			" kutwijf ",
			" kutzooi ",
			" downie "
		]
		for (var i = 0; i < nonos.length; i++){
			text = text.replace(nonos[i], " *** ");
		}
		return text;
	},
}
