import M from './main.js';

export function remove(event) {
	if (event.target.classList.contains('remove')){
		event.target.parentNode.remove(event.target.parentNode);
	}
}

export function toggleCompleted(comp, state, fakeNumber) {
	const compName = comp.constructor.name;
	if (state === "completed"){
		if (typeof fakeNumber !== 'undefined') {
			fakeNumber-=1;
			comp.setState({
				actionState: "",
				fakeNumber: fakeNumber,
			})
		} else {
			comp.setState({
				actionState: "",
			})
		}
	} else {
		if (typeof fakeNumber !== 'undefined') {
			fakeNumber+=1;
			comp.setState({
				actionState: "completed",
				fakeNumber: fakeNumber,
			})
		} else {
			comp.setState({
				actionState: "completed",
			})
		}
		//Toast it, feedback after completion.
		//default:
		let title = "Actie";
		let message = "De actie is voltooid.";
		//non-default:
		if (compName === "Upvote") {
			title = "Upvote";
			message = "Het item is upgevote.";
		} else if (compName === "SaveQuestion") {
			title = "Opgeslagen";
			message = "De vraag is toegevoegd aan uw opgeslagen vragen.";
		} else if (compName === "Report") {
			title = "Gerapporteerd";
			message = "Het item is gerapporteerd.";
		}
		M.toast(title, message);
	}
}
