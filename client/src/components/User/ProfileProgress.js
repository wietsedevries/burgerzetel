import React, { Component } from 'react';
import Relay from 'react-relay';
import '../../stylesheets/user.css';
import {Link} from 'react-router';

class ProfileProgress extends Component {

	render() {
		// const {viewer} = this.props;
		const user = this.props.viewer.AuthUser;
		let points = 0;
		let nextStep = "Gefeliciteerd, uw profiel is volledig ingevuld!";
		if(user.age && user.gender && user.Education.value && user.Income.value && user.MaritalStatus.value && user.children) {
			points+=10;
		} else {
			nextStep = "Vul alle resterende gegevens in";
		}
		if(user.bio){
			points+=10;
		} else {
			nextStep = "Vul een beschrijving over uzelf in"
		}
		if(user.function){
			points+=20;
		} else {
			nextStep = "Geef uw functie aan"
		}
		if(user.avatar) {
			points+=30;
		} else {
			nextStep = "Voeg een avatar toe"
		}
		if (user.email && user.userName && user.firstName && user.lastName && user.City.name) {
			points+=30;
		} else {
			nextStep = "Vul uw email, naam, gebruikersnaam en/of woonplaats in"
		}
		let progress = {width:+points+'%'};
		if(points < 100){
			return (
				<div className="panel padding">
					<div className="panel-title">
						<i className="fa fa-user-circle-o"></i>
						<span>Uw profiel is voor {points}% compleet</span>
					</div>
					<div className="dotted"></div>

					<div className="result-progress">
						<div style={progress}>
							{points < 15 ? (
								<span className="poll-percentage low">{(parseFloat(points).toFixed(1)-1)+1+"%"}</span>
							):(
								<span className="poll-percentage">{(parseFloat(points).toFixed(1)-1)+1+"%"}</span>
							)}
						</div>
					</div>
					<Link to={"/"+user.userName+"/wijzigen"} className="side-link"><i className="fa fa-caret-right"></i> {nextStep}</Link>

					{/* <a href="#" className="side-link"><i className="fa fa-caret-right"></i> Voeg uw tagline toe</a>
						<a href="#" className="side-link"><i className="fa fa-caret-right"></i> Voeg een bio toe</a>
					<a href="#" className="side-link"><i className="fa fa-caret-right"></i> Voeg uw persoonlijke gegevens toe</a> */}
				</div>

			);
		}else{
			return null;
		}
	}
}

export default Relay.createContainer(ProfileProgress, {
	fragments: {
			viewer: () => Relay.QL `
				fragment on viewer {
					AuthUser{
						email,
						userName,
						firstName,
						lastName,
						avatar,
						function,
						age,
						bio,
						gender,
						reputation,
						Interests(first:20){
							edges{
								node{
									Tag{
										value,
										key
									}
								}
							}
						},
						Education {
							value,
						},
						Income {
							value,
						},
						MaritalStatus {
							value,
						},
						City {
							name,
						},
						children,
					},
				}
			`
	}
});
