import React, { Component } from 'react';
import {Link} from 'react-router';
import '../../stylesheets/avatar.css';

class Avatar extends Component {

	render() {
		let classPar;
		let firstLetters;
		let directory;
		if(this.props.type === "page"){
			directory = "pages";
			firstLetters = <i className='fa fa-hashtag'></i>;
			classPar = this.props.tag.charAt(0).toLowerCase();;
		}else if(this.props.type === "flag"){
			directory = "";
			firstLetters = <i className='fa fa-flag'></i>;
			classPar = "flag";
		}else{
			directory = "avatars";
			firstLetters = this.props.userFirstName.charAt(0)+this.props.userLastName.match(/(\w)\w*$/)[1];
			classPar = this.props.userFirstName.charAt(0).toLowerCase();
		}

		let hasAvatar = 0;
		if (this.props.imagePath !== "") {
			hasAvatar = 1;
		}
		let avatarHTML;

		let hasURL = true;
		if (this.props.hasURL === "false") {
			hasURL = false;
		}

		avatarHTML = (
			<div>
				{hasAvatar ? (
					<div className="avatar"
						style={{background: "url('../../images/"+directory+"/"+this.props.imagePath+"')",backgroundRepeat: "no-repeat", backgroundPosition: "center center"}}>
					</div>
					) : (
					<div className={"avatar default"+classPar}>
						<span>{firstLetters}</span>
					</div>
				)}
			</div>
		)
		if (hasURL) {
			return (
				<Link to={"/"+this.props.userName} className="avatar-component">
					{avatarHTML}
				</Link>
			);
		}
		return (
			<div className="avatar-component">
				{avatarHTML}
			</div>
		);
	}
}

export default Avatar;
