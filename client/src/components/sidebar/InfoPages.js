import React, { Component } from 'react';
import {Link} from 'react-router';

class InfoPages extends Component {
  render() {
    return (
			<div className="panel padding">
				<div className="panel-title">
					<i className="fa fa-info-circle" aria-hidden="true"></i>
					<span>Meer informatie</span>
				</div>
				<div className="dotted"></div>
				<Link to="/faq/overzicht" className="side-link">Veel gestelde vragen</Link>
				<div className="dotted"></div>
				<Link to="/info/huisregels" className="side-link">Huisregels</Link>
				<div className="dotted"></div>
				<Link to="/info/privacy-beleid" className="side-link">Privacy beleid</Link>
				<div className="dotted"></div>
				<Link to="/info/cookies" className="side-link">Cookies</Link>
				<div className="dotted"></div>
				<Link to="/info/over-burgerzetel" className="side-link">Over Burgerzetel</Link>
				<div className="dotted"></div>
				<Link to="/contact/" className="side-link">Contact</Link>

			</div>
    );
  }
}

export default InfoPages;
