import React, { Component } from 'react';
import {Link} from 'react-router';

class Footer extends Component {
  render() {
    return (
			<div className="panel padding naked">
				<div className="footer color-grey">
					<Link to="/faq/overzicht" className="blue-link">Veel gestelde vragen</Link><span>&bull;</span>
					<Link to="/info/huisregels" className="blue-link">Huisregels</Link><span>&bull;</span>
					<Link to="/info/privacy-beleid" className="blue-link">Privacybeleid</Link><span>&bull;</span>
					<Link to="/info/cookies" className="blue-link">Cookies</Link><span>&bull;</span>
					<Link to="/info/over-burgerzetel" className="blue-link">Over Brugerzetel</Link><span>&bull;</span>
					<Link to="/contact/feedback" className="blue-link">Feedback</Link><span>&bull;</span>
					<Link to="/contact/" className="blue-link">Contact</Link>
					<div className="color-light">&copy; 2017 BurgerPanel.nl</div>
				</div>
				<Link to="/contact/feedback" className="fixed-feedback grey-link">
					Feedback
					<i className="fa fa-comment"></i>
				</Link>
			</div>
    );
  }
}

export default Footer;
