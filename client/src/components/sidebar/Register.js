import M from '../../main.js';
import React, { Component } from 'react';
import '../../stylesheets/register.css';

import AddInvite from '../../components/sidebar/AddInvite';

let invites = [];

class Register extends Component {
	constructor(props) {
		super(props);
		this.state = {
			email: "",
			userName: "",
			code: 0,
			firstName: "",
			lastName: "",
			city: 0,
			password: "",
			invites: invites,
			error: "",
		};
		this._handleEmailChange = this._handleEmailChange.bind(this);
		this._handleCodeChange = this._handleCodeChange.bind(this);
		this._handlePasswordChange = this._handlePasswordChange.bind(this);
		this._socialData = this._socialData.bind(this);
		this._resendCode = this._resendCode.bind(this);
		this.getLinkedinData = this.getLinkedinData.bind(this);
		this.getGoogleData = this.getGoogleData.bind(this);
	}

	pushInvites = (value) => {
	 invites.push(value);
	 console.log("togevoegd:",value);
	}
	removeInvites = (value) => {
	 for(var i = invites.length - 1; i >= 0; i--) {
		 // eslint-disable-next-line
		 if(invites[i] == value) {
			 invites.splice(i, 1);
		 }
	 }
	}

	_handleEmailChange(e){
		let error = "";
		// eslint-disable-next-line
		var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
		if(!e.target.value){
			error = "Alle velden zijn verplicht.";
		}else if(!re.test(e.target.value)){
			error = "Het door u ingevulde e-mail adres is niet geldig.";
		}
		this.setState({error : error, email : e.target.value});
	}
	_handleCodeChange(e){
		let error = "";
		if(!e.target.value){
			error = "Alle velden zijn verplicht.";
		}
		var re = /^\+?(0|[1-9]\d*)$/;
		if (!re.test(e.target.value)) {
			error = "De code kan alleen uit cijfers bestaan.";
		}
		this.setState({error : error, code : e.target.value});
	}
	_handlePasswordChange(e){
		let error = "";
		if(!e.target.value){
			error = "Alle velden zijn verplicht.";
		}
		if (e.target.value && !M.validatePassword(e.target.value)) {
			error = "Wachtwoord moet aan de volgende eisen voldoen: acht karakters lang, tenminste één cijfer en tenminste één letter";
		}
		this.setState({error : error, password : e.target.value});
	}
	_handleDefaultFieldChange(field, e){
		let error = "";
		if(!e.target.value){
			error = "Vul alle velden in.";
		}
		this.setState({error : error, [field] : e.target.value});
	}

	_resendCode(e) {
		let data = {email: "", userName: "", code: 0, firstName: "", lastName: "", cityId: 0, password: "", invites: invites ,questionPage: false};
		this.props.onRegister('resendCode', data);
	}

	handleFieldError(error) {
		M.toast("Opgelet!", error, "error");
	}

	// toggle login popup
	onboardingStep(step) {
		const elm = document.getElementById('registerBox');
		this.setState({invites: invites});
		let data = {email: "", userName: "", code: 0, firstName: "", lastName: "", cityId: 0, password: "", invites: invites,questionPage: false};
		let error = this.state.error;
		if (error !== "") {
			this.handleFieldError(error);
			return null;
		}
		switch (step) {
			case 1:
				elm.className = "popup open";
				break;
			case 2:
				let {email, userName} = this.state;
				if (email && userName && !error) {
					data.email = email;
					data.userName = userName;
					this.props.onRegister(step, data);
				} else {
					this.handleFieldError("Alle velden zijn verplicht.")
				}
				break;
			case 3:
				let {code} = this.state;
				if (code && !error) {
					data.code = code;
					this.props.onRegister(step, data);
					elm.className = "popup open";
				} else {
					this.handleFieldError("Alle velden zijn verplicht.")
				}
				break;
			case 4:
				let {firstName, lastName, cityId} = this.state;
				if (firstName && lastName && cityId && !error) {
					data.firstName = firstName;
					data.lastName = lastName;
					data.cityId = cityId;
					this.props.onRegister(step, data);
				} else {
					this.handleFieldError("Alle velden zijn verplicht.")
				}
				break;
			case 5:
				let {password} = this.state;
				if (password && !error) {
					data.password = password;
					this.props.onRegister(step, data);
				} else {
					this.handleFieldError("Alle velden zijn verplicht.")
				}
				break;
			case 6:
				this.props.onRegister(step, data);
				break;
			case 7:
				this.props.onRegister(step, data);
				break;
			case 8:
				//step 7, but WITH redirect to questionPage
				data.questionPage = true;
				this.props.onRegister((step-1),data);
				break;
			case 9:
				//close the popup
				elm.className = "popup";
				break;
			case 10:
				//skip inviting people
				this.props.onRegister(step, data);
				break;
			default:
				return null;
		}
	}

	_socialData() {
		let error = this.state.error;
		if (error !== "") {
			this.handleFieldError(error);
			return null;
		}
		let {userName, password, cityId} = this.state;
		if (!M.validatePassword(password)) {
			this.handleFieldError("Wachtwoord moet aan de volgende eisen voldoen: acht karakters lang, tenminste één cijfer en tenminste één letter");
			return null;
		}
		if (userName && password && cityId) {
			let data = {password: "", userName: "", cityId: 0};
			data.password = password;
			data.userName = userName;
			data.cityId = cityId;
			this.props.onSocialRegister(2, data);
		} else {
			this.handleFieldError("Alle velden zijn verplicht.")
		}
	}

	getFacebookData() {
		window.FB.api('/me', {"fields":"id,email,first_name,last_name"}, (response) => {
			this.props.onSocialRegister(1,response,'fb');
		});
	}

	getLinkedinData() {
		window.IN.API.Profile("me").fields("id", "first-name", "last-name", "email-address", "headline", "picture-url").result( (result) => {
			const profile = result.values[0]
			const data = {
				id: profile.id,
				first_name: profile.firstName,
				last_name: profile.lastName,
				email: profile.emailAddress,
			};
			this.props.onSocialRegister(1,data,'li');
		});
	}

	getGoogleData(auth2) {
		const profile = auth2.currentUser.get().getBasicProfile();
		const data = {
			id: profile.getId(),
			email: profile.getEmail(),
			first_name: profile.getGivenName(),
			last_name: profile.getFamilyName(),
		};
		this.props.onSocialRegister(1,data,'g');
	}

	socialRegister(socialId) {
		//socialId's:
		//1 = facebook
		//2 = linkedin
		//3 = google
		switch (socialId) {
			case 1:
				window.FB.getLoginStatus( (response) => {
					window.FB.login( (response) => {
						if (response.status === 'connected') {
							return this.getFacebookData();
						}
						return null;
					}, {scope: 'public_profile,email'});
				});
				break;
			case 2:
				window.IN.User.authorize(this.getLinkedinData, '');
				break;
			case 3:
			window.gapi.load('auth2', () => {
					var auth2 = window.gapi.auth2.init({
						client_id: "630392615970-o09atvjfsoqd4eisfcjrsr4436r66rrs.apps.googleusercontent.com",
						scope: "profile"
					});
					auth2.signIn().then( () => {
						this.getGoogleData(auth2);
					});
				});
				break;
			default:
				break;
		}
	}

	componentDidMount() {
		//INIT OF SOCIAL STUFF ALREADY HAPPENS IN LOGIN, ADD HERE IF NEEDED!!
		// Load the linkedIn SDK, this SDK is not used in login, but is here
		(function(d, s, id) {
			const element = d.getElementsByTagName(s)[0];
			const ljs = element;
			var js = element;
			if (d.getElementById(id)) {
				return; }
			js = d.createElement(s);
			js.id = id;
			js.src = '//platform.linkedin.com/in.js';
			js.text = 'api_key: 863oaf21ytstdm';
			ljs.parentNode.insertBefore(js, ljs);
		}(document, 'script', 'linkedin-sdk'));
	}

	// hide the register component
	hideRegister(event) {
		document.getElementById('registerBox').className = "popup";
	}
	login(event){
		M.login();
		this.hideRegister();
	}

	render() {
		const vidStyle = {
			width: "560px",
			height: "315px",
			borderRadius: "3px",
			boxShadow: "0 1px 2px rgba(0,0,0,.2)",
		};
		if (this.state.error) {
			console.log(this.state.error);
		}
		const Cities = this.props.cities;
		return (
			<div>
				<div className="panel padding naked">
					<div className="sub-title color-grey">
						<span className="color">Welkom op burgerzetel.nl</span>
					</div>
					<div onClick={this.socialRegister.bind(this,1)} className="sm-bar facebook">
						<i className="fa fa-facebook"></i>
						<span>Registreren met Facebook</span>
					</div>
					<div onClick={this.socialRegister.bind(this,2)} className="sm-bar linkedin">
						<i className="fa fa-linkedin"></i>
						<span>Registreren met Linkedin</span>
					</div>
					<div onClick={this.socialRegister.bind(this,3)} className="sm-bar google">
						<i className="fa fa-google"></i>
						<span>Registreren met Google</span>
					</div>
					<a onClick={this.onboardingStep.bind(this,1)} className="side-link  pointer">Of registreer met e-mail adres</a>
				</div>
				{/*  Register popup */}
				<div id="registerBox" className="popup">
					<div className="bg" onClick={this.onboardingStep.bind(this,9)}></div>
					<div className="box-helper"></div>
					<div className="box login">
						<span id="step1" className="window">
							<div className="row">
								<div className="col-xs-12 extra-pad-left extra-pad-right padding-sm">
									<div className="title color">Registreren</div>
									<div onClick={this.onboardingStep.bind(this,9)} className="close blue-link right">Sluiten
									</div>
								</div>
							</div>
							<div className="dotted"></div>

							<div className="row">
								<div className="col-xs-12 col-sm-5 no-pad-right extra-pad-left padding-sm">
									<p className="color-grey">
										Registreren met deze accounts
									</p>
									<div onClick={this.socialRegister.bind(this,1)} className="sm-bar facebook">
										<i className="fa fa-facebook" ></i>
										<span>Registreren met Facebook</span>
									</div>
									<div onClick={this.socialRegister.bind(this,2)} className="sm-bar linkedin">
										<i className="fa fa-linkedin" ></i>
										<span>Registreren met Linkedin</span>
									</div>
									<div onClick={this.socialRegister.bind(this,3)} className="sm-bar google">
										<i className="fa fa-google" ></i>
										<span>Registreren met Google</span>
									</div>
								</div>
								<div className="hidden-xs col-xs-2 no-padding">
									<div className="center">
										<div className="center-line">
											<span className="color-grey">of</span>
										</div>
									</div>
								</div>
								<div className="col-xs-12 col-sm-5 no-pad-left extra-pad-right padding-sm">
									<p className="color-grey">
										Registreren met uw email adres
									</p>
									<input type="email" placeholder="E-mail adres" onChange={this._handleEmailChange}/>
									<input type="text" placeholder="Gebruikersnaam" onChange={this._handleDefaultFieldChange.bind(this, 'userName')}/>
									<div className="btns">
										<div onClick={this.onboardingStep.bind(this,2)} className="btn primary-btn right no-side-margin">Registreren</div>
									</div>
								</div>
							</div>
							<div className="dotted"></div>
							<div className="row">
								<div className="col-xs-12 extra-pad-left extra-pad-right padding-sm">
									<a onClick={this.login.bind(this)} className="blue-link">Ik heb al een account</a>
									<div className="color-light right">&copy; 2017</div>
								</div>
							</div>
						</span>

						<span id="step2social" className="full-window">
							<div className="step">
								<div className="title color">Gegevens</div>
								<p className="color-grey">
									Om uw registratie af te maken dient u een gebruikersnaam en wachtwoord te kiezen.
									Ook dient u uw woonplaats aan te geven.
								</p>
								<br/>
								<span className="field-error"></span>
								<input type="text" placeholder="Gebruikersnaam" onChange={this._handleDefaultFieldChange.bind(this, 'userName')}/>
								<input type="password" placeholder="Wachtwoord" onChange={this._handlePasswordChange}/>
								<select ref="city" defaultValue="0" onChange={this._handleDefaultFieldChange.bind(this, 'cityId')}>
									<option value="0" disabled>Selecteer Woonplaats</option>
									{Object.keys(Cities).map(function(key) {
										const city = Cities[key].node;
										return (
											<option key={key} value={city.cityId}>{city.name}</option>
										)
									})}
								</select>
								<div className="caret"></div>
								<div onClick={this._socialData} className="btn primary-btn centered">Volgende stap
									<i className="fa fa-arrow-right" ></i>
								</div>

								<div className="bullets">
									<div className="bullet"></div>
									<div className="bullet"></div>
									<div className="bullet"></div>
									<div className="bullet active"></div>
									<div className="bullet"></div>
									<div className="bullet"></div>
								</div>
							</div>
						</span>

						<span id="step2" className="full-window">
							<div className="step">
								<div className="title color">Welkom!</div>
								<p className="color-grey">
									Wij hebben een 6-cijferige code verstuurd naar uw email adres. Vul de code hieronder in.
								</p>
								<br/>
								<span className="field-error"></span>
								<input className="code" maxLength="6" type="numer" placeholder="000000" onChange={this._handleCodeChange}/>
								<div onClick={this.onboardingStep.bind(this,3)} className="btn primary-btn centered">Volgende stap
									<i className="fa fa-arrow-right" ></i>
								</div>

								<div className="alt-text blue-link" onClick={this._resendCode}>Code nogmaals verzenden</div>

								<div className="bullets">
									<div className="bullet"></div>
									<div className="bullet active"></div>
									<div className="bullet"></div>
									<div className="bullet"></div>
									<div className="bullet"></div>
									<div className="bullet"></div>
								</div>
							</div>
						</span>

						<span id="step3" className="full-window">
							<div className="step">
								<div className="title color">Gegevens</div>
								<p className="color-grey">
									Deze gegevens zijn openbaar en staan op uw profielpagina.
								</p>
								<br/>
								<input type="text" placeholder="Voornaam" onChange={this._handleDefaultFieldChange.bind(this, 'firstName')}/>
								<input type="text" placeholder="Achternaam" onChange={this._handleDefaultFieldChange.bind(this, 'lastName')}/>
								<select ref="city" defaultValue="0" onChange={this._handleDefaultFieldChange.bind(this, 'cityId')}>
									<option value="0" disabled>Selecteer Woonplaats</option>
									{Object.keys(Cities).map(function(key) {
										const city = Cities[key].node;
										return (
											<option key={key} value={city.cityId}>{city.name}</option>
										)
									})}
								</select>
								<div className="caret"></div>
								<div onClick={this.onboardingStep.bind(this,4)} className="btn primary-btn centered">Volgende stap
									<i className="fa fa-arrow-right" ></i>
								</div>

								<div className="bullets">
									<div className="bullet"></div>
									<div className="bullet"></div>
									<div className="bullet active"></div>
									<div className="bullet"></div>
									<div className="bullet"></div>
									<div className="bullet"></div>
								</div>
							</div>
						</span>

						<span id="step4" className="full-window">
							<div className="step">
								<div className="title color">Wachtwoord</div>
								<p className="color-grey">
									Kies een wachtwoord. In de toekomst kunt u inloggen met uw email adres en dit wachtwoord.
								</p>
								<br/>
								{/* <input type="text" placeholder="Gebruikersnaam"/> */}
								<input type="password" placeholder="Wachtwoord" onChange={this._handlePasswordChange}/>

								<div onClick={this.onboardingStep.bind(this,5)} className="btn primary-btn centered">Volgende stap
									<i className="fa fa-arrow-right" ></i>
								</div>

								<div className="bullets">
									<div className="bullet"></div>
									<div className="bullet"></div>
									<div className="bullet"></div>
									<div className="bullet active"></div>
									<div className="bullet"></div>
									<div className="bullet"></div>
								</div>
							</div>
						</span>

						<span id="step5" className="full-window">
							<div className="step">
								<div className="title color">Vrienden uitnodigen</div>
								<p className="color-grey">
									Nodig uw vrienden en familie uit om deel te nemen aan de community.
								</p>
								<br/>
								<AddInvite onRemove={this.removeInvites} onAdd={this.pushInvites} ref="invites"/>
								<div onClick={this.onboardingStep.bind(this,10)} className="btn secondary-btn left no-side-margin">Overslaan</div>
								<div onClick={this.onboardingStep.bind(this,6)} className="btn primary-btn right no-side-margin">Versturen</div>

								<div className="bullets">
									<div className="bullet"></div>
									<div className="bullet"></div>
									<div className="bullet"></div>
									<div className="bullet"></div>
									<div className="bullet"></div>
									<div className="bullet active"></div>
								</div>
							</div>
						</span>

						<span id="step6" className="full-window">
							<div className="step wide">
								<div className="title color">Uw registratie is voltooid!</div>
								<p className="color-grey">
									Welkom bij de community! Om deze site formeel te houden
									hanteren we een aantal huisregels. Bekijk het onderstaande
									filmpje om te zien wat u wel en niet mag doen op deze site.
								</p>

								<object type="application/x-shockwave-flash" style={vidStyle} data="//www.youtube.com/v/NjwX4J_I4hg?color1=FFFFFF&amp;color2=FFFFFF&amp;rel=0&amp;showsearch=0&amp;showinfo=0&amp;controls=0&amp;version=3&amp;modestbranding=1">
								<param name="movie" defaultValue="//www.youtube.com/v/NjwX4J_I4hg?color1=FFFFFF&amp;color2=FFFFFF&amp;rel=0&amp;showsearch=0&amp;showinfo=0&amp;controls=0&amp;version=3&amp;modestbranding=1" />
								<param name="allowFullScreen" defaultValue="true" />
								<param name="allowscriptaccess" defaultValue="always" />
								</object>
								<div className="double-btns">
									<div className="btn primary-btn full-width no-side-margin" onClick={this.onboardingStep.bind(this,8)}>Stel gelijk een vraag</div>
									<div className="btn secondary-btn full-width no-side-margin" onClick={this.onboardingStep.bind(this,7)}>sluiten</div>
								</div>
							</div>
						</span>

						</div>
					</div>
			</div>
		);
	}
}

export default Register;
