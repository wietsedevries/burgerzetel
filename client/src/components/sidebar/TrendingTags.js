import React, { Component } from 'react';
import Relay from 'react-relay';
import {Link} from 'react-router';

class TrendingTags extends Component {
	render() {
		const tags = this.props.tags;
		const trendingTags = tags.TrendingTags.edges;
		return (
			<div className="panel padding">
				<div className="panel-title bold">
					<i className="fa fa-hashtag"></i>
					<span>Trending topics</span>
				</div>
				{Object.keys(trendingTags).map(function(key) {
					const tagValue = trendingTags[key].node.TagValue;
					return (
						<div key={key}>
							<div className="dotted"></div>
							<Link to={"/onderwerp/"+tagValue.key} className="tag-link color">
								{tagValue.value}
								<span className="color-grey">{tagValue.used}x gebruikt</span>
							</Link>
						</div>
					)
				})}
			</div>
		);
	}
}
export default Relay.createContainer(TrendingTags, {
		fragments: {
				tags: () => Relay.QL `
					fragment on viewer {
						TrendingTags(first:5){

							edges{
								node{
									TagValue {
										key,
										value,
										used
									},
								}
							}
						},
					}
				`
		}
});
