import React, { Component } from 'react';
import M from '../../main.js';
// import '../../stylesheets/ask.css';

var input = "";
var count = 0;

class AddedInvite extends Component {
	render() {
		return (
			<span>
				<div className='created-invite color'>
					<span>{this.props.value}</span>
					<i data-value={this.props.value} className='fa fa-times remove'></i>
				</div>
				<input value={this.props.value} ref='invite' type='hidden'/>
			</span>
		);
	}
}

var addedInvites = [];

class AddInvite extends Component {
	constructor(props) {
			super(props);
			this.state = {
				showResults: "",
				addedInvites: addedInvites,
			};
			this.addInvite = this.addInvite.bind(this);
	}


	addInvite(event) {
		var k = event.keyCode;
		if (!k || k === 13 || k === 188 || k === 32 || k === 9) {
			input = document.getElementById('inputField');
			var value = input.value;
			var error;
			//validate for email
			// eslint-disable-next-line
			var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
			if(!value){
				error = "Vul een e-mail adres in.";
			}else if(!re.test(value)){
				error = "Het door u ingevulde e-mail adres is niet geldig.";
			}
			if (error) {
				M.toast("Fout!", error, "error");
				console.log("Geen email toegevoegd, fout: "+error);
				return null;
			}
			if (value !== null) {
				addedInvites.push(<AddedInvite key={count} inviteKey={count} value={value} />);
				this.props.onAdd(value);
			}
			input.value = "";
			input.focus();
			this.setState({addedInvites: addedInvites});
			count+=1;
		}
	}

	remove(event) {
		if (event.target.classList.contains('remove')){
			event.target.parentNode.remove(event.target.parentNode);
		}
		var value  = event.target.getAttribute('data-value');
		this.props.onRemove(value);
	}

	render() {
		return (
			<div className="preview-holder">
				<input id="inputField" className="color" type="text" placeholder="E-mail adressen gescheiden door een komma" onKeyDown={this.addInvite} />
				<div className="invites">
					<span id="invite-area" onClick={this.remove.bind(this)}>
						{this.state.addedInvites}
					</span>
				</div>
			</div>
		);
	}
}

export default AddInvite;
