import React, { Component } from 'react';
import Relay from 'react-relay';
import {Link} from 'react-router';

class TrendingQs extends Component {
	render() {
		var questions = this.props.questions.TrendingQuestions.edges;
		if(questions.lenght > 0){
			return (
				<div className="panel padding">
					<div className="panel-title bold">
						<i className="fa fa-line-chart"></i>
						<span>Trending vragen</span>
					</div>
					{questions.map((question, i) => {
						return (
							<div key={i}>
								<div className="dotted"></div>
								<Link to={"/vraag/"+question.node.slug} className="side-link">{question.node.title}</Link>
							</div>
						)
					})}
				</div>
			);
		}else{
			return (<span></span>);
		}

	}
}
export default Relay.createContainer(TrendingQs, {
		fragments: {
			questions: () => Relay.QL `
				fragment on viewer {
					TrendingQuestions(first:5){
						edges{
							node{
								slug,
								title,
							}
						}
					},
				}
			`
		}
});
