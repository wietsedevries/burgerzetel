import React, { Component } from 'react';
import { FacebookButton, TwitterButton } from 'react-social';
import '../../stylesheets/actions.css';

class Share extends Component {

  render() {
		let url = "http://burgerzetel.nl/"+this.context.router.location.pathname;
		let title = this.props.title;
		let msg = this.props.description;
		let appId = "1750069001983788";
    return (
			<div className="panel padding share-block">
				<div className="panel-title bold">
					<i className="fa fa-share-alt"></i>
					<span>Deel deze pagina</span>
				</div>
				<div className="dotted"></div>
				<div className="row">
					<div className="col-xs-4">
						<FacebookButton appId={appId} className="facebook share-btn" element="div" url={url} message={msg}>
								<i className="fa fa-facebook"></i>
						</FacebookButton>
					</div>
					<div className="col-xs-4">
						<TwitterButton className="twitter share-btn" element="div" url={url} message={msg}>
								<i className="fa fa-twitter"></i>
						</TwitterButton>
					</div>
					<div className="col-xs-4">
						<a href={"https://www.linkedin.com/shareArticle?mini=true&url="+url+
						"&title="+title+"%20%20%20%20&summary="+msg} target="_blank" className="linkedin share-btn">
							<i className="fa fa-linkedin"></i>
						</a>
					</div>
				</div>
			</div>
		);
	}
}
Share.contextTypes = {
	router: React.PropTypes.object
};

export default Share;
