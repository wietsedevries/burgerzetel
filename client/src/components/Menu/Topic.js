import React, { Component } from 'react';
import Avatar from '../../components/User/Avatar';
import {Link} from 'react-router';

class Topic extends Component {

	render() {
		let topic = this.props;
		let img = "";
		if(topic.tag.Page){
			img = topic.tag.Page.img;
		}
		
		let count = "";
		if(topic.count > 0){
			count = topic.count;
			if(topic.count > 20){
				count = "20+";
			}
		}
		return (
			<Link to={"/onderwerp/"+topic.slug} className="page">
				<Avatar imagePath={img} type="page" tag={topic.value} hasURL="false"/>
				{topic.value}
				<span>{count}</span>
			</Link>
		);
	}
}
export default Topic;
