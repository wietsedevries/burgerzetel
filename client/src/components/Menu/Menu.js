import M from '../../main.js';
import React, { Component } from 'react';
import Relay from 'react-relay';
import {Link} from 'react-router';
import Session from '../../services/Session';
import Avatar from '../../components/User/Avatar';
import Register from '../../components/sidebar/Register';
import Topic from './Topic';
import '../../stylesheets/menu.css';

import RegisterMutation from '../../mutations/RegisterMutation';
import SocialRegisterMutation from '../../mutations/SocialRegisterMutation';
import ResendRegisterCodeMutation from '../../mutations/ResendRegisterCodeMutation';

class Menu extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showMenu: "",
			icon: "fa fa-bars",
		};
	}

	_socialRegister(step, data, platform) {
		const elm = document.getElementById('registerBox');
		let step2social = document.getElementById('step2social');
		let step5 = document.getElementById('step5');
		//prepare data
		let fields = {email: "", userName: "", firstName: "", lastName: "", cityId: 0, password: "", facebookId: "", linkedinId: "", googleId: ""};
		let {email, userName, firstName, lastName, cityId, password, facebookId, linkedinId, googleId,} = fields;
		if (step === 1) {
			email = data.email;
			firstName = data.first_name;
			lastName = data.last_name;
			switch (platform) {
				case 'fb':
					facebookId = data.id;
					break;
				case 'li':
					linkedinId = data.id;
					break;
				case 'g':
					googleId = data.id;
					break;
				default:
					console.log("correct platform key wasn't suplied, social platform id cannot be set!");
					break;
			}
		}
		if (step === 2) {
			email = sessionStorage.getItem('registrationEmail');
			cityId = data.cityId;
			userName = data.userName;
			password = data.password;
		}
		//mutate data
		this.props.relay.commitUpdate(new SocialRegisterMutation({
			viewer: this.props.userData,
			email,
			firstName,
			lastName,
			facebookId,
			linkedinId,
			googleId,
			cityId,
			userName,
			password,
		}), {
			onSuccess: (response) => {
				let {email, message} = response.SocialRegisterMutation;
				switch (step) {
					case 1:
						if (message) {
							M.toast("Mislukt", message, "error");
							return null;
						}
						if (email) {
							sessionStorage.setItem('registrationEmail', email);
						}
						elm.className = "popup open";
						step2social.style.display = 'block';
						break;
					case 2:
						if (message) {
							M.toast("Mislukt", message, "error");
							return null;
						}
						step2social.style.display = 'none';
						step5.style.display = 'block';
						break;
					default:
						return null;
				}
			},
			onFailure: (t) => {
				console.log('Step '+step+' of SocialRegisterMutation FAILED', t.getError());
			},
		});
	}

	_register(step, data) {
		let step2 = document.getElementById('step2');
		let step3 = document.getElementById('step3');
		let step4 = document.getElementById('step4');
		let step5 = document.getElementById('step5');
		let step6 = document.getElementById('step6');

		//prepare data
		let {email, userName, code, firstName, lastName, cityId, password, questionPage, invites} = data;
		if (step !== 2 && sessionStorage.getItem('registrationEmail')) {
			email = sessionStorage.getItem('registrationEmail');
		}
		if (step === 6) {
			if (invites.length <= 0) {
				M.toast("Fout", "Voer tenminste één e-mail adres in", "error");
				return null;
			}
		}
		if (step > 6) {
			//making sure there aren't invites
			invites = [];
		}
		//resend code mutation
		if (step === 'resendCode') {
			if (email) {
				this.props.relay.commitUpdate(new ResendRegisterCodeMutation({
					viewer: this.props.userData,
					email,
				}), {
					onSuccess: (response) => {
						M.toast("Nieuwe code verzonden","Er is een nieuwe code verzonden, controleer uw e-mail.")
					},
					onFailure: (t) => {
						console.log('resend of code in resendRegisterCodeMutation FAILED', t.getError());
					},
				});
			} else {
				//no email (in session)
				M.toast("Fout!", "Er is iets misgegaan, probeer opnieuw te registreren.", "error");
			}
		}
		//skiping inviting people step
		if (step === 10) {
			step5.style.display = 'none';
			step6.style.display = 'block';
			return null;
		}
		//commit the mutation
		this.props.relay.commitUpdate(new RegisterMutation({
			viewer: this.props.userData,
			code,
			userName,
			email,
			firstName,
			lastName,
			cityId,
			password,
			invites,
		}), {
			onSuccess: (response) => {
				switch (step) {
					case 2:
						var {email, message} = response.RegisterMutation;
						if (message) {
							M.toast('Account bestaat al', message, 'error');
						} else {
							sessionStorage.setItem('registrationEmail', email);
							step2.style.display = 'block';
						}
						break;
					case 3:
						step2.style.display = 'none';
						step3.style.display = 'block';
						break;
					case 4:
						step3.style.display = 'none';
						step4.style.display = 'block';
						break;
					case 5:
						step4.style.display = 'none';
						step5.style.display = 'block';
						break;
					case 6:
						step5.style.display = 'none';
						step6.style.display = 'block';
						break;
					case 7:
						var {authToken} = response.RegisterMutation;
						localStorage.authToken = authToken;
						if (questionPage) {
							window.location.reload();
							this.context.router.push('/vraag/nieuw');
						} else {
							window.location.reload();
						}
						break;
					default:
						return null;
				}
			},
			onFailure: (t) => {
				console.log('Step '+step+' of registerMutation FAILED', t.getError());
				if (step === 3) {
					M.toast("Mislukt","Controleer of u de juiste code ingevoerd heeft","error");
				}
			},
		});
	}
	toggleMenu(event) {
		if(this.state.showMenu === 'active'){
			this.setState({
				showMenu: '',
				icon: "fa fa-bars",
			});
		}else{
			this.setState({
				showMenu: 'active',
				icon: "fa fa-times",
			});
		}
	}
	_logout(e) {
		e.preventDefault();
		localStorage.clear();
		window.location = "/";
	}

	render() {
		var userData = this.props.userData;
		var user = userData.AuthUser;
		var cities = userData.Cities.edges;
		let home;
		let profile;
		let favorites;
		let settings;
		let faq;
		var reputationNotification = (<span></span>);

		switch (this.props.page) {
			case "home":
				home = "active";
				break;
			case "profile":
				profile = "active";
				break;
			case "favorites":
				favorites = "active";
				break;
			case "settings":
				settings = "active";
				break;
			case "faq":
				faq = "active";
				break;
			default:

		}

		if(user){
			if(userData){
				var topics = this.props.userData.AuthUser.Interests.edges;
			}
			return (
				<span className={this.state.showMenu}>
					<div className="menu-bg" onClick={this.toggleMenu.bind(this)}></div>
					<div className="hamburger" onClick={this.toggleMenu.bind(this)}>
						<i className={this.state.icon}></i>
					</div>
					<div className="col-sm-4 col-md-3 left-panel menu-panel">
						<div className="panel padding naked">
							<div className="menu">
								<div to={"/"+user.userName} className="user">
									<Avatar imagePath={user.avatar} userName={user.userName} userFirstName={user.firstName} userLastName={user.lastName}/>
									<div className="details">
										<Link to={"/"+user.userName} className="name color-accent">{user.firstName+" "+user.lastName}</Link>
										<Link to={"/"+user.userName+"/reputatie"} className="reputation color">
											<i className="fa fa-trophy" aria-hidden="true"></i>
											{user.reputation}
										</Link>
										{reputationNotification}
									</div>
								</div>
								<div className="dotted no-side-margin"></div>
								<Link to="/" className={"menu-link "+home}>
									<i className="fa fa-home" aria-hidden="true"></i>
									<span>Home</span>
								</Link>
								<Link to={"/"+user.userName} className={"menu-link "+profile}>
									<i className="fa fa-user-circle-o" aria-hidden="true"></i>
									<span>Mijn profiel</span>
								</Link>
								<Link to={"/"+user.userName+"/opgeslagen"} className={"menu-link "+favorites}>
									<i className="fa fa-bookmark" aria-hidden="true"></i>
									<span>Opgeslagen</span>
								</Link>
								<Link to={"/"+user.userName+"/instellingen"} className={"menu-link "+settings}>
									<i className="fa fa-cog" aria-hidden="true"></i>
									<span>Instellingen</span>
								</Link>
								<Link to="/faq/overzicht" className={"menu-link "+faq}>
									<i className="fa fa-question-circle-o" aria-hidden="true"></i>
									<span>Help</span>
								</Link>
								<a onClick={this._logout} className={"menu-link hidden-sm hidden-md hidden-lg"}>
									<i className="fa fa-sign-out" aria-hidden="true"></i>
									<span>Uitloggen</span>
								</a>
								<div className="dotted no-side-margin"></div>
								<Link to="/onderwerpen/overzicht" className="sub-title color-link">Onderwerpen</Link>

								{Object.keys(topics).map(function(key) {
										if (topics[key].node.Tag) {
											let value = topics[key].node.Tag.value;
											let slug = topics[key].node.Tag.key;
											let tag = topics[key].node.Tag;
											return (
												<Topic tag={tag} key={key} value={value} slug={slug} count=""/>
											);
										}
										return ( null );
									})
								}
							</div>
						</div>
					</div>
				</span>
			);
		}else{
			return (
				<div className="hidden-xs col-sm-4 col-md-3 left-panel">
					<Register onRegister={this._register.bind(this)} onSocialRegister={this._socialRegister.bind(this)} cities={cities} />
				</div>
			)
		}
	}
}
Menu.contextTypes = {
	router: React.PropTypes.object
};
export default Relay.createContainer(Menu, {
	initialVariables: {
		username:  null,
		loggedIn: false,
	},
	prepareVariables: prevVariables => {
		let user = Session.User();
		var loggedIn = false;
		if(user){
			loggedIn = true;
		}
		return {
			username: user.userName,
			loggedIn: loggedIn,
		};
	},
	fragments: {
			userData: () => Relay.QL `
				fragment on viewer {
					${RegisterMutation.getFragment('viewer')},
					${SocialRegisterMutation.getFragment('viewer')},
					${ResendRegisterCodeMutation.getFragment('viewer')},
					AuthUser{
						email,
						userName,
						firstName,
						lastName,
						avatar,
						function,
						bio,
						gender,
						reputation,
						Interests(first:20){
							edges{
								node{
									Tag{
										value,
										key
										Page{
											img,
										}
									}
								}
							}
						},
						Education {
							value,
						},
						Income {
							value,
						},
						MaritalStatus {
							value,
						},
						City {
							name,
						},
						children,
					},
					Cities(first:5000){
						edges{
							node{
								name,
								cityId,
							},
						},
					},
				}
			`
	}
});
