import React, { Component } from 'react';
import {Link} from 'react-router';

import M from '../../main.js';

class Login extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loginModal: "popup",
			error: "",
			email: "",
			password: "",
		};
		this.toggleLogin = this.toggleLogin.bind(this);
		this._handlePasswordChange = this._handlePasswordChange.bind(this);
		this._handleEmailChange = this._handleEmailChange.bind(this);
		this._formSubmit = this._formSubmit.bind(this);
		this.handleFacebookClick = this.handleFacebookClick.bind(this);
		this.handleLinkedinClick = this.handleLinkedinClick.bind(this);
		this.handleGoogleClick = this.handleGoogleClick.bind(this);
	}

	componentDidMount() {
		//INITS ARE ALSO USED IN REGISTER COMPONENT, IN CASE OF REMOVING, ADD THEM THERE!
		//FB init
		window.fbAsyncInit = function() {
			window.FB.init({
				appId      : '1750069001983788',
				cookie     : true,
				version    : 'v2.8'
			});
		};
		//If linkedin login, linkedin returns to this url. So check if this is the case and perform next step
		if(localStorage.linkedinState && this.props.onLinkedinLogin) {
			this.checkLinkedinCode();
		}

		// Load the Facebook SDK asynchronously
		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/en_US/sdk.js";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));

		// Load the google SDK
		(function(d, s, id){
			var js, gs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) {return;}
			js = d.createElement(s); js.id = id;
			js.src = 'https://apis.google.com/js/platform.js'
			gs.parentNode.insertBefore(js, gs);
		}(document, 'script', 'google-platform'));
	}

	// *** FACEBOOK *** //
	facebookConnectedCallback(response) {
		this.props.onFbLogin(response.authResponse.accessToken);
	}

	handleFacebookClick(e) {
		// Call to get state of person visiting, they can be:
		// 1: Authorized this app (connected),
		// 2: logged into facebook, but not authorized this app ('not_authorized')
		// 3: Not logged into facebook (so can't tell if authorized this app)
		window.FB.getLoginStatus( (response) => {
			if (response.status === 'connected') {
				console.log("Already authorized this app");
				return this.facebookConnectedCallback(response);
			}
			if (response.status === 'not_authorized') {
				console.log("User has app not authorized, asking permission...");
				window.FB.login( (response) => {
					if (response.status === 'connected') {
						return this.facebookConnectedCallback(response);
					}
					return null;
				});
			} else {
				console.log("Can't establish connection with facebook (user not logged in?)");
				return null;
			};
		});
	}

	// *** LINKEDIN *** //
	handleLinkedinClick(e) {
		var current = encodeURIComponent(window.location.href)
		var state = Math.random().toString(36).substring(7);
		localStorage.linkedinState = state;
    localStorage.linkedinRedirectUri = window.location.href;
		localStorage.popup = 'popup open';
		//make a code request, linkedin returns to the current url (see componentDidMount for next step)
		window.location.href = 'https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=863oaf21ytstdm&redirect_uri='+current+'&state='+state+'&scope=r_basicprofile,r_emailaddress';
	}

	//mutation
	callbackLinkedin (code, state, redirectUri) {
		this.props.onLinkedinLogin(code, state, redirectUri);
  }

	//strip the values from the parameters returned by linkedin
	getQueryParameter(name) {
		const match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
  	return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
	}

	//grab the code returned by linkedin
	checkLinkedinCode() {
		const state = localStorage.linkedinState;
		const redirectUri = localStorage.linkedinRedirectUri;
		if (!state) return;
		if (!redirectUri) return;
		if (state !== this.getQueryParameter('state')) return;
    if (!this.getQueryParameter('code')) return;
		const code = this.getQueryParameter('code');
		this.linkedinReset();
		this.callbackLinkedin(code, state, redirectUri);
	}

	//resetting all localStorage and history set by the linkedin login
	linkedinReset() {
	  const code = this.getQueryParameter('code')
	  const state = this.getQueryParameter('state')
	  let newURL = window.location.href.replace(`code=${code}&state=${state}`, '')
	  if (newURL.endsWith('?')) {
	    newURL = newURL.slice(0, -1)
	  }
	  window.history.replaceState(null, null, newURL)
		localStorage.removeItem('linkedinState');
		localStorage.removeItem('linkedinRedirectUri');
		localStorage.removeItem('popup');
	}

	// *** GOOGLE *** //
	handleGoogleClick(e) {
		window.gapi.load('auth2', () => {
			var auth2 = window.gapi.auth2.init({
				client_id: "630392615970-o09atvjfsoqd4eisfcjrsr4436r66rrs.apps.googleusercontent.com",
				scope: "profile"
			});
			auth2.signIn().then((res) => {
				const authResponse = res.getAuthResponse();
				res.tokenId = authResponse.id_token;
				this.props.onGoogleLogin(res.tokenId);
			});
		});
	}

	// *** LOCAL (EMAIL, PASSWORD) *** //
	_formSubmit(e) {
		//no keycode when clicking on button, keycode for 'return' key when not.
		if (!e.keyCode || e.keyCode === 13) {
			e.preventDefault();
			this.setState({error : ''});
			var {email, password} = this.state;
			this.props.onLogin(email, password);
		}
	}

	_handleEmailChange(e){
		this.setState({error : ""});
		// eslint-disable-next-line
		var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
		if(!e.target.value){
			this.setState({error : "This field is required."});
		}else if(!re.test(e.target.value)){
			this.setState({error : "Email is not valid."});
		}
		this.setState({error : this.state.error});
		this.setState({email : e.target.value});
	}
	_handlePasswordChange(e){
		this.setState({error : ""});
		if(!e.target.value){
			this.setState({error : "This field is required."});
		}else if(e.target.value.length < 6){
			this.setState({error : "Password needs more than 6 characters."});
		}
		this.setState({error : this.state.error});
		this.setState({password : e.target.value});
	}

	// toggle login popup
	toggleLogin(event) {
		if (this.state.loginModal === "popup") {
			this.setState({loginModal: "popup open"});
		}else{
			this.setState({loginModal: "popup"});
		}
	}
	register(event){
		M.register();
		this.toggleLogin();
	}

	render() {
		let modalClassnames = this.state.loginModal;
		if (localStorage.popup) {
			modalClassnames = localStorage.popup;
		}
		return (
			<div>
				<div onClick={this.toggleLogin} style={{width: 100+'px',marginRight:-20+"px"}} className="btn accent-btn no-side-margin right">Login</div>
				<div id="loginBox" className={modalClassnames}>
					<div className="bg" onClick={this.toggleLogin}></div>
					<div className="box-helper"></div>
					<div className="box login">
						<div className="padded-box">
							<div className="row">
								<div className="col-xs-12 extra-pad-left extra-pad-right padding-sm">
									<div className="title color">Inloggen</div>
									<div onClick={this.toggleLogin} className="close blue-link right">Sluiten</div>
								</div>
							</div>
							<div className="dotted"></div>
							<div className="row">
								<div className="col-xs-12 col-sm-5 no-pad-right extra-pad-left padding-sm">
									<p className="color-grey">
										Login met een van deze accounts
									</p>
									<div className="sm-bar facebook" onClick={this.handleFacebookClick}>
										<i className="fa fa-facebook"></i>
										<span>Log in met Facebook</span>
									</div>
									<div className="sm-bar linkedin" onClick={this.handleLinkedinClick}>
										<i className="fa fa-linkedin"></i>
										<span>Log in met Linkedin</span>
									</div>
									<div className="sm-bar google" id="google-btn" onClick={this.handleGoogleClick}>
										<i className="fa fa-google"></i>
										<span>Log in met Google</span>
									</div>
								</div>
								<div className="hidden-xs col-sm-2 no-padding">
									<div className="center">
										<div className="center-line">
											<span className="color-grey">of</span>
										</div>
									</div>
								</div>
								<div className="col-xs-12 col-sm-5 no-pad-left extra-pad-right padding-sm">
									<p className="color-grey">
										Login met uw email adres
									</p>
									<input type="email" placeholder="Email adres" onChange={this._handleEmailChange} onKeyDown={this._formSubmit} value={this.state.email} />
									<input type="password" placeholder="Wachtwoord" onChange={this._handlePasswordChange} onKeyDown={this._formSubmit} value={this.state.password} />
									<div className="btns">
										<div className="btn primary-btn right no-side-margin" onClick={this._formSubmit}>Inloggen</div>
									</div>
									<p className="login-error">{this.state.error}</p>
								</div>
							</div>
							<div className="dotted extra-side-margin"></div>
							<div className="row">
								<div className="col-xs-12 extra-pad-right extra-pad-left padding-sm">
									<a onClick={this.register.bind(this)} className="blue-link">Ik heb nog geen account</a>
									<Link to="/wachtwoord/herstellen" className="blue-link right">Wachtwoord vergeten?</Link>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
export default Login;
