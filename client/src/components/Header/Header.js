import React, { Component } from 'react';
import Login from './Login';
import Notifications from '../Notifications/Notifications';
import ShareBox from './ShareBox';
import Avatar from '../../components/User/Avatar';
import {Link} from 'react-router';
import Relay from 'react-relay';

import M from '../../main.js';
import '../../stylesheets/header.css';

import LoginMutation from '../../mutations/LoginMutation';
import FacebookLoginMutation from '../../mutations/FacebookLoginMutation';
import GoogleLoginMutation from '../../mutations/GoogleLoginMutation';
import LinkedinLoginMutation from '../../mutations/LinkedinLoginMutation';

var input = "";

class Header extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showResults: "",
		};
		this.search = this.search.bind(this);
		this.hide = this.hide.bind(this);
		this.acceptCookies = this.acceptCookies.bind(this);
	}

	search(event) {
		input = event.target;
		this.props.relay.setVariables({ query: input.value});
		if(input.value.length > 1){
			this.setState({
				showResults: "open",
			});
		}else{
			this.setState({
				showResults: "",
			});
		}
	}
	hide(event) {
		this.setState({
			showResults: "",
		});
		input.value = "";
		this.props.relay.setVariables({ query: ""});
	}

	acceptCookies() {
		localStorage.cookiesAccepted = true;
		this.forceUpdate();
	}

	_onLogin(email, password) {
		this.props.relay.commitUpdate(new LoginMutation({
			viewer: this.props.notifications,
			email,
			password,
		}), {
			onSuccess: (response) => {
				var {authToken} = response.LoginMutation;
				localStorage.authToken = authToken;
				window.location.reload();
			},
			onFailure: (t) => {
				M.toast("Inloggen mislukt!", "Controleer of uw gegevens kloppen","error");
				console.log('LoginMutation FAILED', t.getError())
			},
		});
	}

	_onGoogleLogin(googleAuthToken) {
		this.props.relay.commitUpdate(new GoogleLoginMutation({
			viewer: this.props.notifications,
			googleAuthToken,
		}), {
			onSuccess: (response) => {
				var {msg, authToken} = response.GoogleLoginMutation;
				this.socialLoginMutationCb(msg, authToken);
			},
			onFailure: (t) => {
				M.toast("Inloggen mislukt!", "Probeer het nogmaals","error");
			},
		});
	}

	_onLinkedinLogin(linkedinAuthToken, linkedinState, redirectUri) {
		this.props.relay.commitUpdate(new LinkedinLoginMutation({
			viewer: this.props.notifications,
			linkedinAuthToken,
			linkedinState,
			redirectUri,
		}), {
			onSuccess: (response) => {
				var {msg, authToken} = response.LinkedinLoginMutation;
				this.socialLoginMutationCb(msg, authToken);
			},
			onFailure: (t) => {
				M.toast("Inloggen mislukt!", "Probeer het nogmaals","error");
			},
		});
	}

	_onFbLogin(facebookAuthToken) {
		this.props.relay.commitUpdate(new FacebookLoginMutation({
			viewer: this.props.notifications,
			facebookAuthToken,
		}), {
			onSuccess: (response) => {
				var {msg, authToken} = response.FacebookLoginMutation;
				this.socialLoginMutationCb(msg, authToken);
			},
			onFailure: (t) => {
				M.toast("Inloggen mislukt!", "Probeer het nogmaals","error");
			},
		});
	}

	socialLoginMutationCb(msg, authToken) {
		if (msg) {
			M.toast("Mislukt", msg, "error");
			return null;
		}
		localStorage.authToken = authToken;
		window.location.reload()
	}

	render() {
		let userMenuMobile;
		const cookiesAccepted = localStorage.cookiesAccepted;
		var {notifications} = this.props;


		if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
			if(notifications.AuthUser){
				userMenuMobile = <Notifications viewer={this.props.notifications} notifications={this.props.notifications}/>;
			}else{
				userMenuMobile = <Login setLoggedIn={this.setLoggedIn}/>;
			}
		}

		// Query for tags
		let tags = this.props.notifications.TagValueByQuery.edges;
		// Query for users
		let users = this.props.notifications.getUsersByQuery.edges;
		// Query for questions
		let questions = this.props.notifications.QuestionsByQuery.edges;



		return (
			<header>
				<div className="container">
					<div className="row">
						<div className="col-xs-6 col-sm-3 col-md-3 left-panel">
							<Link to="/" className="logo">
								<div className="logo-shape" style={{background: "url('../../images/logo.svg')"}}>
								</div>
								<div className="logo-text">
									<span>Burger</span>zetel
								</div>
							</Link>
						</div>
						<div className="col-xs-6 visible-xs left-panel menu-actions">
							{userMenuMobile}
						</div>
						<div className="col-xs-12 col-sm-6 no-padding padding-sm center-panel">
							<div className="row">
								<div className="col-xs-7 col-sm-8 col-lg-9 no-pad-right searchbox">
									<div className="preview-holder search">

										<input type="search" autoComplete="off" placeholder="Zoeken" className="bg radius" onChange={this.search}/>
										<button type="submit" className="grey-link">
											<i className="fa fa-search"></i>
										</button>
										<div onClick={this.hide} className={"full-bg "+ this.state.showResults}></div>
										<div className={"preview "+ this.state.showResults}>
											<div className="pointy"></div>
											{questions.length ? (
												Object.keys(questions).map((key) => {
													let question = questions[key].node;
													return (
														<Link onClick={this.hide} to={"/vraag/"+question.slug} key={key} className="line-item color">
															{question.title}
														</Link>
													)
												})
											):(
												<div className="preview-item no-result">
													<div className="color">Geen vragen gevonden</div>
													<span className="color-grey">Probeer een ander trefwoord of stel zelf een vraag</span>
												</div>
											)}
											{tags.length ?  <div className="header color-accent">Onderwerpen</div> : <span></span> }
											{tags.length ? (
												Object.keys(tags).map((key) => {
													let tag = tags[key].node;
													let previewImg = "";
													if(tag.Page){
														previewImg = tag.Page.img;
													}

													return (
														<Link onClick={this.hide} to={"/onderwerp/"+tag.key} key={key} className="line-item wa" data-val="Onderwijs">
															<Avatar imagePath={previewImg} type="page" tag={tag.value} hasURL="false"/>
															<div className="color">{tag.value}</div>
														</Link>
													)
												})
											):(
												<span></span>
											)}
											{users.length ?  <div className="header color-accent">Gebruikers</div> : <span></span> }
											{users.length ? (
												Object.keys(users).map((key) => {
													let user = users[key].node;
													return (
														<Link onClick={this.hide} to={"/"+user.userName} key={key} className="line-item wa">
															<Avatar imagePath={user.avatar} userName={user.userName} userFirstName={user.firstName} userLastName={user.lastName} hasURL="false"/>
															<div className="color">{user.firstName+" "+user.lastName}</div>
														</Link>
													)
												})
											):(
												<span></span>
											)}
											<div className="ask-question">
												<span className="color-grey">Staat uw vraag er niet tussen?</span>
												<Link onClick={this.hide} to="/vraag/nieuw" className="btn primary-btn right">Stel een vraag</Link>
											</div>
										</div>
									</div>
								</div>
								<div className="col-xs-5 col-sm-4 col-lg-3">
									{notifications.AuthUser ? (
										<Link to="/vraag/nieuw" className="btn outline-btn" >Stel een vraag</Link>
									):(
										<div className="btn outline-btn" onClick={M.login}>Stel een vraag</div>
									)}
								</div>
							</div>
						</div>
						<div className="col-sm-3 visible-sm visible-md visible-lg">
							<div className="text-right right-menu">
								{(notifications.AuthUser) ?
									<Notifications notifications={notifications} viewer={notifications}/>
								:
									<div>
										<Login onLinkedinLogin={this._onLinkedinLogin.bind(this)} onGoogleLogin={this._onGoogleLogin.bind(this)} onFbLogin={this._onFbLogin.bind(this)} onLogin={this._onLogin.bind(this)} />
									</div>
								}
							</div>
						</div>
					</div>
				</div>
				<ShareBox title={this.props.title} description={this.props.description}/>
				<div id="loader"></div>
				{(!cookiesAccepted || cookiesAccepted !== 'true') ? (
					<div className="cookie-bar color">
						Burgerzetel.nl maakt gebruik van cookies om een zo goed mogelijke ervaring voor u te creëren.
						<div className="btn primary-btn" onClick={this.acceptCookies}>Accepteer</div>
						<Link to="/info/cookies/" className="blue-link">Meer informatie</Link>
					</div>
				):(
					<span></span>
				)}
			</header>
		);
	}
}

export default Relay.createContainer(Header, {
		initialVariables: {
			query: "",
		},
		fragments: {
			notifications: () => Relay.QL `
				fragment on viewer {
					${LoginMutation.getFragment('viewer')},
					${FacebookLoginMutation.getFragment('viewer')},
					${GoogleLoginMutation.getFragment('viewer')},
					${LinkedinLoginMutation.getFragment('viewer')},
					${Notifications.getFragment('viewer')},
					AuthUser {
						id,
						firstName,
						lastName,
						avatar,
						userName,
						Notifications(first:12){
							edges{
								node{
									nid,
									dateCreated,
									NotificationType{
										value,
										key,
									},
									User{
										firstName,
										lastName,
										avatar,
										userName,
									},
									AskedBy{
										firstName,
										lastName,
										avatar,
										userName,
									},
									Reply{
										reply,
										User{
											avatar,
											firstName,
											lastName,
											userName,
										},
									},
									Reaction{
										rid,
										reaction,
										User{
											avatar,
											firstName,
											lastName,
											userName,
										},
									},
									Answer{
										aid,
										answer,
										User{
											avatar,
											firstName,
											lastName,
											userName,
										},
									},
									Question{
										title,
										slug,
									},


									msg,
								},
							},
						},
						NotificationCount,
					}
					QuestionsByQuery(query:$query,first:7){
						edges{
							node{
								title,
								slug,
							},
						},
					},
					TagValueByQuery(query:$query,first:3) {
						edges{
							node{
								tagValueId,
								key,
								value,
								used,
								Page{
									img,
								},
							},
						},
					},
					getUsersByQuery(query:$query,first:3) {
						edges{
							node{
								uid,
								firstName,
								lastName,
								userName,
								avatar,
								function,
							},
						},
					},
				},
			`,
		},
	});
