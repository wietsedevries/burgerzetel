import React, { Component } from 'react';
import { FacebookButton, TwitterButton } from 'react-social';
import '../../stylesheets/actions.css';

class ShareBox extends Component {
	constructor(props) {
		super(props);
		this.toggleShare = this.toggleShare.bind(this);
	}

	toggleShare(e){
		let elm = document.getElementById('sharebox');
		elm.className = "";
	}

  render() {
		let url = "http://burgerzetel.nl/"+this.context.router.location.pathname;
		let title = this.props.title;
		let msg = this.props.description;
		let appId = "1750069001983788";
    return (
			<div id="sharebox">
				<div className="bg" onClick={this.toggleShare}></div>
				<div className="box">
					<div className="title color">Delen</div>
					<div onClick={this.toggleShare} className="close blue-link right">Sluiten
					</div>
					<div className="clearfix"></div>
					<div className="dotted"></div>
					<div className="row">
						<div className="col-xs-4">
							<FacebookButton appId={appId} className="facebook share-btn" element="div" url={url} message={msg}>
									<i className="fa fa-facebook"></i>
									<div></div>
				      </FacebookButton>
						</div>
						<div className="col-xs-4">
							<TwitterButton className="twitter share-btn" element="div" url={url} message={msg}>
									<i className="fa fa-twitter"></i>
									<div></div>
				      </TwitterButton>
						</div>
						<div className="col-xs-4">
							<a href={"https://www.linkedin.com/shareArticle?mini=true&url="+url+
							"&title="+title+"%20%20%20%20&summary="+msg} target="_blank" className="linkedin share-btn">
								<i className="fa fa-linkedin"></i>
								<div></div>
							</a>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
ShareBox.contextTypes = {
	router: React.PropTypes.object
};

export default ShareBox;
