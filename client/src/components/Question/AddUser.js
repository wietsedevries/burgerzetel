import React, { Component } from 'react';
import Relay from 'react-relay';
import Avatar from '../../components/User/Avatar';

import '../../stylesheets/ask.css';

let limit = 0;
var input = "";
var componentList = [];
let count = 0;

// Simple card component for searchlist
class Card extends Component {
	render() {
		var label = this.props.label;
		var avatar = this.props.avatar;
		var firstName = this.props.firstName;
		var lastName = this.props.lastName;
		var userName = this.props.userName;
		var userFunction = this.props.function;
		var uid = this.props.uid;

		return (
			<div className='recipient'>
				<Avatar imagePath={avatar} userName={userName} userFirstName={firstName} userLastName={lastName} hasURL="false"/>
				<div className="color">{label}</div>
				<div className="function color-grey">{userFunction}</div>
				<i data-id={uid} className='fa fa-times remove'></i>
			</div>
		);
	}
}
// Actual component
class AddUser extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showResults: "",
			directedQuestion: this.props.directedQuestion,
		};
		this.addUser = this.addUser.bind(this);
		this.search = this.search.bind(this);
	}

	addUser(event) {
		var uid  = event.target.getAttribute('data-uid');
		var label  = event.target.getAttribute('data-label');
		var avatar  = event.target.getAttribute('data-avatar');
		var userName  = event.target.getAttribute('data-userName');
		var firstName  = event.target.getAttribute('data-firstName');
		var lastName  = event.target.getAttribute('data-lastName');
		var userFunction  = event.target.getAttribute('data-function');
		var createdUsers = document.getElementsByClassName("recipient");
		count = createdUsers.length;
		this.setState({
			showResults: "",
		});
		if ((limit === 0 || count <= limit) && label !== null) {
			componentList.push(<Card uid={uid} function={userFunction} key={userName+Math.random()} userName={userName} avatar={avatar} firstName={firstName} lastName={lastName} label={label} />);
			count++;
			this.props.onAdd(uid);
			if(count >= limit){
				input.disabled = true;
			}
		}
		input.value = "";
		input.focus();
	}
	remove(event) {
		if (event.target.classList.contains('remove')){
			event.target.parentNode.remove(event.target.parentNode);
			input.disabled = false;
			count--;
			var key  = event.target.getAttribute('data-id');
			this.props.onRemove(key);
		}
	}

	search(event) {
		input = event.target;
		this.props.relay.setVariables({ query: input.value});
		if(input.value.length > 1){
			this.setState({
				showResults: "open",
			});
		}else{
			this.setState({
				showResults: "",
			});
		}
	}

  render() {
		limit = this.props.limit;
		let users = this.props.viewer.getUsersByQuery.edges;
		let anyUsers = false;
		if(users.length > 0){
			anyUsers = true;
		}
		//already invited users, for edit (question) page
		let invitedUsers;
		if (this.props.addedUsers) {
			invitedUsers = this.props.addedUsers;
		}
		let invitedUsersEl = (
			<div></div>
		)
		if (invitedUsers) {
			invitedUsersEl = (
				<div>
					{Object.keys(invitedUsers).map(function(key) {
						let invite = invitedUsers[key].node.To;
						return (
							<Card function={invite.function} key={invite.userName+key} userName={invite.userName}
								avatar={invite.avatar} firstName={invite.firstName} lastName={invite.lastName} label={invite.firstName+" "+invite.lastName} />
						)
					})}
				</div>
			)
		}
		let dq = this.state.directedQuestion;
		if (dq) {
			invitedUsersEl = (
				<div>
					<Card uid={dq.uid} function={dq.function} userName={dq.userName} avatar={dq.avatar} firstName={dq.firstName} lastName={dq.lastName} label={dq.firstName+" "+dq.lastName} />
				</div>
			)
		}
		return (

			<div className="specific-user color preview-holder">
				<input type="text" placeholder="Begin met typen om een gebruiker te zoeken" onChange={this.search}/>
				<div className={"preview "+ this.state.showResults} onClick={this.addUser}>
					{anyUsers ? (
						Object.keys(users).map(function(key) {
							let user = users[key].node;
							return (
								<div className="preview-item" key={key} data-uid={user.uid} data-function={user.function} data-userName={user.userName} data-label={user.firstName+" "+user.lastName} data-avatar={user.avatar}  data-firstName={user.firstName} data-lastName={user.lastName} >
									<Avatar imagePath={user.avatar} userName={user.userName} userFirstName={user.firstName} userLastName={user.lastName} hasURL="false"/>
									<div className="color">{user.firstName+" "+user.lastName}</div>
									<span className="color-grey">{user.function}</span>
								</div>
							)
						})
					):(
						<div className="preview-item no-result">
							<div className="color">Geen resultaten</div>
							<span className="color-grey">Probeer een andere gebruiker</span>
						</div>
					)}
				</div>
				<div className="tags recipients">
					<span id="user-area" onClick={this.remove.bind(this)}>
						{componentList}
						{invitedUsersEl}
					</span>
				</div>
			</div>

    );
  }
}

export default Relay.createContainer(AddUser, {
		initialVariables: {
			query: "",
		},
		fragments: {
			viewer: () => Relay.QL `
				fragment on viewer {
					getUsersByQuery(query:$query,first:5) {
						edges{
							node{
								uid,
								firstName,
								lastName,
								userName,
								avatar,
								function,
							},
						},
					},
				},
			`
		},
});
