import React, { Component } from 'react';
import FroalaEditor from 'react-froala-wysiwyg';
import $ from 'jquery';

import "froala-editor/js/froala_editor.pkgd.min.js";
import "froala-editor/js/languages/nl.js";
import "froala-editor/js/plugins/emoticons.min.js";
import "froala-editor/js/plugins/lists.min.js";
import "froala-editor/js/plugins/image.min.js";
import "froala-editor/js/plugins/link.min.js";
import "froala-editor/css/froala_editor.pkgd.min.css";
import "../../stylesheets/froala.css";




class RichEditor extends Component {
	config = {
			nameText: 'description',
			placeholderText: 'Additionele informatie &bull; Zodat anderen uw vraag zo goed mogelijk kunnen beantwoorden',
			heightMin: 140,
			theme: 'burgerzetel',
			charCounterCount: false,
			language: 'nl',
			toolbarButtons: ['insertImage','insertLink','emoticons','bold', 'italic','formatOL','formatUL'],
			toolbarButtonsMD: ['insertImage','insertLink','emoticons','bold', 'italic','formatOL','formatUL'],
			toolbarButtonsSM: ['insertImage','insertLink','emoticons','bold', 'italic','formatOL','formatUL'],
			toolbarButtonsXS: ['insertImage','insertLink','emoticons','bold', 'italic','formatOL','formatUL'],
			emoticonsSet: [
				{code: '1f600', desc: 'Grinning face'},
				{code: '1f601', desc: 'Grinning face with smiling eyes'},
				{code: '1f602', desc: 'Face with tears of joy'},
				{code: '1f609', desc: 'Winking face'},
				{code: '1f914', desc: 'thinking face face'},
				{code: '1f910', desc: 'zipper face'},
				{code: '1f635', desc: 'Dizzy face'},
				{code: '1f621', desc: 'Pouting face'},
			],
			emoticonsStep: 8,
			tooltips: false,
			quickInsertButtons: [],
			pastePlain: true,
			enter: $.FroalaEditor.ENTER_BR,
			shortcutsEnabled: ['bold', 'italic'],
			pluginsEnabled: ['image', 'link','emoticons','lists'],
			imageAllowedTypes: ['jpeg', 'jpg', 'png'],
			imageDefaultAlign: 'left',
			imageDefaultWidth: 200,
			imageEditButtons: ['imageRemove'],
			imageInsertButtons: ['imageUpload'],
			imageMaxSize: 1024 * 1024 * 3,
			imageMove: false,
			imageMinWidth: 32,
			imagePasteProcess: true,
			imageDefaultDisplay: 'block',
			imageUploadMethod: 'POST',
			imageUploadURL: '/upload_image',
			key: 'pkC-9ykhC-22uuewC-9B1H-8z==',

			linkAlwaysBlank: true,
			linkConvertEmailAddress: false,
			linkEditButtons: ['linkEdit', 'linkRemove'],
			linkInsertButtons: [],
			linkList: [],
			linkStyles: {},
			linkText: true,
			options: {
				name: 'description'
			},
			events : {
				'froalaEditor.image.removed' : function(e, editor, img) {
					$.ajax({
						method: 'POST',
						url: '/delete_image',
						data: {
							src: img.attr("src")
						}
					})
					.done (function (data) {
						console.log ('Image was deleted');
					})
					.fail (function (err) {
						console.log ('Image delete problem: ' + JSON.stringify(err));
					})
				}
			}
	}

	componentDidMount() {
		$('#editor textarea').froalaEditor('html.set', this.props.value);
	}

	render() {
		return (
			<span id="editor">
				<FroalaEditor ref="description" config={this.config} tag="textarea" name='description'></FroalaEditor>
			</span>
		);
	}
}

export default RichEditor;
