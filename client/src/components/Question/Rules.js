import React, { Component } from 'react';
import {Link} from 'react-router';

class Rules extends Component {
  render() {
    return (
      <div className="panel padding">
        <div className="panel-title bold">
          <i className="fa fa-gavel"></i>
          <span>Huisregels</span>
        </div>
        <div className="dotted"></div>
				<div className="color-grey">
					Lees de huisregels goed door en voorkom dat uw vraag wordt gerapporteerd door andere leden.
				</div>
				<br/>
				<Link to="/info/huisregels" target="_blank" className="color-accent"><i className="fa fa-caret-right"></i> Bekijk de Huisregels</Link>
      </div>
    );
  }
}

export default Rules;
