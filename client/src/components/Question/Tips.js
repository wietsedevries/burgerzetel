import React, { Component } from 'react';

class Tips extends Component {
  render() {
    return (
      <div className="panel padding alert">
        <div className="panel-title bold">
          <i className="fa fa-check" aria-hidden="true"></i>
          <span>Checklist</span>
        </div>
        <div className="dotted"></div>

        <ul className="tips">
					<li><i></i>Is de vraag niet al eerder gesteld?</li>
          <li><i></i>Is de vraag op een politiek correcte wijze geformuleerd?</li>
          <li><i></i>Is de vraag specifiek genoeg?</li>
					<li><i></i>Is de vraag goed onderbouwd met additionele informatie?</li>
          <li><i></i>Zijn er relevante bijlages toegevoegd?</li>
        </ul>
      </div>
    );
  }
}

export default Tips;
