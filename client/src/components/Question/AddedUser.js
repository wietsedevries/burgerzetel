import React, { Component } from 'react';

class AddedUser extends Component {
	render() {
		return (
			<span>
				<div className='created-tag color'>
					<span>{this.props.value}</span>
					<i data-id={this.props.tagValueId} className='fa fa-times remove'></i>
				</div>
				<input value={this.props.tagKey} ref='tag' type='hidden'/>
			</span>
		);
	}
}

export default AddedUser;
