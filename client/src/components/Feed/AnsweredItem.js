import React, { Component } from 'react';
import {Link} from 'react-router';
import Relay from 'react-relay';

import ViewQuestion from '../../components/Actions/ViewQuestion';
import SaveQuestion from '../../components/Actions/SaveQuestion';
import Avatar from '../../components/User/Avatar';
import '../../stylesheets/feed.css';
import SaveMutation from '../../mutations/SaveMutation';

class AnsweredItem extends Component {
	constructor(props) {
		super(props);
		var Favorites
		if(this.props.own){
			Favorites = this.props.data.Question.Favorites.edges.length;
		}else{
			Favorites = this.props.data.Favorites.edges.length;
		}

		let status = "incomplete";
		if(Favorites > 0){
			status = "completed";
		}

		this.state = {
			actionState: status,
		};
	}

	toggleQuestion (questionId) {
		this.props.relay.commitUpdate(new SaveMutation({
			viewer: this.props.viewer,
			questionId,
		}), {
			onSuccess: (response) => {
				if(this.state.actionState === "completed"){
					this.setState({actionState: "incomplete"});
				}else{
					this.setState({actionState: "completed"});
				}
			},
			onFailure: (t) => {
				console.log('SaveMutation FAILED', t.getError());
			},
		});
	}
	render() {
		const {viewer} = this.props;
		let data = this.props.data;

		let answer;
		let user;
		let title;
		let slug;
		let questionId;

		if(this.props.own){
			answer = data.answer;
			user = data.User;
			title = data.Question.title;
			slug = data.Question.slug;
			questionId = data.Question.qid;
		}else{
			answer = data.Answers.edges[0].node.answer;
			user = data.Answers.edges[0].node.User;
			title = data.title;
			slug = data.slug;
			questionId = data.qid;
		}
		let badge = "";
		if(user.verified > 0) {
			badge = (<i className='fa fa-check-circle'></i>);
		}
		answer = answer.replace(/(<([^>]+)>)/ig, "").replace(/â€‹/g, "").replace(/&nbsp;/g, " ");

		return (
			<div className="panel">
				<div className="item answered">
					<Link to={"/vraag/"+slug} className="title color">
						{title}
					</Link>
					<div className="details">
						<Avatar imagePath={user.avatar} userFirstName={user.firstName} userLastName={user.lastName} userName={user.userName}/>
						<Link to={"/"+user.userName} className="name color-accent">{user.firstName} {user.lastName} {badge}</Link>
						<div className="tag-line">{user.function}</div>
						<div className="description color-grey">{answer}</div>
						<div className="actions color-grey sm">
							<ViewQuestion boxed="true" slug={slug}/>
							<SaveQuestion actionState={this.state.actionState} boxed="true" viewer={viewer} questionId={questionId} onClick={this.toggleQuestion.bind(this)}/>
						</div>
					</div>
				</div>
			</div>
		);
	}
}


export default Relay.createContainer(AnsweredItem, {
	fragments: {
			viewer: () => Relay.QL `
				fragment on viewer {
					${SaveMutation.getFragment('viewer')},
					${SaveQuestion.getFragment('viewer')},
				}
			`
	}
});
