import React, { Component } from 'react';
import {Link} from 'react-router';
import Relay from 'react-relay';

import AnswerQuestion from '../../components/Actions/AnswerQuestion';
import SaveQuestion from '../../components/Actions/SaveQuestion';
import Avatar from '../../components/User/Avatar';
import '../../stylesheets/feed.css';
import SaveMutation from '../../mutations/SaveMutation';

class UnansweredItem extends Component {
	constructor(props) {
		super(props);
		var Favorites = this.props.data.Favorites.edges.length;
		let status = "incomplete";
		if(Favorites > 0){
			status = "completed";
		}

		this.state = {
			actionState: status,
		};
	}
	toggleQuestion (questionId) {
		this.props.relay.commitUpdate(new SaveMutation({
			viewer: this.props.viewer,
			questionId,
		}), {
			onSuccess: (response) => {
				if(this.state.actionState === "completed"){
					this.setState({actionState: "incomplete"});
				}else{
					this.setState({actionState: "completed"});
				}
			},
			onFailure: (t) => {
				console.log('SaveMutation FAILED', t.getError());
			},
		});
	}
	render() {
		const {viewer} = this.props;
		let data = this.props.data;
		let user = this.props.user;
		let desc = data.description.replace(/(<([^>]+)>)/ig, " ").replace(/â€‹/g, "").replace(/&nbsp;/g, " ");
		let badge = "";
		if(user.verified > 0) {
			badge = (<i className='fa fa-check-circle'></i>);
		}

		return (
			<div className="panel">
				<div className="item">
					<Avatar imagePath={user.avatar} userFirstName={user.firstName} userLastName={user.lastName} userName={user.userName}/>
					<Link to={"/"+user.userName} className="name color-accent">{user.firstName} {user.lastName} {badge}</Link>
					<Link to={"/vraag/" + data.slug} className="title color">
						{data.title}
					</Link>
					<div className="description color-grey">
						{desc}
					</div>
					<div className="actions color-grey sm">
						<AnswerQuestion boxed="true" slug={data.slug}/>
						<SaveQuestion actionState={this.state.actionState} boxed="true" viewer={viewer} questionId={data.qid} onClick={this.toggleQuestion.bind(this)}/>
					</div>
				</div>
			</div>
		);
	}
}

export default Relay.createContainer(UnansweredItem, {
	fragments: {
			viewer: () => Relay.QL `
				fragment on viewer {
					${SaveMutation.getFragment('viewer')},
					${SaveQuestion.getFragment('viewer')},
				}
			`
	}
});
