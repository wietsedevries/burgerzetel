import M from '../../main.js';
import React, { Component } from 'react';
import Relay from 'react-relay';

import VoteMutation from '../../mutations/VoteMutation';

class Poll extends Component {
	constructor(props) {
			super(props);
			let edit = false;
			if(this.props.edit){
				edit = true;
			}
			let selected;
			this.state = {
				showResults: false,
				edit: edit,
				selected: selected,
			};
			this.castVote = this.castVote.bind(this);
	}

	getCheckedRadio(radios) {
		for (var i = 0; i < radios.length; i++) {
			var radio = radios[i];
			if (radio.checked) {
				return radio.value;
			}
		}
		return false;
	}

	castVote(event) {
		const radio_group = document.getElementsByName("poll");
		const checkedRadio = this.getCheckedRadio(radio_group);
		console.log(checkedRadio);
		if (checkedRadio) {
			//vote is casted

			this.props.relay.commitUpdate(new VoteMutation({
				viewer: this.props.viewer,
				optionId: parseInt(checkedRadio, 10),
			}), {
				onSuccess: (response) => {
					M.toast("Gestemd", "Uw stem is geteld");
					this.setState({
						showResults: true,
						selected: checkedRadio,
					});
				},
				onFailure: (t) => {
					M.toast("Stemmen mislukt", "Er is iets fout gegaan met het stemmen, probeer opnieuw","error");
					console.log('SaveMutation FAILED', t.getError());
				},
			});
		} else {
			//no options was selected
			const messageBox = document.getElementById("js_message");
			messageBox.innerHTML="Selecteer eerst een optie!";
		}
	}

	render() {
		const options = this.props.options.edges;
		const currentUser = this.props.currentUser;

		let results = [];
		let showResults = false;
		let totalVotes = 0;
		if(currentUser){
			for (let i = 0; i < options.length; i++) {
				const votes = options[i].node.Votes.edges;
				// let count = 0;
				for (let i = 0; i < votes.length; i++) {
					if (votes[i].node.User.userName === currentUser) {
						showResults = true;
					}
				}
				results[i] = votes.length;
				totalVotes += votes.length;
			}
			//calculate percentage per option
			for (let i = 0; i < results.length; i++) {
				if(totalVotes > 0){
					results[i] *= (100 / totalVotes);
				}else{
					results[i] *= 0;
				}
			}
		}

		let pollFormat;
		if (!showResults && !this.state.showResults){
			//render the voting part
			pollFormat = (
				<div>
					<div className="sub-title color">{this.props.title}</div>
					{Object.keys(options).map(function(key) {
						let value = options[key].node.value;
						let optionId = options[key].node.optionId;
						return (
							<label key={key} className="option color" htmlFor={"divid"+key}>
								<input type="radio" id={"divid"+key} name="poll" defaultValue={optionId}/>
								<span>{value}</span>
							</label>
						)
					})}
					<div id="js_message" className="color-grey"></div>
					{this.props.edit ? (
						""
					):(
						currentUser ? (
							<div className="btn primary-btn" onClick={this.castVote}>Stem</div>
						):(
							<div className="btn primary-btn" onClick={M.login}>Stem</div>
						)
					)}


				</div>
			)
		} else {
			//render the results part
			pollFormat = (
				<div>
					<div className="sub-title color">{this.props.title}</div>
					{Object.keys(options).map(function(key) {
						let value = options[key].node.value;
						return (
						<div key={key}><div className="result-label color-grey">{value}</div>
							<div className="result-progress">
								<div style={{width: results[key]+"%"}}>
									{results[key] < 15 ? (
										<span className="poll-percentage low">{(parseFloat(results[key]).toFixed(1)-1)+1+"%"}</span>
									):(
										<span className="poll-percentage">{(parseFloat(results[key]).toFixed(1)-1)+1+"%"}</span>
									)}
								</div>
							</div>
						</div>
						)
					})}
					<div className="sub-title color">{totalVotes}x gestemd</div>
				</div>
			)
		}
		if (this.props.edit) {
			return pollFormat;
		}else{
			return (
				<div className="panel padding">
					<div className="panel-title">
						<i className="fa fa-bar-chart" aria-hidden="true"></i>
						<span>poll</span>
					</div>
					<div className="dotted"></div>
					{pollFormat}
				</div>
			);

	}}
}

export default Relay.createContainer(Poll, {
	fragments: {
		viewer: () => Relay.QL `
			fragment on viewer {
				${VoteMutation.getFragment('viewer')}
			}
		`
	}
});
