import React, { Component } from 'react';

class PollItem extends Component {
	constructor(props) {
			super(props);
			this.state = {
				value: "",
			};
	}
	addPoll(event) {
		var value  = event.target.value;
		this.setState({value: value});
		this.props.onAdd(value);
	}
	remove(event) {
		var value  = event.target.value;
		this.props.onRemove(value);
	}
	removeEl(event){
		event.target.parentNode.remove(event.target.parentNode);
		this.props.onRemove(event.target.getAttribute('data-value'));
	}
	render() {
		return (
			<span>
				<div className="add-option">
					<input type="radio" className="radio" disabled/>
					<input type="text" defaultValue={this.props.value} onBlur={this.addPoll.bind(this)} onFocus={this.remove.bind(this)} className="value" maxLength="50" placeholder="Optie"/>
					<i className="fa fa-times remove" data-value={this.state.value} onClick={this.removeEl.bind(this)}></i>
				</div>
			</span>
		);
	}
}
export default PollItem;
