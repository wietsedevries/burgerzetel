import M from '../../main.js';
import React, { Component } from 'react';
import FroalaEditor from 'react-froala-wysiwyg';
import {Link} from 'react-router';
import $ from 'jquery';
import Avatar from '../User/Avatar';
import NotLoggedIn from './NotLoggedIn';

import "froala-editor/js/froala_editor.pkgd.min.js";
import "froala-editor/js/languages/nl.js";
import "froala-editor/js/plugins/emoticons.min.js";
import "froala-editor/js/plugins/lists.min.js";
import "froala-editor/js/plugins/image.min.js";
import "froala-editor/js/plugins/link.min.js";
import "froala-editor/css/froala_editor.pkgd.min.css";
import "../../stylesheets/froala.css";

let randomNumber;

class PoorEditor extends Component {

	post() {
		let itemId;
		if (this.props.answerId) {
			itemId = this.props.answerId;
		}
		if (this.props.reactionId) {
			itemId = this.props.reactionId;
			console.log("yesh",itemId,this.props.reactionId);
		}
		$("#editor-"+randomNumber).find('textarea').attr('id','description');
		var description = $("#description").val();
		if (description === "" || description === null) {
			M.toast("Geen tekst ingevoerd", "Het veld mag niet leeg zijn", "error");
			return null;
		}

		//passUp in Answer.js OR Reaction.js
		this.props.onClick(randomNumber, itemId, description);
	}

	config = {
			placeholderText: 'Uw reactie',
			heightMin: 90,
			theme: 'burgerzetel',
			charCounterCount: false,
			language: 'nl',
			toolbarButtons: ['insertLink','emoticons'],
			toolbarButtonsMD: ['insertLink','emoticons'],
			toolbarButtonsSM: ['insertLink','emoticons'],
			toolbarButtonsXS: ['insertLink','emoticons'],
			emoticonsSet: [
				{code: '1f600', desc: 'Grinning face'},
				{code: '1f601', desc: 'Grinning face with smiling eyes'},
				{code: '1f602', desc: 'Face with tears of joy'},
				{code: '1f609', desc: 'Winking face'},
				{code: '1f914', desc: 'thinking face face'},
				{code: '1f910', desc: 'zipper face'},
				{code: '1f635', desc: 'Dizzy face'},
				{code: '1f621', desc: 'Pouting face'},
			],
			emoticonsStep: 8,
			tooltips: false,
			quickInsertButtons: [],
			pastePlain: true,
			enter: $.FroalaEditor.ENTER_BR,
			shortcutsEnabled: [],
			pluginsEnabled: ['link','emoticons'],
			key: 'pkC-9ykhC-22uuewC-9B1H-8z==',

			linkAlwaysBlank: true,
			linkConvertEmailAddress: false,
			linkEditButtons: ['linkEdit', 'linkRemove'],
			linkInsertButtons: [],
			linkList: [],
			linkStyles: {},
			linkText: true,
	}

	render() {
		randomNumber = this.props.rNumber;
		let user = this.props.currentUser;
		if(user){
			return (
				<form id={"editor-"+randomNumber} className="poor-editor-form">
					<div className="poor-editor-header">
						<Avatar imagePath={user.avatar} userName={user.userName} userFirstName={user.firstName} userLastName={user.lastName}/>
						<Link to={"/"+user.userName} className="name color">{user.firstName+" "+user.lastName}</Link>
					</div>
					<FroalaEditor config={this.config} tag='textarea'/>
					<div className="editor-actions">
						<div className="btn primary-btn" onClick={this.post.bind(this)}>Reageren</div>
						<div className="share color">
							{/* <span>Delen:</span>
							<input type="checkbox" id="facebook"/>
							<label htmlFor="facebook" className="color">
								<i className="fa fa-facebook-official"></i>
							</label>
							<input type="checkbox" id="twitter"/>
							<label htmlFor="twitter" className="color-accent">
								<i className="fa fa-twitter"></i>
							</label> */}
						</div>
					</div>
				</form>
			);
		}else{
			return (
				<NotLoggedIn/>
			 );
		}
	}
}

export default PoorEditor;
