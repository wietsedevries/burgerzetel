import M from '../../main.js';
import React, { Component } from 'react';

class NotLoggedIn extends Component {

	render() {
		return (
			<div className="not-logged-in">U moet ingelogd zijn om te kunnen reageren. Klik <span className="color-accent" onClick={M.login}>hier</span> om in te loggen.</div>
		 );
	}
}

export default NotLoggedIn;
