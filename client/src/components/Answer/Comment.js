import React, { Component } from 'react';
import Relay from 'react-relay';
import Moment from 'moment';
import {Link} from 'react-router';

import Avatar from '../User/Avatar';
import Upvote from '../Actions/Upvote';
import Report from '../Actions/Report';
import DummyReport from '../../components/Actions/DummyReport';

class Comment extends Component {
	constructor(props) {
		super(props);
		let flagged = false;
		let upvoted = false;
		if(this.props.viewer.AuthUser){
			// Check if reaction has been reported by user
			let flaggedReplies = this.props.viewer.AuthUser.FlaggedReplies.edges;
			Object.keys(flaggedReplies).map((key) => {
				if(this.props.reply.replyId === flaggedReplies[key].node.itemId){
					return flagged = true;
				}else{
					return false;
				}
			})
			// Check if reaction has been upvoted by user
			let upvotedReplies = this.props.viewer.AuthUser.UpvotedReplies.edges;

			Object.keys(upvotedReplies).map((key) => {
				if(this.props.reply.replyId === upvotedReplies[key].node.itemId){
					return upvoted = true;
				}else{
					return false;
				}
			})
		}

		this.state = {
			flagged: flagged,
			upvoted: upvoted,
		};

		// console.log("reaction",this.props);
	}
	render() {
		const reply = this.props.reply;
		const user = reply.User;
		Moment.locale('nl');
		const formattedDT = Moment(reply.dateCreated).format('LLL');
		const replyPoints = reply.upvoted-reply.downvoted;
		const replyLabel = (replyPoints > 1 || replyPoints < -1 || replyPoints === 0) ? 'Punten' : 'Punt';

		let badge = "";
		if(user.verified > 0) {
			badge = (<i className='fa fa-check-circle'></i>);
		}

		return (
			<div>
				<div className="dotted no-side-margin"></div>

				<div className="reaction">
					<div className="header">
						<Avatar imagePath={user.avatar} userName={user.userName} userFirstName={user.firstName} userLastName={user.lastName}/>
						<Link to={"/"+user.userName} className="name color-accent">{user.firstName} {user.lastName} {badge}</Link>
						<div className="date color-grey">{formattedDT}</div>
					</div>

					<div className="comment arvo black">
						<span className="fr-view" dangerouslySetInnerHTML={{__html: reply.reply}}/>
					</div>

					<div className="actions color-grey sm">
						<Upvote upvoted={this.state.upvoted} viewer={this.props.viewer} type="reply" id={reply.replyId} points={replyPoints} label={" "+ replyLabel}/>
						{this.state.flagged ? (
							<DummyReport boxed="true" label="true"/>
						):(
							<Report viewer={this.props.viewer} loggedIn={this.props.viewer.AuthUser} type="reply" id={reply.replyId}/>
						)}
					</div>
				</div>
			</div>
		);
	}
}

export default Relay.createContainer(Comment, {
	fragments: {
		viewer: () => Relay.QL `
			fragment on viewer {
				${Report.getFragment('viewer')},
				${Upvote.getFragment('viewer')},

				AuthUser{
					FlaggedReplies(first:100000){
						edges{
							node{
								itemId
							}
						}
					}
					UpvotedReplies(first:100000){
						edges{
							node{
								itemId
							}
						}
					}
					id,
					firstName,
					lastName,
					userName,
					avatar,
					reputation,
				},
			}
		`
	}
});
