import M from '../../main.js';
import $ from 'jquery';
import React, { Component } from 'react';
import Moment from 'moment';
import Relay from 'react-relay';
import {Link} from 'react-router';

import Reaction from './Reaction';
import Upvote from '../Actions/Upvote';
import Report from '../Actions/Report';
import DummyReport from '../../components/Actions/DummyReport';
import Reactions from '../Actions/Reactions'
import PoorEditor from './PoorEditor';
import Avatar from '../User/Avatar';

import ReactionMutation from '../../mutations/ReactionMutation';
import ReplyMutation from '../../mutations/ReplyMutation';

class Answer extends Component {

	constructor(props) {
		super(props);
		let upvoted = false;
		let flagged = false;
		if(this.props.viewer.AuthUser){
			// Check if answer has been reported by user
			let flaggedAnswers = this.props.viewer.AuthUser.FlaggedAnswers.edges;
			Object.keys(flaggedAnswers).map((key) => {
				if(this.props.answer.aid === flaggedAnswers[key].node.itemId){
					return flagged = true;
				}else{
					return false;
				}
			})
			// Check if answer has been upvoted by user
			let upvotedAnswers = this.props.viewer.AuthUser.UpvotedAnswers.edges;

			Object.keys(upvotedAnswers).map((key) => {
				if(this.props.answer.aid === upvotedAnswers[key].node.itemId){
					return upvoted = true;
				}else{
					return false;
				}
			})
		}


		let openReactions = "none";
		if(this.props.open){
			openReactions = "block";
		}
		this.state = {
			openReactions: openReactions,
			flagged: flagged,
			upvoted: upvoted,
		};
		// console.log("answer",this.props.viewer);
	}

	viewReactions(id) {
		if(this.state.openReactions === "none"){
			this.setState({openReactions: "block"});
		}else{
			this.setState({openReactions: "none"});
		}
	}


	// passUpReaction(r,a,d) {
	// 	//postReaction in QuestionPage.js
	// 	this.props.onClickReaction(r,a,d);
	// }
	// passUpComment(r,ri,d) {
	// 	console.log("2",r,ri,d);
	// 	//postComment in QuestionPage.js
	// 	this.props.onClickComment(r,ri,d);
	// }

	postReaction (randomNumber, answerId, description) {
		if(description !== ""){
			description = M.censor(description);
			this.props.relay.commitUpdate(new ReactionMutation({
				viewer: this.props.viewer,
				answerId,
				description,
			}), {
				onSuccess: (response) => {
					$(".poor-editor-form").find('textarea').froalaEditor('html.set', '');
					M.toast("Reactie geplaatst", "Uw reactie is succesvol geplaatst");
				},
				onFailure: (t) => {
					M.toast("Reactie plaatsen mislukt", "Uw reactie is niet geplaatst. Probeer opnieuw.","error");
					console.log('SaveMutation FAILED', t.getError());
				},
			});
		}
	}
	postComment (randomNumber, reactionId, description) {
		if(description !== ""){
			description = M.censor(description);
			this.props.relay.commitUpdate(new ReplyMutation({
				viewer: this.props.viewer,
				reactionId,
				description,
			}), {
				onSuccess: (response) => {
					$(".poor-editor-form").find('textarea').froalaEditor('html.set', '');
					M.toast("Reactie geplaatst", "Uw reactie is succesvol geplaatst");
				},
				onFailure: (t) => {
					M.toast("Reactie plaatsen mislukt", "Uw reactie is niet geplaatst. Probeer opnieuw.","error");
					console.log('SaveMutation FAILED', t.getError());
				},
			});
		}
	}

	render() {
		const rNumber = Math.floor(Math.random()*10000);
		Moment.locale('nl');
		const answer = this.props.answer;
		const aid = answer.aid;
		const user = answer.User;
		const currentUser = this.props.currentUser;
		const formattedDT = Moment(answer.dateCreated).format('LLL');
		const answerPoints = answer.upvoted-answer.downvoted;
		const upvoteLabel = (answerPoints > 1 || answerPoints < -1 || answerPoints === 0) ? 'Punten' : 'Punt';
		const numberOfReactions = answer.Reactions.edges.length;
		const reactionLabel = (numberOfReactions > 1 || numberOfReactions === 0) ? 'Reacties' : 'Reactie';
		const reactions = this.props.reactions;

		let badge = "";
		if(user.verified > 0) {
			badge = (<i className='fa fa-check-circle'></i>);
		}

		return (
			<div>
				<Avatar imagePath={user.avatar}  userName={user.userName} userFirstName={user.firstName} userLastName={user.lastName}/>
				<Link to={"/"+user.userName} className="name color-accent">{user.firstName} {user.lastName} {badge}</Link>
				<span className="postdate color-grey">&bull; {formattedDT}</span>
				<div className="color-grey function">{user.function}</div>
				<div className="text black">
					<span className="fr-view" dangerouslySetInnerHTML={{__html: answer.answer}}/>
				</div>
				<div className="actions color-grey sm">
					<Upvote upvoted={this.state.upvoted} viewer={this.props.viewer} type="answer" id={answer.aid} boxed="true" points={answerPoints} label={" "+ upvoteLabel}/>
					<Reactions onClick={this.viewReactions.bind(this,"reaction"+this.props.answer.id)} boxed="true" label={numberOfReactions +" "+ reactionLabel}/>
					{this.state.flagged ? (
						<DummyReport boxed="true" label="true"/>
					):(
						<Report boxed="true" viewer={this.props.viewer} loggedIn={this.props.viewer.AuthUser} type="answer" id={aid}/>
					)}
				</div>
				<div id={"reaction"+this.props.answer.id} style={{display: this.state.openReactions}}>
					{Object.keys(reactions).map((key) => {
						const reaction = reactions[key].node;
						return (
							<div className="reaction" key={key}>
								<div className="dotted no-side-margin"></div>
								<Reaction viewer={this.props.viewer} reaction={reaction} currentUser={currentUser} onClick={this.postComment.bind(this)}/>
							</div>
						)
					})}
					<div id={"new-reaction-"+rNumber}></div>
					<PoorEditor type="reaction" rNumber={rNumber} currentUser={currentUser} answerId={aid} onClick={this.postReaction.bind(this)}/>
				</div>
			</div>
		);
	}

}


export default Relay.createContainer(Answer, {
	fragments: {
		viewer: () => Relay.QL `
			fragment on viewer {
				${Report.getFragment('viewer')},
				${Upvote.getFragment('viewer')},
				${Reaction.getFragment('viewer')},
				${ReactionMutation.getFragment('viewer')}
				${ReplyMutation.getFragment('viewer')}
				AuthUser{

					FlaggedAnswers(first:100000){
						edges{
							node{
								itemId
							}
						}
					}
					UpvotedAnswers(first:100000){
						edges{
							node{
								itemId
							}
						}
					}
					id,
					firstName,
					lastName,
					userName,
					avatar,
					reputation,
					verified
				},
			}
		`
	}
});
