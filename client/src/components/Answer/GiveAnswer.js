import M from '../../main.js';
import React, { Component } from 'react';
import Relay from 'react-relay';
import {Link} from 'react-router';
import Avatar from '../User/Avatar';
import RichEditor from './RichEditor';
import $ from 'jquery';

import AnswerMutation from '../../mutations/AnswerMutation';

class GiveAnswer extends Component {

	constructor(props) {
		super(props);
		this.state = {
			hasClicked: false,
			disabled: "",
		};
	}

	handleAnswerClick() {
		this.setState({hasClicked: true});
	}

	post() {
		let questionId = this.props.questionId;
		let slug = "meer-journalisten-vast-dan-vorig-jaar";
		$("#editor").find('textarea').attr('id','description');
		var description = $("#description").val();
		if(description !== ""){
			description = M.censor(description);
			this.setState({disabled: "disabled"});
			this.props.relay.commitUpdate(new AnswerMutation({
				viewer: this.props.viewer,
				questionId,
				description,
				slug,
			}), {
				onSuccess: (response) => {
					if(this.props.userId !== this.props.user.uid){
						M.toast("Antwoord geplaatst", "+15 Reputatie punten voor het plaatsen van een antwoord.","rep");
					}
					this.setState({hasClicked: false});
					$("#editor").find('textarea').val("");
					this.setState({disabled: ""});
				},
				onFailure: (t) => {
					M.toast("Fout opgetreden", "Uw antwoord is niet geplaatst. Probeer opnieuw.","error");
					console.log('SaveMutation FAILED', t.getError());
					this.setState({disabled: ""});
				},
			});
		}
	}

	render() {
		let hasClicked = this.state.hasClicked;
		let user = this.props.user;
		if(user){
			return (
				<div className="answer">
					<Avatar imagePath={user.avatar} userName={user.userName} userFirstName={user.firstName} userLastName={user.lastName}/>
					<Link to={user.userName} className="name color-accent">{user.firstName+" "+user.lastName}</Link>
					{hasClicked ? (
						<div>
							<RichEditor/>
							<div className="actions">
								<div className={"btn primary-btn "+this.state.disabled} onClick={this.post.bind(this)}>
									<span>Plaatsen</span>
									<i className="fa fa-circle-o-notch fa-spin"></i>
								</div>
								<div className="share color">
									{/* <span>Delen:</span>
									<input type="checkbox" id="facebook"/>
									<label htmlFor="facebook" className="color">
										<i className="fa fa-facebook-official"></i>
									</label>
									<input type="checkbox" id="twitter"/>
									<label htmlFor="twitter" className="color-accent">
										<i className="fa fa-twitter"></i>
									</label> */}
								</div>
							</div>
						</div>
					) : (
						<div>
							<div className="label color">Kunt u deze vraag beantwoorden?</div>
							<div className="btn primary-btn" onClick={this.handleAnswerClick.bind(this)}>Beantwoorden</div>
							<br/><br/>
						</div>
					)}
				</div>
			);
		}else{
			return (
				<div className="answer">
						<div className="avatar defaultb"><span>G</span></div>
						<div className="name color-accent">Gast</div>
						<div>
							<div className="label color">Kunt u deze vraag beantwoorden?</div>
							<div className="btn primary-btn" onClick={M.login}>Beantwoorden</div>
							<br/><br/>
						</div>
				</div>
			);
		}

	}
}


export default Relay.createContainer(GiveAnswer, {
	fragments: {
			viewer: () => Relay.QL `
				fragment on viewer {
					${AnswerMutation.getFragment('viewer')},
					AuthUser{
						id,
						firstName,
						lastName,
						userName,
						avatar,
					},
				}
			`
	}
});
