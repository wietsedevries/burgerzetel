import React, { Component } from 'react';
import Answer from './Answer';

class AnswerPanel extends Component {
  render() {
    const answers = this.props.answers;
    return (
      <div>
        {Object.keys(answers).map(function(key) {
          const answer = answers[key].node;
          return (
            <div className="panel" key={key}>
              <div className="question answers">
                <div className="given-answer">
                  <Answer answer={answer} reactions={answer.Reactions.edges}/>
                </div>
              </div>
            </div>
          )
        })}
      </div>
    );
  }
}

export default AnswerPanel;
