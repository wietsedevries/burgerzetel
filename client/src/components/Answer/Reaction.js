import React, { Component } from 'react';
import Relay from 'react-relay';
import Moment from 'moment';
import {Link} from 'react-router';

import Report from '../Actions/Report';
import DummyReport from '../../components/Actions/DummyReport';
import Upvote from '../Actions/Upvote';
import Reactions from '../Actions/Reactions'
import Avatar from '../User/Avatar';
import Comment from './Comment';
import PoorEditor from './PoorEditor';

import '../../stylesheets/question.css';

class Reaction extends Component {

	constructor(props) {
		super(props);
		let upvoted = false;
		let flagged = false;
		if(this.props.viewer.AuthUser){
			// Check if reaction has been reported by user
			let flaggedReactions = this.props.viewer.AuthUser.FlaggedReactions.edges;
			Object.keys(flaggedReactions).map((key) => {
				if(this.props.reaction.rid === flaggedReactions[key].node.itemId){
					return flagged = true;
				}else{
					return false;
				}
			})
			// Check if reaction has been upvoted by user
			let upvotedReactions = this.props.viewer.AuthUser.UpvotedReactions.edges;
			Object.keys(upvotedReactions).map((key) => {
				if(this.props.reaction.rid === upvotedReactions[key].node.itemId){
					return upvoted = true;
				}else{
					return false;
				}
			})
		}


		let openReplies = "none";
		if(this.props.open){
			openReplies = "block";
		}
		this.state = {
			openReplies: openReplies,
			flagged: flagged,
			upvoted: upvoted,
		};

		// console.log("reaction",this.props);
	}

	viewReplies(id) {
		if(this.state.openReplies === "none"){
			this.setState({openReplies: "block"});
		}else{
			this.setState({openReplies: "none"});
		}
	}
	passUp(r,ri,d) {
		console.log("1",r,ri,d);
		this.props.onClick(r,ri,d);
	}
	render() {
		const rNumber = Math.floor(Math.random()*10000);
		const reaction = this.props.reaction;
		const reactionid = reaction.rid;
		const user = reaction.User;
		const currentUser = this.props.currentUser;

		Moment.locale('nl');

		const formattedDT = Moment(reaction.dateCreated).format('LLL');
		const replies = reaction.Replies.edges;
		const reactionPoints = reaction.upvoted-reaction.downvoted;
		const reactionLabel = (reactionPoints > 1 || reactionPoints < -1 || reactionPoints === 0) ? 'Punten' : 'Punt';
		const replyLabel = (replies.length > 1 || replies.length === 0) ? 'Reacties' : 'Reactie';

		let badge = "";
		if(user.verified > 0) {
			badge = (<i className='fa fa-check-circle'></i>);
		}

		return (
			<div>
				<div className="reaction">
					<div className="header">
						<Avatar imagePath={user.avatar}  userName={user.userName} userFirstName={user.firstName} userLastName={user.lastName}/>
						<Link to={"/"+user.userName} className="name color-accent">{user.firstName} {user.lastName} {badge}</Link>
						<div className="date color-grey">{formattedDT}</div>
					</div>

					<div className="comment arvo black">
						<span className="fr-view" dangerouslySetInnerHTML={{__html: reaction.reaction}}/>
					</div>

					<div className="actions color-grey sm">
						<Upvote upvoted={this.state.upvoted} viewer={this.props.viewer} type="reaction" id={reaction.rid} points={reactionPoints} label={" "+ reactionLabel}/>
						<Reactions onClick={this.viewReplies.bind(this,"reply"+this.props.reaction.id)} label={replies.length +" "+ replyLabel}/>
						{this.state.flagged ? (
							<DummyReport boxed="true" label="true"/>
						):(
							<Report viewer={this.props.viewer} loggedIn={this.props.viewer.AuthUser} type="reaction" id={reactionid}/>
						)}
					</div>
					<div className="reply-to" id={"reply"+this.props.reaction.id} style={{display: this.state.openReplies}}>
						{Object.keys(replies).map((key) => {
							var reply = replies[key].node;
							return (
								<div className="comment arvo black" key={key}>
									<Comment reply={reply} viewer={this.props.viewer}/>
								</div>
							)
						})}
						<div id={"new-comment-"+rNumber}></div>
						<PoorEditor type="comment" rNumber={rNumber} currentUser={currentUser} reactionId={reactionid} onClick={this.passUp.bind(this)}/>
					</div>
				</div>
			</div>
		);
	}
}

export default Relay.createContainer(Reaction, {
	fragments: {
		viewer: () => Relay.QL `
			fragment on viewer {
				${Report.getFragment('viewer')},
				${Upvote.getFragment('viewer')},
				${Comment.getFragment('viewer')},

				AuthUser{
					FlaggedReactions(first:100000){
						edges{
							node{
								itemId
							}
						}
					}
					UpvotedReactions(first:100000){
						edges{
							node{
								itemId
							}
						}
					}
					id,
					firstName,
					lastName,
					userName,
					avatar,
					reputation,
				},
			}
		`
	}
});
