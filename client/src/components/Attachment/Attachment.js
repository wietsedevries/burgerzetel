import React, { Component } from 'react';

class Attachment extends Component {
  render() {
    let iconClass;
      switch(this.props.mediaKey) {
      case "pdf":
        iconClass = "fa-file-pdf-o"
        break;
      case "video":
        iconClass = "fa-play-circle"
        break;
      case "link":
        iconClass = "fa-link"
        break;
      case "`img`":
        iconClass = "fa-picture-o"
        break;
      default:
        iconClass = "fa-link"
      }
    return (
      <a href={this.props.url} target="_blank" className="side-link w-icon"><i className={"fa "+iconClass}></i> {this.props.title}</a>
    );
  }
}

export default Attachment;
