import React, { Component } from 'react';
import Attachment from '../Attachment/Attachment';

class AttachmentPanel extends Component {
  render() {
    let Media = this.props.media;
    return (
			<div className="panel padding">
				<div className="panel-title">
          <i className="fa fa-file-text-o" aria-hidden="true"></i>
          <span>Ondersteunende documenten</span>
				</div>
        <div className="dotted"></div>
        {Object.keys(Media).map(function(key) {
          return <Attachment title={Media[key].node.title} url={Media[key].node.url} mediaKey={Media[key].node.Type.key} key={key} />;
        })}
			</div>
    );
  }
}

export default AttachmentPanel;
