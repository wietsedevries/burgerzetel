import M from '../../../main.js';
import $ from 'jquery';

import React, { Component } from 'react';
import {Link} from 'react-router';
import Relay from 'react-relay';

import Reaction from '../../Answer/Reaction';

import ReplyMutation from '../../../mutations/ReplyMutation';

class ReactionBlock extends Component {
	postComment (randomNumber, reactionId, description) {
		if(description !== ""){
			this.props.relay.commitUpdate(new ReplyMutation({
				viewer: this.props.viewer,
				reactionId,
				description,
			}), {
				onSuccess: (response) => {
					$(".poor-editor-form").find('textarea').froalaEditor('html.set', '');
					M.toast("Reactie geplaatst", "Uw reactie is succesvol geplaatst");
				},
				onFailure: (t) => {
					M.toast("Reactie plaatsen mislukt", "Uw reactie is niet geplaatst. Probeer opnieuw.","error");
					console.log('SaveMutation FAILED', t.getError());
				},
			});
		}
	}

	render() {
		return (
			<div className="panel">
				<Link className="color block-header" to={"/vraag/"+this.props.reaction.Answer.Question.slug}>{this.props.reaction.Answer.Question.title}</Link>
				<div className="dotted no-side-margin"></div>
				<div className="padding reaction">
					<Reaction viewer={this.props.viewer} reaction={this.props.reaction} open={true}  currentUser={this.props.viewer.AuthUser} onClick={this.postComment.bind(this)}/>
				</div>
			</div>
		);
	}
}

export default Relay.createContainer(ReactionBlock, {
	fragments: {
		viewer: () => Relay.QL `
			fragment on viewer {
				${Reaction.getFragment('viewer')},
				${ReplyMutation.getFragment('viewer')}
				AuthUser{
					id,
					firstName,
					lastName,
					userName,
					avatar,
					reputation,
				},
			}
		`
	}
});
