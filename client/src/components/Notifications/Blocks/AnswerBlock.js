import React, { Component } from 'react';
import {Link} from 'react-router';
import Answer from '../../Answer/Answer';
import Relay from 'react-relay';

class AnswerBlock extends Component {

render() {
	return (
			<div className="panel">
				<Link className="color block-header" to={"/vraag/"+this.props.answer.Question.slug}>{this.props.answer.Question.title}</Link>
				<div className="dotted no-side-margin"></div>
				<div className="question answers">
					<div className="given-answer">
						<Answer viewer={this.props.viewer} answer={this.props.answer} reactions={this.props.reactions} open={true} currentUser={this.props.viewer.AuthUser} />
					</div>
				</div>
			</div>
		);
	}
}
export default Relay.createContainer(AnswerBlock, {
	fragments: {
		viewer: () => Relay.QL `
			fragment on viewer {
				${Answer.getFragment('viewer')},
				AuthUser{
					id,
					firstName,
					lastName,
					userName,
					avatar,
					reputation,
				},
			}
		`
	}
});
