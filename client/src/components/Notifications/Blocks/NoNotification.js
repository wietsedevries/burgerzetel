import React, { Component } from 'react';

class NoNotification extends Component {

  render() {

		return (
			<div className="panel padding">
				<div className="default-text color-grey">
					<i className="fa fa-bell-slash-o"></i> &nbsp; Deze notificatie is niet meer beschikbaar
				</div>
			</div>
    );
  }
}
export default NoNotification;
