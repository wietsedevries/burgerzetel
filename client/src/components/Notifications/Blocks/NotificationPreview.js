import React, { Component } from 'react';
import {Link} from 'react-router';
import Avatar from '../../User/Avatar';
import Moment from 'moment';

class NotificationPreview extends Component {

  render() {
		var data = this.props.data;
		Moment.locale('nl');
		// const fullDate = Moment(data.dateCreated).startOf('hour');
    const fullDate = Moment(data.dateCreated).startOf('hour').fromNow();
		var type = data.NotificationType.key;
		var avatarType = "user";
		var ABavatar;
		var ABuserName;
		var ABfirstName;
		var ABlastName;
		var title;
		var subtitle;
		var besje;

		switch (type) {
			case "deletedAnswer":
				ABavatar = "";
				ABuserName = "";
				ABfirstName = "";
				ABlastName = "";
				title = "";
				subtitle = '"'+data.Answer.answer+'"';
				besje = "Uw antwoord is gerapporteerd en verwijderd door de community.";
				avatarType = "flag";
				break;
			case "deletedComment":
				ABavatar = "";
				ABuserName = "";
				ABfirstName = "";
				ABlastName = "";
				title = "";
				subtitle = '"'+data.Reaction.reaction+'"';
				besje = "Uw reactie is gerapporteerd en verwijderd door de community.";
				avatarType = "flag";
				break;
			case "deletedQuestion":
				ABavatar = "";
				ABuserName = "";
				ABfirstName = "";
				ABlastName = "";
				title = "";
				subtitle = '"'+data.Question.title+'"';
				besje = "Uw vraag is gerapporteerd en verwijderd door de community.";
				avatarType = "flag";
				break;
			case "deletedReply":
				ABavatar = "";
				ABuserName = "";
				ABfirstName = "";
				ABlastName = "";
				title = "";
				subtitle = '"'+data.Reply.reply+'"';
				besje = "Uw reactie is gerapporteerd en verwijderd door de community.";
				avatarType = "flag";
				break;
			default:
		}

		return (
			<div className="panel notification-box padding">
				<div className="list-item-lg color-grey">
					<Avatar imagePath={ABavatar} userName={ABuserName} type={avatarType} hasURL={false} userFirstName={ABfirstName} userLastName={ABlastName}/>
					<div className="list-item-text">
							<strong data-href="#" className="color">{ABfirstName+" "+ABlastName} </strong>
							{besje}
							<br/>
							<strong className="color">{title}</strong>
							<br/>
							<quote>{subtitle}</quote>
					</div>
					<div className="date color-light">{fullDate}</div>
						<div className="default-text warning-box">
							<h3><i className="fa fa-flag"></i> &nbsp; {besje}</h3>
							<p>
								De, door u geplaatste, content is door de community gerapporteerd en verwijderd.
								Voorkom dat dit nogmaal gebeurt door de <Link to="/info/huisregels">huisregels</Link> goed door te nemen.
							</p>
						</div>
				</div>
			</div>
    );
  }
}
export default NotificationPreview;
