import React, { Component } from 'react';
import Relay from 'react-relay';
import {Link} from 'react-router';

import Avatar from '../../components/User/Avatar';
import NotificationItem from './NotificationItem';

import readNotificationsMutation from '../../mutations/readNotificationsMutation';

class Notifications extends Component {
	// toggle login popup
	toggleNotifications(event) {
		document.getElementById('UserMenu').className = "";
		const elm = document.getElementById('notification-box');
		if (elm.className.match(/active\b/)) {
			elm.className = "";
		}else{
			elm.className = "active";
		}

		this.props.relay.commitUpdate(new readNotificationsMutation({
			viewer: this.props.viewer,
			id: 4,
		}), {
			onSuccess: (response) => {
				// console.log("notifications cleared");
			},
			onFailure: (t) => {
				console.log('EditMutation FAILED', t.getError());
			},
		});


	}
	// toggle user-menu popup
	toggleUserMenu(event) {
		document.getElementById('notification-box').className = "";
		const elm = document.getElementById('UserMenu');
		if (elm.className.match(/active\b/)) {
			elm.className = "";
		}else{
			elm.className = "active";
		}
	}
	_logout(e) {
		e.preventDefault();
		localStorage.clear();
		window.location = "/";
	}

	render() {
		var user = this.props.notifications.AuthUser;
		var notifications = user.Notifications.edges;
		var any = false;
		var count;
		if(notifications.length > 0){
			any = true;
		}
		let dot;
		if(user.NotificationCount > 0){
			any = true;
			dot = "active";
			if(user.NotificationCount > 99){
				count = "!";
			}else{
				count = user.NotificationCount;
			}
		}

		return (
			<div>
				<div className="header-menu right" onClick={this.toggleUserMenu}>
					<Avatar imagePath={user.avatar} userName={user.userName} userFirstName={user.firstName} userLastName={user.lastName} hasURL="false"/>
					<div id="UserMenu">
						<div className="blackout"></div>
						<div className="user-menu">
							<div className="user-caret"></div>
							<Link to={"/"+user.userName} className="blue-link"><i className="fa fa-user-circle-o"></i> Mijn profiel</Link>
							<Link to={"/"+user.userName+"/reputatie"} className="blue-link"><i className="fa fa-trophy"></i> Mijn reputatie</Link>
							<Link to={"/"+user.userName+"/instellingen"} className="blue-link"><i className="fa fa-cog"></i> Instellingen</Link>

							<div className="dotted"></div>
							<div className="blue-link" onClick={this._logout}><i className="fa fa-sign-out"></i> Uitloggen</div>
						</div>
					</div>
				</div>
				<div className="notification right grey-link" onClick={this.toggleNotifications.bind(this)}>

					{any ? (
						<div className={"dot " + dot}>
							<i className="fa fa-bell-o"></i>
							<span>{count}</span>
						</div>
					):(
						<i className="fa fa-bell-o"></i>
					)}
					<div id="notification-box">
						<div className="blackout"></div>
						<div className="notification-caret"></div>
						<div className="list">
							<div className="title">
								<Link to="/notificaties/overzicht" className="color">Notificaties</Link>
								<Link to={"/"+user.userName+"/instellingen"}><i className="fa fa-cog grey-link"></i></Link>
							</div>
							<div className="dotted"></div>
							<div className="notifications-scroll">
								{any ? (
									Object.keys(notifications).map(function(key) {
										var data = notifications[key].node;
										return (
											<NotificationItem key={key} data={data} rows={2}/>
										)
									})
								):(
									<div className="empty-notification color-grey"><i className="fa fa-bell-slash-o"></i> Geen nieuwe notificaties</div>
								)}
							</div>
							<Link to="/notificaties/overzicht" className="side-link all-notifications">Bekijk alle notificaties</Link>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
export default Relay.createContainer(Notifications, {
		fragments: {
			viewer: () => Relay.QL `
				fragment on viewer {
					${readNotificationsMutation.getFragment('viewer')},

				},
			`
		},
});
