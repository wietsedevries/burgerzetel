import React, { Component } from 'react';
import {Link} from 'react-router';
import Avatar from '../User/Avatar';
import Dotdotdot from 'react-dotdotdot';
import Moment from 'moment';

class NotificationItem extends Component {

	render() {
		var data = this.props.data;
		Moment.locale('nl');
		const fullDate = Moment(data.dateCreated).startOf('hour').fromNow();
		var type = data.NotificationType.key;
		var avatarType = "user";
		var ABavatar;
		var ABuserName;
		var ABfirstName;
		var ABlastName;
		var title;
		var subtitle;
		var besje;
		var slug;
		var showAvatar = true;

		switch (type) {
			case "newAnswer":
				ABavatar = data.Answer.User.avatar;
				ABuserName = data.Answer.User.userName;
				ABfirstName = data.Answer.User.firstName;
				ABlastName = data.Answer.User.lastName;
				title = data.Question.title ;
				subtitle = '"'+data.Answer.answer+'"';
				besje = "heeft antwoord gegeven op uw vraag:";
				slug = "/vraag/"+data.Question.slug;
				break;
			case "newCommentAnswer":
				ABavatar = data.Reaction.User.avatar;
				ABuserName = data.Reaction.User.userName;
				ABfirstName = data.Reaction.User.firstName;
				ABlastName = data.Reaction.User.lastName;
				title = data.Answer.answer ;
				subtitle = '"'+data.Reaction.reaction+'"';
				besje = "heeft gereageerd op uw antwoord:";
				slug = "/notificaties/antwoord/"+data.Answer.aid;
				break;
			case "newReplyComment":
				ABavatar = data.Reply.User.avatar;
				ABuserName = data.Reply.User.userName;
				ABfirstName = data.Reply.User.firstName;
				ABlastName = data.Reply.User.lastName;
				title = data.Reaction.reaction;
				subtitle = '"'+data.Reply.reply+'"';
				besje = "heeft gereageerd op uw reactie:";
				slug = "/notificaties/reactie/"+data.Reaction.rid;
				break;
			case "newRequest":
				ABavatar = data.AskedBy.avatar;
				ABuserName = data.AskedBy.userName;
				ABfirstName = data.AskedBy.firstName;
				ABlastName = data.AskedBy.lastName;
				title = data.Question.title;
				subtitle = "";
				besje = "Heeft u gevraagd om een vraag te beantwoorden:";
				slug = "/vraag/"+data.Question.slug;
				break;
			case "deletedAnswer":
				ABavatar = "";
				ABuserName = "";
				ABfirstName = "";
				ABlastName = "";
				title = "";
				subtitle = '"'+data.Answer.answer+'"';
				besje = "Uw antwoord is gerapporteerd en verwijderd door de community.";
				avatarType = "flag";
				slug = "/notificaties/gerapporteerd/"+data.nid;
				break;
			case "deletedComment":
				ABavatar = "";
				ABuserName = "";
				ABfirstName = "";
				ABlastName = "";
				title = "";
				subtitle = '"'+data.Reaction.reaction+'"';
				besje = "Uw reactie is gerapporteerd en verwijderd door de community.";
				avatarType = "flag";
				slug = "/notificaties/gerapporteerd/"+data.nid;
				break;
			case "deletedQuestion":
				ABavatar = "";
				ABuserName = "";
				ABfirstName = "";
				ABlastName = "";
				title = "";
				subtitle = '"'+data.Question.title+'"';
				besje = "Uw vraag is gerapporteerd en verwijderd door de community.";
				avatarType = "flag";
				slug = "/notificaties/gerapporteerd/"+data.nid;
				break;
			case "deletedReply":
				ABavatar = "";
				ABuserName = "";
				ABfirstName = "";
				ABlastName = "";
				title = "";
				subtitle = '"'+data.Reply.reply+'"';
				besje = "Uw reactie is gerapporteerd en verwijderd door de community.";
				avatarType = "flag";
				slug = "/notificaties/gerapporteerd/"+data.nid;
				break;
			case "newReputation":
				showAvatar = false;
				ABavatar = "";
				ABuserName = "";
				ABfirstName = "";
				ABlastName = "";
				title = data.msg;
				subtitle = "";
				besje = "";
				avatarType = "";
				slug = "/"+data.User.userName+"/reputatie";
				break;
			default:
		}

		return (
			<Link to={slug} className="list-item color-grey">
				{showAvatar ? (
					<Avatar imagePath={ABavatar} userName={ABuserName} type={avatarType} hasURL="false" userFirstName={ABfirstName} userLastName={ABlastName}/>
				):(
					<div className="reputation-avatar">
						<i className="fa fa-trophy"></i>
					</div>
				)}
				<div className="list-item-text">
					<Dotdotdot clamp={this.props.rows}>
						<strong className="color">{ABfirstName+" "+ABlastName} </strong> {besje} <strong className="color">{title}</strong> <quote>{subtitle}</quote>
					</Dotdotdot>
				</div>
				<div className="date color-light">{fullDate}</div>
			</Link>
		);
	}
}
export default NotificationItem;
