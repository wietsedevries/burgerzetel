import React, { Component } from 'react';
import Relay from 'react-relay';

import AddedTag from './AddedTag';
import Avatar from '../User/Avatar';
import '../../stylesheets/ask.css';

let limit = 0;
var input = "";
let addedTags = [];

class AddTag extends Component {
	constructor(props) {
			super(props);
			addedTags = [];

			// created tags, for edit page
			if (this.props.addedTags) {
				const addedTagList = this.props.addedTags;
				Object.keys(addedTagList).map((key) => {
					let tag = addedTagList[key];

					let value = tag.value;
					let tagKey = tag.key;
					let tagValueId = tag.tagValueId;
					if(this.props.edit){
						value = tag.node.TagValue.value;
						tagKey = tag.node.TagValue.key;
						tagValueId = tag.node.TagValue.tagValueId;
					}
					addedTags.push(<AddedTag key={key} tagKey={tagKey} value={value} tagValueId={tagValueId}/>);
					this.props.onAdd(tagValueId);
					return true;
				})
			}
			this.state = {
				showResults: "",
				addedTags: addedTags,
			};
			this.addTag = this.addTag.bind(this);
	}


	addTag(event) {
		this.setState({showResults: ""});

		var key  = event.target.getAttribute('data-key');
		var value  = event.target.getAttribute('data-val');
		var id = event.target.getAttribute('data-id');

		const createdTags = document.getElementsByClassName("created-tag");
		if ((limit === 0 || createdTags.length < limit) && value !== null) {
			addedTags.push(<AddedTag key={key} tagKey={key} value={value} tagValueId={id}/>);
			this.props.onAdd(id);
			if(createdTags.length >= limit){
				input.disabled = true;
			}
		}
		input.value = "";
		input.focus();

	}
	remove(event) {
		if (event.target.classList.contains('remove')){
			event.target.parentNode.remove(event.target.parentNode);
			document.getElementById('inputField').removeAttribute('disabled');
			var key  = event.target.getAttribute('data-id');
			this.props.onRemove(key);
		}
	}

	search(event) {
		input = event.target;
		this.props.relay.setVariables({ query: input.value});
		if(input.value.length > 1){
			this.setState({
				showResults: "open",
			});
		}else{
			this.setState({
				showResults: "",
			});
		}
	}

	render() {
		let tags = this.props.viewer.TagValueByQuery.edges;
		let anyTags = false;
		if(tags.length > 0){
			anyTags = true;
		}
		limit = this.props.limit;

		return (
			<div className="preview-holder">
				<input id="inputField" className="color" type="text" placeholder="Begin met typen om te zoeken" onChange={this.search.bind(this)}/>
				<div className={"preview "+ this.state.showResults} onClick={this.addTag}>
					{anyTags ? (
						Object.keys(tags).map((key) => {
							let tag = tags[key].node;
							let previewImg = "";
							if(tag.Page > 0){
								previewImg = tag.Page.img;
							}
							return (
								<div className="preview-item" key={key} data-val={tag.value} data-key={tag.key} data-id={tag.tagValueId}>
									<Avatar imagePath={previewImg} type="page" tag={tag.value} hasURL="false"/>
									<div className="color">{tag.value}</div>
									<span className="color-grey">{tag.used}x Gebruikt</span>
								</div>
							)
						})
					):(
						<div className="preview-item no-result">
							<div className="color">Geen resultaten</div>
							<span className="color-grey">Probeer een ander trefwoord</span>
						</div>
					)}
				</div>
				<div className="tags">
					<span id="tag-area" onClick={this.remove.bind(this)}>
						{this.state.addedTags}
					</span>
				</div>
			</div>
		);
	}
}

export default Relay.createContainer(AddTag, {
		initialVariables: {
			query: "",
		},
		fragments: {
			viewer: () => Relay.QL `
				fragment on viewer {
					TagValueByQuery(query:$query,first:5) {
						edges{
							node{
								tagValueId,
								key,
								value,
								used,
								Page{
									img,
								},
							},
						},
					},
				},
			`
		},
});
