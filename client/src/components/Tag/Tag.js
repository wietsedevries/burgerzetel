import React, { Component } from 'react';
import {Link} from 'react-router';

class Tag extends Component {
  render() {
    return (
      <Link to={"/onderwerp/"+this.props.tagKey} className="tag">{this.props.value}</Link>
    );
  }
}

export default Tag;
