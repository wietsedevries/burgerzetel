import React, { Component } from 'react';
import '../../stylesheets/actions.css';

class Confirm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			status: "",
		};
	}
	componentWillReceiveProps(nextProps){
		if(JSON.stringify(this.props.show) !== JSON.stringify(nextProps.show)) {
			this.openConfirm();
		}
	}
	openConfirm(e){
		this.setState({status: "open"});
	}
	closeConfirm(e){
		this.setState({status: ""});
	}
	confirm = () => {
		this.props.onClick();
		this.closeConfirm();
	}

	render() {
		return (
			<div className={"popup "+this.state.status}>
				<div className="blackout top" onClick={this.closeConfirm.bind(this)}></div>
				<div className="box-helper"></div>
				<div className="box confirm">
					<div className="title color">{this.props.title}</div>
					<p className="color-grey">{this.props.msg}</p>
					<div className="btns">
						<div className="btn secondary-btn" onClick={this.closeConfirm.bind(this)}>Annuleren</div>
						<div className="btn primary-btn" onClick={this.confirm.bind(this)}>Doorgaan</div>
					</div>
				</div>
			</div>
		);
	}
}

export default Confirm;
