import React, { Component } from 'react';
// import {Link} from 'react-router';
import '../../stylesheets/actions.css';

class Downvote extends Component {

  render() {
    let classes;
    if(this.props.boxed){
      classes = "action-btn boxed";
    }else{
      classes = "action-btn";
    }
    return (
			<div className={classes}>
				<i className="fa fa-chevron-down"></i>
			</div>
		);
	}
}

export default Downvote;
