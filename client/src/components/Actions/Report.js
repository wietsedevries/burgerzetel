import M from '../../main.js';
import React, { Component } from 'react';
import Relay from 'react-relay';

import Confirm from './Confirm';
import '../../stylesheets/actions.css';

import ReportMutation from '../../mutations/ReportMutation';

var count = 0;
class Report extends Component {
	constructor (props) {
		super(props);
		this.state = {
			actionState: "",
			showConfirm: false,
		};
	}
	confirm() {
		count++;
		this.setState({showConfirm: count});
	}
	confirmSuccess = () => {
		this.props.relay.commitUpdate(new ReportMutation({
			viewer: this.props.viewer,
			type: this.props.type,
			id: this.props.id,
			reputation: this.props.loggedIn.reputation
		}), {
			onSuccess: (response) => {
				if(this.state.actionState === "completed"){
					this.setState({actionState: ""});
				}else{
					this.setState({actionState: "completed"});
				}
				M.toast("Gerapporteerd","Dit item is successvol gerapporteerd.");
			},
			onFailure: (t) => {
				console.log('ReportMutation FAILED', t.getError());
				M.toast("Er is iets mis gegaan!","Dit item is niet gerapporteerd.","error");
			},
		});
	}
	render() {
		let loggedIn = this.props.loggedIn;
		let classes = "action-btn report right";
		if(this.props.boxed && !localStorage.enabled){
			classes = "action-btn report boxed right";
		}
		let label = "";
		if(this.props.label){
			label = "Rapporteren";
		}
		if(this.state.actionState === "completed"){
			label = "Gerapporteerd";
		}
		if(loggedIn){
			return (
				<span>
					<div className={classes+" "+this.state.actionState} onClick={this.confirm.bind(this)}>
						<i className="fa fa-flag"></i> {label}
					</div>
					<Confirm show={this.state.showConfirm} onClick={this.confirmSuccess.bind(this)} title="Weet u het zeker?" msg="U staat op het punt om te rapporteren en u kunt deze actie niet ongedaan maken, wilt u doorgaan?"/>
				</span>
			);
		} else{
			return (
				<div className={classes} onClick={M.login}>
					<i className="fa fa-flag boo"></i> {label}
				</div>
			);
		}
	}
}


export default Relay.createContainer(Report, {
	fragments: {
		viewer: () => Relay.QL `
			fragment on viewer {
				${ReportMutation.getFragment('viewer')},
				AuthUser{
					userName
				}
			}
		`
	}
});
