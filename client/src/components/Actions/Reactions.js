import React, { Component } from 'react';
// import {Link} from 'react-router';
import '../../stylesheets/actions.css';

class Reactions extends Component {

  render() {
    let classes;
    if(this.props.boxed){
      classes = "action-btn boxed";
    }else{
      classes = "action-btn";
    }
    return (
			<div className={classes} onClick={this.props.onClick}>
				<i className="fa fa-commenting-o"></i>  <label>{this.props.label}</label>
			</div>
		);
	}
}

export default Reactions;
