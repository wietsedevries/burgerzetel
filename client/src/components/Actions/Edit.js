import React, { Component } from 'react';
import {Link} from 'react-router';
import '../../stylesheets/actions.css';

class Edit extends Component {

  render() {
    let classes;
		let slug = this.props.slug;
    if(this.props.boxed){
      classes = "action-btn boxed right";
    }else{
      classes = "action-btn right";
    }
    return (
			<Link to={"/"+slug+"/wijzig"} className={classes}>
				Wijzigen
			</Link>
		);
	}
}

export default Edit;
