import React, { Component } from 'react';
import '../../stylesheets/actions.css';

class Share extends Component {
	constructor(props) {
		super(props);
		this.state = {
			share: false,
		};
		this.toggleShare = this.toggleShare.bind(this);
	}
	toggleShare(e){
		let elm = document.getElementById('sharebox');
		if(this.state.share){
			elm.className = "";
			this.setState({share: true});
		}else{
			elm.className = "open";
			this.setState({share: false});
		}
	}

  render() {
		let classes;
		if(this.props.boxed){
			classes = "action-btn boxed";
		}else{
			classes = "action-btn";
		}
    return (
			<div onClick={this.toggleShare} className={classes}>
				<i className="fa fa-share-alt"></i> Delen
			</div>
		);
	}
}

export default Share;
