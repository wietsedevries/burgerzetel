import M from '../../main.js';
import React, { Component } from 'react';
import Relay from 'react-relay';
import '../../stylesheets/actions.css';

import SaveMutation from '../../mutations/SaveMutation';

class SaveQuestion extends Component {
	componentWillReceiveProps(nextProps){
		if(JSON.stringify(this.props.actionState) !== JSON.stringify(nextProps.actionState)) {
			// console.log(this.props.actionState);
		}
	}
	save() {
		let questionId = this.props.questionId;
		this.props.onClick(questionId);
	}
	render() {
		let user = this.props.viewer.AuthUser;
		let classes;
		let label;
		if(this.props.actionState === "completed") {
			label = "Opgeslagen";
		} else {
			label = "Opslaan";
		}
		if(this.props.boxed){
			classes = "action-btn boxed";
		}else{
			classes = "action-btn";
		}
		if(user){
			return (
				<div className={classes+" "+this.props.actionState}  onClick={this.save.bind(this)}>
					<i className="fa fa-bookmark"></i> {label}
				</div>
			);
		}else{
			return (
				<div className={classes} onClick={M.login}>
					<i className="fa fa-bookmark"></i> {label}
				</div>
			);
		}

	}
}

export default Relay.createContainer(SaveQuestion, {
		fragments: {
			viewer: () => Relay.QL `
				fragment on viewer {
					${SaveMutation.getFragment('viewer')}
					AuthUser {
						id,
					},
				},
			`,
		},
	});
