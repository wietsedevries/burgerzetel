import M from '../../main.js';
import React, { Component } from 'react';
import Relay from 'react-relay';

// import {toggleCompleted} from '../../utils.js';
import '../../stylesheets/actions.css';

import UpvoteMutation from '../../mutations/UpvoteMutation';

class Upvote extends Component {
	constructor (props) {
		super(props);
		let actionState = "";
		if(this.props.upvoted){
			actionState = "completed";
		}
		//@NOTE fakeNumber for testing purposes, when the number is REALLY added in the database, remove this (also in render!)
		this.state = {
			actionState: actionState,
			fakeNumber: 0,

		};
	}
	upvote() {
		this.props.relay.commitUpdate(new UpvoteMutation({
			viewer: this.props.viewer,
			type: this.props.type,
			id: this.props.id,
		}), {
			onSuccess: (response) => {
				if(this.state.actionState === "completed"){
					M.toast("Onmogelijke actie","U heeft dit item al beoordeeld.","warning");
					// this.setState({actionState: ""});
					// this.setState({fakeNumber: this.state.fakeNumber-1});
				}else{
					this.setState({actionState: "completed"});
					this.setState({fakeNumber: this.state.fakeNumber+1});
				}
			},
			onFailure: (t) => {
				M.toast("Onmogelijke actie","U kan uw eigen content niet upvoten.","warning");
				console.log('SaveMutation FAILED', t.getError());
			},
		});
	}

  render() {
		let user = this.props.viewer.AuthUser;
    let classes;
    if(this.props.boxed){
      classes = "action-btn boxed";
    }else{
      classes = "action-btn";
    }
		if(user){
	    return (
				<div className={classes+" "+this.state.actionState} onClick={this.upvote.bind(this)}>
					<i className="fa fa-heart"></i> <label>{(this.props.points+this.state.fakeNumber)+this.props.label}</label>
				</div>
			);
		} else{
			return (
				<div className={classes} onClick={M.login}>
					<i className="fa fa-heart"></i> <label>{this.props.points+this.props.label}</label>
				</div>
			);
		}
	}
}

export default Relay.createContainer(Upvote, {
	fragments: {
		viewer: () => Relay.QL `
			fragment on viewer {
				${UpvoteMutation.getFragment('viewer')},
				AuthUser{
					id,
					userName,
				}
			}
		`
	}
});
