import React, { Component } from 'react';
import {Link} from 'react-router';
import '../../stylesheets/actions.css';

class ViewQuestion extends Component {

  render() {
		const slug = this.props.slug;
    let classes;
    if(this.props.boxed){
      classes = "action-btn boxed";
    }else{
      classes = "action-btn";
    }
    return (
			<Link to={"/vraag/"+slug} className={classes}>
				<i className="fa fa-eye"></i> Bekijk alle antwoorden
			</Link>
		);
	}
}

export default ViewQuestion;
