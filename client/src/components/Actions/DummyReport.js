import React, { Component } from 'react';
import '../../stylesheets/actions.css';

class DummyReport extends Component {
	render() {
		let classes = "action-btn report right";
		if(this.props.boxed){
			classes = "action-btn report boxed right";
		}
		return (
			<span>
				<div className={classes+" completed"}>
					<i className="fa fa-flag"></i>
				</div>
			</span>
		);
	}
}

export default DummyReport;
