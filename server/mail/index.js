import nodemailer from 'nodemailer';
import * as fs from 'fs';
var transporter = nodemailer.createTransport();

var generateContent = (template,params) => {
	var content = fs.readFileSync(__dirname + '/templates/'+template+'.html','UTF-8', function(err, html){
		if (err) {
			console.log(err);
		}
		return html;
	});


	for (var key in params) {
		if (params.hasOwnProperty(key)) {
			var rgx = new RegExp('#'+key+'#',"g");
			content = content.replace(rgx, params[key]);
		}
	}
	return content;
};

module.exports = {
	send: function (recipient,subject,params,template) {
		var content= "Geen content";
		if(template){
			content = generateContent(template,params);
		}else{
			content = generateContent('basic',params);
		}
		var mailOptions = {
			from: '"Burgerzetel" <noreply@burgerzetel.nl>', // sender address
			to: recipient, // list of receivers
			subject: subject, // Subject line
			html: content, // html body
		};
		transporter.sendMail(mailOptions, function(error, info){
			if(error){
				return console.log(error);
			}
		});
  },
};
