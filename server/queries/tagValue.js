import _ from 'lodash';

export var getTagValuesByIds = (knex,ids) => {
	return knex.select().from('tagValue').whereIn('id', ids).then(TagValues => {
		TagValues = _.keyBy(TagValues, 'id');
		return ids.map(id => TagValues[id] || null);
	});
};


export var getTagValueByKey = (knex,key) => {
	return knex.select().from('tagValue').whereIn('key', key).then(TagValues => {
		TagValues = _.keyBy(TagValues, 'key');
		return key.map(id => TagValues[id] || null);
	});
};

export var GetAllTags = (knex,key) => {
	return knex.select().from('tagValue').orderByRaw('used DESC, value ASC').then(TagValues => {
		return TagValues.map(row => row.id);
	});
};

export var getTagValueIdsByQuery = (knex,Query) => {
	var each = Query.split(" ");
	var q = "";
	for (var i = 0; i < each.length; i++) {
		if(i===0){
			q += "value LIKE '%"+each[i]+"%' ";
		}else{
			if(each[i] !== ""){
				q += "AND value LIKE '%"+each[i]+"%' ";
			}
		}
	}
	return knex.select().from('tagValue').whereRaw(q).limit(5).then(TagValues => {
		return TagValues.map(row => row.id);
	});
};
