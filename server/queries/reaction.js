import _ from 'lodash';

export var getReactionsByIds = (knex,ids) => {
	return knex.select().from('reaction').whereIn('id', ids).then(Reactions => {
		Reactions = _.keyBy(Reactions, 'id');
		return ids.map(id => Reactions[id] || null);
	});
};
export var getReactionIdsByAnswerId = (knex,AnswerId) => {
	return knex.select('id').from('reaction').where('answerId', AnswerId).orderBy('upvoted', 'desc').then(ReactionIds => {
		return ReactionIds.map(row => row.id);
	});
};

export var getReactionIdsByUserId = (knex,UserId) => {
	return knex.select('id').from('reaction').where('userId', UserId).then(ReactionIds => {
		return ReactionIds.map(row => row.id);
	});
};

export async function getReportedReaction(knex, ReactionId) {
	var reported = await knex.select('reported','userId').first().from('reaction').where({
		id: ReactionId,
	});
	if (! reported) return null;
	return reported;
}

export async function getUpvotedReaction(knex, Id,UserId) {
	var upvoted = await knex.select('id').first().from('action').where({
		itemId: Id,
		userId: UserId,
		actionTypeId: 8,
	});
	if (!upvoted) return false;
	return upvoted;
}

export async function getUserIdByReactionId(knex, ReactionId) {
	var user = await knex.select('userId').first().from('reaction').where({id:ReactionId});
	return user.userId;
}
