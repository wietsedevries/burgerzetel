import _ from 'lodash';

export var getMaritalStatusesByIds = (knex,ids) => {
	return knex.select().from('maritalStatus').whereIn('id', ids).then(MaritalStatuses => {
		MaritalStatuses = _.keyBy(MaritalStatuses, 'id');
		return ids.map(id => MaritalStatuses[id] || null);
	});
};

export var getAllMaritalStatuses = (knex,ids) => {
	return knex.select().from('maritalStatus').then(MaritalStatuses => {
		return MaritalStatuses.map(row => row.id);
	});
};
