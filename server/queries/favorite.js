import _ from 'lodash';

export var getFavoriteByIds = (knex,ids) => {
	return knex.select().from('favorite').whereIn('id', ids).then(Favorite => {
		Favorite = _.keyBy(Favorite, 'id');
		return ids.map(id => Favorite[id] || null);
	});
};
export var getFavoriteIdsByUserId = (knex,UserId) => {
	return knex.select('id').from('favorite').where('userId', UserId).then(FavoriteIds => {
		return FavoriteIds.map(row => row.id);
	});
};
export var getFavoriteIdsByQuestionId = (knex,QuestionId,UserId) => {
	let uid = 0;
	if(UserId){
		uid = UserId;
	}
	return knex.select('id').from('favorite').where('questionId', QuestionId).andWhere('userId', uid).then(FavoriteIds => {
		return FavoriteIds.map(row => row.id);
	});
};
