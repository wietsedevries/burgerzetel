import _ from 'lodash';

export var getTagsByIds = (knex,ids) => {
	return knex.select().from('tag').whereIn('id', ids).then(Tags => {
		Tags = _.keyBy(Tags, 'id');
		return ids.map(id => Tags[id] || null);
	});
};

export var getTagIdsByTagValueIds = (knex,tagValueId) => {
	return knex.select('id').from('tag').whereIn('tagValueId', tagValueId).then(TagIds => {
		return TagIds.map(row => row.id);
	});
};

export var getTagIdsByQuestionId = (knex,QuestionId) => {
	return knex.select('id').from('tag').where('questionId', QuestionId).then(TagIds => {
		return TagIds.map(row => row.id);
	});
};

export var getTrendingTags = (knex,ids) => {
	const currentDate = new Date();
	const range = 1 //month
	const lastMonth = new Date(currentDate.setMonth(currentDate.getMonth()-range));
	return knex.select('id', 'tagValueId').from('tag').where('dateCreated', '>', lastMonth).count().groupBy('tagValueId').then(Tags => {
			function compare(a,b) {
				if (a['count(*)'] < b['count(*)']) {
					return 1;
				}
				if (a["count(*)"] > b["count(*)"]) {
					return -1;
				}
				return 0;
			}
			Tags.sort(compare);
			// Tags.totalCount = Tags.length;
			return Tags.map(row => row.id);
	});
};
