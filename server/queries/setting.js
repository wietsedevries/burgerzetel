import _ from 'lodash';

export var getSettingsByIds = (knex,ids) => {
	return knex.select().from('setting').whereIn('id', ids).then(Settings => {
		Settings = _.keyBy(Settings, 'id');
		return ids.map(id => Settings[id] || null);
	});
};

export var getSettingIdsByUserId = (knex,UserId) => {
	return knex.select('id').from('setting').where('userId', UserId).then(SettingIds => {
		return SettingIds.map(row => row.id);
	});
};
