import _ from 'lodash';
import bcrypt from 'bcrypt-nodejs';

export var getUsersByIds = (knex,ids) => {
	return knex.select().from('user').whereIn('id', ids).then(users => {
		users = _.keyBy(users, 'id');
		return ids.map(id => users[id] || null);
	});
};

export async function getUserIdByEmailAndPassword(knex, email, password) {
	if (! email || ! password) return null;
	var user = await knex.select('id','password').first().from('user').where({email});
	if (! user) return null;
	var verified = bcrypt.compareSync(password, user.password);
	if (! verified) return null;
	return user.id;
}

export async function getUserIdByFacebookId(knex, id) {
	if (! id) return null;
	var user = await knex.select('id').first().from('user').where('facebookId', id);
	if (! user) return null;
	return user.id;
}

export async function getUserIdByGoogleId(knex, id) {
	if (! id) return null;
	var user = await knex.select('id').first().from('user').where('googleId', id);
	if (! user) return null;
	return user.id;
}

export async function getUserIdByLinkedinId(knex, id) {
	if (! id) return null;
	var user = await knex.select('id').first().from('user').where('linkedinId', id);
	if (! user) return null;
	return user.id;
}

export var getUsersByEmails = (knex,email) => {
	return knex.select().from('user').whereIn('email', email).then(users => {
		users = _.keyBy(users, 'email');
		return email.map(id => users[id] || null);
	});
};

export var getUserByEmail = (knex,email) => {
	return knex.select().from('user').whereIn('email', email).then(Users => {
		Users = _.keyBy(email, 'email');
		return email.map(id => Users[id] || null);
	});
};

export async function getUserIdByEmail(knex, email) {
	if (! email) return null;
	var user = await knex.select('id').first().from('user').where({email});
	if (! user) return null;
	return user.id;
}

export async function getUserNameByUserName(knex, userName) {
	if (! userName) return null;
	var username = await knex.select('userName').first().from('user').where({userName});
	if (! username) return null;
	return username;
}

export var getUsersByUsername = (knex,username) => {
	return knex.select().from('user').whereIn('userName', username).then(Users => {
		Users = _.keyBy(Users, 'userName');
		return username.map(id => Users[id] || null);
	});
};

export var getTopTenUsers = (knex) => {
	return knex.select().from('user').orderBy('reputation', 'desc').limit(10).then(Users => {
		return Users.map(row => row.id);
	});
};


export var getUsersByQuery = (knex,Query) => {
	var each = Query.split(" ");
	var q = "";
	for (var i = 0; i < each.length; i++) {
		if(i===0){
			q += "(firstName LIKE '%"+each[i]+"%' OR lastName LIKE '%"+each[i]+"%') ";
		}else{
			if(each[i] !== ""){
				q += "AND (firstName LIKE '%"+each[i]+"%' OR lastName LIKE '%"+each[i]+"%') ";
			}
		}
	}
	return knex.select().from('user').whereRaw(q).limit(5).then(Users => {
		return Users.map(row => row.id);
	});
};
export async function getMax(knex) {
	var max = await knex.select('reputation').first().from('user').orderBy('reputation', 'desc').limit(1).offset(9);
	return max.reputation;
}

export async function getCityById(knex, CityId) {
	var city = await knex.select('municipalityId','tagValueId').first().from('city').where({id:CityId});
	return city;
}
export async function getMunicipalityById(knex, MunicipalityId) {
	var municipality = await knex.select('provinceId','tagValueId').first().from('municipality').where({id:MunicipalityId});
	return municipality;
}
export async function getProvinceById(knex, ProvinceId) {
	var province = await knex.select('tagValueId').first().from('province').where({id:ProvinceId});
	return province.tagValueId;
}
