import _ from 'lodash';

export var getFaqsByIds = (knex,ids) => {
	return knex.select().from('faq').whereIn('category', ids).then(Faqs => {
		Faqs = _.keyBy(Faqs, 'category');
		return ids.map(id => Faqs[id] || null);
	});
};
export var getFaqs = (knex,Category) => {
	return knex.select().from('faq').whereIn('category', Category).then(Faqs => {
		return Faqs.map(row => row.id);
	});
};
