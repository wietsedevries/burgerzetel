import _ from 'lodash';

export var getResetListItemsByIds = (knex,ids) => {
	return knex.select().from('resetList').whereIn('id', ids).then(ResetListItems => {
		ResetListItems = _.keyBy(ResetListItems, 'id');
		return ids.map(id => ResetListItems[id] || null);
	});
};
export var getResetListItemsByEmails = (knex,ids) => {
	return knex.select().from('resetList').whereIn('emailAddress', ids).then(ResetListItems => {
		ResetListItems = _.keyBy(ResetListItems, 'emailAddress');
		return ids.map(id => ResetListItems[id] || null);
	});
};

export var getResetListItemsByEmail = (knex,email) => {
	return knex.select().from('resetList').whereIn('emailAddress', email).then(ResetListItem => {
		return ResetListItem.map(row => row.id);
	});
};

export async function getResetListTokenById(knex, id) {
	if (! id) return null;
	var token = await knex.select('hash').first().from('resetList').where('id', id);
	if (! token) return null;
	return token.hash;
}

export var getResetListByHash = (knex,hash) => {
	return knex.select().from('resetList').whereIn('hash', hash).then(ResetLists => {
		ResetLists = _.keyBy(ResetLists, 'hash');
		return hash.map(id => ResetLists[id] || null);
	});
};
