import _ from 'lodash';

export var getNotificationTypesByIds = (knex,ids) => {
	return knex.select().from('notificationType').whereIn('id', ids).then(NotificationTypes => {
		NotificationTypes = _.keyBy(NotificationTypes, 'id');
		return ids.map(id => NotificationTypes[id] || null);
	});
};
