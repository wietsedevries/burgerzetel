import _ from 'lodash';

export var getMediumTypesByIds = (knex,ids) => {
	return knex.select().from('mediumType').whereIn('id', ids).then(MediumTypes => {
		MediumTypes = _.keyBy(MediumTypes, 'id');
		return ids.map(id => MediumTypes[id] || null);
	});
};
