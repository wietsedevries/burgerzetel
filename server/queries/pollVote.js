import _ from 'lodash';

export var getPollVotesByIds = (knex,ids) => {
	return knex.select().from('pollVote').whereIn('id', ids).then(PollVotes => {
		PollVotes = _.keyBy(PollVotes, 'id');
		return ids.map(id => PollVotes[id] || null);
	});
};

export var getPollVoteIdsByPollOptionId = (knex,PollOptionId) => {
	return knex.select('id').from('pollVote').where('pollOptionId', PollOptionId).then(PollOptionIds => {
		return PollOptionIds.map(row => row.id);
	});
};
