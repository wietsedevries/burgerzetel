import _ from 'lodash';

export var getMunicipalitiesByIds = (knex,ids) => {
	return knex.select().from('municipality').whereIn('id', ids).then(Municipalities => {
		Municipalities = _.keyBy(Municipalities, 'id');
		return ids.map(id => Municipalities[id] || null);
	});
};
