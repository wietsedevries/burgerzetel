import _ from 'lodash';

export var getQuestionsByIds = (knex,ids) => {
	return knex.select().from('question').whereIn('id', ids).then(questions => {
		questions = _.keyBy(questions, 'id');
		return ids.map(id => questions[id] || null);
	});
};

export var getQuestionsBySlugs = (knex,ids) => {
	return knex.select().from('question').whereIn('slug', ids).then(questions => {
		questions = _.keyBy(questions, 'slug');
		return ids.map(id => questions[id] || null);
	});
};

export async function getQuestionBySlug(knex, slug) {
	if (!slug) return null;
	var question = await knex.first().from('question').where({slug})
	if (! question) return false;
	return question;
}

export var getQuestionIdsByUserId = (knex,UserId) => {
	return knex.select('id').from('question').where('userId', UserId).then(questionIds => {
		return questionIds.map(row => row.id);
	});
};
export var getAllQuestions = (knex,ids) => {
	return knex.select().from('question').then(Questions => {
		return Questions.map(row => row.id);
	});
};
export var QuestionsByQuery = (knex,Query) => {
	var each = Query.split(" ");
	var q = "";
	for (var i = 0; i < each.length; i++) {
		if(i===0){
			q += "title LIKE '%"+each[i]+"%' ";
		}else{
			if(each[i] !== ""){
				q += "OR title LIKE '%"+each[i]+"%' ";
			}
		}
	}
	return knex.select().from('question').whereRaw(q).limit(7).then(Questions => {
		return Questions.map(row => row.id);
	});
};

export var getTrendingQuestions = (knex,ids) => {
	const currentDate = new Date();
	const range = 1; //month
	const dateNow = new Date(currentDate.setMonth(currentDate.getMonth()));
	const dateEarlier = new Date(currentDate.setMonth(currentDate.getMonth()-range));
	return knex.select('q.*').from('question as q').count('a.id as total').innerJoin('answer as a', function() {
			this.on('q.id', '=', 'a.questionId')
	}).whereBetween('lastAnswered', [dateEarlier,dateNow]).groupBy('q.id').then(Questions => {
		for (let i = 0; i < Questions.length; i++) {
			Questions[i].trendingValue = 0;
			//Calculate value of question, depending on number of answers and upvotes
			const Upvotes = Questions[i].upvoted;
			const trendingValue = (Questions[i].total+1)*((Upvotes+1)/0.5);
			Questions[i].trendingValue = trendingValue;
		}
		function compare(a,b) {
			if (a.trendingValue < b.trendingValue) {
				return 1;
			}
			if (a.trendingValue > b.trendingValue) {
				return -1;
			}
			return 0;
		}
		Questions.sort(compare);
		return Questions.map(row => row.id);
	});
};

export async function getFavoriteByQuestionId(knex, QuestionId, UserId) {
	var favorited = await knex.select('id').first().from('favorite').where({
		QuestionId: QuestionId,
		userId: UserId,
	});
	if (! favorited) return null;
	return favorited.id;
}

export async function getReportedQuestion(knex, QuestionId) {
	var reported = await knex.select('reported','userId').first().from('question').where({
		id: QuestionId,
	});
	if (! reported) return null;
	return reported;
}
export async function getUpvotedQuestion(knex, QuestionId,UserId) {
	var upvoted = await knex.select('id','dateCreated').first().from('action').where({
		itemId: QuestionId,
		userId: UserId,
		actionTypeId: 6,
	});
	if (!upvoted) return false;
	return upvoted;
}

export async function getUserIdByQuestionId(knex, QuestionId) {
	var user = await knex.select('userId').first().from('question').where({id:QuestionId});
	return user.userId;
}

export var getFeedByUserId = (knex,UserId) => {
	let Query = "SELECT i.tagValueId,t.questionId AS id, i.points AS rank,q.lastAnswered FROM "+
	"interest AS i INNER JOIN tag AS t ON i.tagValueId = t.tagValueId INNER JOIN question AS q "+
	"ON t.questionId = q.id  WHERE i.userId = "+UserId+" GROUP BY q.id ORDER BY rank DESC, q.lastAnswered DESC, q.upvoted DESC";

	return knex.raw(Query).then(QuestionIds => {
		return QuestionIds[0].map(row => row.id);
	});
};
