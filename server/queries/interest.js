import _ from 'lodash';

export var getInterestByIds = (knex,ids) => {
	return knex.select().from('interest').whereIn('id', ids).then(Interest => {
		Interest = _.keyBy(Interest, 'id');
		return ids.map(id => Interest[id] || null);
	});
};
export var getInterestById = (knex,tagValueId,UserId) => {
	return knex.select().from('interest').where({
		'tagValueId': tagValueId,
		'userId': UserId,
	}).then(Interest => {
		return Interest.map(row => row.id);
	});
};
export var getInterestIdsByUserId = (knex,UserId) => {
	return knex.select('id').from('interest').where({'userId': UserId, 'hidden': 0}).then(InterestIds => {
		return InterestIds.map(row => row.id);
	});
};
export var getAllInterestIdsByUserId = (knex,UserId) => {
	return knex.select('id').from('interest').where('userId', UserId).then(InterestIds => {
		return InterestIds.map(row => row.id);
	});
};
export var getInterestIdsByTagValueId = (knex,TagValueId) => {
	return knex.select('id').from('interest').where('tagValueId', TagValueId).then(TagValueIds => {
		return TagValueIds.map(row => row.id);
	});
};
