import _ from 'lodash';

export var getPollOptionsByIds = (knex,ids) => {
	return knex.select().from('pollOption').whereIn('id', ids).then(PollOptions => {
		PollOptions = _.keyBy(PollOptions, 'id');
		return ids.map(id => PollOptions[id] || null);
	});
};

export var getPollOptionIdsByPollId = (knex,PollId) => {
	return knex.select('id').from('pollOption').where('pollId', PollId).then(PollOptionIds => {
		return PollOptionIds.map(row => row.id);
	});
};
