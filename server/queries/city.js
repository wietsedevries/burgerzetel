import _ from 'lodash';

export var getCitiesByIds = (knex,ids) => {
	return knex.select().from('city').whereIn('id', ids).then(Cities => {
		Cities = _.keyBy(Cities, 'id');
		return ids.map(id => Cities[id] || null);
	});
};


export var getAllCities = (knex,ids) => {
	return knex.select().from('city').orderBy('name', 'asc').then(Cities => {
		return Cities.map(row => row.id);
	});
};
