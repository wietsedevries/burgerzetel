import _ from 'lodash';

export var getSettingTypesByIds = (knex,ids) => {
	return knex.select().from('settingType').whereIn('id', ids).then(SettingTypes => {
		SettingTypes = _.keyBy(SettingTypes, 'id');
		return ids.map(id => SettingTypes[id] || null);
	});
};

export var getAllSettingTypes = (knex,ids) => {
	return knex.select().from('settingType').then(SettingTypes => {
		return SettingTypes.map(row => row.id);
	});
};
