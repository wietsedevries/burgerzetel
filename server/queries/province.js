import _ from 'lodash';

export var getProvincesByIds = (knex,ids) => {
	return knex.select().from('province').whereIn('id', ids).then(Provinces => {
		Provinces = _.keyBy(Provinces, 'id');
		return ids.map(id => Provinces[id] || null);
	});
};
