import _ from 'lodash';

export var getActionTypeByIds = (knex,ids) => {
	return knex.select().from('actionType').whereIn('id', ids).then(ActionType => {
		ActionType = _.keyBy(ActionType, 'id');
		return ids.map(id => ActionType[id] || null);
	});
};
