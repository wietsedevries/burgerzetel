import _ from 'lodash';

export var getPartiesByIds = (knex,ids) => {
	return knex.select().from('party').whereIn('id', ids).then(Parties => {
		Parties = _.keyBy(Parties, 'id');
		return ids.map(id => Parties[id] || null);
	});
};
