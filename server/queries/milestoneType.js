import _ from 'lodash';

export var getMilestoneTypesByIds = (knex,ids) => {
	return knex.select().from('milestoneType').whereIn('id', ids).then(MilestoneTypes => {
		MilestoneTypes = _.keyBy(MilestoneTypes, 'id');
		return ids.map(id => MilestoneTypes[id] || null);
	});
};

export var getAllMilestoneTypes = (knex,ids) => {
	return knex.select().from('milestoneType').then(MilestoneTypes => {
		return MilestoneTypes.map(row => row.id);
	});
};
