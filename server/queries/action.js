import _ from 'lodash';

export var getActionByIds = (knex,ids) => {
	return knex.select().from('action').whereIn('id', ids).then(Action => {
		Action = _.keyBy(Action, 'id');
		return ids.map(id => Action[id] || null);
	});
};
export var getFlaggedQuestionIdsByUserId = (knex,UserId) => {
	return knex.select('id').from('action').where({
		'userId': UserId,
		'actionTypeId': 2,
		}).then(questionIds => {
		return questionIds.map(row => row.id);
	});
};
export var getFlaggedAnswerIdsByUserId = (knex,UserId) => {
	return knex.select('id').from('action').where({
		'userId': UserId,
		'actionTypeId': 3,
	}).then(answerIds => {
		return answerIds.map(row => row.id);
	});
};
export var getFlaggedReactionIdsByUserId = (knex,UserId) => {
	return knex.select('id').from('action').where({
		'userId': UserId,
		'actionTypeId': 4,
	}).then(reactionIds => {
		return reactionIds.map(row => row.id);
	});
};
export var getFlaggedReplyIdsByUserId = (knex,UserId) => {
	return knex.select('id').from('action').where({
		'userId': UserId,
		'actionTypeId': 5,
	}).then(replyIds => {
		return replyIds.map(row => row.id);
	});
};
export var getUpvotedQuestionIdsByUserId = (knex,UserId) => {
	return knex.select('id').from('action').where({
		'userId': UserId,
		'actionTypeId': 6,
		}).then(questionIds => {
		return questionIds.map(row => row.id);
	});
};
export var getUpvotedAnswerIdsByUserId = (knex,UserId) => {
	return knex.select('id').from('action').where({
		'userId': UserId,
		'actionTypeId': 7,
	}).then(answerIds => {
		return answerIds.map(row => row.id);
	});
};
export var getUpvotedReactionIdsByUserId = (knex,UserId) => {
	return knex.select('id').from('action').where({
		'userId': UserId,
		'actionTypeId': 8,
	}).then(reactionIds => {
		return reactionIds.map(row => row.id);
	});
};
export var getUpvotedReplyIdsByUserId = (knex,UserId) => {
	return knex.select('id').from('action').where({
		'userId': UserId,
		'actionTypeId': 9,
	}).then(replyIds => {
		return replyIds.map(row => row.id);
	});
};

export async function actionPerformed(knex, id, ati) {
	var user = await knex.select('userId').first().from('answer').where({
		itemId:id,
		actionTypeId: ati,
	}).orWhere(knex.raw(""));
	if(!user){
		return false;
	}
	return user.userId;
}
