import _ from 'lodash';

export var getAnswersByIds = (knex,ids) => {
	return knex.select().from('answer').whereIn('id', ids).orderBy('upvoted', 'desc').then(Answers => {
		Answers = _.keyBy(Answers, 'id');
		return ids.map(id => Answers[id] || null);
	});
};

export var getAnswerIdsByQuestionId = (knex,QuestionId) => {
	return knex.select('id').from('answer').where('questionId', QuestionId).orderBy('upvoted', 'desc').then(AnswerIds => {
		return AnswerIds.map(row => row.id);
	});
};

export var getAnswerIdsByUserId = (knex,UserId) => {
	return knex.select('id').from('answer').where('userId', UserId).orderBy('upvoted', 'desc').then(AnswerIds => {
		return AnswerIds.map(row => row.id);
	});
};

export async function getReportedAnswer(knex, AnswerId) {
	var reported = await knex.select('reported','userId').first().from('answer').where({
		id: AnswerId,
	});
	if (! reported) return null;
	return reported;
}
export async function getUpvotedAnswer(knex, Id,UserId) {
	var upvoted = await knex.select('id').first().from('action').where({
		itemId: Id,
		userId: UserId,
		actionTypeId: 7,
	});
	if (!upvoted) return false;
	return upvoted;
}
export async function getUserIdByAnswerId(knex, AnswerId) {
	var user = await knex.select('userId').first().from('answer').where({id:AnswerId});
	return user.userId;
}
