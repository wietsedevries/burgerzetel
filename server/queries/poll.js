import _ from 'lodash';

export var getPollByIds = (knex,ids) => {
	return knex.select().from('poll').whereIn('id', ids).then(Poll => {
		Poll = _.keyBy(Poll, 'id');
		return ids.map(id => Poll[id] || null);
	});
};
export var getPollIdsByQuestionId = (knex,QuestionId) => {
	return knex.select('id').from('poll').where('questionId', QuestionId).then(PollIds => {
		return PollIds.map(row => row.id);
	});
};
