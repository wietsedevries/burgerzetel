import _ from 'lodash';

export var getNotificationsByIds = (knex,ids) => {
	return knex.select().from('notification').whereIn('id', ids).then(Notifications => {
		Notifications = _.keyBy(Notifications, 'id');
		return ids.map(id => Notifications[id] || null);
	});
};

export var getNotificationIdsByUserId = (knex,UserId) => {
	return knex.select('id').from('notification').where('userId', UserId).orderBy('dateCreated', 'desc').then(NotificationIds => {
		return NotificationIds.map(row => row.id);
	});
};
export var getNotificationCountByUserId = (knex,UserId) => {
	return knex('notification').count('id as count').where({
		'userId': UserId,
		'status': 0,
	}).then(NotificationIds => {
		return NotificationIds[0].count;
	});
};
