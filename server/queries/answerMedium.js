import _ from 'lodash';

export var getAnswerMediumByIds = (knex,ids) => {
	return knex.select().from('answerMedium').whereIn('id', ids).then(AnswerMedium => {
		AnswerMedium = _.keyBy(AnswerMedium, 'id');
		return ids.map(id => AnswerMedium[id] || null);
	});
};
export var getAnswerMediumIdsByAnswerId = (knex,AnswerId) => {
	return knex.select('id').from('answerMedium').where('answerId', AnswerId).then(AnswerMediumIds => {
		return AnswerMediumIds.map(row => row.id);
	});
};
