import _ from 'lodash';
export var getContentByIds = (knex,ids) => {
  return knex.select().from('content').whereIn('id', ids).then(Content => {
		Content = _.keyBy(Content, 'id');
		return ids.map(id => Content[id] || null);
	});
};

export var getContentBySlug = (knex,ids) => {
	return knex.select().from('content').whereIn('slug', ids).then(Content => {
		Content = _.keyBy(Content, 'slug');
		return ids.map(id => Content[id] || null);
	});
};
