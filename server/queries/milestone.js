import _ from 'lodash';

export var getMilestonesByIds = (knex,ids) => {
	return knex.select().from('milestone').whereIn('id', ids).then(Milestones => {
		Milestones = _.keyBy(Milestones, 'id');
		return ids.map(id => Milestones[id] || null);
	});
};
export var getMilestoneIdsByUserId = (knex,UserId) => {
	return knex.select('id').from('milestone').where('userId', UserId).then(MilestoneIds => {
		return MilestoneIds.map(row => row.id);
	});
};
