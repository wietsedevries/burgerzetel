import _ from 'lodash';

export var getRepliesByIds = (knex,ids) => {
	return knex.select().from('reply').whereIn('id', ids).then(Reply => {
		Reply = _.keyBy(Reply, 'id');
		return ids.map(id => Reply[id] || null);
	});
};
export var getReplyIdsByReactionId = (knex,ReactionId) => {
	return knex.select('id').from('reply').where('reactionId', ReactionId).orderBy('upvoted', 'desc').then(ReactionIds => {
		return ReactionIds.map(row => row.id);
	});
};

export var getReplyIdsByUserId = (knex,UserId) => {
	return knex.select('id').from('reaction').where('userId', UserId).then(ReactionIds => {
		return ReactionIds.map(row => row.id);
	});
};

export async function getReportedReply(knex, ReplyId) {
	var reported = await knex.select('reported','userId').first().from('reply').where({
		id: ReplyId,
	});
	if (! reported) return null;
	return reported;
}
export async function getUpvotedReply(knex, Id,UserId) {
	var upvoted = await knex.select('id').first().from('action').where({
		itemId: Id,
		userId: UserId,
		actionTypeId: 9,
	});
	if (!upvoted) return false;
	return upvoted;
}
export async function getUserIdByReplyId(knex, ReplyId) {
	var user = await knex.select('userId').first().from('reply').where({id:ReplyId});
	return user.userId;
}
