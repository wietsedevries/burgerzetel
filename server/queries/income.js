import _ from 'lodash';

export var getIncomesByIds = (knex,ids) => {
	return knex.select().from('income').whereIn('id', ids).then(Incomes => {
		Incomes = _.keyBy(Incomes, 'id');
		return ids.map(id => Incomes[id] || null);
	});
};

export var getAllIncomes = (knex,ids) => {
	return knex.select().from('income').then(Incomes => {
		return Incomes.map(row => row.id);
	});
};
