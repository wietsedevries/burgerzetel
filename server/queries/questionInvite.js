import _ from 'lodash';

export var getQuestionInvitesByIds = (knex,ids) => {
	return knex.select().from('questionInvite').whereIn('id', ids).then(QuestionInvites => {
		QuestionInvites = _.keyBy(QuestionInvites, 'id');
		return ids.map(id => QuestionInvites[id] || null);
	});
};

export var getQuestionInvitesByQuestionId = (knex,QuestionId) => {
	return knex.select('id').from('questionInvite').where('questionId', QuestionId).then(Invites => {
		return Invites.map(row => row.id);
	});
};

export var getQuestionInviteIdsByUserId = (knex,UserId) => {
	return knex.select('id').from('questionInvite').where('to', UserId).then(SettingIds => {
		return SettingIds.map(row => row.id);
	});
};
