import _ from 'lodash';

export var getPagesByIds = (knex,ids) => {
	return knex.select().from('page').whereIn('id', ids).then(Pages => {
		Pages = _.keyBy(Pages, 'id');
		return ids.map(id => Pages[id] || null);
	});
};
