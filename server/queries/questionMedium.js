import _ from 'lodash';

export var getQuestionMediumByIds = (knex,ids) => {
	return knex.select().from('questionMedium').whereIn('id', ids).then(QuestionMedium => {
		QuestionMedium = _.keyBy(QuestionMedium, 'id');
		return ids.map(id => QuestionMedium[id] || null);
	});
};
export var getQuestionMediumIdsByQuestionId = (knex,QuestionId) => {
	return knex.select('id').from('questionMedium').where('QuestionId', QuestionId).then(QuestionMediumIds => {
		return QuestionMediumIds.map(row => row.id);
	});
};
