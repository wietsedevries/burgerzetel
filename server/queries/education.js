import _ from 'lodash';

export var getEducationByIds = (knex,ids) => {
	return knex.select().from('education').whereIn('id', ids).then(Education => {
		Education = _.keyBy(Education, 'id');
		return ids.map(id => Education[id] || null);
	});
};

export var getAllEducations = (knex,ids) => {
	return knex.select().from('education').then(Educations => {
		return Educations.map(row => row.id);
	});
};
