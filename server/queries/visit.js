import _ from 'lodash';

export var getVisitsByIds = (knex,ids) => {
	return knex.select().from('visit').whereIn('id', ids).then(Visits => {
		Visits = _.keyBy(Visits, 'id');
		return ids.map(id => Visits[id] || null);
	});
};

export var getVisitIdsByUserId = (knex,UserId) => {
	return knex.select('id').from('visit').where('userId', UserId).then(VisitIds => {
		return VisitIds.map(row => row.id);
	});
};
