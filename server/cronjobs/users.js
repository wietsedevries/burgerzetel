exports.cleanPasswordResets = function (CronJob, knex) {
	console.log("CRONJOB: 'cleanPasswordResets' initialized!");
	//runs at 23:58:00, every day, every month, sunday (0) - saturday (6)
	let cleanPasswordResets = new CronJob('00 58 23 * * 0-6', function() {
		const date = new Date();
		//get yesterday's date, give new users some time to activate their account.
		const dateYesterday = new Date(date.setDate(date.getDate()-1));
		knex('resetList').where('dateCreated', '<', dateYesterday).count().del().then(total => {
			console.log('CRONJOB: '+total+' password resets were removed!');
		});
	}, function () {
		//function when cleanActivatedUsers.stop() is called
		console.log("CRONJOB: 'cleanPasswordResets' was stopped!")
	},true,'Europe/Amsterdam');
};

exports.cleanUnactivatedUsers = function (CronJob, knex) {
	console.log("CRONJOB: 'cleanUnactivatedUsers' initialized!");
	//runs at 23:59:00, every day, every month, sunday (0) - saturday (6)
	let cleanUnactivatedUsers = new CronJob('00 59 23 * * 0-6', function() {
		const date = new Date();
		//get yesterday's date, give new users some time to activate their account.
		const dateYesterday = new Date(date.setDate(date.getDate() - 1));
		knex('user').where('dateRegistered', '<', dateYesterday).andWhere({statusId: 0}).count().del().then(total => {
			console.log('CRONJOB: '+total+' unactivated users were cleaned!');
		});
	}, function () {
		//function when cleanActivatedUsers.stop() is called
		console.log("CRONJOB: 'cleanUnactivatedUsers' was stopped!")
	},true,'Europe/Amsterdam');
};
