
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

DROP TABLE IF EXISTS `content`;
CREATE TABLE `content` (
  `id` int(11) unsigned NOT NULL,
  `slug` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` mediumtext NOT NULL,
  `dateUpdated` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

ALTER TABLE `content`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `content`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;

INSERT INTO `content` (`slug`, `title`, `content`, `dateUpdated`) VALUES
('cookies', 'Cookies informatie',
	-- Cookie pagina
	'deze pagina bevat inof over cookie gebruik'

,'2016-11-02 00:00:00');

INSERT INTO `content` (`slug`, `title`, `content`, `dateUpdated`) VALUES
('privacy-beleid', 'Privacybeleid',
	-- Privay beleid pagina
	'<strong>1) Waarborgen Privacy</strong>
<br/>
Het waarborgen van de privacy van bezoekers van burgerzetel.nl is een belangrijke
taak voor ons. Daarom beschrijven we in onze privacy policy welke informatie we
verzamelen en hoe we deze informatie gebruiken.
<br/><br/>
<strong>2) Toestemming</strong>
<br/>
Door de informatie en de diensten op burgerzetel.nl te gebruiken, gaat u akkoord
met onze privacy policy en de voorwaarden die wij hierin hebben opgenomen.
<br/><br/>
<strong>3) Vragen</strong>
<br/>
Als u meer informatie wilt ontvangen, of vragen hebt over de privacy policy van
Burgerzetel en specifiek burgerzetel.nl, kun u ons benaderen via e-mail. Ons
e-mailadres is info@burgerzetel.nl.
<br/><br/>
<strong>4) Monitoren gedrag bezoeker</strong>
<br/>
burgerzetel.nl maakt gebruik van verschillende technieken om bij te houden wie
de website bezoekt, hoe deze bezoeker zich op de website gedraagt en welke
pagina’s worden bezocht. Dat is een gebruikelijke manier van werken voor websites
omdat het informatie oplevert op die bijdraagt aan de kwaliteit van de
gebruikerservaring. De informatie die we, via cookies, registreren, bestaat uit
onder meer IP-adressen, het type browser en de bezochte pagina’s.
<br/><br/>
Tevens monitoren we waar bezoekers de website voor het eerst bezoeken en vanaf
welke pagina ze vertrekken. Deze informatie houden we anoniem bij en is niet
gekoppeld aan andere persoonlijke informatie.
<br/><br/>
<strong>5) Gebruik van cookies</strong>
<br/>
burgerzetel.nl plaatst cookies bij bezoekers. Dat doen we om informatie te
verzamelen over de pagina’s die gebruikers op onze website bezoeken, om bij te
houden hoe vaak bezoekers terug komen en om te zien welke pagina’s het goed doen
op de website. Ook houden we bij welke informatie de browser deelt.
<br/><br/>
<strong>6) Cookies uitschakelen</strong>
<br/>
U kunt er voor kiezen om cookies uit te schakelen. Dat doet u door gebruik te
maken de mogelijkheden van uw browser. U vindt meer informatie over deze
mogelijkheden op de website van de aanbieder van uw browser.
<br/><br/>
<strong>7) Cookies van derde partijen</strong>
<br/>
Het is mogelijk dat derde partijen, zoals Google, op onze website adverteren of
dat wij gebruik maken van een andere dienst. Daarvoor plaatsen deze derde
partijen in sommige gevallen cookies. Deze cookies zijn niet door burgerzetel.nl
te beïnvloeden.'

, '2016-11-02 00:00:00');

INSERT INTO `content` (`slug`, `title`, `content`, `dateUpdated`) VALUES
('media-kit', 'Media Kit',
	-- Media kit pagina
	'Info over de media kit'

, '2016-11-02 00:00:00');

INSERT INTO `content` (`slug`, `title`, `content`, `dateUpdated`) VALUES
('huisregels', 'Huisregels',
	-- Huisregels pagina
	'Info over de huisregels'

, '2016-11-02 00:00:00');
