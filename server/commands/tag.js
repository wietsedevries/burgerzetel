import _ from 'lodash';

export async function unSubscribe(knex,tagValueId,UserId) {
	return await knex('interest').where({
		tagValueId: tagValueId,
		userId:  UserId,
	}).del();
}
export async function subscribe(knex,tagValueId,Points,UserId,hidden = 0) {
	return await knex('interest').insert({
		tagValueId: tagValueId,
		points: Points,
		userId:  UserId,
		hidden: hidden,
	});
}
export async function updateInterest(knex,tagValueId,Points,UserId) {
	return await knex('interest').update({
		points: knex.raw('points + '+Points),
	}).where({
		'tagValueId': tagValueId,
		'userId':  UserId,
	});
}
export async function updateInterestHard(knex,tagValueId,Points,UserId) {
	return await knex('interest').update({
		points: knex.raw('points + '+Points),
		hidden: 0,
	}).where({
		'tagValueId': tagValueId,
		'userId':  UserId,
	});
}
export async function updateUsedTag(knex,tagValueId) {
	return await knex('tagValue').update({
		used: knex.raw('used + '+1),
	}).where({
		'id': tagValueId,
	});
}


export async function removeTags(knex,UserId) {
	return await knex('interest').where({
		userId:  UserId,
		hidden: 0,
	}).del();
}
export async function removeQuestionTags(knex,QuestionId) {
	return await knex('tag').where({
		questionId:  QuestionId,
	}).del();
}
export async function addTags(knex,tagValueId,questionId) {
	return await knex('tag').insert({
		tagValueId: tagValueId,
		questionId:  questionId,
		dateCreated: new Date(),
	});
}
