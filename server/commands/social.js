import GoogleAuth from 'google-auth-library';
var Linkedin = require('node-linkedin')('863oaf21ytstdm', 'q8O9xucP2ifQ4K1n');
import FB from 'fb';
import request from 'request';

export async function verifyGoogleToken(knex, token) {
	const clientId = '630392615970-o09atvjfsoqd4eisfcjrsr4436r66rrs.apps.googleusercontent.com';
	var auth = new GoogleAuth;
	var client = new auth.OAuth2(clientId, '', '');
	return new Promise(
		function(resolve) {
			client.verifyIdToken(
				token,
				clientId,
				function (e, login){
					if (login) {
						var payload = login.getPayload();
						var googleId = payload['sub'];
						var googleEmail = payload['email'];
						resolve({googleId: googleId, googleEmail: googleEmail});
					} else {
						resolve(null);
					}
				}
			)
		}
	);
}

export async function verifyLinkedinToken(knex, token, state, redirectUri) {

	var url = "https://www.linkedin.com/uas/oauth2/accessToken"
	var form = {
		"grant_type": "authorization_code",
		"code": token,
		"redirect_uri": redirectUri,
		"client_id": '863oaf21ytstdm',
		"client_secret": 'q8O9xucP2ifQ4K1n',
	};

	return new Promise(
		function(resolve) {
			request.post({url: url, form: form}, function (err, response, body) {
					if (err)
							resolve(err);
					var res = JSON.parse(body);
					if (typeof res.error !== 'undefined') {
							err = new Error(res.error_description);
							err.name = res.error;
							resolve(err, null);
					}
					var linkedin = Linkedin.init(res.access_token);
					linkedin.people.me(function(err, $in) {
						resolve($in);
					});
			});
		}
	);
}

export async function verifyFacebookToken(knex, token) {
	return new Promise(
		function(resolve) {
			FB.api('me', { fields: ['id', 'email'], access_token: token }, function (res) {
				resolve(res);
			});
		}
	);
}
