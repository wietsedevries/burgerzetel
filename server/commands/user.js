import _ from 'lodash';
import bcrypt from 'bcrypt-nodejs';

function randomString(length) {
	var text = "";
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	for(var i = 0; i < length; i++) {
			text += possible.charAt(Math.floor(Math.random() * possible.length));
	}
	return text;
}
export async function updateLastUserLogin(knex, id) {
	return await knex('user').update({
		lastLogin: new Date(),
		loginCount: knex.raw('loginCount + 1'),
	}).where('id', id);
}
export async function updateUserStatusId(knex, userId, statusId) {
	return await knex('user').update({
		statusId: statusId,
	}).where('id', userId);
}
export async function updateProfile(knex,firstName,lastName,functio,bio,city,education,maritalStatus,children,age,income,gender,id) {

	return await knex('user').update({
		firstName: firstName,
		lastName: lastName,
		function: functio,
		bio: bio,
		cityId: city,
		age: age,
		incomeId: income,
		educationId: education,
		maritalStatusId: maritalStatus,
		children: children,
		gender: gender,
	}).where('id', id);
}

export async function updateProfilePic(knex, avatar,id) {
	return await knex('user').update({
		avatar: avatar,
	}).where('id', id);
}

export async function createUser(knex, email,userName,code) {
	return await knex('user').insert({
		dateRegistered: new Date(),
		loginCount: 0,
		reputation: 0,
		verified: 0,
		statusId: 0,
		avatar: "",
		code: code,
		email: email,
		userName: userName,
	});
}
export async function createUserFromSocial(knex,email,firstName,lastName) {
	return await knex('user').insert({
		dateRegistered: new Date(),
		loginCount: 0,
		reputation: 0,
		verified: 0,
		statusId: 1,
		avatar: "",
		email: email,
		firstName: firstName,
		lastName: lastName,
	});
}
export async function updateUserNameAndCity(knex, userName, cityId, id) {
	return await knex('user').update({
		cityId: cityId,
		userName: userName,
	}).where('id', id);
}
export async function updateStatus(knex, id,statusId) {
	return await knex('user').update({
		statusId: statusId,
	}).where('id', id);
}

export async function updateFullNameAndCity(knex,firstName,lastName,city,id) {
	return await knex('user').update({
		firstName: firstName,
		lastName: lastName,
		cityId: city,
	}).where('id', id);
}
export async function updateSocialId(knex, id,userId,platform) {
	if (platform === "fb") {
		return await knex('user').update({
			facebookId: id,
		}).where('id', userId);
	} else if (platform === "li") {
		return await knex('user').update({
			linkedinId: id,
		}).where('id', userId);
	} else if (platform === "g") {
		return await knex('user').update({
			googleId: id,
		}).where('id', userId);
	} else {
		return null;
	}
}

export async function checkPassword(knex, password, id) {
	if (! id || ! password) return null;
	var user = await knex.select('password', 'id').first().from('user').where({id});
	if (! user) return null;
	var verified = bcrypt.compareSync(password, user.password);
	if (! verified) return false;
	return true;
}

export async function updatePassword(knex, password, id) {
	let hash = await bcrypt.hashSync(password);
	return await knex('user').update({
		password: hash,
	}).where('id', id);
}

export async function updateRegisterCode(knex, code, id) {
	return await knex('user').update({
		code: code,
	}).where('id', id);
}

export async function incrementReputation(knex, UserId, Points) {
	return await knex('user').update({
		reputation: knex.raw('reputation + '+Points),
	}).where('id', UserId);
}

export async function createPasswordReset(knex, email, userId) {
	var token = randomString(32);
	return await knex('resetList').insert({
		dateCreated: new Date(),
		userId: userId,
		emailAddress: email,
		hash: token,
	});
}

export async function destroyPasswordReset(knex, hash, userId) {
	return await knex('resetList').where({
		hash: hash,
		userId:  userId,
	}).del();
}
