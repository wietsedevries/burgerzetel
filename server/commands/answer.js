import _ from 'lodash';

export async function addAnswer(knex, QuestionId,Answer,UserId) {
	return await knex('answer').insert({
		dateCreated: new Date(),
		questionId: QuestionId,
		userId: UserId,
		answer: Answer,
	});
}
export async function hideAnswer(knex,id) {
	return await knex('answer').update({
		hidden: 1,
	}).where('id', id);
}
export async function reportAnswer(knex,id,reputation) {
	return await knex('answer').update({
		reported: knex.raw('reported + '+reputation),
	}).where('id', id);
}
export async function flagAnswer(knex, ItemId,UserId) {
	return await knex('action').insert({
		itemId: ItemId,
		userId: UserId,
		actionTypeId: 3,
		dateCreated: new Date(),
	});
}
