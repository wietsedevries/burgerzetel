import _ from 'lodash';

export async function upvoteQuestion(knex, ItemId,UserId) {
	return await knex('action').insert({
		itemId: ItemId,
		userId: UserId,
		actionTypeId: 6,
		dateCreated: new Date(),
	});
}

export async function removeUpvoteQuestion(knex,ItemId,UserId) {
	return await knex('action').where({
		itemId: ItemId,
		userId:  UserId,
		actionTypeId: 6,
	}).del();
}
export async function upvoteAnswer(knex, ItemId,UserId) {
	return await knex('action').insert({
		itemId: ItemId,
		userId: UserId,
		actionTypeId: 7,
		dateCreated: new Date(),
	});
}

export async function removeUpvoteAnswer(knex,ItemId,UserId) {
	return await knex('action').where({
		itemId: ItemId,
		userId:  UserId,
		actionTypeId: 7,
	}).del();
}
export async function upvoteReaction(knex, ItemId,UserId) {
	return await knex('action').insert({
		itemId: ItemId,
		userId: UserId,
		actionTypeId: 8,
		dateCreated: new Date(),
	});
}

export async function removeUpvoteReaction(knex,ItemId,UserId) {
	return await knex('action').where({
		itemId: ItemId,
		userId:  UserId,
		actionTypeId: 8,
	}).del();
}
export async function upvoteReply(knex, ItemId,UserId) {
	return await knex('action').insert({
		itemId: ItemId,
		userId: UserId,
		actionTypeId: 9,
		dateCreated: new Date(),
	});
}

export async function removeUpvoteReply(knex,ItemId,UserId) {
	return await knex('action').where({
		itemId: ItemId,
		userId:  UserId,
		actionTypeId: 9,
	}).del();
}

// update
export async function updateQuestion(knex,ItemId) {
	return await knex('question').update({
		upvoted: knex.raw('upvoted + 1'),
	}).where('id', ItemId);
}

export async function removeUpdateQuestion(knex,ItemId) {
	return await knex('question').update({
		upvoted: knex.raw('upvoted - 1'),
	}).where('id', ItemId);
}
export async function updateAnswer(knex, ItemId) {
	return await knex('answer').update({
		upvoted: knex.raw('upvoted + 1'),
	}).where('id', ItemId);
}

export async function removeUpdateAnswer(knex,ItemId) {
	return await knex('answer').update({
		upvoted: knex.raw('upvoted - 1'),
	}).where('id', ItemId);
}
export async function updateReaction(knex, ItemId) {
	return await knex('reaction').update({
		upvoted: knex.raw('upvoted + 1'),
	}).where('id', ItemId);
}

export async function removeUpdateReaction(knex,ItemId) {
	return await knex('reaction').update({
		upvoted: knex.raw('upvoted - 1'),
	}).where('id', ItemId);
}
export async function updateReply(knex, ItemId) {
	return await knex('reply').update({
		upvoted: knex.raw('upvoted + 1'),
	}).where('id', ItemId);
}

export async function removeUpdateReply(knex,ItemId) {
	return await knex('reply').update({
		upvoted: knex.raw('upvoted - 1'),
	}).where('id', ItemId);
}
