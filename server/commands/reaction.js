import _ from 'lodash';

export async function addReaction(knex, AnswerId,Reaction,UserId) {
	return await knex('reaction').insert({
		dateCreated: new Date(),
		answerId: AnswerId,
		userId: UserId,
		reaction: Reaction,
	});
}
export async function hideReaction(knex,id) {
	return await knex('reaction').update({
		hidden: 1,
	}).where('id', id);
}
export async function reportReaction(knex,id,reputation) {
	return await knex('reaction').update({
		reported: knex.raw('reported + '+reputation),
	}).where('id', id);
}
export async function flagReaction(knex, ItemId,UserId) {
	return await knex('action').insert({
		itemId: ItemId,
		userId: UserId,
		actionTypeId: 4,
		dateCreated: new Date(),
	});
}
