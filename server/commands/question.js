import _ from 'lodash';

export async function addFavorite(knex, QuestionId,UserId) {
	return await knex('favorite').insert({
		dateCreated: new Date(),
		questionId: QuestionId,
		userId: UserId,
	});
}
export async function deleteFavorite(knex, QuestionId,UserId) {
	return await knex('favorite').where({
		questionId: QuestionId,
		userId:  UserId,
	}).del();
}
export async function addQuestion(knex, Question,Description,UserId,Slug) {
	return await knex('question').insert({
		title: Question,
		slug: Slug,
		description: Description,
		dateCreated: new Date(),
		userId: UserId,
	});
}
export async function hideQuestion(knex,id) {
	return await knex('question').update({
		hidden: 1,
	}).where('id', id);
}
export async function viewQuestion(knex,id) {
	return await knex('question').update({
		views: knex.raw('views + 1'),
	}).where('id', id);
}
export async function reportQuestion(knex,id,reputation) {
	return await knex('question').update({
		reported: knex.raw('reported + '+reputation),
	}).where('id', id);
}
export async function flagQuestion(knex, ItemId,UserId) {
	return await knex('action').insert({
		itemId: ItemId,
		userId: UserId,
		actionTypeId: 2,
		dateCreated: new Date(),
	});
}
export async function addQuestionInvite(knex, from, to, questionId) {
	return await knex('questionInvite').insert({
		from: from,
		to: to,
		questionId: questionId,
	});
}
export async function updateQuestionData(knex,id,title,description) {
	return await knex('question').update({
		title: title,
		description: description,
	}).where('id', id);
}

export async function updateQuestionLastAnswered(knex,id) {
	return await knex('question').update({
		lastAnswered: new Date(),
	}).where('id', id);
}
