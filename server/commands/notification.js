import _ from 'lodash';

export async function createNewAnswerNotification(knex,QuestionId,AnswerId,UserId) {
	return await knex('notification').insert({
		dateCreated: new Date(),
		questionId: QuestionId,
		answerId: AnswerId,
		userId: UserId,
		notificationTypeId: 1,
	});
}
export async function createNewReactionNotification(knex,AnswerId,ReactionId,UserId) {
	return await knex('notification').insert({
		dateCreated: new Date(),
		reactionId: ReactionId,
		answerId: AnswerId,
		userId: UserId,
		notificationTypeId: 2,
	});
}
export async function createNewReplyNotification(knex,ReplyId,ReactionId,UserId) {
	return await knex('notification').insert({
		dateCreated: new Date(),
		reactionId: ReactionId,
		replyId: ReplyId,
		userId: UserId,
		notificationTypeId: 3,
	});
}
export async function inviteUserNotification(knex,QuestionId,UserId,askedBy) {
	return await knex('notification').insert({
		dateCreated: new Date(),
		questionId: QuestionId,
		userId: UserId,
		askedBy: askedBy,
		notificationTypeId: 4,
	});
}

export async function createFlaggedAnswerNotification(knex,AnswerId,UserId) {
	return await knex('notification').insert({
		dateCreated: new Date(),
		answerId: AnswerId,
		userId: UserId,
		notificationTypeId: 5,
	});
}
export async function createFlaggedReactionNotification(knex,ReactionId,UserId) {
	return await knex('notification').insert({
		dateCreated: new Date(),
		reactionId: ReactionId,
		userId: UserId,
		notificationTypeId: 6,
	});
}
export async function createFlaggedQuestionNotification(knex,QuestionId,UserId) {
	return await knex('notification').insert({
		dateCreated: new Date(),
		questionId: QuestionId,
		userId: UserId,
		notificationTypeId: 7,
	});
}
export async function createFlaggedReplyNotification(knex,ReplyId,UserId) {
	return await knex('notification').insert({
		dateCreated: new Date(),
		replyId: ReplyId,
		userId: UserId,
		notificationTypeId: 8,
	});
}

export async function createReputationNotification(knex,UserId,Msg) {
	return await knex('notification').insert({
		dateCreated: new Date(),
		msg: Msg,
		userId: UserId,
		notificationTypeId: 9,
	});
}


export async function readNotifications(knex,id) {
	return await knex('notification').update({
		status: 1,
	}).where('userId', id);
}
