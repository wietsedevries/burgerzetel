import _ from 'lodash';

export async function addReply(knex, ReactionId,Reply,UserId) {
	return await knex('reply').insert({
		dateCreated: new Date(),
		reactionId: ReactionId,
		userId: UserId,
		reply: Reply,
	});
}
export async function hideReply(knex,id) {
	return await knex('reply').update({
		hidden: 1,
	}).where('id', id);
}
export async function reportReply(knex,id,reputation) {
	return await knex('reply').update({
		reported: knex.raw('reported + '+reputation),
	}).where('id', id);
}
export async function flagReply(knex, ItemId,UserId) {
	return await knex('action').insert({
		itemId: ItemId,
		userId: UserId,
		actionTypeId: 5,
		dateCreated: new Date(),
	});
}
