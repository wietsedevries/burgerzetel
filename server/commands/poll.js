import _ from 'lodash';

export async function addPoll(knex,questionId,title) {
	return await knex('poll').insert({
		title: title,
		questionId:  questionId,
		dateCreated: new Date(),
	});
}
export async function addPollItem(knex,pollId,value) {
	return await knex('pollOption').insert({
		pollId: pollId,
		value:  value,
	});
}
export async function deletePoll(knex,QuestionId) {
	return await knex('poll').where({
		questionId:  QuestionId,
	}).del();
}
export async function deletePollItems(knex,PollId) {
	return await knex('pollOption').where({
		pollId:  PollId,
	}).del();
}
export async function vote(knex,UserId,OptionId) {
	return await knex('pollVote').insert({
		userId: UserId,
		pollOptionId:  OptionId,
	});
}
