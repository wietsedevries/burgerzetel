import _ from 'lodash';

export async function addSetting(knex,TypeId,UserId) {
	return await knex('setting').insert({
		userId: UserId,
		settingTypeId: TypeId,
		value: 1,
	});
}
export async function removeSetting(knex,TypeId,UserId) {
	return await knex('setting').where({
		userId:  UserId,
		settingTypeId: TypeId,
	}).del();
}
