import express from 'express';
import graphqlHTTP from 'express-graphql';
import _ from 'lodash';
import DataLoader from 'dataloader';
import FroalaEditor from 'wysiwyg-editor-node-sdk';
import bodyParser from 'body-parser';
import bcrypt from 'bcrypt-nodejs';
import jwt from 'jsonwebtoken';
import expressJWT from 'express-jwt';
import P from 'bluebird';
import formidable from 'formidable';
import path from 'path';
import fs from 'fs';

import schema from './schema';
import * as models from './schema/models';
import * as queryDefinitions from './queries';
import * as commandDefinitions from './commands';
import * as cronjobs from './cronjobs';
import Mail from './mail';

const app = express();

app.use(express.static(path.join(__dirname,'build')));

app.get('*', (req, res) => {
	res.sendFile(path.join(__dirname,'build', 'index.html'));
});


// db connection
var knex = require('knex')({
	client: 'mysql',
	connection: {
		host : 'localhost',
		user : 'tdawg',
		password : 'Pechtoldyaso17',
		database : 'maindb',
	},
});
// var knex = require('knex')({
// 	client: 'mysql',
// 	connection: {
// 		host : 'localhost',
// 		user : 'root',
// 		password : 'root',
// 		database : 'main',
// 	},
// });

// GraphQL
var queries = _.mapValues(queryDefinitions,queryDefinition => {
	return (...args) => {
		return P.resolve(queryDefinition(knex,...args));
	};
});

var commands = _.mapValues(commandDefinitions,commandDefinition => {
	return (...args) => {
		return P.resolve(commandDefinition(knex,...args));
	};
});

var createDataLoader = (batchLoad, Model) => new DataLoader(
	ids => batchLoad(ids).then(results => (Model) ?
		results.map(result => result && new Model(result))
	: results)
);

var initDataLoaders = () => ({
	User: createDataLoader(queries.getUsersByIds, models.UserModel),
	ActionType: createDataLoader(queries.getActionTypeByIds, models.ActionTypeModel),
	Action: createDataLoader(queries.getActionByIds, models.ActionModel),
	UserEmail: createDataLoader(queries.getUsersByEmails, models.UserModel),
	UserByName: createDataLoader(queries.getUsersByUsername, models.UserModel),
	getUsersByQuery: createDataLoader(queries.getUsersByQuery, models.TagValueModel),
	Question: createDataLoader(queries.getQuestionsByIds, models.QuestionModel),
	AllQuestions: createDataLoader(queries.getAllQuestions, models.QuestionModel),
	QuestionsByQuery: createDataLoader(queries.QuestionsByQuery, models.QuestionModel),
	QuestionBySlug: createDataLoader(queries.getQuestionsBySlugs, models.QuestionModel),
	TrendingQuestions: createDataLoader(queries.getTrendingQuestions, models.QuestionModel),
	City: createDataLoader(queries.getCitiesByIds, models.CityModel),
	Cities: createDataLoader(queries.getAllCities, models.CityModel),
	Municipality: createDataLoader(queries.getMunicipalitiesByIds, models.MunicipalityModel),
	Province: createDataLoader(queries.getProvincesByIds, models.ProvinceModel),
	Education: createDataLoader(queries.getEducationByIds, models.EducationModel),
	Educations: createDataLoader(queries.getAllEducations, models.MaritalStatusModel),
	Income: createDataLoader(queries.getIncomesByIds, models.IncomeModel),
	Incomes: createDataLoader(queries.getAllIncomes, models.MaritalStatusModel),
	MaritalStatus: createDataLoader(queries.getMaritalStatusesByIds, models.MaritalStatusModel),
	MaritalStatuses: createDataLoader(queries.getAllMaritalStatuses, models.MaritalStatusModel),
	Setting: createDataLoader(queries.getSettingsByIds, models.SettingModel),
	SettingType: createDataLoader(queries.getSettingTypesByIds, models.SettingTypeModel),
	SettingTypes: createDataLoader(queries.getAllSettingTypes, models.SettingTypeModel),
	Notification: createDataLoader(queries.getNotificationsByIds, models.NotificationModel),
	NotificationType: createDataLoader(queries.getNotificationTypesByIds, models.NotificationTypeModel),
	QuestionInvite: createDataLoader(queries.getQuestionInvitesByIds, models.QuestionInviteModel),
	QuestionInvites: createDataLoader(queries.getQuestionInvitesByQuestionId, models.QuestionInviteModel),
	Tag: createDataLoader(queries.getTagsByIds, models.TagModel),
	Tags: createDataLoader(queries.getTagIdsByTagValueIds, models.TagModel),
	TrendingTags: createDataLoader(queries.getTrendingTags, models.TagModel),
	TagValue: createDataLoader(queries.getTagValuesByIds, models.TagValueModel),
	TagValueByKey: createDataLoader(queries.getTagValueByKey, models.TagValueModel),
	TagValueByQuery: createDataLoader(queries.getTagValueIdsByQuery, models.TagValueModel),
	GetAllTags: createDataLoader(queries.GetAllTags, models.TagValueModel),
	GetTopTenUsers: createDataLoader(queries.getTopTenUsers, models.UserModel),
	ResetList: createDataLoader(queries.getResetListItemsByIds, models.ResetListModel),
	ResetListByHash: createDataLoader(queries.getResetListByHash, models.ResetListModel),
	ResetListEmail: createDataLoader(queries.getResetListItemsByEmails, models.ResetListModel),
	Faq: createDataLoader(queries.getFaqsByIds, models.FaqModel),
	Visit: createDataLoader(queries.getVisitsByIds, models.VisitModel),
	Party: createDataLoader(queries.getPartiesByIds, models.PartyModel),
	MilestoneType: createDataLoader(queries.getMilestoneTypesByIds, models.MilestoneTypeModel),
	MilestoneTypes: createDataLoader(queries.getAllMilestoneTypes, models.MilestoneTypeModel),
	Milestone: createDataLoader(queries.getMilestonesByIds, models.MilestoneModel),
	Interest: createDataLoader(queries.getInterestByIds, models.InterestModel),
	getInterestIdsByTagValueId: createDataLoader(queries.getInterestIdsByTagValueId, models.InterestModel),
	Favorite: createDataLoader(queries.getFavoriteByIds, models.FavoriteModel),
	MediumType: createDataLoader(queries.getMediumTypesByIds, models.MediumTypeModel),
	QuestionMedium: createDataLoader(queries.getQuestionMediumByIds, models.QuestionMediumModel),
	AnswerMedium: createDataLoader(queries.getAnswerMediumByIds, models.AnswerMediumModel),
	Poll: createDataLoader(queries.getPollByIds, models.PollModel),
	Answer: createDataLoader(queries.getAnswersByIds, models.AnswerModel),
	PollOption: createDataLoader(queries.getPollOptionsByIds, models.PollOptionModel),
	PollVote: createDataLoader(queries.getPollVotesByIds, models.PollVoteModel),
	Reaction: createDataLoader(queries.getReactionsByIds, models.ReactionModel),
	Reply: createDataLoader(queries.getRepliesByIds, models.ReplyModel),
	Page: createDataLoader(queries.getPagesByIds, models.PageModel),
	ContentBySlug: createDataLoader(queries.getContentBySlug, models.ContentModel),
});

const secret = "tijmen shaves his legs";
app.use(expressJWT({credentialsRequired: false, secret: secret}));

app.use('/graphql', graphqlHTTP(req => ({
	schema,
	graphiql: true,
	pretty: true,
	context: {
		queries,
		commands,
		loaders: initDataLoaders(),
		auth: req.user || {},
		authSign: id => jwt.sign({id}, secret),
	},
})));
// Add headers
app.use(function (req, res, next) {
		res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
		// res.setHeader('Authorization', 'Bearer ' + localStorage.token);
		next();
});

app.use(bodyParser.urlencoded({ extended: true }));

// Delete image
app.post('/delete_image', function (req, res) {
	let file = "/build/images/uploads/"+req.body.src;
	FroalaEditor.Image.delete(file, function(err) {
		if (err) {
			return res.status(404).end(JSON.stringify(err));
		}
		return res.end();
	});
});

// Image upload
app.post('/upload_image', function (req, res) {
	FroalaEditor.Image.upload(req, '/build/images/uploads/', function(err, data) {
		if (err) {
			console.log("Fout bij het uploaden van bestand");
			return res.send(JSON.stringify(err));
		}
		let patt = new RegExp(/[a-z0-9]+\.[a-z]+/g);
		let filename = patt.exec(data.link);
		data.link = "/images/uploads/"+filename[0];
		res.send(data);
	});
});

// Upload avatar
app.post('/uploadAvatar', function(req, res) {
	var form = new formidable.IncomingForm();
	form.multiples = false;
	// TODO: Change path here for build
	form.uploadDir = path.join(__dirname, '/build/images/avatars/');

	let filename;
	form.on('file', function(field, file) {
		let patt = new RegExp(/\.[a-z]+/g);
		let ext = patt.exec(file.name);
		filename = new Date().valueOf()+ext[0];

		fs.rename(file.path, path.join(form.uploadDir, filename));
	});

	form.on('error', function(err) {
		console.log('Error bij het uploaden: \n' + err);
	});

	form.on('end', function() {
		res.send(filename);
		res.end('success');
	});

	form.parse(req);
});

//Cronjobs
var CronJob = require('cron').CronJob;
cronjobs.cleanUnactivatedUsers(CronJob, knex);
cronjobs.cleanPasswordResets(CronJob, knex);

// Mailer
app.post('/sendMail', function (req, res) {
	Mail.send(req.body.recipient,req.body.subject,req.body.params,req.body.tpl);
	return res.sendStatus(200);
});

app.listen(3001, () => console.log("Server listening on port 3001"));
