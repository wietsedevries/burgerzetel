import bcrypt from 'bcrypt-nodejs';
import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean, GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions, mutationWithClientMutationId} from 'graphql-relay';

import {getViewer} from '../viewer';
import {ViewerType} from '../types/ViewerType';

import Mail from '../../mail';

export var LoginMutation = mutationWithClientMutationId({
	name: 'Login',
	outputFields: {
		viewer: {
			type: ViewerType,
			resolve: (data, args, ctx) => getViewer(),
		},
		authToken: {
			type: GraphQLString,
			resolve: (data, args, ctx) => data.authToken,
		},
	},
	inputFields: {
		email: {type: new GraphQLNonNull(GraphQLString)},
		password: {type: new GraphQLNonNull(GraphQLString)},
	},
	mutateAndGetPayload: async (input, ctx) => {
		var userId = await ctx.queries.getUserIdByEmailAndPassword(input.email, input.password);
		if (! userId) throw new Error('Permission denied');
		var user = await ctx.loaders.User.load(userId);
		if (! user) throw new Error('User not found');
		if (user.statusId < 1) throw new Error('User not activated');
		await ctx.commands.updateLastUserLogin(user.id);
		if (user.statusId === 1) {
			await ctx.commands.updateUserStatusId(user.id, 2);
		}
		var authToken = ctx.authSign(user.id);
		ctx.auth.id = user.id;
		return {authToken};
	},
});

export var GoogleLoginMutation = mutationWithClientMutationId({
	name: 'GoogleLogin',
	outputFields: {
		viewer: {
			type: ViewerType,
			resolve: (data, args, ctx) => getViewer(),
		},
		authToken: {
			type: GraphQLString,
			resolve: (data, args, ctx) => data.authToken,
		},
		msg: {
			type: GraphQLString,
			resolve: (data, args, ctx) => data.msg,
		},
	},
	inputFields: {
		googleAuthToken: {type: new GraphQLNonNull(GraphQLString)},
	},
	mutateAndGetPayload: async (input, ctx) => {
		var userId;
		var googleData = await ctx.commands.verifyGoogleToken(input.googleAuthToken);
		if (! googleData) throw new Error('invalid token');
		userId = await ctx.queries.getUserIdByGoogleId(googleData.googleId);
		if (!userId) {
			//no userId, try again with social email
			userId = await ctx.queries.getUserIdByEmail(googleData.googleEmail);

		}
		if (!userId) {
			var msg = "Er is geen geregistreerd account gevonden, u dient zich eerst te registreren.";
			return {msg};
		}
		//login user
		var user = await ctx.loaders.User.load(userId);
		if (! user) throw new Error('User not found');
		if (!user.googleId || user.googleId !== googleData.googleId) await ctx.commands.updateSocialId(googleData.googleId,user.id,'g');
		if (user.statusId < 1) throw new Error('User not activated');
		await ctx.commands.updateLastUserLogin(user.id);
		if (user.statusId === 1) {
			await ctx.commands.updateUserStatusId(user.id, 2);
		}
		var authToken = ctx.authSign(user.id);
		ctx.auth.id = user.id;
		return {authToken};
	},
});

export var LinkedinLoginMutation = mutationWithClientMutationId({
	name: 'LinkedinLogin',
	outputFields: {
		viewer: {
			type: ViewerType,
			resolve: (data, args, ctx) => getViewer(),
		},
		authToken: {
			type: GraphQLString,
			resolve: (data, args, ctx) => data.authToken,
		},
		msg: {
			type: GraphQLString,
			resolve: (data, args, ctx) => data.msg,
		},
	},
	inputFields: {
		linkedinAuthToken: {type: new GraphQLNonNull(GraphQLString)},
		linkedinState: {type: new GraphQLNonNull(GraphQLString)},
		redirectUri: {type: new GraphQLNonNull(GraphQLString)},
	},
	mutateAndGetPayload: async (input, ctx) => {
		var linkedinData = await ctx.commands.verifyLinkedinToken(input.linkedinAuthToken, input.linkedinState, input.redirectUri);
		var userId;
		if (! linkedinData) throw new Error('invalid request');
		userId = await ctx.queries.getUserIdByLinkedinId(linkedinData.id);
		if (!userId) {
			//no userId, try again with social email
			userId = await ctx.queries.getUserIdByEmail(linkedinData.emailAddress);
		}
		if (!userId) {
			var msg = "Er is geen geregistreerd account gevonden, u dient zich eerst te registreren.";
			return {msg};
		}
		//login user
		var user = await ctx.loaders.User.load(userId);
		if (! user) throw new Error('User not found');
		if (!user.linkedinId || user.linkedinId !== linkedinData.id) await ctx.commands.updateSocialId(linkedinData.id,user.id,'li');
		if (user.statusId < 1) throw new Error('User not activated');
		await ctx.commands.updateLastUserLogin(user.id);
		if (user.statusId === 1) {
			await ctx.commands.updateUserStatusId(user.id, 2);
		}
		var authToken = ctx.authSign(user.id);
		ctx.auth.id = user.id;
		return {authToken};
	},
});

export var FacebookLoginMutation = mutationWithClientMutationId({
	name: 'FacebookLogin',
	outputFields: {
		viewer: {
			type: ViewerType,
			resolve: (data, args, ctx) => getViewer(),
		},
		authToken: {
			type: GraphQLString,
			resolve: (data, args, ctx) => data.authToken,
		},
		msg: {
			type: GraphQLString,
			resolve: (data, args, ctx) => data.msg,
		},
	},
	inputFields: {
		facebookAuthToken: {type: new GraphQLNonNull(GraphQLString)},
	},
	mutateAndGetPayload: async (input, ctx) => {
		var facebookData = await ctx.commands.verifyFacebookToken(input.facebookAuthToken);
		var userId;
		if (! facebookData) throw new Error('invalid token');
		userId = await ctx.queries.getUserIdByFacebookId(facebookData.id);
		if (!userId) {
			//no userId, try again with social email
			userId = await ctx.queries.getUserIdByEmail(facebookData.email);
		}
		if (!userId) {
			var msg = "Er is geen geregistreerd account gevonden, u dient zich eerst te registreren.";
			return {msg};
		}
		//login user
		var user = await ctx.loaders.User.load(userId);
		if (! user) throw new Error('User not found');
		if (!user.facebookId || user.facebookId !== facebookData.id) await ctx.commands.updateSocialId(facebookData.id,user.id,'fb');
		if (user.statusId < 1) throw new Error('User not activated');
		await ctx.commands.updateLastUserLogin(user.id);
		if (user.statusId === 1) {
			await ctx.commands.updateUserStatusId(user.id, 2);
		}
		var authToken = ctx.authSign(user.id);
		ctx.auth.id = user.id;
		return {authToken};
	},
});

export var ResetPasswordLinkMutation = mutationWithClientMutationId({
	name: 'ResetPasswordLink',
	outputFields: {
		viewer: {
			type: ViewerType,
			resolve: (data, args, ctx) => getViewer(),
		},
		message: {
			type: GraphQLString,
			resolve: (data, args, ctx) => data.message,
		},
	},
	inputFields: {
		email: {type: new GraphQLNonNull(GraphQLString)},
	},
	mutateAndGetPayload: async (input, ctx) => {
		var message;
		var userId = await ctx.queries.getUserIdByEmail(input.email);
		if (!userId) {
			message = "Geen gebruiker bekend met het opgegeven e-mail adres.";
			return {message};
		}
		const resetId = await ctx.commands.createPasswordReset(input.email, userId);
		const token = await ctx.queries.getResetListTokenById(resetId[0]);
		if (!token) throw new Error("Token couldn't be found.");
		let mailData = {TOKEN: token};
		Mail.send(input.email,"Wachtwoord herstel link Burgerzetel.nl",mailData,"passwordReset");
		return {};
	},
});

export var ResetPasswordMutation = mutationWithClientMutationId({
	name: 'ResetPassword',
	outputFields: {
		viewer: {
			type: ViewerType,
			resolve: (data, args, ctx) => getViewer(),
		},
	},
	inputFields: {
		password: {type: new GraphQLNonNull(GraphQLString)},
		userId: {type: new GraphQLNonNull(GraphQLInt)},
		hash: {type: new GraphQLNonNull(GraphQLString)},
	},
	mutateAndGetPayload: async (input, ctx) => {
		await ctx.commands.updatePassword(input.password, input.userId);
		await ctx.commands.destroyPasswordReset(input.hash, input.userId);
		return {};
	},
});
