import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean, GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions, mutationWithClientMutationId} from 'graphql-relay';

import {getViewer} from '../viewer';
import {ViewerType} from '../types/ViewerType';

export var UpvoteMutation = mutationWithClientMutationId({
	name: 'Upvote',
	outputFields: {
		viewer: {
			type: ViewerType,
			resolve: (data, args, ctx) => getViewer(),
		},
	},
	inputFields: {
		id: {type: new GraphQLNonNull(GraphQLInt)},
		type: {type: new GraphQLNonNull(GraphQLString)},
	},
	mutateAndGetPayload: async (input, ctx) => {
		let upvoted;
		let userId;
		switch (input.type) {
			case "question":
				userId = await ctx.queries.getUserIdByQuestionId(input.id);
				if(userId !== ctx.auth.id){
					upvoted = await ctx.queries.getUpvotedQuestion(input.id,ctx.auth.id);
					if(upvoted){
						// await ctx.commands.removeUpvoteQuestion(input.id,ctx.auth.id);
						// await ctx.commands.removeUpdateQuestion(input.id,ctx.auth.id);
					}else{
						await ctx.commands.upvoteQuestion(input.id,ctx.auth.id);
						await ctx.commands.updateQuestion(input.id,ctx.auth.id);
						await ctx.commands.incrementReputation(userId,5);
						await ctx.commands.createReputationNotification(userId,"Uw vraag is gewaardeerd. +5 reputatie punten");
					}
				}else{
					return false;
				}
				break;
			case "answer":
				userId = await ctx.queries.getUserIdByAnswerId(input.id);
				if(userId !== ctx.auth.id){
					upvoted = await ctx.queries.getUpvotedAnswer(input.id,ctx.auth.id);
					if(upvoted){
						// await ctx.commands.removeUpvoteAnswer(input.id,ctx.auth.id);
						// await ctx.commands.removeUpdateAnswer(input.id,ctx.auth.id);
					}else{
						await ctx.commands.upvoteAnswer(input.id,ctx.auth.id);
						await ctx.commands.updateAnswer(input.id,ctx.auth.id);
						await ctx.commands.incrementReputation(userId,5);
						await ctx.commands.createReputationNotification(userId,"Uw antwoord is gewaardeerd. +10 reputatie punten");
					}
				}else{
					return false;
				}
				break;
			case "reaction":
				userId = await ctx.queries.getUserIdByReactionId(input.id);
				if(userId !== ctx.auth.id){
					upvoted = await ctx.queries.getUpvotedReaction(input.id,ctx.auth.id);
					if(upvoted){
						// await ctx.commands.removeUpvoteReaction(input.id,ctx.auth.id);
						// await ctx.commands.removeUpdateReaction(input.id,ctx.auth.id);
					}else{
						await ctx.commands.upvoteReaction(input.id,ctx.auth.id);
						await ctx.commands.updateReaction(input.id,ctx.auth.id);
						await ctx.commands.incrementReputation(userId,5);
						await ctx.commands.createReputationNotification(userId,"Uw reactie is gewaardeerd. +2 reputatie punten");
					}
				}else{
					return false;
				}
				break;
			case "reply":
				userId = await ctx.queries.getUserIdByReplyId(input.id);
				if(userId !== ctx.auth.id){
					upvoted = await ctx.queries.getUpvotedReply(input.id,ctx.auth.id);
					if(upvoted){
						// await ctx.commands.removeUpvoteReply(input.id,ctx.auth.id);
						// await ctx.commands.removeUpdateReply(input.id,ctx.auth.id);
					}else{
						await ctx.commands.upvoteReply(input.id,ctx.auth.id);
						await ctx.commands.updateReply(input.id,ctx.auth.id);
						await ctx.commands.incrementReputation(userId,5);
						await ctx.commands.createReputationNotification(userId,"Uw reactie is gewaardeerd. +2 reputatie punten");
					}
				}else{
					return false;
				}
				break;
			default:

		}
		return {};
	},
});
