import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean, GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions, mutationWithClientMutationId} from 'graphql-relay';

import {getViewer} from '../viewer';
import {ViewerType} from '../types/ViewerType';

export var ReportMutation = mutationWithClientMutationId({
	name: 'Report',
	outputFields: {
		viewer: {
			type: ViewerType,
			resolve: (data, args, ctx) => getViewer(),
		},
	},
	inputFields: {
		type: {type: new GraphQLNonNull(GraphQLString)},
		id: {type: new GraphQLNonNull(GraphQLInt)},
		reputation: {type: new GraphQLNonNull(GraphQLInt)},
	},
	mutateAndGetPayload: async (input, ctx) => {
		let max = await ctx.queries.getMax();
		switch (input.type) {
			case "question":
				let question = await ctx.queries.getReportedQuestion(input.id);
				await ctx.commands.flagQuestion(input.id,ctx.auth.id);
				if((question.reported + input.reputation) > max){
					// set question to hidden
					await ctx.commands.hideQuestion(input.id);
					await ctx.commands.createFlaggedQuestionNotification(input.id,question.userId);
					await ctx.commands.reportQuestion(input.id,max);
				}else{
					// update reported count
					await ctx.commands.reportQuestion(input.id,input.reputation);
				}
				break;
			case "answer":
				let answer = await ctx.queries.getReportedAnswer(input.id);
				await ctx.commands.flagAnswer(input.id,ctx.auth.id);
				if((answer.reported + input.reputation) > max){
					await ctx.commands.hideAnswer(input.id);
					await ctx.commands.createFlaggedAnswerNotification(input.id,answer.userId);
					await ctx.commands.reportAnswer(input.id,max);
				}else{
					await ctx.commands.reportAnswer(input.id,input.reputation);
				}
				break;
			case "reaction":
				let reaction = await ctx.queries.getReportedReaction(input.id);
				await ctx.commands.flagReaction(input.id,ctx.auth.id);
				if((reaction.reported + input.reputation) > max){
					await ctx.commands.hideReaction(input.id);
					await ctx.commands.createFlaggedReactionNotification(input.id,reaction.userId);
					await ctx.commands.reportReaction(input.id,max);
				}else{
					await ctx.commands.reportReaction(input.id,input.reputation);
				}
				break;
			case "reply":
				let reply = await ctx.queries.getReportedReply(input.id);
				await ctx.commands.flagReply(input.id,ctx.auth.id);
				if((reply.reported + input.reputation) > max){
					await ctx.commands.hideReply(input.id);
					await ctx.commands.createFlaggedReplyNotification(input.id,reply.userId);
					await ctx.commands.reportReply(input.id,max);
				}else{
					await ctx.commands.reportReply(input.id,input.reputation);
				}
				break;
			default:

		}
		return {};
	},
});
