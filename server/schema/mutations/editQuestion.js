import slugify from 'slugify';
import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean, GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions, mutationWithClientMutationId} from 'graphql-relay';

import {getViewer} from '../viewer';
import {ViewerType} from '../types/ViewerType';

export var EditQuestionMutation = mutationWithClientMutationId({
	name: 'EditQuestion',
	outputFields: {
		viewer: {
			type: ViewerType,
			resolve: (data, args, ctx) => getViewer(),
		},
	},
	inputFields: {
		question: {type: new GraphQLNonNull(GraphQLString)},
		description: {type: new GraphQLNonNull(GraphQLString)},
		tags: {type: new GraphQLNonNull(GraphQLString)},
		invited: {type: new GraphQLNonNull(GraphQLString)},
		pollTitle: {type: new GraphQLNonNull(GraphQLString)},
		pollItems: {type: new GraphQLNonNull(GraphQLString)},
		slug: {type: new GraphQLNonNull(GraphQLString)},
		questionId: {type: new GraphQLNonNull(GraphQLInt)},
	},

	mutateAndGetPayload: async (input, ctx) => {
		let questionId = input.questionId;

		// update question
		await ctx.commands.updateQuestionData(questionId, input.question, input.description);

		// delete all tags
		await ctx.commands.removeQuestionTags(questionId);
		// Insert new tags one by one
		var tags = input.tags.split(",");
		for(var i = 0; i < tags.length; i++){
			if(tags[i] !== null && tags[i] !== undefined && tags[i] !== 0 && tags[i] !== ""){
				await ctx.commands.addTags(tags[i],questionId);
				await ctx.commands.subscribe(tags[i],5,ctx.auth.id,1);
			}
		}
		// creat poll
		let PollId = await ctx.commands.deletePoll(questionId);
		await ctx.commands.deletePollItems(PollId)
		if(input.pollTitle !== ""){
			let pollId = await ctx.commands.addPoll(questionId,input.pollTitle);
			var pollItems = input.pollItems.split(",");
			for(var j = 0; j < pollItems.length; j++){
				if(pollItems[j] !== null && pollItems[j] !== undefined && pollItems[j] !== 0 && pollItems[j] !== ""){
					await ctx.commands.addPollItem(pollId,pollItems[j]);
				}
			}
		}

		// invite users
		var invited = input.invited.split(",");
		for(var k = 0; k < invited.length; k++){
			if(invited[k] !== null && invited[k] !== undefined && invited[k] !== 0 && invited[k] !== ""){
				await ctx.commands.inviteUserNotification(questionId,invited[k],ctx.auth.id);
				await ctx.commands.addQuestionInvite(ctx.auth.id,invited[k],questionId);
			}
		}
		return {};
	},
});
