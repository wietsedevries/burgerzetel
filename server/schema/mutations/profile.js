import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean, GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions, mutationWithClientMutationId} from 'graphql-relay';

import {getViewer} from '../viewer';
import {ViewerType} from '../types/ViewerType';

export var ProfileMutation = mutationWithClientMutationId({
	name: 'Profile',
	outputFields: {
		viewer: {
			type: ViewerType,
			resolve: (data, args, ctx) => getViewer(),
		},
		error: {
			type: GraphQLString,
			resolve: (data, args, ctx) => data.error,
		},
	},
	inputFields: {
		firstName: {type: new GraphQLNonNull(GraphQLString)},
		lastName: {type: new GraphQLNonNull(GraphQLString)},
		functio: {type: new GraphQLNonNull(GraphQLString)},
		bio: {type: new GraphQLNonNull(GraphQLString)},
		city: {type: new GraphQLNonNull(GraphQLInt)},
		education: {type: new GraphQLNonNull(GraphQLInt)},
		maritalStatus: {type: new GraphQLNonNull(GraphQLInt)},
		children: {type: new GraphQLNonNull(GraphQLString)},
		age: {type: new GraphQLNonNull(GraphQLInt)},
		income: {type: new GraphQLNonNull(GraphQLInt)},
		gender: {type: new GraphQLNonNull(GraphQLString)},
		tags: {type: new GraphQLNonNull(GraphQLString)},
		password: {type: new GraphQLNonNull(GraphQLString)},
		newPassword: {type: new GraphQLNonNull(GraphQLString)},
	},
	mutateAndGetPayload: async (input, ctx) => {
		//change password, if provided
		if (input.password && input.newPassword) {
			//check if current password is right
			if (await ctx.commands.checkPassword(input.password, ctx.auth.id)) {
				//all good
				ctx.commands.updatePassword(input.newPassword, ctx.auth.id);
			} else {
				var error = "Opgegeven huidige wachtwoord is incorrect, probeer het nogmaals";
				return {error};
			}
		}

		await ctx.commands.updateProfile(
			input.firstName,
			input.lastName,
			input.functio,
			input.bio,
			input.city,
			input.education,
			input.maritalStatus,
			input.children,
			input.age,
			input.income,
			input.gender,
			ctx.auth.id);
		// Remove all prior tags
		await ctx.commands.removeTags(ctx.auth.id)
		// Insert new tags one by one
		var tags = input.tags.split(",");
		for(var i = 0; i < tags.length; i++){
			if(tags[i] !== null && tags[i] !== undefined && tags[i] !== 0 && tags[i] !== ""){
				await ctx.commands.subscribe(tags[i],50,ctx.auth.id);
			}
		}


		return {};
	},
});
