import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean, GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions, mutationWithClientMutationId} from 'graphql-relay';

import {getViewer} from '../viewer';
import {ViewerType} from '../types/ViewerType';

import Mail from '../../mail';

export var VerifyMutation = mutationWithClientMutationId({
	name: 'Verify',
	outputFields: {
		viewer: {
			type: ViewerType,
			resolve: (data, args, ctx) => getViewer(),
		},
	},
	inputFields: {
		username: {type: new GraphQLNonNull(GraphQLString)},
	},
	mutateAndGetPayload: async (input, ctx) => {
		let mailData = {USERNAME: input.username};
		Mail.send("burgerzetelnl@gmail.com","Verificatie aanvraag",mailData,"verify");
		return {};
	},
});
