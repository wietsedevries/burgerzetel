import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean, GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions, mutationWithClientMutationId} from 'graphql-relay';

import {getViewer} from '../viewer';
import {ViewerType} from '../types/ViewerType';

export var ReplyMutation = mutationWithClientMutationId({
	name: 'Reply',
	outputFields: {
		viewer: {
			type: ViewerType,
			resolve: (data, args, ctx) => getViewer(),
		},
	},
	inputFields: {
		reactionId: {type: new GraphQLNonNull(GraphQLInt)},
		description: {type: new GraphQLNonNull(GraphQLString)},
	},
	mutateAndGetPayload: async (input, ctx) => {
		var userId = await ctx.queries.getUserIdByReactionId(input.reactionId);
		let replyId = await ctx.commands.addReply(input.reactionId,input.description,ctx.auth.id);
		if(userId !== ctx.auth.id){
			// notify creator of question
			await ctx.commands.createNewReplyNotification(replyId,input.reactionId,userId);
		}
		return {};
	},
});
