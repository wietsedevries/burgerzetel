import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean, GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions, mutationWithClientMutationId} from 'graphql-relay';

import {getViewer} from '../viewer';
import {ViewerType} from '../types/ViewerType';

export var SaveMutation = mutationWithClientMutationId({
	name: 'Save',
	outputFields: {
		viewer: {
			type: ViewerType,
			resolve: (data, args, ctx) => getViewer(),
		},
	},
	inputFields: {
		questionId: {type: new GraphQLNonNull(GraphQLInt)},
	},
	mutateAndGetPayload: async (input, ctx) => {
		var favorited = await ctx.queries.getFavoriteByQuestionId(input.questionId, ctx.auth.id);
		if(favorited){
			await ctx.commands.deleteFavorite(input.questionId, ctx.auth.id);
		}else{
			await ctx.commands.addFavorite(input.questionId, ctx.auth.id);
		}
		var response = ctx.auth.id;
		return {response};
	},
});
