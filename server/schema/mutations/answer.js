import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean, GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions, mutationWithClientMutationId} from 'graphql-relay';

import {getViewer} from '../viewer';
import {ViewerType} from '../types/ViewerType';

export var AnswerMutation = mutationWithClientMutationId({
	name: 'Answer',
	outputFields: {
		viewer: {
			type: ViewerType,
			resolve: (data, args, ctx) => getViewer(),
		},
	},
	inputFields: {
		questionId: {type: new GraphQLNonNull(GraphQLInt)},
		description: {type: new GraphQLNonNull(GraphQLString)},
		slug: {type: new GraphQLNonNull(GraphQLString)},
	},
	mutateAndGetPayload: async (input, ctx) => {
		var userId = await ctx.queries.getUserIdByQuestionId(input.questionId);
		let answerId = await ctx.commands.addAnswer(input.questionId,input.description,ctx.auth.id);
		await ctx.commands.updateQuestionLastAnswered(input.questionId);
		if(userId !== ctx.auth.id){
			// notify creator of question (if creator is not logged in user)
			await ctx.commands.createNewAnswerNotification(input.questionId,answerId,userId);
			// Award reputation points to user who answers and notify them// 15pts
			await ctx.commands.incrementReputation(ctx.auth.id,15);
			await ctx.commands.createReputationNotification(ctx.auth.id,"U heeft 15 reputatie punten verdiend met het beantwoorden van een vraag.");
		}
		return {};
	},
});
