import bcrypt from 'bcrypt-nodejs';
import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean, GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions, mutationWithClientMutationId} from 'graphql-relay';

import {getViewer} from '../viewer';
import {ViewerType} from '../types/ViewerType';

import Mail from '../../mail';

export var RegisterMutation = mutationWithClientMutationId({
	name: 'Register',
	outputFields: {
		viewer: {
			type: ViewerType,
			resolve: (data, args, ctx) => getViewer(),
		},
		email: {
			type: GraphQLString,
			resolve: (data, args, ctx) => data.email,
		},
		message: {
			type: GraphQLString,
			resolve: (data, args, ctx) => data.message,
		},
		authToken: {
			type: GraphQLString,
			resolve: (data, args, ctx) => data.authToken,
		},
	},
	inputFields: {
		email: {type: new GraphQLNonNull(GraphQLString)},
		userName: {type: new GraphQLNonNull(GraphQLString)},
		code: {type: new GraphQLNonNull(GraphQLInt)},
		cityId: {type: new GraphQLNonNull(GraphQLInt)},
		lastName: {type: new GraphQLNonNull(GraphQLString)},
		firstName: {type: new GraphQLNonNull(GraphQLString)},
		password: {type: new GraphQLNonNull(GraphQLString)},
		invites: {type: new GraphQLNonNull(GraphQLString)},
	},
	//this mutation is a progress working in steps, it's built to save the data in every step
	mutateAndGetPayload: async (input, ctx) => {
		var message;
		//Try to find a user
		if (input.email) {
			var userId = await ctx.queries.getUserIdByEmail(input.email);
			if (userId) {
				var user = await ctx.loaders.User.load(userId);
			}
		}
		//creating new user account with email and username
		if (input.email && input.userName) {
			//check if userName does exist
			if(await ctx.queries.getUserNameByUserName(input.userName)) {
				message = "Gebruikersnaam is al bezet";
				return {email, message};
			}
			let email = input.email;
			let code = Math.floor(Math.random()*1000000);
			if (userId) {
				//already a user!
				user = await ctx.loaders.User.load(userId);
				if (user.statusId > 0) {
					message = "Er is al een account geregistreerd met dit e-mail adres.";
					return {email, message}
				} else {
					//user not verified, update code
					await ctx.commands.updateRegisterCode(code, user.id);
				}
			} else {
				//no user, create it
				await ctx.commands.createUser(email,input.userName,code);
			}
			let mailData = {CODE: code};
			Mail.send(email,"Verificatiecode Burgerzetel.nl",mailData,"emailValidation");
			return {email};
		}
		//for the next steps, there MUST be a user!
		if (!user) {throw new Error("No user found")}
		//verifying the email
		if (input.code && user) {
			if (input.code !== user.code) throw new Error("Code doesn't match");
			await ctx.commands.updateStatus(user.id, 1);
			return {};
		}
		if (user.statusId <= 0) throw new Error("User email should be verified");
		//saving the firstName, lastName and cityId
		else if (input.cityId && input.lastName && input.firstName && user) {
			let {cityId, lastName, firstName, email} = input;
			await ctx.commands.subscribe(3627,50,user.id);
			// Add interest based on city Id
			let interests = [];
			var city = await ctx.queries.getCityById(cityId);
			var municipality = await ctx.queries.getMunicipalityById(city.municipalityId);
			let provinceTagValueId = await ctx.queries.getProvinceById(municipality.provinceId);
			interests.push(city.tagValueId);
			interests.push(municipality.tagValueId);
			interests.push(provinceTagValueId);
			for(var i = 0; i < interests.length; i++){
				await ctx.commands.subscribe(interests[i],50,user.id);
			}
			await ctx.commands.updateFullNameAndCity(firstName, lastName, cityId, user.id);
			return {};
		}
		//setting the password
		else if (input.password && user) {
			let {password} = input;
			await ctx.commands.updatePassword(password, user.id);
			return {};
		}
		//send invites to people when provided
		else if (input.invites) {
			var fullName = user.firstName+" "+user.lastName;
			var invites = input.invites.split(",");
			for(var i = 0; i < invites.length; i++){
				let mailData = {FULLNAME: fullName};
				Mail.send(invites[i],"U bent uitgenodigd om deel te nemen aan Burgerzetel.nl",mailData,"registerInvitation");
			}
			return {};
		}
		//login if only email provided and account email is verified
		//only allow logging in without password (during registration) when statusId is 1.
		else if (input.email && user.statusId === 1) {
			await ctx.commands.updateLastUserLogin(user.id);
			await ctx.commands.updateUserStatusId(user.id, 2);
			var authToken = ctx.authSign(user.id);
			ctx.auth.id = user.id;
			return {authToken};
		} else if (user.statusId >= 2) {
			throw new Error("User statusId should be 1 to perform this action");
		}
		throw new Error("Nothing mutated");
	},
});

export var ResendRegisterCodeMutation = mutationWithClientMutationId({
	name: 'ResendRegisterCode',
	outputFields: {
		viewer: {
			type: ViewerType,
			resolve: (data, args, ctx) => getViewer(),
		},
	},
	inputFields: {
		email: {type: new GraphQLNonNull(GraphQLString)},
	},
	mutateAndGetPayload: async (input, ctx) => {
		let email = input.email;
		var userId = await ctx.queries.getUserIdByEmail(input.email);
		if (! userId) throw new Error('No user with that email, no user created in previous step?');
		var user = await ctx.loaders.User.load(userId);
		if (! user) throw new Error('User not found');
		let code = Math.floor(Math.random()*999999) + 111111;
		await ctx.commands.updateRegisterCode(code, user.id);
		let mailData = {CODE: code};
		Mail.send(email,"Verificatiecode Burgerzetel.nl",mailData,"emailValidation");
		return {email};
	},
});

export var SocialRegisterMutation = mutationWithClientMutationId({
	name: 'SocialRegister',
	outputFields: {
		viewer: {
			type: ViewerType,
			resolve: (data, args, ctx) => getViewer(),
		},
		email: {
			type: GraphQLString,
			resolve: (data, args, ctx) => data.email,
		},
		message: {
			type: GraphQLString,
			resolve: (data, args, ctx) => data.message,
		},
	},
	inputFields: {
		email: {type: new GraphQLNonNull(GraphQLString)},
		userName: {type: new GraphQLNonNull(GraphQLString)},
		cityId: {type: new GraphQLNonNull(GraphQLInt)},
		lastName: {type: new GraphQLNonNull(GraphQLString)},
		firstName: {type: new GraphQLNonNull(GraphQLString)},
		password: {type: new GraphQLNonNull(GraphQLString)},
		facebookId: {type: new GraphQLNonNull(GraphQLString)},
		linkedinId: {type: new GraphQLNonNull(GraphQLString)},
		googleId: {type: new GraphQLNonNull(GraphQLString)},
	},
	//social mutation will have two steps, both steps are performed in the same mutation, depending on the fields provided
	mutateAndGetPayload: async (input, ctx) => {
		var message;
		//Try to find a user
		if (input.email) {
			var email = input.email;
			var userId = await ctx.queries.getUserIdByEmail(email);
			if (userId) {
				var user = await ctx.loaders.User.load(userId);
			}
		}
		//creating new user account with email, firstName and lastName
		if (email && input.firstName && input.lastName) {
			//check for existing account with the email provided
			if (userId) {
				user = await ctx.loaders.User.load(userId);
				if (user.statusId > 1) {
					message = "Er is al een account geregistreerd met dit e-mail adres.";
					return {message};
				}
			}
			if (!user) {
				userId = await ctx.commands.createUserFromSocial(email,input.firstName,input.lastName);
				userId = userId[0];
			}
			if (input.facebookId) {
				await ctx.commands.updateSocialId(input.facebookId,userId,'fb');
			}
			if (input.linkedinId) {
				await ctx.commands.updateSocialId(input.linkedinId,userId,'li');
			}
			if (input.googleId) {
				await ctx.commands.updateSocialId(input.googleId,userId,'g');
			}
			return {email};
		}
		//from this point on, there should be a user and is verified
		if (!user) throw new Error("No user found");
		if (user.statusId <= 0) throw new Error("User should be verified");

		//setting the password, userName and city
		else if (input.password && input.userName && input.cityId && user) {
			//check if userName is available
			if(await ctx.queries.getUserNameByUserName(input.userName)) {
				message = "Er is al een account geregistreerd met deze gebruikersnaam";
				return {message};
			}
			await ctx.commands.subscribe(3627,50,user.id);
			// Add interest based on city Id
			let {cityId} = input;
			let interests = [];
			var city = await ctx.queries.getCityById(cityId);
			var municipality = await ctx.queries.getMunicipalityById(city.municipalityId);
			let provinceTagValueId = await ctx.queries.getProvinceById(municipality.provinceId);
			interests.push(city.tagValueId);
			interests.push(municipality.tagValueId);
			interests.push(provinceTagValueId);
			for(var i = 0; i < interests.length; i++){
				await ctx.commands.subscribe(interests[i],50,user.id);
			}
			let {password} = input;
			await ctx.commands.updatePassword(password, user.id);
			await ctx.commands.updateUserNameAndCity(input.userName, input.cityId, user.id);
			return {};
		}
		throw new Error("Nothing mutated");
	},
});
