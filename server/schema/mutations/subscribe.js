import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean, GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions, mutationWithClientMutationId} from 'graphql-relay';

import {getViewer} from '../viewer';
import {ViewerType} from '../types/ViewerType';

export var SubscriptionMutation = mutationWithClientMutationId({
	name: 'Subscription',
	outputFields: {
		viewer: {
			type: ViewerType,
			resolve: (data, args, ctx) => getViewer(),
		},
	},
	inputFields: {
		tagValueId: {type: new GraphQLNonNull(GraphQLInt)},
		state: {type: new GraphQLNonNull(GraphQLBoolean)},
	},
	mutateAndGetPayload: async (input, ctx) => {

		if(input.state){
			let tagExists = await ctx.queries.getInterestById(input.tagValueId,ctx.auth.id);
			if(tagExists.length > 0){
				await ctx.commands.updateInterestHard(input.tagValueId,50,ctx.auth.id);
			}else{
				await ctx.commands.subscribe(input.tagValueId,50,ctx.auth.id);
			}
		}else{
			await ctx.commands.unSubscribe(input.tagValueId,ctx.auth.id);
		}
		return {};
	},
});
