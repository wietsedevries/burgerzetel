import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean, GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions, mutationWithClientMutationId} from 'graphql-relay';

import {getViewer} from '../viewer';
import {ViewerType} from '../types/ViewerType';

export var ViewMutation = mutationWithClientMutationId({
	name: 'View',
	outputFields: {
		viewer: {
			type: ViewerType,
			resolve: (data, args, ctx) => getViewer(),
		},
	},
	inputFields: {
		questionId: {type: new GraphQLNonNull(GraphQLInt)},
		tags: {type: new GraphQLNonNull(GraphQLString)},
	},
	mutateAndGetPayload: async (input, ctx) => {
		await ctx.commands.viewQuestion(input.questionId);
		var tags = input.tags.split(",");
		for(var i = 0; i < tags.length; i++){
			if(tags[i] !== null && tags[i] !== undefined && tags[i] !== 0 && tags[i] !== ""){
				let tagExists = await ctx.queries.getInterestById(tags[i],ctx.auth.id);
				if(tagExists.length > 0){
					await ctx.commands.updateInterest(tags[i],1,ctx.auth.id);
				}else{
					await ctx.commands.subscribe(tags[i],1,ctx.auth.id,1);
				}
			}
		}
		return {};
	},
});
