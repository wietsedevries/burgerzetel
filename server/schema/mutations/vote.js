import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean, GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions, mutationWithClientMutationId} from 'graphql-relay';

import {getViewer} from '../viewer';
import {ViewerType} from '../types/ViewerType';

export var VoteMutation = mutationWithClientMutationId({
	name: 'Vote',
	outputFields: {
		viewer: {
			type: ViewerType,
			resolve: (data, args, ctx) => getViewer(),
		},
	},
	inputFields: {
		optionId: {type: new GraphQLNonNull(GraphQLInt)},
	},
	mutateAndGetPayload: async (input, ctx) => {
		await ctx.commands.vote(ctx.auth.id,input.optionId)
		return {};
	},
});
