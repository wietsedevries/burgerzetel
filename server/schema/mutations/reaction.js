import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean, GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions, mutationWithClientMutationId} from 'graphql-relay';

import {getViewer} from '../viewer';
import {ViewerType} from '../types/ViewerType';

export var ReactionMutation = mutationWithClientMutationId({
	name: 'Reaction',
	outputFields: {
		viewer: {
			type: ViewerType,
			resolve: (data, args, ctx) => getViewer(),
		},
	},
	inputFields: {
		answerId: {type: new GraphQLNonNull(GraphQLInt)},
		description: {type: new GraphQLNonNull(GraphQLString)},
	},
	mutateAndGetPayload: async (input, ctx) => {
		var userId = await ctx.queries.getUserIdByAnswerId(input.answerId);
		let reactionId = await ctx.commands.addReaction(input.answerId,input.description,ctx.auth.id);
		if(userId !== ctx.auth.id){
			// notify creator of question
			await ctx.commands.createNewReactionNotification(input.answerId,reactionId,userId);
			// Award reputation points to user who answers and notify them// 5pts
			// await ctx.commands.incrementReputation(ctx.auth.id,5);
			// await ctx.commands.createReputationNotification(ctx.auth.id,"U heeft 5 reputatie punten verdiend met het reageren op een antwoord.");
		}
		return {};
	},
});
