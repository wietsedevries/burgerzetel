import slugify from 'slugify';
import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean, GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions, mutationWithClientMutationId} from 'graphql-relay';

import {getViewer} from '../viewer';
import {ViewerType} from '../types/ViewerType';

export var QuestionMutation = mutationWithClientMutationId({
	name: 'Question',
	outputFields: {
		viewer: {
			type: ViewerType,
			resolve: (data, args, ctx) => getViewer(),
		},
		slug: {
			type: GraphQLString,
			resolve: (data, args, ctx) => data.slug,
		},
	},
	inputFields: {
		question: {type: new GraphQLNonNull(GraphQLString)},
		description: {type: new GraphQLNonNull(GraphQLString)},
		tags: {type: new GraphQLNonNull(GraphQLString)},
		invited: {type: new GraphQLNonNull(GraphQLString)},
		pollTitle: {type: new GraphQLNonNull(GraphQLString)},
		pollItems: {type: new GraphQLNonNull(GraphQLString)},
	},

	mutateAndGetPayload: async (input, ctx) => {
		let slug = slugify(input.question);
		// Check if slug already exists
		var question = await ctx.queries.getQuestionBySlug(slug);
		if(question){
			// else slug+random number
			slug = slug+Math.floor(Math.random() * 99999);
			question = await ctx.queries.getQuestionBySlug(slug);
			// check if that one also exists
			if(question){
				// else slug+random number+random number // TODO: better failsafe
				slug = slug+Math.floor(Math.random() * 99999);
			}
		}
		let questionId = await ctx.commands.addQuestion(input.question,input.description,ctx.auth.id,slug);
		// Insert new tags one by one
		var tags = input.tags.split(",");
		for(var i = 0; i < tags.length; i++){
			if(tags[i] !== null && tags[i] !== undefined && tags[i] !== 0 && tags[i] !== ""){
				await ctx.commands.updateUsedTag(tags[i]);
				await ctx.commands.addTags(tags[i],questionId);
				let tagExists = await ctx.queries.getInterestById(tags[i],ctx.auth.id);
				if(tagExists.length > 0){
					await ctx.commands.updateInterest(tags[i],5,ctx.auth.id);
				}else{
					await ctx.commands.subscribe(tags[i],5,ctx.auth.id,1);
				}
			}
		}
		// creat poll
		if(input.pollTitle !== ""){
			let pollId = await ctx.commands.addPoll(questionId,input.pollTitle);
			var pollItems = input.pollItems.split(",");
			for(var j = 0; j < pollItems.length; j++){
				if(pollItems[j] !== null && pollItems[j] !== undefined && pollItems[j] !== 0 && pollItems[j] !== ""){
					await ctx.commands.addPollItem(pollId,pollItems[j]);
				}
			}
		}

		// invite users
		var invited = input.invited.split(",");
		for(var k = 0; k < invited.length; k++){
			if(invited[k] !== null && invited[k] !== undefined && invited[k] !== 0 && invited[k] !== ""){
				await ctx.commands.inviteUserNotification(questionId,invited[k],ctx.auth.id);
				await ctx.commands.addQuestionInvite(ctx.auth.id,invited[k],questionId);
			}
		}
		await ctx.commands.incrementReputation(ctx.auth.id,15);
		await ctx.commands.createReputationNotification(ctx.auth.id,"U heeft 15 reputatie punten verdiend met het stellen van een vraag.");

		return {slug};
	},
});
