import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean, GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions, mutationWithClientMutationId} from 'graphql-relay';

import {getViewer} from '../viewer';
import {ViewerType} from '../types/ViewerType';

export var readNotificationsMutation = mutationWithClientMutationId({
	name: 'readNotifications',
	outputFields: {
		viewer: {
			type: ViewerType,
			resolve: (data, args, ctx) => getViewer(),
		},
	},
	inputFields: {
		id: {type: new GraphQLNonNull(GraphQLInt)},
	},
	mutateAndGetPayload: async (input, ctx) => {
		await ctx.commands.readNotifications(ctx.auth.id);
		return {};
	},
});
