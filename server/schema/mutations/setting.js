import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean, GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions, mutationWithClientMutationId} from 'graphql-relay';

import {getViewer} from '../viewer';
import {ViewerType} from '../types/ViewerType';

export var SettingMutation = mutationWithClientMutationId({
	name: 'Setting',
	outputFields: {
		viewer: {
			type: ViewerType,
			resolve: (data, args, ctx) => getViewer(),
		},
	},
	inputFields: {
		key: {type: new GraphQLNonNull(GraphQLString)},
		set: {type: new GraphQLNonNull(GraphQLBoolean)},
	},
	mutateAndGetPayload: async (input, ctx) => {
		await ctx.commands.removeSetting(input.key,ctx.auth.id);
		if(!input.set){
			await ctx.commands.addSetting(input.key,ctx.auth.id);
		}

		return {};
	},
});
