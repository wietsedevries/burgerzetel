import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean,
	GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions,
	mutationWithClientMutationId, connectionFromPromisedArray} from 'graphql-relay';

import NodeInterface from '../NodeInterface';
import {IncomeModel} from '../models';

export var IncomeType = new GraphQLObjectType({
	name: 'Income',
	interfaces: [NodeInterface],
	isTypeOf: obj => obj instanceof IncomeModel,
	fields: () => ({
		id: globalIdField('Income'),
		incomeId: {
			type: GraphQLInt,
			resolve: (root, args) => {
				return root.id;
			},
		},
		key: {type: GraphQLString},
		value: {type: GraphQLString},
	}),
});

var {connectionType, edgeType} = connectionDefinitions({
	name: 'Income',
	nodeType: IncomeType,
	resolveNode: (edge, args, ctx) => ctx.loaders.Income.load(edge.node),
});



export var IncomeConnection = connectionType;
export var IncomeEdge = edgeType;
