import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean,
	GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions,
	mutationWithClientMutationId, connectionFromPromisedArray} from 'graphql-relay';

import NodeInterface from '../NodeInterface';
import {PartyModel} from '../models';

export var PartyType = new GraphQLObjectType({
	name: 'Party',
	interfaces: [NodeInterface],
	isTypeOf: obj => obj instanceof PartyModel,
	fields: () => ({
		id: globalIdField('Party'),
		name: {type: GraphQLString},
		img: {type: GraphQLString},
		leader: {type: GraphQLString},
		leaderUrl: {type: GraphQLString},
		established: {type: GraphQLInt},
		members: {type: GraphQLInt},
		pageId: {type: GraphQLInt},
		seatsFirst: {type: GraphQLInt},
		seatsSecond: {type: GraphQLInt},
		seatsEU: {type: GraphQLInt},
	}),
});

var {connectionType, edgeType} = connectionDefinitions({
	name: 'Party',
	nodeType: PartyType,
	resolveNode: (edge, args, ctx) => ctx.loaders.Party.load(edge.node),
});



export var PartyConnection = connectionType;
export var PartyEdge = edgeType;
