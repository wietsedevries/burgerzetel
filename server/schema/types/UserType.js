import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean,
	GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions,
	mutationWithClientMutationId, connectionFromPromisedArray} from 'graphql-relay';
import CustomGraphQLDateType from 'graphql-custom-datetype';

import NodeInterface from '../NodeInterface';
import {UserModel} from '../models';
// Connections
import {QuestionConnection} from './QuestionType';
import {SettingConnection} from './SettingType';
import {NotificationConnection} from './NotificationType';
import {QuestionInviteConnection} from './QuestionInviteType';
import {MilestoneConnection} from './MilestoneType';
import {VisitConnection} from './VisitType';
import {ResetListConnection} from './ResetListType';
import {InterestConnection} from './InterestType';
import {FavoriteConnection} from './FavoriteType';
import {AnswerConnection} from './AnswerType';
import {ReplyConnection} from './ReplyType';
import {ReactionConnection} from './ReactionType';
import {ActionConnection} from './ActionType';
// Types
import {CityType} from './CityType';
import {EducationType} from './EducationType';
import {IncomeType} from './IncomeType';
import {MaritalStatusType} from './MaritalStatusType';


export var UserType = new GraphQLObjectType({
	name: 'User',
	interfaces: [NodeInterface],
	isTypeOf: obj => obj instanceof UserModel,
	fields: () => ({
		id: globalIdField('User'),
		uid: {
			type: GraphQLInt,
			resolve: (root, args) => {
				return root.id;
			},
		},
		code: {type: GraphQLInt},
		email: {type: GraphQLString},
		password: {type: GraphQLString},
		userName: {type: GraphQLString},
		avatar: {type: GraphQLString},
		firstName: {type: GraphQLString},
		lastName: {type: GraphQLString},
		function: {type: GraphQLString},
		bio: {type: GraphQLString},
		gender: {type: GraphQLString},
		children: {type: GraphQLString},
		statusId: {type: GraphQLInt},
		verified: {type: GraphQLInt},
		dateRegistered: {type: CustomGraphQLDateType},
		lastLogin: {type: CustomGraphQLDateType},
		loginCount: {type: GraphQLInt},
		reputation: {type: GraphQLInt},
		age: {type: GraphQLInt},
		FlaggedQuestions: {
			type: ActionConnection,
			args: connectionArgs,
			resolve: ({id}, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.getFlaggedQuestionIdsByUserId(id),args);
			},
		},
		FlaggedAnswers: {
			type: ActionConnection,
			args: connectionArgs,
			resolve: ({id}, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.getFlaggedAnswerIdsByUserId(id),args);
			},
		},
		FlaggedReactions: {
			type: ActionConnection,
			args: connectionArgs,
			resolve: ({id}, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.getFlaggedReactionIdsByUserId(id),args);
			},
		},
		FlaggedReplies: {
			type: ActionConnection,
			args: connectionArgs,
			resolve: ({id}, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.getFlaggedReplyIdsByUserId(id),args);
			},
		},
		UpvotedQuestions: {
			type: ActionConnection,
			args: connectionArgs,
			resolve: ({id}, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.getUpvotedQuestionIdsByUserId(id),args);
			},
		},
		UpvotedAnswers: {
			type: ActionConnection,
			args: connectionArgs,
			resolve: ({id}, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.getUpvotedAnswerIdsByUserId(id),args);
			},
		},
		UpvotedReactions: {
			type: ActionConnection,
			args: connectionArgs,
			resolve: ({id}, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.getUpvotedReactionIdsByUserId(id),args);
			},
		},
		UpvotedReplies: {
			type: ActionConnection,
			args: connectionArgs,
			resolve: ({id}, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.getUpvotedReplyIdsByUserId(id),args);
			},
		},
		Questions: {
			type: QuestionConnection,
			args: connectionArgs,
			resolve: ({id}, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.getQuestionIdsByUserId(id),args);
			},
		},
		City: {
			type: CityType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.City.load(root.cityId);
			},
		},
		Income: {
			type: IncomeType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.Income.load(root.incomeId);
			},
		},
		Education: {
			type: EducationType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.Education.load(root.educationId);
			},
		},
		MaritalStatus: {
			type: MaritalStatusType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.MaritalStatus.load(root.maritalStatusId);
			},
		},
		Settings: {
			type: SettingConnection,
			args: connectionArgs,
			resolve: ({id}, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.getSettingIdsByUserId(id),args);
			},
		},
		Notifications: {
			type: NotificationConnection,
			args: connectionArgs,
			resolve: ({id}, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.getNotificationIdsByUserId(id),args);
			},
		},
		NotificationCount: {
			type: GraphQLInt,
			resolve: ({id}, args, ctx) => {
				return ctx.queries.getNotificationCountByUserId(id);
			},
		},
		QuestionInvites: {
			type: QuestionInviteConnection,
			args: connectionArgs,
			resolve: ({id}, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.getQuestionInviteIdsByUserId(id),args);
			},
		},
		ResetList: {
			type: ResetListConnection,
			args: connectionArgs,
			resolve: (root, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.getResetListItemsByEmail(root.email),args);
			},
		},
		Visits: {
			type: VisitConnection,
			args: connectionArgs,
			resolve: ({id}, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.getVisitIdsByUserId(id),args);
			},
		},
		Milestones: {
			type: MilestoneConnection,
			args: connectionArgs,
			resolve: ({id}, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.getMilestoneIdsByUserId(id),args);
			},
		},
		Interests: {
			type: InterestConnection,
			args: connectionArgs,
			resolve: ({id}, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.getInterestIdsByUserId(id),args);
			},
		},
		AllInterests: {
			type: InterestConnection,
			args: connectionArgs,
			resolve: ({id}, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.getAllInterestIdsByUserId(id),args);
			},
		},
		Favorites: {
			type: FavoriteConnection,
			args: connectionArgs,
			resolve: ({id}, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.getFavoriteIdsByUserId(id),args);
			},
		},
		Answers: {
			type: AnswerConnection,
			args: connectionArgs,
			resolve: ({id}, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.getAnswerIdsByUserId(id),args);
			},
		},
		Replies: {
			type: ReplyConnection,
			args: connectionArgs,
			resolve: ({id}, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.getReplyIdsByUserId(id),args);
			},
		},
		Reactions: {
			type: ReactionConnection,
			args: connectionArgs,
			resolve: ({id}, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.getReactionIdsByUserId(id),args);
			},
		},
		Feed: {
			type: QuestionConnection,
			args: connectionArgs,
			resolve: (root, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.getFeedByUserId(root.id),args);
			},
		},
	}),
});

var {connectionType, edgeType} = connectionDefinitions({
	name: 'User',
	nodeType: UserType,
	resolveNode: (edge, args, ctx) => ctx.loaders.User.load(edge.node),
});

export var UserConnection = connectionType;
export var UserEdge = edgeType;
