import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean,
	GraphQLNonNull, GraphQLID, GraphQLList} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions,
	mutationWithClientMutationId, connectionFromPromisedArray} from 'graphql-relay';

import NodeInterface from '../NodeInterface';
import {ViewerModel} from '../models';
import {UserType, UserConnection} from './UserType';
import {QuestionType,QuestionConnection} from './QuestionType';
import {CityType,CityConnection} from './CityType';
import {MunicipalityType} from './MunicipalityType';
import {EducationType, EducationConnection} from './EducationType';
import {IncomeType, IncomeConnection} from './IncomeType';
import {MaritalStatusType, MaritalStatusConnection} from './MaritalStatusType';
import {SettingType} from './SettingType';
import {SettingTypeType, SettingTypeConnection} from './SettingTypeType';
import {NotificationType} from './NotificationType';
import {NotificationTypeType} from './NotificationTypeType';
import {QuestionInviteType, QuestionInviteConnection} from './QuestionInviteType';
import {TagType, TagConnection} from './TagType';
import {TagValueType, TagValueConnection} from './TagValueType';
import {ResetListType} from './ResetListType';
import {FaqType, FaqConnection} from './FaqType';
import {VisitType} from './VisitType';
import {PartyType} from './PartyType';
import {MilestoneTypeType,MilestoneTypeConnection} from './MilestoneTypeType';
import {MilestoneType} from './MilestoneType';
import {InterestType} from './InterestType';
import {FavoriteType} from './FavoriteType';
import {MediumTypeType} from './MediumTypeType';
import {QuestionMediumType} from './QuestionMediumType';
import {AnswerMediumType} from './AnswerMediumType';
import {PollType} from './PollType';
import {AnswerType} from './AnswerType';
import {PollOptionType} from './PollOptionType';
import {PollVoteType} from './PollVoteType';
import {ReactionType} from './ReactionType';
import {ReplyType} from './ReplyType';
import {PageType} from './PageType';
import {ContentType} from './ContentType';
import {ActionType} from './ActionType';
import {ActionTypeType} from './ActionTypeType';

export var ViewerType = new GraphQLObjectType({
	name: 'viewer',
	interfaces: [NodeInterface],
	isTypeOf: obj => obj instanceof ViewerModel,
	fields: () => ({
		id: globalIdField('viewer'),
		AuthUser: {
			type: UserType,
			resolve: (viewer, args, ctx) => ctx.auth.id && ctx.loaders.User.load(ctx.auth.id),
		},
		User: {
			type: UserType,
			args: {
				id: {type: new GraphQLNonNull(GraphQLString)},
			},
			resolve: (viewer, args, ctx) => ctx.loaders.User.load(args.id),
		},
		UserEmail: {
			type: UserType,
			args: {
				id: {type: new GraphQLNonNull(GraphQLString)},
			},
			resolve: (viewer, args, ctx) => ctx.loaders.UserEmail.load(args.id),
		},
		UserByName: {
			type: UserType,
			args: {
				username: {type: new GraphQLNonNull(GraphQLString)},
			},
			resolve: (viewer, args, ctx) => ctx.loaders.UserByName.load(args.username),
		},
		Question: {
			type: QuestionType,
			args: {
				slug: {type: new GraphQLNonNull(GraphQLString)},
			},
			resolve: (viewer, args, ctx) => ctx.loaders.QuestionBySlug.load(args.slug),
		},
		Content: {
			type: ContentType,
			args: {
				slug: {type: new GraphQLNonNull(GraphQLString)},
			},
			resolve: (viewer, args, ctx) => ctx.loaders.ContentBySlug.load(args.slug),
		},
		AllQuestions: {
			type: QuestionConnection,
			args: connectionArgs,
			resolve: ({id}, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.getAllQuestions(id),args);
			},
		},
		TrendingQuestions: {
			type: QuestionConnection,
			args: connectionArgs,
			resolve: ({id}, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.getTrendingQuestions(id),args);
			},
		},
		City: {
			type: CityType,
			args: {
				id: {type: new GraphQLNonNull(GraphQLString)},
			},
			resolve: (viewer, args, ctx) => ctx.loaders.City.load(args.id),
		},
		Cities: {
			type: CityConnection,
			args: connectionArgs,
			resolve: ({id}, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.getAllCities(id),args);
			},
		},
		Municipality: {
			type: MunicipalityType,
			args: {
				id: {type: new GraphQLNonNull(GraphQLString)},
			},
			resolve: (viewer, args, ctx) => ctx.loaders.Municipality.load(args.id),
		},
		Education: {
			type: EducationType,
			args: {
				id: {type: new GraphQLNonNull(GraphQLString)},
			},
			resolve: (viewer, args, ctx) => ctx.loaders.Education.load(args.id),
		},
		Educations: {
			type: EducationConnection,
			args: connectionArgs,
			resolve: ({id}, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.getAllEducations(id),args);
			},
		},
		Income: {
			type: IncomeType,
			args: {
				id: {type: new GraphQLNonNull(GraphQLString)},
			},
			resolve: (viewer, args, ctx) => ctx.loaders.Income.load(args.id),
		},
		Incomes: {
			type: IncomeConnection,
			args: connectionArgs,
			resolve: ({id}, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.getAllIncomes(id),args);
			},
		},
		MaritalStatus: {
			type: MaritalStatusType,
			args: {
				id: {type: new GraphQLNonNull(GraphQLString)},
			},
			resolve: (viewer, args, ctx) => ctx.loaders.MaritalStatus.load(args.id),
		},
		MaritalStatuses: {
			type: MaritalStatusConnection,
			args: connectionArgs,
			resolve: ({id}, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.getAllMaritalStatuses(id),args);
			},
		},
		Setting: {
			type: SettingType,
			args: {
				id: {type: new GraphQLNonNull(GraphQLString)},
			},
			resolve: (viewer, args, ctx) => ctx.loaders.Setting.load(args.id),
		},
		SettingType: {
			type: SettingTypeType,
			args: {
				id: {type: new GraphQLNonNull(GraphQLString)},
			},
			resolve: (viewer, args, ctx) => ctx.loaders.SettingType.load(args.id),
		},
		SettingTypes: {
			type: SettingTypeConnection,
			args: connectionArgs,
			resolve: ({id}, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.getAllSettingTypes(id),args);
			},
		},
		Notification: {
			type: NotificationType,
			args: {
				id: {type: new GraphQLNonNull(GraphQLString)},
			},
			resolve: (viewer, args, ctx) => ctx.loaders.Notification.load(args.id),
		},
		NotificationType: {
			type: NotificationTypeType,
			args: {
				id: {type: new GraphQLNonNull(GraphQLString)},
			},
			resolve: (viewer, args, ctx) => ctx.loaders.NotificationType.load(args.id),
		},
		QuestionInvite: {
			type: QuestionInviteType,
			args: {
				id: {type: new GraphQLNonNull(GraphQLString)},
			},
			resolve: (viewer, args, ctx) => ctx.loaders.QuestionInvite.load(args.id),
		},
		Tag: {
			type: TagType,
			args: {
				tagValueId: {type: new GraphQLNonNull(GraphQLString)},
			},
			resolve: (viewer, args, ctx) => ctx.loaders.Tag.load(args.tagValueId),
		},
		TrendingTags: {
			type: TagConnection,
			args: connectionArgs,
			resolve: ({id}, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.getTrendingTags(id),args);
			},
		},
		TagValue: {
			type: TagValueType,
			args: {
				id: {type: new GraphQLNonNull(GraphQLString)},
			},
			resolve: (viewer, args, ctx) => ctx.loaders.TagValue.load(args.id),
		},
		TagValueByKey: {
			type: TagValueType,
			args: {
				key: {type: new GraphQLNonNull(GraphQLString)},
			},
			resolve: (viewer, args, ctx) => ctx.loaders.TagValueByKey.load(args.key),
		},
		TagValueByQuery: {
			type: TagValueConnection,
			args: {
				query: {type: GraphQLString},
				before: {type: GraphQLString},
				after: {type: GraphQLString},
				first: {type: GraphQLInt},
				last: {type: GraphQLInt},
			},
			resolve: (viewer, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.getTagValueIdsByQuery(args.query),args);
			},
		},
		GetAllTags: {
			type: TagValueConnection,
			args: connectionArgs,
			resolve: ({id}, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.GetAllTags(id),args);
			},
		},
		GetTopTenUsers: {
			type: UserConnection,
			args: connectionArgs,
			resolve: ({id}, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.getTopTenUsers(id),args);
			},
		},
		QuestionsByQuery: {
			type: QuestionConnection,
			args: {
				query: {type: GraphQLString},
				before: {type: GraphQLString},
				after: {type: GraphQLString},
				first: {type: GraphQLInt},
				last: {type: GraphQLInt},
			},
			resolve: (viewer, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.QuestionsByQuery(args.query),args);
			},
		},
		getUsersByQuery: {
			type: UserConnection,
			args: {
				query: {type: GraphQLString},
				before: {type: GraphQLString},
				after: {type: GraphQLString},
				first: {type: GraphQLInt},
				last: {type: GraphQLInt},
			},
			resolve: (viewer, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.getUsersByQuery(args.query),args);
			},
		},
		ResetList: {
			type: ResetListType,
			args: {
				id: {type: new GraphQLNonNull(GraphQLString)},
			},
			resolve: (viewer, args, ctx) => ctx.loaders.ResetList.load(args.id),
		},
		ResetListByHash: {
			type: ResetListType,
			args: {
				hash: {type: new GraphQLNonNull(GraphQLString)},
			},
			resolve: (viewer, args, ctx) => ctx.loaders.ResetListByHash.load(args.hash),
		},
		ResetListEmail: {
			type: ResetListType,
			args: {
				email: {type: new GraphQLNonNull(GraphQLString)},
			},
			resolve: (viewer, args, ctx) => ctx.loaders.ResetListEmail.load(args.email),
		},
		Faq: {
			type: FaqType,
			args: {
				category: {type: new GraphQLNonNull(GraphQLString)},
			},
			resolve: (viewer, args, ctx) => ctx.loaders.Faq.load(args.category),
		},
		Faqs: {
			type: FaqConnection,
			args: {
				category: {type: GraphQLString},
				before: {type: GraphQLString},
				after: {type: GraphQLString},
				first: {type: GraphQLInt},
				last: {type: GraphQLInt},
			},
			resolve: ({id}, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.getFaqs(args.category),args);
			},
		},
		Visit: {
			type: VisitType,
			args: {
				id: {type: new GraphQLNonNull(GraphQLString)},
			},
			resolve: (viewer, args, ctx) => ctx.loaders.Visit.load(args.id),
		},
		Party: {
			type: PartyType,
			args: {
				id: {type: new GraphQLNonNull(GraphQLString)},
			},
			resolve: (viewer, args, ctx) => ctx.loaders.Party.load(args.id),
		},
		MilestoneType: {
			type: MilestoneTypeType,
			args: {
				id: {type: new GraphQLNonNull(GraphQLString)},
			},
			resolve: (viewer, args, ctx) => ctx.loaders.MilestoneType.load(args.id),
		},
		MilestoneTypes: {
			type: MilestoneTypeConnection,
			args: connectionArgs,
			resolve: ({id}, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.getAllMilestoneTypes(id),args);
			},
		},
		Milestone: {
			type: MilestoneType,
			args: {
				id: {type: new GraphQLNonNull(GraphQLString)},
			},
			resolve: (viewer, args, ctx) => ctx.loaders.Milestone.load(args.id),
		},
		Interest: {
			type: InterestType,
			args: {
				id: {type: new GraphQLNonNull(GraphQLString)},
			},
			resolve: (viewer, args, ctx) => ctx.loaders.Interest.load(args.id),
		},
		MediumType: {
			type: MediumTypeType,
			args: {
				id: {type: new GraphQLNonNull(GraphQLString)},
			},
			resolve: (viewer, args, ctx) => ctx.loaders.MediumType.load(args.id),
		},
		QuestionMedium: {
			type: QuestionMediumType,
			args: {
				id: {type: new GraphQLNonNull(GraphQLString)},
			},
			resolve: (viewer, args, ctx) => ctx.loaders.QuestionMedium.load(args.id),
		},
		AnswerMedium: {
			type: AnswerMediumType,
			args: {
				id: {type: new GraphQLNonNull(GraphQLString)},
			},
			resolve: (viewer, args, ctx) => ctx.loaders.AnswerMedium.load(args.id),
		},
		Poll: {
			type: PollType,
			args: {
				id: {type: new GraphQLNonNull(GraphQLString)},
			},
			resolve: (viewer, args, ctx) => ctx.loaders.Poll.load(args.id),
		},
		Answer: {
			type: AnswerType,
			args: {
				id: {type: new GraphQLNonNull(GraphQLString)},
			},
			resolve: (viewer, args, ctx) => ctx.loaders.Answer.load(args.id),
		},
		PollOption: {
			type: PollOptionType,
			args: {
				id: {type: new GraphQLNonNull(GraphQLString)},
			},
			resolve: (viewer, args, ctx) => ctx.loaders.PollOption.load(args.id),
		},
		PollVote: {
			type: PollVoteType,
			args: {
				id: {type: new GraphQLNonNull(GraphQLString)},
			},
			resolve: (viewer, args, ctx) => ctx.loaders.PollVote.load(args.id),
		},
		Reaction: {
			type: ReactionType,
			args: {
				id: {type: new GraphQLNonNull(GraphQLString)},
			},
			resolve: (viewer, args, ctx) => ctx.loaders.Reaction.load(args.id),
		},
		Reply: {
			type: ReplyType,
			args: {
				id: {type: new GraphQLNonNull(GraphQLString)},
			},
			resolve: (viewer, args, ctx) => ctx.loaders.Reply.load(args.id),
		},
		Page: {
			type: PageType,
			args: {
				id: {type: new GraphQLNonNull(GraphQLString)},
			},
			resolve: (viewer, args, ctx) => ctx.loaders.Page.load(args.id),
		},
		Action: {
			type: ActionType,
			args: {
				id: {type: new GraphQLNonNull(GraphQLString)},
			},
			resolve: (viewer, args, ctx) => ctx.loaders.Action.load(args.id),
		},
		ActionType: {
			type: ActionTypeType,
			args: {
				id: {type: new GraphQLNonNull(GraphQLString)},
			},
			resolve: (viewer, args, ctx) => ctx.loaders.ActionType.load(args.id),
		},
	}),
});
