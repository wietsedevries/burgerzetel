import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean,
	GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions,
	mutationWithClientMutationId, connectionFromPromisedArray} from 'graphql-relay';

import NodeInterface from '../NodeInterface';
import {AnswerMediumModel} from '../models';
import {MediumTypeType} from './MediumTypeType';
import {AnswerType} from './AnswerType';

export var AnswerMediumType = new GraphQLObjectType({
	name: 'AnswerMedium',
	interfaces: [NodeInterface],
	isTypeOf: obj => obj instanceof AnswerMediumModel,
	fields: () => ({
		id: globalIdField('AnswerMedium'),
		title: {type: GraphQLString},
		url: {type: GraphQLString},
		Type: {
			type: MediumTypeType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.MediumType.load(root.mediumTypeId);
			},
		},
		Answer: {
			type: AnswerType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.Answer.load(root.AnswerId);
			},
		},
	}),
});

var {connectionType, edgeType} = connectionDefinitions({
	name: 'AnswerMedium',
	nodeType: AnswerMediumType,
	resolveNode: (edge, args, ctx) => ctx.loaders.AnswerMedium.load(edge.node),
});



export var AnswerMediumConnection = connectionType;
export var AnswerMediumEdge = edgeType;
