import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean,
	GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions,
	mutationWithClientMutationId, connectionFromPromisedArray} from 'graphql-relay';
import CustomGraphQLDateType from 'graphql-custom-datetype';

import NodeInterface from '../NodeInterface';
import {ReplyModel} from '../models';
import {ReactionType} from './ReactionType';
import {UserType} from './UserType';

export var ReplyType = new GraphQLObjectType({
	name: 'Reply',
	interfaces: [NodeInterface],
	isTypeOf: obj => obj instanceof ReplyModel,
	fields: () => ({
		id: globalIdField('Reply'),
		replyId: {
			type: GraphQLInt,
			resolve: (root, args) => {
				return root.id;
			},
		},
		reply: {type: GraphQLString},
		upvoted: {type: GraphQLInt},
		downvoted: {type: GraphQLInt},
		shared: {type: GraphQLInt},
		reported: {type: GraphQLInt},
		dateCreated: {type: CustomGraphQLDateType},
		Reaction: {
			type: ReactionType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.Reaction.load(root.reactionId);
			},
		},
		User: {
			type: UserType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.User.load(root.userId);
			},
		},
	}),
});

var {connectionType, edgeType} = connectionDefinitions({
	name: 'Reply',
	nodeType: ReplyType,
	resolveNode: (edge, args, ctx) => ctx.loaders.Reply.load(edge.node),
});



export var ReplyConnection = connectionType;
export var ReplyEdge = edgeType;
