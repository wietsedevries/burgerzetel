import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean,
	GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions,
	mutationWithClientMutationId, connectionFromPromisedArray} from 'graphql-relay';
import CustomGraphQLDateType from 'graphql-custom-datetype';

import NodeInterface from '../NodeInterface';
import {ResetListModel} from '../models';
import {UserType} from './UserType';

export var ResetListType = new GraphQLObjectType({
	name: 'ResetList',
	interfaces: [NodeInterface],
	isTypeOf: obj => obj instanceof ResetListModel,
	fields: () => ({
		id: globalIdField('ResetList'),
		hash: {type: GraphQLString},
		emailAddress: {type: GraphQLString},
		userId: {type: GraphQLInt},
		dateCreated: {type: CustomGraphQLDateType},
		User: {
			type: UserType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.User.load(root.userId);
			},
		},
	}),
});

var {connectionType, edgeType} = connectionDefinitions({
	name: 'ResetList',
	nodeType: ResetListType,
	resolveNode: (edge, args, ctx) => ctx.loaders.ResetList.load(edge.node),
});

export var ResetListConnection = connectionType;
export var ResetListEdge = edgeType;
