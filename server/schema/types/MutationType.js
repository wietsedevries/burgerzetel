import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean, GraphQLNonNull, GraphQLID} from 'graphql';

import * as mutations from '../mutations';

export default new GraphQLObjectType({
	name: 'Mutation',
	fields: {
		...mutations,
	},
});
