import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean,
	GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions,
	mutationWithClientMutationId, connectionFromPromisedArray} from 'graphql-relay';

import NodeInterface from '../NodeInterface';
import {TagModel} from '../models';
import {UserType} from './UserType';
import {QuestionType} from './QuestionType';
import {TagValueType} from './TagValueType';

export var TagType = new GraphQLObjectType({
	name: 'Tag',
	interfaces: [NodeInterface],
	isTypeOf: obj => obj instanceof TagModel,
	fields: () => ({
		id: globalIdField('Tag'),
		tagId: {
			type: GraphQLInt,
			resolve: (root, args) => {
				return root.id;
			},
		},
		tagValueId: {type: GraphQLInt},
		Question: {
			type: QuestionType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.Question.load(root.questionId);
			},
		},
		TagValue: {
			type: TagValueType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.TagValue.load(root.tagValueId);
			},
		},
	}),
});

var {connectionType, edgeType} = connectionDefinitions({
	name: 'Tag',
	nodeType: TagType,
	connectionFields: () => ({
    totalCount: {
      type: GraphQLInt,
      resolve: (connection) => {/*console.log(connection);*/connection.totalCount;},
    },
	}),
	resolveNode: (edge, args, ctx) => ctx.loaders.Tag.load(edge.node),
});



export var TagConnection = connectionType;
export var TagEdge = edgeType;
