import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean,
	GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions,
	mutationWithClientMutationId, connectionFromPromisedArray} from 'graphql-relay';

import NodeInterface from '../NodeInterface';
import {NotificationTypeModel} from '../models';

export var NotificationTypeType = new GraphQLObjectType({
	name: 'NotificationType',
	interfaces: [NodeInterface],
	isTypeOf: obj => obj instanceof NotificationTypeModel,
	fields: () => ({
		id: globalIdField('NotificationType'),
		key: {type: GraphQLString},
		value: {type: GraphQLString},
	}),
});

var {connectionType, edgeType} = connectionDefinitions({
	name: 'NotificationType',
	nodeType: NotificationTypeType,
	resolveNode: (edge, args, ctx) => ctx.loaders.NotificationType.load(edge.node),
});



export var NotificationTypeConnection = connectionType;
export var NotificationTypeEdge = edgeType;
