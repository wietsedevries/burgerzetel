import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean,
	GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions,
	mutationWithClientMutationId, connectionFromPromisedArray} from 'graphql-relay';

import NodeInterface from '../NodeInterface';
import {PollOptionModel} from '../models';
import {PollType} from './PollType';
import {PollVoteConnection} from './PollVoteType';

export var PollOptionType = new GraphQLObjectType({
	name: 'PollOption',
	interfaces: [NodeInterface],
	isTypeOf: obj => obj instanceof PollOptionModel,
	fields: () => ({
		id: globalIdField('PollOption'),
		optionId: {
			type: GraphQLInt,
			resolve: (root, args) => {
				return root.id;
			},
		},
		value: {type: GraphQLString},
		Votes: {
			type: PollVoteConnection,
			args: connectionArgs,
			resolve: ({id}, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.getPollVoteIdsByPollOptionId(id),args);
			},
		},
		Poll: {
			type: PollType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.Poll.load(root.pollId);
			},
		},
	}),
});

var {connectionType, edgeType} = connectionDefinitions({
	name: 'PollOption',
	nodeType: PollOptionType,
	resolveNode: (edge, args, ctx) => ctx.loaders.PollOption.load(edge.node),
});



export var PollOptionConnection = connectionType;
export var PollOptionEdge = edgeType;
