import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean,
	GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions,
	mutationWithClientMutationId, connectionFromPromisedArray} from 'graphql-relay';

import NodeInterface from '../NodeInterface';
import {CityModel} from '../models';
import {MunicipalityType} from './MunicipalityType';

export var CityType = new GraphQLObjectType({
	name: 'City',
	interfaces: [NodeInterface],
	isTypeOf: obj => obj instanceof CityModel,
	fields: () => ({
		id: globalIdField('City'),
		cityId: {
			type: GraphQLInt,
			resolve: (root, args) => {
				return root.id;
			},
		},
		name: {type: GraphQLString},
		img: {type: GraphQLString},
		Municipality: {
			type: MunicipalityType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.Municipality.load(root.municipalityId);
			},
		},
	}),
});

var {connectionType, edgeType} = connectionDefinitions({
	name: 'City',
	nodeType: CityType,
	resolveNode: (edge, args, ctx) => ctx.loaders.City.load(edge.node),
});



export var CityConnection = connectionType;
export var CityEdge = edgeType;
