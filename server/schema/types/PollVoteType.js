import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean,
	GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions,
	mutationWithClientMutationId, connectionFromPromisedArray} from 'graphql-relay';
import CustomGraphQLDateType from 'graphql-custom-datetype';

import NodeInterface from '../NodeInterface';
import {PollVoteModel} from '../models';
import {UserType} from './UserType';
import {PollOptionType} from './PollOptionType';

export var PollVoteType = new GraphQLObjectType({
	name: 'PollVote',
	interfaces: [NodeInterface],
	isTypeOf: obj => obj instanceof PollVoteModel,
	fields: () => ({
		id: globalIdField('PollVote'),
		dateCreated: {type: CustomGraphQLDateType},
		PollOption: {
			type: PollOptionType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.PollOption.load(root.pollOptionId);
			},
		},
		User: {
			type: UserType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.User.load(root.userId);
			},
		},
	}),
});

var {connectionType, edgeType} = connectionDefinitions({
	name: 'PollVote',
	nodeType: PollVoteType,
	resolveNode: (edge, args, ctx) => ctx.loaders.PollVote.load(edge.node),
});



export var PollVoteConnection = connectionType;
export var PollVoteEdge = edgeType;
