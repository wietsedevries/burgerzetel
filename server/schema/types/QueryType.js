import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean, GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId} from 'graphql-relay';

import {ViewerType} from './ViewerType';
import NodeInterface from '../NodeInterface';
import {getViewer} from '../viewer';

function resolveNodeField(parent, args, ctx) {
	var {id, type} = fromGlobalId(args.id);
	switch (type) {
		case 'viewer':
			return getViewer();
		case 'User':
			return ctx.loaders.User.load(id);
		case 'UserEmail':
			return ctx.loaders.UserEmail.load(id);
		case 'UserByName':
			return ctx.loaders.UserByName.load(id);
		case 'Question':
			return ctx.loaders.Question.load(id);
		case 'AllQuestions':
			return ctx.loaders.AllQuestions.load(id);
		case 'GetTopTenUsers':
			return ctx.loaders.GetTopTenUsers.load(id);
		case 'City':{
			return ctx.loaders.City.load(id);}
		case 'Cities':{
			return ctx.loaders.Cities.load(id);}
		case 'Municipality':{
			return ctx.loaders.Municipality.load(id);}
		case 'Province':{
			return ctx.loaders.Province.load(id);}
		case 'Education':{
			return ctx.loaders.Education.load(id);}
		case 'Educations':{
			return ctx.loaders.Educations.load(id);}
		case 'Income':{
			return ctx.loaders.Income.load(id);}
		case 'Incomes':{
			return ctx.loaders.Incomes.load(id);}
		case 'Age':{
			return ctx.loaders.Age.load(id);}
		case 'Ages':{
				return ctx.loaders.Ages.load(id);}
		case 'MaritalStatus':{
			return ctx.loaders.MaritalStatus.load(id);}
		case 'MaritalStatuses':{
			return ctx.loaders.MaritalStatuses.load(id);}
		case 'Setting':{
			return ctx.loaders.Setting.load(id);}
		case 'SettingType':{
			return ctx.loaders.SettingType.load(id);}
		case 'SettingTypes':{
				return ctx.loaders.SettingTypes.load(id);}
		case 'Notification':{
			return ctx.loaders.Notification.load(id);}
		case 'NotificationType':{
			return ctx.loaders.NotificationType.load(id);}
		case 'Tag':{
			return ctx.loaders.Tag.load(id);}
		case 'TagValue':{
			return ctx.loaders.TagValue.load(id);}
		case 'ResetList':{
			return ctx.loaders.ResetList.load(id);}
		case 'ResetListEmail':{
			return ctx.loaders.ResetListEmail.load(id);}
		case 'Faq':{
			return ctx.loaders.Faq.load(id);}
		case 'Visit':{
			return ctx.loaders.Visit.load(id);}
		case 'Party':{
			return ctx.loaders.Party.load(id);}
		case 'MilestoneType':{
			return ctx.loaders.MilestoneType.load(id);}
		case 'Milestone':{
			return ctx.loaders.Milestone.load(id);}
		case 'Interest':{
			return ctx.loaders.Interest.load(id);}
		case 'Favorite':{
			return ctx.loaders.Favorite.load(id);}
		case 'MediumType':{
			return ctx.loaders.MediumType.load(id);}
		case 'QuestionMedium':{
			return ctx.loaders.QuestionMedium.load(id);}
		case 'AnswerMedium':{
			return ctx.loaders.AnswerMedium.load(id);}
		case 'Poll':{
			return ctx.loaders.Poll.load(id);}
		case 'Answer':{
			return ctx.loaders.Answer.load(id);}
		case 'PollOption':{
			return ctx.loaders.PollOption.load(id);}
		case 'PollVote':{
			return ctx.loaders.PollVote.load(id);}
		case 'Reaction':{
			return ctx.loaders.Reaction.load(id);}
		case 'Reply':{
			return ctx.loaders.Reply.load(id);}
		case 'Page':{
			return ctx.loaders.Page.load(id);}
		case 'Content':{
			return ctx.loaders.Content.load(id);}
		case 'Action':{
			return ctx.loaders.Action.load(id);}
		case 'ActionType':{
			return ctx.loaders.ActionType.load(id);}
	}
}

export default new GraphQLObjectType({
	name: 'Query',
	fields: () => ({
		node: {
			type: NodeInterface,
			args: {
				id: {type: new GraphQLNonNull(GraphQLID)},
			},
			resolve: resolveNodeField,
		},
		viewer: {
			type: ViewerType,
			resolve: (parent, args, ctx) => getViewer(),
		},
	}),
});
