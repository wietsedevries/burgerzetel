import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean,
	GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions,
	mutationWithClientMutationId, connectionFromPromisedArray} from 'graphql-relay';

import NodeInterface from '../NodeInterface';
import {SettingTypeModel} from '../models';

export var SettingTypeType = new GraphQLObjectType({
	name: 'SettingType',
	interfaces: [NodeInterface],
	isTypeOf: obj => obj instanceof SettingTypeModel,
	fields: () => ({
		id: globalIdField('SettingType'),
		settingTypeId: {
			type: GraphQLInt,
			resolve: (root, args) => {
				return root.id;
			},
		},
		key: {type: GraphQLString},
		value: {type: GraphQLString},
		hidden: {type: GraphQLInt},
	}),
});

var {connectionType, edgeType} = connectionDefinitions({
	name: 'SettingType',
	nodeType: SettingTypeType,
	resolveNode: (edge, args, ctx) => ctx.loaders.SettingType.load(edge.node),
});



export var SettingTypeConnection = connectionType;
export var SettingTypeEdge = edgeType;
