import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean,
	GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions,
	mutationWithClientMutationId, connectionFromPromisedArray} from 'graphql-relay';

import NodeInterface from '../NodeInterface';
import {QuestionInviteModel} from '../models';
import {QuestionType} from './QuestionType';
import {UserType} from './UserType';

export var QuestionInviteType = new GraphQLObjectType({
	name: 'QuestionInvite',
	interfaces: [NodeInterface],
	isTypeOf: obj => obj instanceof QuestionInviteModel,
	fields: () => ({
		id: globalIdField('QuestionInvite'),
		From: {
			type: UserType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.User.load(root.from);
			},
		},
		To: {
			type: UserType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.User.load(root.to);
			},
		},
		status: {type: GraphQLInt},
		Question: {
			type: QuestionType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.Question.load(root.questionId);
			},
		},
	}),
});

var {connectionType, edgeType} = connectionDefinitions({
	name: 'QuestionInvite',
	nodeType: QuestionInviteType,
	resolveNode: (edge, args, ctx) => ctx.loaders.QuestionInvite.load(edge.node),
});



export var QuestionInviteConnection = connectionType;
export var QuestionInviteEdge = edgeType;
