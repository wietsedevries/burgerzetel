import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean,
	GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions,
	mutationWithClientMutationId, connectionFromPromisedArray} from 'graphql-relay';

import NodeInterface from '../NodeInterface';
import {InterestModel} from '../models';
import {TagValueType} from './TagValueType';
import {UserType} from './UserType';

export var InterestType = new GraphQLObjectType({
	name: 'Interest',
	interfaces: [NodeInterface],
	isTypeOf: obj => obj instanceof InterestModel,
	fields: () => ({
		id: globalIdField('Interest'),
		points: {type: GraphQLInt},
		hidden: {type: GraphQLBoolean},
		Tag: {
			type: TagValueType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.TagValue.load(root.tagValueId);
			},
		},
		User: {
			type: UserType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.User.load(root.userId);
			},
		},
	}),
});

var {connectionType, edgeType} = connectionDefinitions({
	name: 'Interest',
	nodeType: InterestType,
	resolveNode: (edge, args, ctx) => ctx.loaders.Interest.load(edge.node),
});



export var InterestConnection = connectionType;
export var InterestEdge = edgeType;
