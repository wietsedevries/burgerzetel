import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean,
	GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions,
	mutationWithClientMutationId, connectionFromPromisedArray} from 'graphql-relay';

import NodeInterface from '../NodeInterface';
import {MilestoneTypeModel} from '../models';

export var MilestoneTypeType = new GraphQLObjectType({
	name: 'MilestoneType',
	interfaces: [NodeInterface],
	isTypeOf: obj => obj instanceof MilestoneTypeModel,
	fields: () => ({
		id: globalIdField('MilestoneType'),
		key: {type: GraphQLString},
		value: {type: GraphQLString},
		number: {type: GraphQLInt},
		reward: {type: GraphQLInt},
		hidden: {type: GraphQLBoolean},
	}),
});

var {connectionType, edgeType} = connectionDefinitions({
	name: 'MilestoneType',
	nodeType: MilestoneTypeType,
	resolveNode: (edge, args, ctx) => ctx.loaders.MilestoneType.load(edge.node),
});



export var MilestoneTypeConnection = connectionType;
export var MilestoneTypeEdge = edgeType;
