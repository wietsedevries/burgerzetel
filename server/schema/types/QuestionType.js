import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean,
	GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions,
	mutationWithClientMutationId, connectionFromPromisedArray} from 'graphql-relay';
import CustomGraphQLDateType from 'graphql-custom-datetype';

import NodeInterface from '../NodeInterface';
import {QuestionModel} from '../models';
import {UserType} from './UserType';
import {FavoriteConnection} from './FavoriteType';
import {TagConnection} from './TagType';
import {QuestionMediumConnection} from './QuestionMediumType';
import {PollConnection} from './PollType';
import {AnswerConnection} from './AnswerType';
import {QuestionInviteConnection} from './QuestionInviteType';

export var QuestionType = new GraphQLObjectType({
	name: 'Question',
	interfaces: [NodeInterface],
	isTypeOf: obj => obj instanceof QuestionModel,
	fields: () => ({
		id: globalIdField('Question'),
		qid: {
			type: GraphQLInt,
			resolve: (root, args) => {
				return root.id;
			},
		},
		title: {type: GraphQLString},
		slug: {type: GraphQLString},
		description: {type: GraphQLString},
		views: {type: GraphQLInt},
		upvoted: {type: GraphQLInt},
		downvoted: {type: GraphQLInt},
		shared: {type: GraphQLInt},
		reported: {type: GraphQLInt},
		dateCreated: {type: CustomGraphQLDateType},
		lastAnswered: {type: CustomGraphQLDateType},
		closingDate: {type: CustomGraphQLDateType},
		userId: {type: GraphQLInt},
		Answers: {
			type: AnswerConnection,
			args: connectionArgs,
			resolve: ({id}, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.getAnswerIdsByQuestionId(id),args);
			},
		},
		Favorites: {
			type: FavoriteConnection,
			args: connectionArgs,
			resolve: ({id}, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.getFavoriteIdsByQuestionId(id,ctx.auth.id),args);
			},
		},
		User: {
			type: UserType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.User.load(root.userId);
			},
		},
		Tags: {
			type: TagConnection,
			args: connectionArgs,
			resolve: ({id}, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.getTagIdsByQuestionId(id),args);
			},
		},
		Media: {
			type: QuestionMediumConnection,
			args: connectionArgs,
			resolve: ({id}, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.getQuestionMediumIdsByQuestionId(id),args);
			},
		},
		Poll: {
			type: PollConnection,
			args: connectionArgs,
			resolve: ({id}, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.getPollIdsByQuestionId(id),args);
			},
		},
		Invites: {
			type: QuestionInviteConnection,
			args: connectionArgs,
			resolve: ({id}, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.getQuestionInvitesByQuestionId(id),args);
			},
		},
	}),
});

var {connectionType, edgeType} = connectionDefinitions({
	name: 'Question',
	nodeType: QuestionType,
	resolveNode: (edge, args, ctx) => ctx.loaders.Question.load(edge.node),
});



export var QuestionConnection = connectionType;
export var QuestionEdge = edgeType;
