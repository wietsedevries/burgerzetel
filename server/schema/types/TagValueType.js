import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean,
	GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions,
	mutationWithClientMutationId, connectionFromPromisedArray} from 'graphql-relay';
import CustomGraphQLDateType from 'graphql-custom-datetype';

import NodeInterface from '../NodeInterface';
import {TagValueModel} from '../models';
import {UserType} from './UserType';
import {PageType} from './PageType';
import {TagConnection} from './TagType';
import {InterestConnection} from './InterestType';

export var TagValueType = new GraphQLObjectType({
	name: 'TagValue',
	interfaces: [NodeInterface],
	isTypeOf: obj => obj instanceof TagValueModel,
	fields: () => ({
		id: globalIdField('TagValue'),
		tagValueId: {
			type: GraphQLInt,
			resolve: (root, args) => {
				return root.id;
			},
		},
		key: {type: GraphQLString},
		value: {type: GraphQLString},
		used: {type: GraphQLInt},
		pageId: {type: GraphQLInt},
		dateCreated: {type: CustomGraphQLDateType},
		Insterested: {
			type: InterestConnection,
			args: connectionArgs,
			resolve: (root, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.getInterestIdsByTagValueId(root.id),args);
			},
		},
		Tags: {
			type: TagConnection,
			args: connectionArgs,
			resolve: (root, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.getTagIdsByTagValueIds(root.id),args);
			},
		},
		User: {
			type: UserType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.User.load(root.userId);
			},
		},
		Page: {
			type: PageType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.Page.load(root.pageId);
			},
		},
	}),
});

var {connectionType, edgeType} = connectionDefinitions({
	name: 'TagValue',
	nodeType: TagValueType,
	resolveNode: (edge, args, ctx) => ctx.loaders.TagValue.load(edge.node),
});

export var TagValueConnection = connectionType;
export var TagValueEdge = edgeType;
