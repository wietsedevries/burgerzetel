import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean,
	GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions,
	mutationWithClientMutationId, connectionFromPromisedArray} from 'graphql-relay';

import NodeInterface from '../NodeInterface';
import {ProvinceModel} from '../models';
// import {UserType} from './UserType';

export var ProvinceType = new GraphQLObjectType({
	name: 'Province',
	interfaces: [NodeInterface],
	isTypeOf: obj => obj instanceof ProvinceModel,
	fields: () => ({
		id: globalIdField('Province'),
		name: {type: GraphQLString},
		img: {type: GraphQLString},
	}),
});

var {connectionType, edgeType} = connectionDefinitions({
	name: 'Province',
	nodeType: ProvinceType,
	resolveNode: (edge, args, ctx) => ctx.loaders.Province.load(edge.node),
});



export var ProvinceConnection = connectionType;
export var ProvinceEdge = edgeType;
