import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean,
	GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions,
	mutationWithClientMutationId, connectionFromPromisedArray} from 'graphql-relay';

import NodeInterface from '../NodeInterface';
import {MaritalStatusModel} from '../models';

export var MaritalStatusType = new GraphQLObjectType({
	name: 'MaritalStatus',
	interfaces: [NodeInterface],
	isTypeOf: obj => obj instanceof MaritalStatusModel,
	fields: () => ({
		id: globalIdField('MaritalStatus'),
		maritalStatusId: {
			type: GraphQLInt,
			resolve: (root, args) => {
				return root.id;
			},
		},
		key: {type: GraphQLString},
		value: {type: GraphQLString},
	}),
});

var {connectionType, edgeType} = connectionDefinitions({
	name: 'MaritalStatus',
	nodeType: MaritalStatusType,
	resolveNode: (edge, args, ctx) => ctx.loaders.MaritalStatus.load(edge.node),
});



export var MaritalStatusConnection = connectionType;
export var MaritalStatusEdge = edgeType;
