import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean,
	GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions,
	mutationWithClientMutationId, connectionFromPromisedArray} from 'graphql-relay';
import CustomGraphQLDateType from 'graphql-custom-datetype';

import NodeInterface from '../NodeInterface';
import {ContentModel} from '../models';

export var ContentType = new GraphQLObjectType({
	name: 'Content',
	interfaces: [NodeInterface],
	isTypeOf: obj => obj instanceof ContentModel,
	fields: () => ({
		id: globalIdField('content'),
		title: {type: GraphQLString},
		content: {type: GraphQLString},
		dateUpdated: {type: CustomGraphQLDateType},
	}),
});

var {connectionType, edgeType} = connectionDefinitions({
	name: 'Content',
	nodeType: ContentType,
	resolveNode: (edge, args, ctx) => ctx.loaders.Content.load(edge.node),
});



export var ContentConnection = connectionType;
export var ContentEdge = edgeType;
