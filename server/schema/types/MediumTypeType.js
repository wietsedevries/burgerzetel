import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean,
	GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions,
	mutationWithClientMutationId, connectionFromPromisedArray} from 'graphql-relay';

import NodeInterface from '../NodeInterface';
import {MediumTypeModel} from '../models';

export var MediumTypeType = new GraphQLObjectType({
	name: 'MediumType',
	interfaces: [NodeInterface],
	isTypeOf: obj => obj instanceof MediumTypeModel,
	fields: () => ({
		id: globalIdField('MediumType'),
		key: {type: GraphQLString},
		value: {type: GraphQLString},
	}),
});

var {connectionType, edgeType} = connectionDefinitions({
	name: 'MediumType',
	nodeType: MediumTypeType,
	resolveNode: (edge, args, ctx) => ctx.loaders.MediumType.load(edge.node),
});



export var MediumTypeConnection = connectionType;
export var MediumTypeEdge = edgeType;
