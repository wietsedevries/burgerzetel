import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean,
	GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions,
	mutationWithClientMutationId, connectionFromPromisedArray} from 'graphql-relay';
import CustomGraphQLDateType from 'graphql-custom-datetype';

import NodeInterface from '../NodeInterface';
import {NotificationModel} from '../models';
import {UserType} from './UserType';
import {QuestionType} from './QuestionType';
import {AnswerType} from './AnswerType';
import {ReactionType} from './ReactionType';
import {ReplyType} from './ReplyType';
import {NotificationTypeType} from './NotificationTypeType';

export var NotificationType = new GraphQLObjectType({
	name: 'Notification',
	interfaces: [NodeInterface],
	isTypeOf: obj => obj instanceof NotificationModel,
	fields: () => ({
		id: globalIdField('Notification'),
		nid: {
			type: GraphQLInt,
			resolve: (root, args) => {
				return root.id;
			},
		},
		status: {type: GraphQLInt},
		dateCreated: {type: CustomGraphQLDateType},
		msg: {type: GraphQLString},
		User: {
			type: UserType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.User.load(root.userId);
			},
		},
		AskedBy: {
			type: UserType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.User.load(root.askedBy);
			},
		},
		Answer: {
			type: AnswerType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.Answer.load(root.answerId);
			},
		},
		Reaction: {
			type: ReactionType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.Reaction.load(root.reactionId);
			},
		},
		Reply: {
			type: ReplyType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.Reply.load(root.replyId);
			},
		},
		Question: {
			type: QuestionType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.Question.load(root.questionId);
			},
		},
		NotificationType: {
			type: NotificationTypeType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.NotificationType.load(root.notificationTypeId);
			},
		},
	}),
});

var {connectionType, edgeType} = connectionDefinitions({
	name: 'Notification',
	nodeType: NotificationType,
	resolveNode: (edge, args, ctx) => ctx.loaders.Notification.load(edge.node),
});



export var NotificationConnection = connectionType;
export var NotificationEdge = edgeType;
