import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean,
	GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions,
	mutationWithClientMutationId, connectionFromPromisedArray} from 'graphql-relay';
import CustomGraphQLDateType from 'graphql-custom-datetype';
import NodeInterface from '../NodeInterface';
import {FavoriteModel} from '../models';
import {UserType} from './UserType';
import {QuestionType} from './QuestionType';

export var FavoriteType = new GraphQLObjectType({
	name: 'Favorite',
	interfaces: [NodeInterface],
	isTypeOf: obj => obj instanceof FavoriteModel,
	fields: () => ({
		id: globalIdField('Favorite'),
		dateCreated: {type: CustomGraphQLDateType},
		User: {
			type: UserType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.User.load(root.userId);
			},
		},
		Question: {
			type: QuestionType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.Question.load(root.questionId);
			},
		},
	}),
});

var {connectionType, edgeType} = connectionDefinitions({
	name: 'Favorite',
	nodeType: FavoriteType,
	resolveNode: (edge, args, ctx) => ctx.loaders.Favorite.load(edge.node),
});



export var FavoriteConnection = connectionType;
export var FavoriteEdge = edgeType;
