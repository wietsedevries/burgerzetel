import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean,
	GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions,
	mutationWithClientMutationId, connectionFromPromisedArray} from 'graphql-relay';

import NodeInterface from '../NodeInterface';
import {QuestionMediumModel} from '../models';
import {MediumTypeType} from './MediumTypeType';
import {QuestionType} from './QuestionType';

export var QuestionMediumType = new GraphQLObjectType({
	name: 'QuestionMedium',
	interfaces: [NodeInterface],
	isTypeOf: obj => obj instanceof QuestionMediumModel,
	fields: () => ({
		id: globalIdField('QuestionMedium'),
		title: {type: GraphQLString},
		url: {type: GraphQLString},
		Type: {
			type: MediumTypeType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.MediumType.load(root.mediumTypeId);
			},
		},
		Question: {
			type: QuestionType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.Question.load(root.questionId);
			},
		},
	}),
});

var {connectionType, edgeType} = connectionDefinitions({
	name: 'QuestionMedium',
	nodeType: QuestionMediumType,
	resolveNode: (edge, args, ctx) => ctx.loaders.QuestionMedium.load(edge.node),
});



export var QuestionMediumConnection = connectionType;
export var QuestionMediumEdge = edgeType;
