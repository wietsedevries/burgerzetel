import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean,
	GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions,
	mutationWithClientMutationId, connectionFromPromisedArray} from 'graphql-relay';

import NodeInterface from '../NodeInterface';
import {FaqModel} from '../models';
import {QuestionType} from './QuestionType';

export var FaqType = new GraphQLObjectType({
	name: 'Faq',
	interfaces: [NodeInterface],
	isTypeOf: obj => obj instanceof FaqModel,
	fields: () => ({
		id: globalIdField('Faq'),
		Question: {
			type: QuestionType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.Question.load(root.questionId);
			},
		},
	}),
});

var {connectionType, edgeType} = connectionDefinitions({
	name: 'Faq',
	nodeType: FaqType,
	resolveNode: (edge, args, ctx) => ctx.loaders.Faq.load(edge.node),
});



export var FaqConnection = connectionType;
export var FaqEdge = edgeType;
