import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean,
	GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions,
	mutationWithClientMutationId, connectionFromPromisedArray} from 'graphql-relay';
import CustomGraphQLDateType from 'graphql-custom-datetype';

import NodeInterface from '../NodeInterface';
import {ReactionModel} from '../models';
import {AnswerType} from './AnswerType';
import {ReplyConnection} from './ReplyType';
import {UserType} from './UserType';

export var ReactionType = new GraphQLObjectType({
	name: 'Reaction',
	interfaces: [NodeInterface],
	isTypeOf: obj => obj instanceof ReactionModel,
	fields: () => ({
		id: globalIdField('Reaction'),
		rid: {
			type: GraphQLInt,
			resolve: (root, args) => {
				return root.id;
			},
		},
		reaction: {type: GraphQLString},
		upvoted: {type: GraphQLInt},
		downvoted: {type: GraphQLInt},
		shared: {type: GraphQLInt},
		reported: {type: GraphQLInt},
		dateCreated: {type: CustomGraphQLDateType},
		Replies: {
			type: ReplyConnection,
			args: connectionArgs,
			resolve: ({id}, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.getReplyIdsByReactionId(id),args);
			},
		},
		Answer: {
			type: AnswerType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.Answer.load(root.answerId);
			},
		},
		User: {
			type: UserType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.User.load(root.userId);
			},
		},
	}),
});

var {connectionType, edgeType} = connectionDefinitions({
	name: 'Reaction',
	nodeType: ReactionType,
	resolveNode: (edge, args, ctx) => ctx.loaders.Reaction.load(edge.node),
});



export var ReactionConnection = connectionType;
export var ReactionEdge = edgeType;
