import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean,
	GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions,
	mutationWithClientMutationId, connectionFromPromisedArray} from 'graphql-relay';

import NodeInterface from '../NodeInterface';
import {EducationModel} from '../models';

export var EducationType = new GraphQLObjectType({
	name: 'Education',
	interfaces: [NodeInterface],
	isTypeOf: obj => obj instanceof EducationModel,
	fields: () => ({
		id: globalIdField('Education'),
		educationId: {
			type: GraphQLInt,
			resolve: (root, args) => {
				return root.id;
			},
		},
		key: {type: GraphQLString},
		value: {type: GraphQLString},
		order: {type: GraphQLInt},
	}),
});

var {connectionType, edgeType} = connectionDefinitions({
	name: 'Education',
	nodeType: EducationType,
	resolveNode: (edge, args, ctx) => ctx.loaders.Education.load(edge.node),
});



export var EducationConnection = connectionType;
export var EducationEdge = edgeType;
