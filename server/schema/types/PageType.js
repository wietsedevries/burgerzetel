import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean,
	GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions,
	mutationWithClientMutationId, connectionFromPromisedArray} from 'graphql-relay';
import CustomGraphQLDateType from 'graphql-custom-datetype';

import NodeInterface from '../NodeInterface';
import {PageModel} from '../models';
import {PartyType} from './PartyType';

export var PageType = new GraphQLObjectType({
	name: 'Page',
	interfaces: [NodeInterface],
	isTypeOf: obj => obj instanceof PageModel,
	fields: () => ({
		id: globalIdField('Page'),
		content: {type: GraphQLString},
		data: {type: GraphQLString},
		img: {type: GraphQLString},
		partyId: {type: GraphQLInt},
		dateUpdated: {type: CustomGraphQLDateType},
		Party: {
			type: PartyType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.Party.load(root.partyId);
			},
		},
	}),
});

var {connectionType, edgeType} = connectionDefinitions({
	name: 'Page',
	nodeType: PageType,
	resolveNode: (edge, args, ctx) => ctx.loaders.Page.load(edge.node),
});



export var PageConnection = connectionType;
export var PageEdge = edgeType;
