import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean,
	GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions,
	mutationWithClientMutationId, connectionFromPromisedArray} from 'graphql-relay';
import CustomGraphQLDateType from 'graphql-custom-datetype';

import NodeInterface from '../NodeInterface';
import {PollModel} from '../models';
import {UserType} from './UserType';
import {QuestionType} from './QuestionType';
import {PollOptionConnection} from './PollOptionType';

export var PollType = new GraphQLObjectType({
	name: 'Poll',
	interfaces: [NodeInterface],
	isTypeOf: obj => obj instanceof PollModel,
	fields: () => ({
		id: globalIdField('Poll'),
		title: {type: GraphQLString},
		dateCreated: {type: CustomGraphQLDateType},
		Options: {
			type: PollOptionConnection,
			args: connectionArgs,
			resolve: ({id}, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.getPollOptionIdsByPollId(id),args);
			},
		},
		Question: {
			type: QuestionType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.Question.load(root.questionId);
			},
		},
	}),
});

var {connectionType, edgeType} = connectionDefinitions({
	name: 'Poll',
	nodeType: PollType,
	resolveNode: (edge, args, ctx) => ctx.loaders.Poll.load(edge.node),
});



export var PollConnection = connectionType;
export var PollEdge = edgeType;
