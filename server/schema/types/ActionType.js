import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean,
	GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions,
	mutationWithClientMutationId, connectionFromPromisedArray} from 'graphql-relay';
import CustomGraphQLDateType from 'graphql-custom-datetype';

import NodeInterface from '../NodeInterface';
import {ActionModel} from '../models';
import {ActionTypeType} from './ActionTypeType';
import {UserType} from './UserType';

export var ActionType = new GraphQLObjectType({
	name: 'Action',
	interfaces: [NodeInterface],
	isTypeOf: obj => obj instanceof ActionModel,
	fields: () => ({
		id: globalIdField('Action'),
		User: {
			type: UserType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.User.load(root.userId);
			},
		},
		itemId: {
			type: GraphQLInt,
			resolve: (root, args) => {
				return root.itemId;
			},
		},
		ActionType: {
			type: ActionTypeType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.ActionType.load(root.actionTypeId);
			},
		},
		dateCreated: {type: CustomGraphQLDateType},
	}),
});

var {connectionType, edgeType} = connectionDefinitions({
	name: 'Action',
	nodeType: ActionType,
	resolveNode: (edge, args, ctx) => ctx.loaders.Action.load(edge.node),
});



export var ActionConnection = connectionType;
export var ActionEdge = edgeType;
