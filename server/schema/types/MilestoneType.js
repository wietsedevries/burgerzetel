import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean,
	GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions,
	mutationWithClientMutationId, connectionFromPromisedArray} from 'graphql-relay';
import CustomGraphQLDateType from 'graphql-custom-datetype';

import NodeInterface from '../NodeInterface';
import {MilestoneModel} from '../models';
import {MilestoneTypeType} from './MilestoneTypeType';
import {UserType} from './UserType';

export var MilestoneType = new GraphQLObjectType({
	name: 'Milestone',
	interfaces: [NodeInterface],
	isTypeOf: obj => obj instanceof MilestoneModel,
	fields: () => ({
		id: globalIdField('Milestone'),
		total: {type: GraphQLInt},
		hidden: {type: GraphQLBoolean},
		dateFirst: {type: CustomGraphQLDateType},
		dateLast: {type: CustomGraphQLDateType},
		Details: {
			type: MilestoneTypeType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.MilestoneType.load(root.milestoneTypeId);
			},
		},
		User: {
			type: UserType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.User.load(root.userId);
			},
		},
	}),
});

var {connectionType, edgeType} = connectionDefinitions({
	name: 'Milestone',
	nodeType: MilestoneType,
	resolveNode: (edge, args, ctx) => ctx.loaders.Milestone.load(edge.node),
});



export var MilestoneConnection = connectionType;
export var MilestoneEdge = edgeType;
