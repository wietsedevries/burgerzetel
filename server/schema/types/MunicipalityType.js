import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean,
	GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions,
	mutationWithClientMutationId, connectionFromPromisedArray} from 'graphql-relay';

import NodeInterface from '../NodeInterface';
import {MunicipalityModel} from '../models';
import {ProvinceType} from './ProvinceType';

export var MunicipalityType = new GraphQLObjectType({
	name: 'Municipality',
	interfaces: [NodeInterface],
	isTypeOf: obj => obj instanceof MunicipalityModel,
	fields: () => ({
		id: globalIdField('Municipality'),
		name: {type: GraphQLString},
		img: {type: GraphQLString},
		Province: {
			type: ProvinceType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.Province.load(root.provinceId);
			},
		},
	}),
});

var {connectionType, edgeType} = connectionDefinitions({
	name: 'Municipality',
	nodeType: MunicipalityType,
	resolveNode: (edge, args, ctx) => ctx.loaders.Municipality.load(edge.node),
});



export var MunicipalityConnection = connectionType;
export var MunicipalityEdge = edgeType;
