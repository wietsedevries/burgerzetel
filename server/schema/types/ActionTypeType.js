import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean,
	GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions,
	mutationWithClientMutationId, connectionFromPromisedArray} from 'graphql-relay';
import CustomGraphQLDateType from 'graphql-custom-datetype';

import NodeInterface from '../NodeInterface';
import {ActionTypeModel} from '../models';

export var ActionTypeType = new GraphQLObjectType({
	name: 'ActionType',
	interfaces: [NodeInterface],
	isTypeOf: obj => obj instanceof ActionTypeModel,
	fields: () => ({
		id: globalIdField('ActionType'),
		value: {type: GraphQLString},
	}),
});

var {connectionType, edgeType} = connectionDefinitions({
	name: 'ActionType',
	nodeType: ActionTypeType,
	resolveNode: (edge, args, ctx) => ctx.loaders.ActionType.load(edge.node),
});

export var ActionTypeConnection = connectionType;
export var ActionTypeEdge = edgeType;
