import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean,
	GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions,
	mutationWithClientMutationId, connectionFromPromisedArray} from 'graphql-relay';
import CustomGraphQLDateType from 'graphql-custom-datetype';
import NodeInterface from '../NodeInterface';
import {SettingModel} from '../models';
import {UserType} from './UserType';
import {SettingTypeType} from './SettingTypeType';

export var SettingType = new GraphQLObjectType({
	name: 'Setting',
	interfaces: [NodeInterface],
	isTypeOf: obj => obj instanceof SettingModel,
	fields: () => ({
		id: globalIdField('Setting'),
		value: {type: GraphQLString},
		dateChanged: {type: CustomGraphQLDateType},
		User: {
			type: UserType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.User.load(root.userId);
			},
		},
		SettingType: {
			type: SettingTypeType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.SettingType.load(root.settingTypeId);
			},
		},
	}),
});

var {connectionType, edgeType} = connectionDefinitions({
	name: 'Setting',
	nodeType: SettingType,
	resolveNode: (edge, args, ctx) => ctx.loaders.Setting.load(edge.node),
});



export var SettingConnection = connectionType;
export var SettingEdge = edgeType;
