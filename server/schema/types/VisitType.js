import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean,
	GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions,
	mutationWithClientMutationId, connectionFromPromisedArray} from 'graphql-relay';
import CustomGraphQLDateType from 'graphql-custom-datetype';

import NodeInterface from '../NodeInterface';
import {VisitModel} from '../models';
import {UserType} from './UserType';
import {QuestionType} from './QuestionType';

export var VisitType = new GraphQLObjectType({
	name: 'Visit',
	interfaces: [NodeInterface],
	isTypeOf: obj => obj instanceof VisitModel,
	fields: () => ({
		id: globalIdField('Visit'),
		dateCreated: {type: CustomGraphQLDateType},
		User: {
			type: UserType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.User.load(root.userId);
			},
		},
		Question: {
			type: QuestionType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.Question.load(root.questionId);
			},
		},
	}),
});

var {connectionType, edgeType} = connectionDefinitions({
	name: 'Visit',
	nodeType: VisitType,
	resolveNode: (edge, args, ctx) => ctx.loaders.Visit.load(edge.node),
});

export var VisitConnection = connectionType;
export var VisitEdge = edgeType;
