import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLBoolean,
	GraphQLNonNull, GraphQLID} from 'graphql';
import {fromGlobalId, globalIdField, connectionArgs, connectionDefinitions,
	mutationWithClientMutationId, connectionFromPromisedArray} from 'graphql-relay';
import CustomGraphQLDateType from 'graphql-custom-datetype';

import NodeInterface from '../NodeInterface';
import {AnswerModel} from '../models';
import {UserType} from './UserType';
import {QuestionType} from './QuestionType';
import {AnswerMediumConnection} from './AnswerMediumType';
import {ReactionConnection} from './ReactionType';

export var AnswerType = new GraphQLObjectType({
	name: 'Answer',
	interfaces: [NodeInterface],
	isTypeOf: obj => obj instanceof AnswerModel,
	fields: () => ({
		id: globalIdField('Answer'),
		aid: {
			type: GraphQLInt,
			resolve: (root, args) => {
				return root.id;
			},
		},
		answer: {type: GraphQLString},
		upvoted: {type: GraphQLInt},
		downvoted: {type: GraphQLInt},
		shared: {type: GraphQLInt},
		reported: {type: GraphQLInt},
		dateCreated: {type: CustomGraphQLDateType},
		correct: {type: GraphQLBoolean},
		Reactions: {
			type: ReactionConnection,
			args: connectionArgs,
			resolve: ({id}, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.getReactionIdsByAnswerId(id),args);
			},
		},
		Question: {
			type: QuestionType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.Question.load(root.questionId);
			},
		},
		User: {
			type: UserType,
			resolve: (root, args, ctx) => {
				return ctx.loaders.User.load(root.userId);
			},
		},
		Media: {
			type: AnswerMediumConnection,
			args: connectionArgs,
			resolve: ({id}, args, ctx) => {
				return connectionFromPromisedArray(ctx.queries.getAnswerMediumIdsByAnswerId(id),args);
			},
		},
	}),
});

var {connectionType, edgeType} = connectionDefinitions({
	name: 'Answer',
	nodeType: AnswerType,
	resolveNode: (edge, args, ctx) => ctx.loaders.Answer.load(edge.node),
});



export var AnswerConnection = connectionType;
export var AnswerEdge = edgeType;
