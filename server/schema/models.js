import _ from 'lodash';

class BaseModel {
	constructor(values) {
		_.extend(this, values);
	}
}

export class ViewerModel extends BaseModel {}
export class UserModel extends BaseModel {}
export class QuestionModel extends BaseModel {}
export class CityModel extends BaseModel {}
export class MunicipalityModel extends BaseModel {}
export class ProvinceModel extends BaseModel {}
export class EducationModel extends BaseModel {}
export class IncomeModel extends BaseModel {}
export class AgeModel extends BaseModel {}
export class MaritalStatusModel extends BaseModel {}
export class SettingModel extends BaseModel {}
export class SettingTypeModel extends BaseModel {}
export class NotificationModel extends BaseModel {}
export class NotificationTypeModel extends BaseModel {}
export class QuestionInviteModel extends BaseModel {}
export class TagModel extends BaseModel {}
export class TagValueModel extends BaseModel {}
export class ResetListModel extends BaseModel {}
export class FaqModel extends BaseModel {}
export class VisitModel extends BaseModel {}
export class PartyModel extends BaseModel {}
export class MilestoneTypeModel extends BaseModel {}
export class MilestoneModel extends BaseModel {}
export class InterestModel extends BaseModel {}
export class FavoriteModel extends BaseModel {}
export class MediumTypeModel extends BaseModel {}
export class AnswerMediumModel extends BaseModel {}
export class QuestionMediumModel extends BaseModel {}
export class PollModel extends BaseModel {}
export class AnswerModel extends BaseModel {}
export class PollOptionModel extends BaseModel {}
export class PollVoteModel extends BaseModel {}
export class ReactionModel extends BaseModel {}
export class ReplyModel extends BaseModel {}
export class PageModel extends BaseModel {}
export class ContentModel extends BaseModel {}
export class ActionModel extends BaseModel {}
export class ActionTypeModel extends BaseModel {}
