// import 'babel-polyfill';
import fs from 'fs';
import path from 'path';
import schema from './index';
import { graphql } from 'graphql';
import { introspectionQuery, printSchema } from 'graphql/utilities';

// Save user readable type system shorthand of schema
fs.writeFileSync(
	path.join(__dirname, './schema.graphql'),
	printSchema(schema)
);

// Save JSON of full schema introspection for Babel Relay Plugin to use
graphql(schema, introspectionQuery).then(result => {
	if (result.errors) {
		console.error(
			'ERROR introspecting schema: ',
			JSON.stringify(result.errors, null, 2)
		);
	} else {
		console.log('\x1b[5m', ' Updating schema... ');
		fs.writeFileSync(
			path.join(__dirname, './schema.json'),
			JSON.stringify(result, null, 2)
		);

		console.log("\x1b[32m","...Done");
	}
});
